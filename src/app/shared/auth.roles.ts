export enum UserRole {
  Editor,
  Admin,
  Trader,
  Buyer,
  Seller,
  PaidUser,
  None
}
