import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpParams, HttpErrorResponse, HttpHeaders
} from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import * as $ from 'jquery';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { BehaviorSubject } from 'rxjs';

export interface IProduct {
  id: number;
  title: string;
  img: string;
  category: string;
  status: string;
  statusColor: string;
  description: string;
  sales: number;
  stock: number;
  date: string;
}

export interface IProductResponse {
  data: IProduct[];
  status: boolean;
  totalItem: number;
  totalPage: number;
  pageSize: string;
  currentPage: string;
}


@Injectable({ providedIn: 'root' })
export class ApiService {
  BusinessTypeId: any;
  public reqNo;
  public MarketreqNo;
  public orderbook = [];
  webSocketEndPoint: string = 'http://54.177.218.175:8080/socketStream/ws';
  public WEBSERVICE = "http://54.177.218.175:8080/crmFoApi";  
  public REPORT_WEBSERVICE = "http://54.177.218.175:9080/reportApi";
  public BLOCK_CHAIN_URL='http://13.57.233.186/#/transaction/';
  topic: string = "/user/topic/stream";
  stompClient: any;
  private dataSource = new BehaviorSubject(this.orderbook);

  currentData = this.dataSource.asObservable();
  constructor(private http: HttpClient) {
    this._connect();
    // setTimeout(() => {
    //   this.sendmessagedata();
    // }, 5000);
  }

  getProducts(pageSize: number = 10, currentPage: number = 1, search: string = '', orderBy: string = '') {
    const url = environment.apiUrl + '/cakes/paging';
    let params = new HttpParams();
    params = params.append('pageSize', pageSize + '');
    params = params.append('currentPage', currentPage + '');
    params = params.append('search', search);
    params = params.append('orderBy', orderBy);

    return this.http.get(url, { params })
      .pipe(
        map((res: IProductResponse) => {
          return res;
        }),
        catchError(errorRes => {
          return throwError(errorRes);
        })
      );
  }

  //login() {
  //
  // localStorage.setItem('symbolcode', this.data.symbolcode);
  // this._sendrequest(req);
  //}
  _connect() {
    //  alert('got it');
    console.log("Initialize paybito WebSocket Connection-------------");
    let ws = new SockJS(this.webSocketEndPoint);
    this.stompClient = Stomp.over(ws);
    this.stompClient.debug = null;
    const _this = this;
    _this.stompClient.connect({}, function (frame) {
      _this.stompClient.subscribe(_this.topic, function (sdkEvent) {

        _this.onMessageReceive(sdkEvent);

      });

      //_this.stompClient.reconnect_delay = 2000;
    }, this.errorCallBack);

  };

  _disconnect() {
    if (this.stompClient !== null) {
      this.stompClient.disconnect();
    }
    console.log("Disconnected");
  }
  errorCallBack(error) {
    console.log("errorCallBack -> " + error)
    setTimeout(() => {
      this._connect();
    }, 5000);
  }
  sendmessagedata() {
    debugger;
    this.reqNo = Math.floor((Math.random() * 1000) + 1);
    this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
    var companyID = localStorage.getItem('CompanyID');
    var PId = localStorage.getItem('ProcuctId');
    var Ordertype = localStorage.getItem('OrderType');
    var Nigotiation = localStorage.getItem('Nigotiation');
    const req = {
      "method": "SUBSCRIBE",
      "event": "orderbook",
      "productId": PId, // can be send 0
      "orderType": Ordertype,  // buy (1) or sell (2)
      "negotiationTerms": Nigotiation,   //negotitation (1) and firm(2)
      "businessType": this.BusinessTypeId,  //trade (1), buyer(1) , seller(2)
      "companyId": companyID,
      "pageNo": 1,
      "noOfItemsPerPage": 10,
      "id": this.reqNo  //random number
    };
    this._sendrequest(req);
    localStorage.setItem('reqno', this.reqNo);
  }
  sendmessageHistorydata() {
    this.reqNo = Math.floor((Math.random() * 1000) + 1);
    var companyID = localStorage.getItem('CompanyID');

    const req = {
      "method": "SUBSCRIBE",
      "event": "tradeHistory",
      "companyId": companyID,
      "pageNo": 1,
      "noOfItemsPerPage": 10,
      "id": this.reqNo  //random number
    };
    this._sendrequest(req);
    localStorage.setItem('reqno', this.reqNo);
  }
  unsubscribeHistorydata() {
    this.reqNo = Math.floor((Math.random() * 1000) + 1);
    var companyID = localStorage.getItem('CompanyID');
    var requestno = localStorage.getItem('reqno');
    const req = {
      "method": "UNSUBSCRIBE",
      "event": "tradeHistory",
      "orderType": 0,
      "companyId": companyID,
      "pageNo": 1,
      "noOfItemsPerPage": 10,
      "id": requestno //random number
    };
    this._sendrequest(req);
  }
  sendmessageWatchlistdata() {
    debugger;
    this.reqNo = Math.floor((Math.random() * 1000) + 1);
    const req = {
      "method": "SUBSCRIBE",
      "event": "watchList",
      "limit": 10,
      "productId": 0,
      "id": this.reqNo  //random number
    };
    this._sendrequest(req);
    localStorage.setItem('wreqno', this.reqNo);
  }
  UnsunscribemessageWatchlistdata() {
    //debugger;
    var requestno = localStorage.getItem('wreqno');
    const req = {
      "method": "UNSUBSCRIBE",
      "event": "watchList",
      "limit": 10,
      "productId": 0,
      "id": requestno  //random number
    };
    this._sendrequest(req);

  }


  unsubscribe() {
    debugger;
    this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
    var companyID = localStorage.getItem('CompanyID');
    var requestno = localStorage.getItem('reqno');
    var PId = localStorage.getItem('ProcuctId');
    var Ordertype = localStorage.getItem('OrderType');
    var Nigotiation = localStorage.getItem('Nigotiation');
    const req = {
      "method": "UNSUBSCRIBE",
      "event": "orderbook",
      "productId": PId, // can be send 0 
      "orderType": Ordertype,  // buy (1) or sell (2)
      "negotiationTerms": Nigotiation,   //negotitation (1) and firm(2)
      "businessType": this.BusinessTypeId,  //trade (1), buyer(1) , seller(2)
      "companyId": companyID,
      "pageNo": 1,
      "noOfItemsPerPage": 10,
      "id": requestno //random number
    };

    this._sendrequest(req);
  }
  sendmessageMarkettdata() {
    //debugger;
    this.MarketreqNo = Math.floor((Math.random() * 1000) + 1);
    const req = {
      "method": "SUBSCRIBE",
      "event": "marketTrade",
      "limit": 10,
      "productId": 0,
      "id": this.MarketreqNo  //random number
    };
    this._sendrequest(req);
    localStorage.setItem('Mreqno', this.MarketreqNo);
  }
  unsubscribeMarketdata() {
    var requestno = localStorage.getItem('Mreqno');
    const req = {
      "method": "SUBSCRIBE",
      "event": "marketTrade",
      "limit": 10,
      "productId": 0,
      "id": requestno  //random number
    };
    this._sendrequest(req);
  }
  _sendrequest(message) {
    // alert('sdfsd');
    // console.log("calling logout api via web socket");
    //console.log(message);
    this.stompClient.send("/app/send", {}, JSON.stringify(message));
  }
  onMessageReceive(message) {

    var str = JSON.stringify(message.body);
    var obj = JSON.parse(str);
    var data = JSON.parse(obj);
    this.orderbook = data;
    console.log("Message Recieved from Server for all order :: " + data.e);
    this.dataSource.next(data);

  }
  handleError(error: HttpErrorResponse) {
    console.log("lalalalalalalala");

    return throwError(error);
  }

  Logindata(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/companyLogin', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })

      .pipe(
        catchError(this.handleError)
      );


  }

  GetcompanyDetails(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/getCompanyDetails', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
      .pipe(
        catchError(this.handleError)
      );
  }
  Getmywallet(data) {
    return this.http.post<any>(this.WEBSERVICE + '/trade/getWalletInfo', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
      .pipe(
        catchError(this.handleError)
      );
  }
  Forgetpassword(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/forgotPassword', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });

  }
  Resetpassword(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/resetPassword', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });

  }
  Authorisedpersondata(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/getAuthorizedPersonInformation', data);
  }
  SaveprofileImage(data) {
    return this.http.post<any>(this.WEBSERVICE + '/files/addProfileImage', data);
  }
  Otpgenarate(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/genOtp', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  OtpCheck(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/verifyOtp', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  UpdatecontactandEmail(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/updateAuthorizedContactAndEmail', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  companyInfodata(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/getCompanyInformation', data);
  }
  // companyDetails(data){
  //   return this.http.post<any>(this.WEBSERVICE + '/users/getCompanyInformation', data);
  // }
  BankingInfodata(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/getBankingInformation', data);
  }
  BankingInfoSave(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/updateBankDetails', data);
  }
  BankBranchList(companyId) {
    return this.http.get<any>(this.WEBSERVICE + '/users/getBankBranchList?companyId=' + companyId);
  }
  UpdateBranch(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/updateBankBranchInfo', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  AddBranch(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/saveBankBranchInfo', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  UpdateBankPrimery(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/updateBankBranchPrimary', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  DeleteBank(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/deleteBankBranchInfo', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  productpreferencedata() {
    return this.http.get<any>(this.WEBSERVICE + '/users/getProductList');

  }
  getselectedProducts(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/getSelectedProducts', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });

  }
  Saveproduct(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/selectProductPreferences', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });

  }
  SavePassword(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/changePassword', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }

  SessionInfodata(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/getAuthorizedPersonInformation', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  getcompanystatedistrict() {
    // return  this.http.get<any>(this.data.WEBSERVICE+'/location/getLocationListByCountry?countryId=1');

    return this.http.post<any>(this.WEBSERVICE + "/location/getLocationListByCountry?countryId=1", '')
  }
  getcompanystatedistrictForOrder(businessID) {

    var companyID = localStorage.getItem('CompanyID');
    if (businessID == 3) {

      return this.http.post<any>(this.WEBSERVICE + "/location/getLocationListByCountry?countryId=1&companyId=" + companyID, '')
    }
    else {

      return this.http.post<any>(this.WEBSERVICE + "/location/getLocationListByCountry?countryId=1", '')

    }


  }
  CheckProdState(data) {
    return this.http.post<any>(this.WEBSERVICE + '/trade/checkProductState', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  getcompanylist(companyID) {
    return this.http.get<any>(this.WEBSERVICE + '/users/getCompanyBranchList?companyId=' + companyID);
  }
  UpdateCompanyBranch(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/updateCompanyBranchInfo', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  AddCompanyBranch(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/saveCompanyBranchInfo', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  CompanyDeleteBank(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/deleteCompanyBranchInfo', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  getshipmentlist(companyID) {
    return this.http.get<any>(this.WEBSERVICE + '/users/getShipmentLocationList?companyId=' + companyID);
  }
  UpdateshipmentBranch(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/saveShipmentLocationInfo', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  AddShipmentBranch(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/saveShipmentLocationInfo', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  ShipmenLocationtDelete(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/deleteShipmentLocationInfo', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  GetpaymentType() {
    return this.http.get<any>(this.WEBSERVICE + '/payments/getPaymentCategories');
  }

  GetPendingpaymentType(data) {
    return this.http.post<any>(this.WEBSERVICE + '/payments/getPendingPayments', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  Updatepayment(data) {
    return this.http.post<any>(this.WEBSERVICE + '/payments/updatePaymentModal', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  Confirmpayment(data) {
    return this.http.post<any>(this.WEBSERVICE + '/payments/getConfirmedPayments', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  paymenthistory(data) {
    return this.http.post<any>(this.WEBSERVICE + '/payments/getAllPayments', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  Getallproduct(data) {
    return this.http.post<any>(this.WEBSERVICE + '/trade/getAllProducts', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
      .pipe(
        catchError(this.handleError)
      );
  }
  Getcommodities(data) {
    return this.http.post<any>(this.WEBSERVICE + '/trade/getAllCommodities', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  GetMarketTrade() {
    return this.http.get<any>('http://54.177.218.175:8080/socketStream/api/getMarketTrade?limit=10')
  }
  GetWatchList(Pid) {
    return this.http.get<any>('http://54.177.218.175:8080/socketStream/api/getWatchlist?productId=' + Pid + '&limit=10')
  }

  GetSeason(data) {
    return this.http.post<any>(this.WEBSERVICE + '/trade/getSeason', data)
  }
  GenerateAllotmentNo(data) {
    return this.http.post<any>(this.WEBSERVICE + '/allotment/generateAllotmentNo', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  AddUpdateSelectionDetails(data) {
    return this.http.post<any>(this.WEBSERVICE + '/allotment/addUpdateSelectionDetails', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  GetSelectionDetails(data) {
    return this.http.post<any>(this.WEBSERVICE + '/allotment/getSelectionDetails', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  GetAllProducts(data) {
    return this.http.post<any>(this.WEBSERVICE + '/payments/getAllProducts', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  GetAllSubProducts(data) {
    return this.http.post<any>(this.WEBSERVICE + '/payments/getAllSubProducts', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  Getvariety(data) {
    return this.http.post<any>(this.WEBSERVICE + '/trade/getAllProductVariety', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  Getgrade(data) {
    return this.http.post<any>(this.WEBSERVICE + '/trade/getProductGrade', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  DeliveryType() {
    return this.http.get<any>(this.WEBSERVICE + '/trade/selectDeliveryType');
  }
  Getunit(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/getSpecificUnits", JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
  }
  PaymentType() {
    return this.http.get<any>(this.WEBSERVICE + '/trade/selectPaymentMode');
  }
  Pterms(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/getPaymentSettings", JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
  }
  searchCity(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/searchByCityName", JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
  }
  gradespecification(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/getGradeSpecification", JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
  }
  createOrder(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/createOrder", data);
  }
  Getbalance(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/getWalletInfo", data);
  }
  GetTradeLimit(data) {
    return this.http.post<any>(this.WEBSERVICE + "/payments/getTradeLimitBySubProduct", data)
  }

  getBusinesstype() {
    return this.http.get<any>(this.WEBSERVICE + '/users/getCompanyBusinessTypes');
  }
  Opendeals(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/getOpenOrders", data);
  }
  Canceldeals(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/getDeclinedOrders", data);
  }
  Insightdata(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/getOrderBook", data);
  }
  Editdata(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/addOrderBid", data);
  }
  Ordercancel(data) {
    return this.http.post<any>(this.WEBSERVICE + '/trade/cancelOrder', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  GeneratePGDAdvice(data) {
    return this.http.post<any>(this.WEBSERVICE + '/payments/generatePGDAdvice', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  GenarateInvoice(input) {
    return this.http.post<any>(this.WEBSERVICE + '/pdf/createInvoice', JSON.stringify(input), { headers: { 'Content-Type': 'application/json' } })
  }
  Getexchangedetails() {
    return this.http.get<any>(this.WEBSERVICE + '/users/getAdminExchangeDetails');
  }
  GetTradeLimitBySubProduct(data) {
    return this.http.post<any>(this.WEBSERVICE + '/payments/getTradeLimitBySubProduct', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  OrderUpdate(data) {
    return this.http.post<any>(this.WEBSERVICE + '/trade/updateOrder', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  GetorderRest(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/getOrderBook", data);
  }
  GetmatchingInterestRest(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/getMatchIntOrders", data);
  }
  GetNigotiation(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/getNegotiation", data);
  }
  Approvorder(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/approveOrder", data);
  }
  Rejectorder(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/rejectOrder", data);
  }
  Tradehistory(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/getTradeHistory", data);
  }
  // -----margin
  UpdateMarginFlag(data) {
    return this.http.post<any>(this.WEBSERVICE + "/payments/updateMarginFlag", data);
  }
  checkMarginFlag(data) {
    return this.http.post<any>(this.WEBSERVICE + "/payments/checkMarginFlag", data);
  }
  GetMarginValue(data) {
    return this.http.post<any>(this.WEBSERVICE + "/payments/getMarginValues", data);
  }
  GenerateMarginAdvice(data) {
    return this.http.post<any>(this.WEBSERVICE + '/payments/generateMarginAdvice', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  // GetAllProduct(data){
  //   return this.http.post<any>(this.WEBSERVICE+"/trade/getAllProductsForMargin",data);
  // }
  // GetAllSubProduct(data){
  //   return this.http.post<any>(this.WEBSERVICE+"/trade/getAllSubProductsForMargin",data);
  // }
  GetAllMarginProducts(data) {

    return this.http.post<any>(this.WEBSERVICE + '/payments/getAllProductsForMargin', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
      .pipe(
        catchError(this.handleError)
      );
  }
  GetAllMarginSubProducts(data) {
    return this.http.post<any>(this.WEBSERVICE + '/payments/getAllSubProductsForMargin', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  //marginend
  getSeason(data) {
    return this.http.post<any>(this.WEBSERVICE + "/trade/getSeason", JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  getSelectionTcn(data) {
    return this.http.post<any>(this.WEBSERVICE + '/selection/getSelectionTcn', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  permissionForAdding(data) {
    return this.http.post<any>(this.WEBSERVICE + '/selection/permissionForAdding', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  getStateList(data) {
    return this.http.post<any>(this.WEBSERVICE + '/location/getStateList', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  addedSelectionDetails(data) {
    return this.http.post<any>(this.WEBSERVICE + '/selection/addedSelectionDetails', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  getRDCList(data) {
    return this.http.post<any>(this.WEBSERVICE + '/location/getRDCList', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  checkQty(data) {
    return this.http.post<any>(this.WEBSERVICE + '/selection/checkQty', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  viewAllSelectionDetails(data) {
    return this.http.post<any>(this.WEBSERVICE + '/selection/viewAllSelectionDetails', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  updateSelectionDetails(data) {
    return this.http.post<any>(this.WEBSERVICE + '/selection/updateSelectionDetails', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  getTCNAgainstCompanyId(data) {
    return this.http.post<any>(this.WEBSERVICE + '/selection/getTCNAgainstCompanyId', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  viewSelectionDetailsAgainstTCN(data) {
    return this.http.post<any>(this.WEBSERVICE + '/selection/viewSelectionDetailsAgainstTCN', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  approveSelection(data) {
    return this.http.post<any>(this.WEBSERVICE + '/selection/approveSelection', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  rejectSelection(data) {
    return this.http.post<any>(this.WEBSERVICE + '/selection/rejectSelection', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  getTcnList(data) {
    return this.http.post<any>(this.WEBSERVICE + '/deliveryShipment/getTcnList', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  getViewByTcn(data) {
    return this.http.post<any>(this.WEBSERVICE + '/deliveryShipment/getViewByTcn', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  addDeliveryAddress(data) {
    return this.http.post<any>(this.WEBSERVICE + '/deliveryShipment/addDeliveryAddress', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  mailSellerBuyer(data) {
    return this.http.post<any>(this.WEBSERVICE + '/deliveryShipment/mailSellerBuyer', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  //-------------registration
  // GetOtp(data) {

  //   return this.http.post<any>(this.WEBSERVICE + '/users/insertTempUserDetails', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  // }

  // new service for registration
  GetOtp(data) {

    return this.http.post<any>(this.WEBSERVICE + '/users/generateOtp', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  // OTPverify(data) {

  //   return this.http.post<any>(this.WEBSERVICE + '/users/checkOtp', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
  // }

  OTPverify(data) {

    return this.http.post<any>(this.WEBSERVICE + '/users/checkOtp', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
  }

  getAllPromoter(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/getPromoterList', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
  }


  PromoterVeryPan(obj: FormData) {
    return this.http.post<any>(this.WEBSERVICE + '/users/verifyPan', obj);
  }

  verifyBankAccountData(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/verifyBankAccount', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
  }

  CheckAadharNumber(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/generateAadhaarOtp', data, { headers: { 'content-Type': 'application/json' } });
  }

  VerifyAdharOtp(obj: FormData) {
    return this.http.post<any>(this.WEBSERVICE + '/users/verifyAadhaarOtp', obj);
  }

  // new service for registration

  AddCompanyDetails(obj: FormData) {
    return this.http.post<any>(this.WEBSERVICE + '/users/addCompanyDetails', obj);
  }
  InsertCompanydata(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/addCompanyDetails', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
  }
  InsertPersonaldata(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/addPersonalDetails', data, { headers: { 'content-Type': 'application/json' } })
  }
  InsertAssociationdata(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/associationInfo', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
  }
  InsertAothorizationdata(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/addAuthorizationInfo', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  Getregtype() {
    return this.http.get<any>(this.WEBSERVICE + '/users/getCompanyRegTypes');
  }
  GetallInfo(companyId) {
    // return  this.http.post<any>(this.data.WEBSERVICE+'/users/getPreviewDetails?'+companyId);
    return this.http.post<any>(this.WEBSERVICE + "/users/getPreviewDetails", companyId)
  }
  Updatecompanydetails(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/updateCompanyDetails', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  Updatepersonaldetails(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/updatePersonalDetails', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  Updateassociationdetails(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/updateAssociationInfo', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  Updateaauthorizationdetails(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/updateAuthorizationInfo', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  Updatebankdetails(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/updateBankDetails', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  // getBusinesstype(){
  //   return  this.http.get<any>(this.WEBSERVICE+'/users/getCompanyBusinessTypes');
  // }
  // getcompanystatedistrict(){
  //  // return  this.http.get<any>(this.data.WEBSERVICE+'/location/getLocationListByCountry?countryId=1');
  //  var countryId=1;
  //   return this.http.post<any>(this.WEBSERVICE + "/location/getLocationListByCountry?countryId=1",'')

  // }
  CheckPhoneForConpanyInfo(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/checkPhone', data, { headers: { 'content-Type': 'application/json' } });
  }
  CheckEmailForCompanyEmail(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/checkEmail', data, { headers: { 'content-Type': 'application/json' } });
  }
  CheckphoneForAuthorisedInfo(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/checkAuthPhone', data, { headers: { 'content-Type': 'application/json' } });
  }
  CheckEmailForAuthorisedEmail(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/checkAuthEmail', data, { headers: { 'content-Type': 'application/json' } });
  }
  // CheckGst(data) {
  //   return this.http.post<any>(this.WEBSERVICE + '/users/checkGst', data, { headers: { 'content-Type': 'application/json' } });
  // }

  CheckGst(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/gstChecked', data, { headers: { 'content-Type': 'application/json' } });
  }
  CheckPan(data) {
    return this.http.post<any>(this.WEBSERVICE + '/users/checkPan', data, { headers: { 'content-Type': 'application/json' } });
  }
  getTcnBuyer(data) {
    return this.http.post<any>(this.WEBSERVICE + '/selection/getTcnBuyer', data, { headers: { 'content-Type': 'application/json' } });
  }
  viewApproveSelectionAgainstTCN(data) {
    return this.http.post<any>(this.WEBSERVICE + '/selection/viewApproveSelectionAgainstTCN', data, { headers: { 'content-Type': 'application/json' } });
  }
  getApproveTcnForDelivery(data) {
    return this.http.post<any>(this.WEBSERVICE + '/deliveryShipment/getApproveTcnForDelivery', data, { headers: { 'content-Type': 'application/json' } });
  }
  /////start trade payment
  GetPCompanyName(data) {
    return this.http.post<any>(this.WEBSERVICE + '/payments/getPaymentCompanyNames', data, { headers: { 'content-Type': 'application/json' } });;
  }
  GetPaymentTcn(data) {
    return this.http.post<any>(this.WEBSERVICE + '/payments/getPaymentTcn', data, { headers: { 'content-Type': 'application/json' } });;
  }
  GetPaymentTypes() {
    return this.http.get<any>(this.WEBSERVICE + '/payments/getPaymentTypes');
  }
  getTcnListForShipments(data) {
    return this.http.post<any>(this.WEBSERVICE + '/deliveryShipment/getTcnListForShipments', data, { headers: { 'content-Type': 'application/json' } });
  }
  getDeliveryNoListByTcn(data) {
    return this.http.post<any>(this.WEBSERVICE + '/deliveryShipment/getDeliveryNoListByTcn', data, { headers: { 'content-Type': 'application/json' } });
  }
  getDeliveryDetailsByDeliverNo(data) {
    return this.http.post<any>(this.WEBSERVICE + '/deliveryShipment/getDeliveryDetailsByDeliverNo', data, { headers: { 'content-Type': 'application/json' } });
  }
  getAllCompanyByComId(data) {
    return this.http.post<any>(this.WEBSERVICE + '/selection/getAllCompanyByComId', data, { headers: { 'content-Type': 'application/json' } });
  }
  getTradePaymentDtl(data) {
    return this.http.post<any>(this.WEBSERVICE + '/payments/getTradePaymentDetails', data, { headers: { 'content-Type': 'application/json' } })
  }
  UpdatebalencePmt(data) {
    return this.http.post<any>(this.WEBSERVICE + '/payments/updateBalancePayment', data, { headers: { 'content-type': 'application/json' } })
  }
  createPAdvicePreview(data) {
    return this.http.post<any>(this.WEBSERVICE + '/payments/getTradePaymentPreview', data, { headers: { 'content-Type': 'application/json' } })
  }
  GetpaymentInvoiceno(data) {
    return this.http.post<any>(this.WEBSERVICE + '/payments/getPaymentInvoiceNo', data, { headers: { 'content-Type': 'application/json' } })
  }
  SendMailtoBuyerSeller(data) {
    return this.http.post<any>(this.WEBSERVICE + '/pdf/createTradePaymentAdvice', data, { headers: { 'content-Type': 'application/json' } })
  }
  ////////end payment
  getTradeInfoByproductTcn(data) {
    return this.http.post<any>(this.WEBSERVICE + '/deliveryShipment/getTradeInfoByproductTcn', data, { headers: { 'content-Type': 'application/json' } });
  }
  addShipmentInformation(data: FormData) {
    return this.http.post<any>(this.WEBSERVICE + '/deliveryShipment/addShipmentInformation', data);
  }
  getShipmentNoListByDeliveryNO(data) {
    return this.http.post<any>(this.WEBSERVICE + '/deliveryShipment/getShipmentNoListByDeliveryNO', data, { headers: { 'content-Type': 'application/json' } });
  }
  getConfmShippmentDetails(data) {

    return this.http.post<any>(this.WEBSERVICE + '/deliveryShipment/getConfmShippmentDetails', data, { headers: { 'content-Type': 'application/json' } });
  }
  getClaimData(data) {

    return this.http.post<any>(this.WEBSERVICE + '/claim/getClaims', data, { headers: { 'content-Type': 'application/json' } });
  }
  RaiseClaimConfirmation(data) {
    return this.http.post<any>(this.WEBSERVICE + '/claim/getRaiseClaims', data, { headers: { 'content-Type': 'application/json' } });
  }
  GetissuetypeList() {
    return this.http.get<any>(this.WEBSERVICE + '/claim/getIssueTypeList');
  }
  GetIssueId(data) {
    return this.http.post<any>(this.WEBSERVICE + '/claim/getIssueIdByIssueType', data, { headers: { 'content-Type': 'application/json' } })
  }
  SubmitClaim(data) {
    return this.http.post<any>(this.WEBSERVICE + '/claim/addClaimDocument', data);
  }
  viewClaim(data) {
    return this.http.post<any>(this.WEBSERVICE + '/claim/getAllViewAfterClaim', data);
  }
  GetClaimInfo(data) {
    return this.http.post<any>(this.WEBSERVICE + '/claim/getClaimTabInfo', data, { headers: { 'content-Type': 'application/json' } });

  }
  checkVariationsForBags(data) {
    return this.http.post<any>(this.WEBSERVICE + '/deliveryShipment/checkVariationsForBags', data, { headers: { 'content-Type': 'application/json' } });
  }
  getReportCategories() {
    return this.http.get<any>(this.REPORT_WEBSERVICE + '/report/getReportCategories');
  }
  getAllReports(obj: any) {
    return this.http.post<any>(this.REPORT_WEBSERVICE + '/report/getAllReports', JSON.stringify(obj), { headers: { 'content-Type': 'application/json' } });
  }
  generatePdf(obj: any, fileName: string) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: this.WEBSERVICE + '/files/generatePdf',
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify({ html: obj }),
        cache: false,
        xhr: function () {
          var xhr = new XMLHttpRequest();
          xhr.onreadystatechange = function () {
            if (xhr.readyState == 2) {
              if (xhr.status == 200) {
                xhr.responseType = "blob";
              } else {
                xhr.responseType = "text";
              }
            }
          };
          return xhr;
        },
        success: function (data) {
          var blob = new Blob([data], { type: "application/octetstream" });
          var url = window.URL || window.webkitURL;
          var link = url.createObjectURL(blob);
          var a = $("<a id='987'/>");
          a.attr("download", fileName);
          a.attr("href", link);
          $("body").append(a);
          a[0].click();
          $("#987").remove();
          resolve('Successfully Downloaded.')
        },
        error: function (error) {
          console.log(error);
          reject('Opps Something Went Wrong!!!')
        }
      })
    })
  }
  generatePaymentLink(obj: any) {
    return this.http.post<any>(this.WEBSERVICE + '/cashfreepayment/createorder', JSON.stringify(obj), { headers: { 'content-Type': 'application/json' }});
  }  
  checkBalanceforQty(obj: any) {
    return this.http.post<any>(this.WEBSERVICE + '/deliveryShipment/checkBalanceforQty', JSON.stringify(obj), { headers: { 'content-Type': 'application/json' }});
  }
  getMembershipDetails() {
    return this.http.get<any>(this.WEBSERVICE + '/users/getMembershipDetails');
  }
  generateAdviceOrInvoice(obj: any) {
    return this.http.post<any>(this.WEBSERVICE + '/users/generateAdviceOrInvoice', JSON.stringify(obj), { headers: { 'content-Type': 'application/json' }});
  }  
  saveCompanyBranchDetails(obj:FormData){
    return this.http.post<any>(this.WEBSERVICE + '/users/saveCompanyBranchDetails',obj);
  }
  updateVendor(obj:any){
    return this.http.post<any>(this.WEBSERVICE + '/cashfreepayment/updateVendor', JSON.stringify(obj), { headers: { 'content-Type': 'application/json' }});    
  }
  generateWithdrawalAdvice(obj:any){
    return this.http.post<any>(this.WEBSERVICE + '/payments/generateWithdrawalAdvice', JSON.stringify(obj), { headers: { 'content-Type': 'application/json' }});    
  }
}
