import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';
import{ApiService} from 'src/app/data/api.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
})
export class ResetPasswordComponent  {
  @ViewChild('resetForm') resetForm: NgForm;
  emailModel = 'demo@vien.com';
  passwordModel = 'demovien1122';

  buttonDisabled = false;
  buttonState = '';

  constructor(private _ApiService:ApiService,
    private authService: AuthService,
    private notifications: NotificationsService,
    private router: Router
  ) {}


  onSubmit(): void {
    debugger;
    if (!this.resetForm.valid || this.buttonDisabled) {
      return;
    }
    this.buttonDisabled = true;
    this.buttonState = 'show-spinner';
    let resetform={};
    resetform['tradingCode']=this.resetForm.value['code'];
    resetform['password']=this.resetForm.value['newPassword'] ;
    resetform['otp']=this.resetForm.value['otp'];
   // alert(this.resetForm.value['newPassword']);
    this._ApiService.Resetpassword(resetform)
    .subscribe(data=>{
console.log('=============================',data);
if(data.error.errorData==0){
  this.buttonState = '';
        this.notifications.create(
          'Done',
          'Password reset completed, you will be redirected to Login page!',
          NotificationType.Bare,
          { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
        );
        this.buttonDisabled = false;
this.router.navigate(['user/login']);
}
else{
  this.buttonState = '';
  this.buttonDisabled = false;
  this.notifications.create('Error', data.error.errorMsg,
   NotificationType.Bare, { theClass: 'outline primary', timeOut: 6000, showProgressBar: false });
}

    })
    // this.authService
    //   .resetPassword(this.resetForm.value)
    //   .then((data) => {
    //     this.notifications.create(
    //       'Done',
    //       'Password reset completed, you will be redirected to Login page!',
    //       NotificationType.Bare,
    //       { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
    //     );
    //     this.buttonDisabled = false;
    //     this.buttonState = '';
    //     setTimeout(() => {
    //       this.router.navigate(['user/login']);
    //     }, 6000);
    //   })
    //   .catch((error) => {
    //     this.buttonDisabled = false;
    //     this.buttonState = '';
    //     this.notifications.create(
    //       'Error',
    //       error.message,
    //       NotificationType.Bare,
    //       { theClass: 'outline primary', timeOut: 6000, showProgressBar: false }
    //     );
    //   });
  }
}
