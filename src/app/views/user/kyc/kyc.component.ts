import { Component, ViewChild, OnInit, TemplateRef, ElementRef } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { WizardComponent as ArcWizardComponent } from 'angular-archwizard';
import { ApiService } from 'src/app/data/api.service';
import { NotificationsService, NotificationType } from "angular2-notifications";
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from "ngx-spinner";
import * as $ from 'jquery'
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-kyc',
  templateUrl: './kyc.component.html',
  styleUrls: ['./kyc.component.scss']
})
export class KycComponent implements OnInit {
  WEBSERVICE: string = '';
  mobileNo: string = '';
  email: string = '';
  name: string = '';
  companyID: string = '';
  processStep: number = 0;
  posting = false;
  businessTypeList: Array<any> = [];
  promotorList: Array<any> = [];
  panArray: Array<boolean> = [];
  membershipList: Array<any> = [];
  aadhaarClientId: string = '';
  membershipObj: any = null;
  membershipId: any = '';
  modalRef: BsModalRef = null;
  isPanVerified: boolean = false;
  @ViewChild('wizard') wizard: ArcWizardComponent;

  gstClientId: string = null;
  gstForm = new FormGroup({
    gstNo: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(15), Validators.maxLength(15)])),
    businessType: new FormControl(null, Validators.required),
    otp: new FormControl(null, Validators.required),
    gstFile: new FormControl(null, Validators.required)
  });

  panForm = new FormGroup({
    panFormArray: new FormArray([], Validators.required)
  });
  get panFormArray() {
    return this.panForm.get('panFormArray') as FormArray;
  }

  aadhaarForm = new FormGroup({
    aadhaarNo: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(12), Validators.maxLength(12)])),
    otp: new FormControl(null, Validators.required),
    aadhaarFile: new FormControl(null, Validators.required),
    phoneNo: new FormControl(null, Validators.required),
    email: new FormControl(null, Validators.required),
    panNo: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)]))
  });

  bankForm = new FormGroup({
    accountNo: new FormControl(null, Validators.required),
    ifscCode: new FormControl(null, Validators.required)
  });

  @ViewChild('gstFile', { static: false })
  gstFileInput: ElementRef;
  @ViewChild('panFile', { static: false })
  panFileInput: ElementRef;
  @ViewChild('aadhaarFile', { static: false })
  aadhaarFileInput: ElementRef;

  constructor(private http: HttpClient, private _service: ApiService, private notifications: NotificationsService,
    private translate: TranslateService, private spinner: NgxSpinnerService, private modalService: BsModalService) {
    this.WEBSERVICE = _service.WEBSERVICE;
  }

  ngOnInit(): void {
    this.mobileNo = localStorage.getItem('tmpMobileno');
    this.email = localStorage.getItem('inregEmail');
    this.name = localStorage.getItem('name');
    this.processStep = Number(localStorage.getItem('processStep'));
    this.companyID = localStorage.getItem("companyId");
    if (this.processStep == 1)
      this.getBusinesstype();
    if (this.processStep == 2)
      this.getAllPromoter();
    if (this.processStep == 5)
      this.getMembershipDetails();
    this.aadhaarForm.get('phoneNo').setValue(this.mobileNo);
    this.aadhaarForm.get('email').setValue(this.email);
    this.aadhaarForm.get('phoneNo').disable();
    this.aadhaarForm.get('email').disable();
  }

  getBusinesstype() {
    this.businessTypeList = [];
    this.spinner.show();
    this._service.getBusinesstype().subscribe(
      data => {
        this.spinner.hide();
        this.businessTypeList = data != null ? data : [];
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getAllPromoter() {
    this.panArray = [];
    this.isPanVerified = false;
    this.panFormArray.clear();
    this.panFormArray.reset();
    this.spinner.show();
    this._service.getAllPromoter({ companyId: this.companyID }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.promotorList = data.promoterList != null ? data.promoterList : [];
          for (let x = 0; x < this.promotorList.length; x++) {
            this.panFormArray.push(new FormGroup({
              id: new FormControl(this.promotorList[x].promoterId),
              fullName: new FormControl(this.promotorList[x].promoterName, Validators.required),
              panNo: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])),
              dob: new FormControl(null, Validators.required),
              panFile: new FormControl(null, Validators.required)
            }));
            if (this.promotorList[x].panNo)
              this.panArray.push(true);
            else
              this.panArray.push(false);
          }
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getMembershipDetails() {
    this.membershipList = [];
    this.spinner.show();
    this._service.getMembershipDetails().subscribe(
      data => {
        this.spinner.hide();
        this.membershipList = data != null ? data : [];
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  validateGstNo(gstNo: string) {
    if (gstNo != null && gstNo.length == 15) {
      this.gstClientId = '';
      this.spinner.show();
      this._service.CheckGst({ gstNo: gstNo }).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.gstClientId = data.clientId;
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }
  }

  addCompanyDetails() {
    let otp = this.gstForm.get('otp').value;
    let businessType = this.gstForm.get('businessType').value ? this.gstForm.get('businessType').value.toString() : null;
    let gstFile = this.gstForm.get('gstFile').value as File;
    if (otp && businessType && gstFile && this.gstClientId) {
      let formData = new FormData();
      formData.append("clientId", this.gstClientId);
      formData.append("businessTypeId", businessType);
      formData.append("uploadFile", gstFile);
      formData.append("companyId", this.companyID);
      formData.append("otp", otp);
      this.spinner.show();
      this._service.AddCompanyDetails(formData).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.onSuccess("Company Details Added Successfully");
            this.gstClientId = '';
            this.gstForm.reset();
            this.gstFileInput.nativeElement.value = "";
            this.wizard.goToNextStep();
            this.getAllPromoter();
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    } else {
      this.gstForm.get('gstNo').markAsTouched();
      this.gstForm.get('businessType').markAsTouched();
      this.gstForm.get('otp').markAsTouched();
      this.gstForm.get('gstFile').markAsTouched();
      this.onError('Please Enter Valid Info!');
    }
  }

  promoterVeryPan(index: any) {
    let id = this.panFormArray.controls[index].value.id;
    let fullName = this.panFormArray.controls[index].value.fullName;
    let dob = this.formatDate(this.panFormArray.controls[index].value.dob);
    let panNo = this.panFormArray.controls[index].value.panNo;
    let panFile = this.panFormArray.controls[index].value.panFile as File;
    if (id && fullName && dob && panNo && panFile) {
      let formData = new FormData();
      formData.append("companyId", this.companyID);
      formData.append("promoterId", id);
      formData.append("fullName", fullName);
      formData.append("panNo", panNo);
      formData.append("dob", dob);
      formData.append("uploadFile", panFile);
      this.spinner.show();
      this._service.PromoterVeryPan(formData).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.isPanVerified = true;
            this.onSuccess("PAN Verified Successfully");
            this.panArray[index] = true;
            this.panFormArray.controls[index].reset();
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    } else {
      this.panFormArray.controls[index].get('dob').markAsTouched();
      this.panFormArray.controls[index].get('panNo').markAsTouched();
      this.panFormArray.controls[index].get('panFile').markAsTouched();
      this.onError('Please Enter Valid Info!');
    }
  }

  checkAadharNumber(aadhaarNo: string) {
    this.aadhaarClientId = '';
    if (aadhaarNo != null && aadhaarNo.length == 12) {
      this.spinner.show();
      this._service.CheckAadharNumber({ aadhaarNo: aadhaarNo }).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.aadhaarClientId = data.clientId;
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }
  }

  verifyAadhaarOtp() {
    let otp = this.aadhaarForm.get('otp').value;
    let uploadFile = this.aadhaarForm.get('aadhaarFile').value as File;
    let panNo = this.aadhaarForm.get('panNo').value;
    if (otp && uploadFile && this.mobileNo && this.email && panNo) {
      let formData = new FormData();
      formData.append("companyId", this.companyID);
      formData.append("otp", otp);
      formData.append("clientId", this.aadhaarClientId);
      formData.append("uploadFile", uploadFile);
      formData.append("phoneNo", this.mobileNo);
      formData.append("email", this.email);
      formData.append("panNo", panNo);
      this.spinner.show();
      this._service.VerifyAdharOtp(formData).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.aadhaarForm.reset();
            this.aadhaarFileInput.nativeElement.value = "";
            this.onSuccess("Aadhaar Verified Successfully");
            this.wizard.goToNextStep();
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    } else {
      this.aadhaarForm.get('aadhaarNo').markAsTouched();
      this.aadhaarForm.get('otp').markAsTouched();
      this.aadhaarForm.get('aadhaarFile').markAsTouched();
      this.aadhaarForm.get('panNo').markAsTouched();
      this.onError('Please Enter Valid Info!');
    }
  }

  verifyBankAccountData() {
    let accountNo = this.bankForm.get('accountNo').value;
    let ifscCode = this.bankForm.get('ifscCode').value;
    if (accountNo && ifscCode) {
      this.spinner.show();
      this._service.verifyBankAccountData({
        companyId: this.companyID, accountNo: accountNo, ifscCode: ifscCode
      }).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.bankForm.reset();
            this.onSuccess("Bank Verified Successfully");
            this.getMembershipDetails();
            this.wizard.goToNextStep();
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    } else {
      this.bankForm.get('accountNo').markAsTouched();
      this.bankForm.get('ifscCode').markAsTouched();
      this.onError('Please Enter Valid Info!');
    }
  }

  invoiceDescription: any = '';
  invoicePurposeCode: any = '';
  invoiceMemberShipType: any = '';
  invoiceBankBeneficiaryName: any = '';
  invoiceAmount: any = '';
  invoiceGST: any = '';
  invoiceBankName: any = '';
  invoiceBankAccountNo: any = '';
  invoiceBankIfscCode: any = '';
  invoiceBankAccountType: any = '';
  invoiceId: any = '';

  generateAdviceOrInvoice(template: TemplateRef<any>) {
    this.spinner.show();
    this._service.generateAdviceOrInvoice({ companyId: this.companyID, membershipTypeId: this.membershipId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.onSuccess("Advice Generated Successfully");
          let invoice = data.adviceDetails;
          this.invoiceDescription = invoice.adviceDescription;
          this.invoicePurposeCode = invoice.advicePurposeCode;
          this.invoiceMemberShipType = invoice.membershipTypeId;
          this.invoiceBankBeneficiaryName = invoice.adminBankDetails.beneficiaryName;
          this.invoiceAmount = invoice.amount;
          this.invoiceGST = invoice.igstValue;
          this.invoiceBankName = invoice.adminBankDetails.bankName;
          this.invoiceBankAccountNo = invoice.adminBankDetails.accountNo;
          this.invoiceBankIfscCode = invoice.adminBankDetails.ifscCode;
          this.invoiceBankAccountType = invoice.adminBankDetails.accountType;
          this.invoiceId = invoice.adviceId;
          this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'modal-lg' });
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  /*** Method defination for handling download invoice ***/
  handleDownloadInvoice = () => {
    this.spinner.show();
    setTimeout(() => {
      this.wizard.goToNextStep();
      this.spinner.hide();
    }, 1000);
    //-------------  this.data.alert('Loading...', 'dark');
    //this.data.loader = false;/true
    let payload = {
      companyId: this.companyID,
    }

    /* API call for extracting company details  */
    this.http.post<any>(this.WEBSERVICE + '/users/getCompanyInformation?companyId=' + this.companyID, JSON.stringify(payload), { headers: { 'Content-Type': 'application/json' } }).subscribe(data => {

      var companyName = data.companyName;
      var companyEmail = data.email;

      var companyPhone = data.contactNo;
      /* API call for extracting Admin exchange  details  */
      this.http.get<any>(this.WEBSERVICE + '/users/getAdminExchangeDetails').subscribe(data => {
        if (data.error.errorData !== 0) {
          //---------------  this.data.alert(data.error.errorMsg, 'danger');
          //this.onError(data.error.errorMsg);
        } else {
          var exchangeName = data.adminExchangeDetails.exchangeName;
          var exchangeAddress = data.adminExchangeDetails.address;
          var exchangeEmail = data.adminExchangeDetails.email;
          var exchangeMobile = data.adminExchangeDetails.mobile;

          /* Creating invoice html string  */
          let html = '';
          html += '<html><head></head><body>'
          html += '<style>';
          html += '.invoice-box {';
          html += '    max-width: 800px;';
          html += '    margin: auto;';
          html += '    padding: 30px;';
          html += '    border: 1px solid #eee;';
          html += '    box-shadow: 0 0 10px rgba(0, 0, 0, .15);';
          html += '    font-size: 16px;';
          html += '    line-height: 24px;';
          html += "    font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;";
          html += '    color: #555;}';

          html += '.invoice-box table {';
          html += '    width: 100%;';
          html += '    line-height: inherit;';
          html += '    text-align: left;';
          html += '}';

          html += '.invoice-box table td {';
          html += '    padding: 5px;';
          html += '    vertical-align: top;';
          html += '}';

          html += '.invoice-box table tr td:nth-child(2) {';
          html += '    text-align: right;';
          html += '}';

          html += '.invoice-box table tr.top table td {';
          html += '    padding-bottom: 20px;';
          html += '}';

          html += '.invoice-box table tr.top table td.title {';
          html += '    font-size: 45px;';
          html += '    line-height: 45px;';
          html += '    color: #333;';
          html += '}';

          html += '.invoice-box table tr.information table td {';
          html += '    padding-bottom: 40px;';
          html += '}';

          html += '.invoice-box table tr.heading td {';
          html += '    background: #eee;';
          html += '    border-bottom: 1px solid #ddd;';
          html += '    font-weight: bold;';
          html += '}';

          html += '.invoice-box table tr.details td {';
          html += '    padding-bottom: 20px;';
          html += '}';

          html += '.invoice-box table tr.item td{';
          html += '    border-bottom: 1px solid #eee;';
          html += '}';
          html += '';
          html += '.invoice-box table tr.item.last td {';
          html += '    border-bottom: none;';
          html += '}';

          html += '.invoice-box table tr.total td:nth-child(2) {';
          html += '    border-top: 2px solid #eee;';
          html += '    font-weight: bold;';
          html += '}';

          html += '@media only screen and (max-width: 600px) {';
          html += '    .invoice-box table tr.top table td {';
          html += '        width: 100%;';
          html += '        display: block;';
          html += '        text-align: center;';
          html += '    }';

          html += '    .invoice-box table tr.information table td {';
          html += '        width: 100%;';
          html += '        display: block;';
          html += '        text-align: center;';
          html += '    }';
          html += '}';

          html += '.rtl {';
          html += '    direction: rtl;';
          html += "    font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;";
          html += '}';

          html += ' .rtl table {';
          html += '    text-align: right;';
          html += ' }';

          html += '  .rtl table tr td:nth-child(2) {';
          html += '      text-align: left;';
          html += '  }';
          html += '  </style>';


          html += '<div class="invoice-box">';
          html += '<table cellpadding="0" cellspacing="0">';
          html += '<tr class="top">';
          html += ' <td colspan="2">';
          html += '<table>';
          html += '<tr>';
          html += '<td class="title"> ADVICE';

          html += '</td>';

          html += '<td>Advice #: ' + this.invoiceId;
          // html +='<br>Created: January 1, 2015<br> Due: February 1, 2015';
          // html+='</td>';
          html += ' </tr>';
          html += '</table>';
          html += '</td>';
          html += '</tr>';

          html += '<tr class="information">';
          html += '<td colspan="2">';
          html += '<table>';
          html += '<tr>';
          html += '<td>Company Name : ' + companyName + ' <br> Company Email : ' + companyEmail + '<br> Company Phone : ' + companyPhone;
          html += '</td>';

          html += '<td>   Exchange Name : ' + exchangeName + '<br> Exchange Address :' + exchangeAddress + '<br>Exchange Email : ' + exchangeEmail + '<br>Exchange Phone : ' + exchangeMobile;
          html += '</td>';
          html += '</tr>';
          html += '</table>';
          html += '</td>';
          html += '</tr>';



          html += '<tr class="heading">';
          html += '<td>Item</td>';

          html += ' <td>Price</td>';
          html += '</tr>';

          html += '<tr class="item">';
          html += '<td>';
          html += this.invoiceDescription
          html += '</td>';

          html += '<td>';
          html += parseFloat(this.invoiceAmount).toFixed(2)
          html += '</td>';
          html += '</tr>';



          html += '<tr class="total">';
          html += '<td></td>';


          html += '<td> GST: ' + parseFloat(this.invoiceGST).toFixed(2) + '   </td>';
          html += '</tr>';
          html += '<tr class="total">';
          html += '<td></td>';


          html += '<td> Total: ' + (parseFloat(this.invoiceAmount) + parseFloat(this.invoiceGST)).toFixed(2) + '   </td>';
          html += '</tr>';
          html += '</table>';
          html += '<tr class="information">';
          html += '<td colspan="2">';
          html += '<table>';
          html += '<tr>';
          html += '<td> Bank Name : ' + this.invoiceBankName + '<br> Beneficiary : ' + this.invoiceBankBeneficiaryName + '<br> A/C No : ' + this.invoiceBankAccountNo + ' <br> IFSC : ' + this.invoiceBankIfscCode + ' <br> Type : ' + this.invoiceBankAccountType;
          html += '</td>';

          //html += '<td>Acme Corp.<br> John Doe<br> john@example.com';
          html += '</td>';
          html += '</tr>';
          html += '</table>';
          html += '</td>';
          html += '</tr>';
          html += '</div>';
          html += '</body>';
          console.log(html);



          /* calling API to send invoice via email */
          var emailforAdvice = this.email;
          let input = {
            "companyId": this.companyID,
            "emailId": emailforAdvice,
            "fileContent": html
          }
          this.http.post<any>(this.WEBSERVICE + '/pdf/createInvoice', JSON.stringify(input), { headers: { 'Content-Type': 'application/json' } }).subscribe(data => {
            if (data.error.errorData != 0) {
              //this.onError(data.error.errorMsg);
              //---------- this.data.alert(data.message, 'danger');
            } else {
              //this.adviceGenarateFlag = false;
              this.onSuccess(data.error.errorMsg);
              // this.modalRef.hide();
              //------.---this.data.alert('Invoice send to your email', 'success');
            }
          });

          /* calling API to grab user details  */

          let fd = new FormData()
          fd.append('companyId', this.companyID)
          this.http.post<any>(this.WEBSERVICE + '/users/getPreviewDetails', fd).subscribe(data => {
            if (data.error.errorData != 0) {
              //this.onError(data.error.errorMsg);
              //---------- this.data.alert(data.message, 'danger');
            } else {

              var company: any = data.companyDetails;
              var bank: any = data.bankDetails;
              var auhorization: any = data.authorizationInfo;
              var personal: any = data.personalDetailsList[0];
              var association: any = data.associInformationList;

              /* generating html for preview details */
              let previewHtml = '';
              previewHtml += '<html><head></head><body>'
              previewHtml += '<h3>Personal Details</h3>'
              previewHtml += '<ul>'
              previewHtml += '<li><b> Name </b> : ' + personal.firstName + '  ' + personal.lastName + ' </li>'
              previewHtml += '<li><b> Designation </b> : ' + personal.designation + ' </li>'
              previewHtml += '<li><b>Contact </b> : ' + personal.contact + ' </li>'
              previewHtml += '<li><b> Email </b> : ' + personal.email + ' </li>'
              previewHtml += '<li><b>Pan No </b> : ' + personal.panNo + ' </li>'
              previewHtml += '<li><b>AadharNo </b> : ' + personal.aadharNo + ' </li>'
              previewHtml += '<li><b>Address Line1 </b> : ' + personal.addressLineOne + ' </li>'
              previewHtml += '<li><b>Address Line2 </b> : ' + personal.addressLineTwo + ' </li>'
              previewHtml += '<li><b> City </b> : ' + personal.city + ' </li>'
              previewHtml += '<li><b> Pincode </b> : ' + personal.pinCode + ' </li>'
              previewHtml += '<li><b> Country </b> : ' + personal.country + ' </li>'
              previewHtml += '<li><b> State </b> : ' + personal.state + ' </li>'
              previewHtml += '<li><b> District </b> : ' + personal.district + ' </li>'
              previewHtml += '</ul>'
              previewHtml += '<h3>Company Details</h3>'
              previewHtml += '<ul>'
              previewHtml += '<li><b> Company Name </b> : ' + company.companyName + ' </li>'
              previewHtml += '<li><b> Business Type </b> : ' + company.businessType + ' </li>'
              previewHtml += '<li><b>Company Website </b> : ' + company.companyWebsite + ' </li>'
              previewHtml += '<li><b>Contact </b> : ' + company.contactNo + ' </li>'
              previewHtml += '<li><b> Email </b> : ' + company.email + ' </li>'
              previewHtml += '<li><b>Pan No </b> : ' + company.panNo + ' </li>'
              previewHtml += '<li><b>Address Line1 </b> : ' + company.corrAddressLineOne + ' </li>'
              previewHtml += '<li><b>Address Line2 </b> : ' + company.corrAddressLineTwo + ' </li>'
              previewHtml += '<li><b> City </b> : ' + company.city + ' </li>'
              previewHtml += '<li><b> Pincode </b> : ' + company.corrPinCode + ' </li>'
              previewHtml += '<li><b> Country </b> : ' + company.country + ' </li>'
              previewHtml += '<li><b> State </b> : ' + company.state + ' </li>'
              previewHtml += '<li><b> District </b> : ' + company.district + ' </li>'
              previewHtml += '<li><b> GST No </b> : ' + company.gstNo + ' </li>'
              previewHtml += '<li><b> CIN No </b> : ' + company.cinNo + ' </li>'
              previewHtml += '</ul>'
              previewHtml += '<h3>Authorization Details</h3>'
              previewHtml += '<ul>'
              previewHtml += '<li><b> Name </b> : ' + auhorization.firstName + ' ' + auhorization.lastName + '</li>'
              previewHtml += '<li><b>Contact </b> : ' + auhorization.contactNo + ' </li>'
              previewHtml += '<li><b> Email </b> : ' + auhorization.email + ' </li>'
              previewHtml += '<li><b>Pan No </b> : ' + auhorization.panNo + ' </li>'
              previewHtml += '<li><b>Address Line1 </b> : ' + auhorization.addressLineOne + ' </li>'
              previewHtml += '<li><b>Address Line2 </b> : ' + auhorization.addressLineTwo + ' </li>'
              previewHtml += '<li><b> City </b> : ' + auhorization.city + ' </li>'
              previewHtml += '<li><b> Pincode </b> : ' + auhorization.pinCode + ' </li>'
              previewHtml += '<li><b> Country </b> : ' + auhorization.country + ' </li>'
              previewHtml += '<li><b> State </b> : ' + auhorization.state + ' </li>'
              previewHtml += '<li><b> District </b> : ' + auhorization.district + ' </li>'
              previewHtml += '</ul>'
              previewHtml += '<h3>Bank Details</h3>'
              previewHtml += '<ul>'
              previewHtml += '<li><b> Bank Name </b> : ' + bank.bankName + '</li>'
              previewHtml += '<li><b>A/C No </b> : ' + bank.accountNo + ' </li>'
              previewHtml += '<li><b>Branch Name </b> : ' + bank.branchName + ' </li>'
              previewHtml += '<li><b> Account Type </b> : ' + bank.accountType + ' </li>'
              previewHtml += '<li><b>IFSC Code </b> : ' + bank.ifscCode + ' </li>'
              previewHtml += '</ul>';

              previewHtml += '<body></html>';

              console.log(previewHtml)

              /* calling API to send user data via email */
              var emailforAdvice = this.email;
              let prev = {
                "companyId": this.companyID,
                "emailId": emailforAdvice,
                "fileContent": previewHtml
              }
              this.http.post<any>(this.WEBSERVICE + '/pdf/createPreview', JSON.stringify(prev), { headers: { 'Content-Type': 'application/json' } }).subscribe(data => {
                if (data.error.errorData != 0) {
                  // this.modalRef.hide();
                  //this.onError(data.error.errorMsg);
                  //---------- this.data.alert(data.message, 'danger');
                } else {
                  //this.adviceGenarateFlag = false;
                  this.onSuccess('Data send to your email');
                  // this.modalRef.hide();
                  //---------------  this.data.alert('Data send to your email', 'success');
                }
              });
            }
          });
        }
      });
      //}

    })
    //-----//  this.modalService.dismissAll();    
  }
  privacyPolicy: any = '';
  disclosureAgreement = '';
  terms: any = '';
  handleSubmitTermsAndCondition = () => {
    if (this.privacyPolicy && this.disclosureAgreement && this.terms) {
      this.spinner.show();
      let payload = {
        'companyId': this.companyID,
        'privacyPolicyLink': 1,
        'disclosureAgreement': 1,
        'termsNConditions': 1,
      }
      this.http.post<any>(this.WEBSERVICE + '/users/selectTermsNConditions', JSON.stringify(payload), { headers: { 'Content-Type': 'application/json' } }).subscribe(data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.onSuccess("Registration Successfull");
          this.wizard.goToNextStep();
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      })
    } else {
      this.onError("Please agree with Privacy Policy,Disclosure Agreement and Terms and conditions");
    }
  }

  setMembership(id: any) {
    for (let i = 0; this.membershipList.length; i++) {
      if (this.membershipList[i].typeId == id) {
        this.membershipObj = this.membershipList[i];
        break;
      }
    }
  }

  setFile(file: File, formControl: AbstractControl) {
    formControl.markAsTouched();
    if (file != null)
      formControl.setValue(file);
    else
      formControl.setValue(null);
  }

  next() {
    this.wizard.goToNextStep();
  }

  formatDate(date: Date) {
    if (date != null)
      return date.getFullYear() + "-" + (date.getMonth() >= 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-" + (date.getDate() > 9 ? date.getDate() : "0" + date.getDate());
    else
      return "";
  }

  checkBusinessType(value: any) {
    if (value) {
      if (value.length > 1) {
        for (let x = 0; x < value.length; x++) {
          if (value[x].businessTypeId == 1) {
            this.gstForm.get('businessType').setValue(null);
            break;
          }
        }
      }
    }
  }

  onError(message: string): void {
    this.notifications.create(this.translate.instant('alert.error'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 5000, showProgressBar: true });
  }
  onSuccess(message: string): void {
    this.notifications.create(this.translate.instant('alert.success'),
      this.translate.instant(message),
      NotificationType.Success, { timeOut: 5000, showProgressBar: true });
  }
}
