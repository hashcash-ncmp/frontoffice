import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { UserComponent } from './user.component';
import { UserRoutingModule } from './user.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ComponentsStateButtonModule } from 'src/app/components/state-button/components.state-button.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { RegfirstpageComponent } from './regfirstpage/regfirstpage.component';
import { RegotpComponent } from './regotp/regotp.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AccountModule } from '../app/account/account.module';
import { NgxSpinnerModule } from "ngx-spinner";
import { KycComponent } from './kyc/kyc.component';
import { ArchwizardModule } from 'angular-archwizard';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [LoginComponent, RegisterComponent, ForgotPasswordComponent, UserComponent, ResetPasswordComponent, RegfirstpageComponent, RegotpComponent, KycComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    SharedModule,
    SimpleNotificationsModule.forRoot(),
    ComponentsStateButtonModule,
    NgMultiSelectDropDownModule.forRoot(),AccountModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    ArchwizardModule,
    BsDatepickerModule.forRoot(),
    NgSelectModule
  ],
 
  providers: [BsModalService ]
})
export class UserModule { }
