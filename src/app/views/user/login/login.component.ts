import { Component, ViewChild, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';
import { environment } from 'src/environments/environment';



import { ApiService } from 'src/app/data/api.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  @ViewChild('loginForm') loginForm: NgForm;
  public emailModel;// = 'demo@vien.com';
  public passwordModel;// = 'demovien1122';
  public businessType: string = '';
  public tradingCode;
  public password;
  public buttonDisabled = false;
  public buttonState = '';
  public allbusinessType = []
  public cityList = [];
  constructor(private _AService: ApiService, private authService: AuthService, private notifications: NotificationsService, private router: Router) { }

  ngOnInit() {
    this.UpdateMargin();
    this.getbusinesstype();
  }
  onSubmit(): void {
    debugger;
    //  this.router.navigate([environment.adminRoot]);
    if (!this.loginForm.valid || this.buttonDisabled) {
      return;
    }
    this.buttonDisabled = true;
    this.buttonState = 'show-spinner';
    let logdatavalue = {};
    logdatavalue['tradingCode'] = this.emailModel;
    logdatavalue['password'] = this.passwordModel;
    logdatavalue['businessTypeId'] = this.businessType;
    
    this._AService.Logindata(logdatavalue)
      .subscribe(data => {
        console.log(data);
        this.buttonState = '';
        if (data.error.errorData == 0) {
          var companydtl = data.companyDetails;
          console.log('-----llll', companydtl);
          //alert(companydtl.companyId);
         
          localStorage.setItem('CompanyID', companydtl.companyId);

          if (companydtl.companyId != '') {
            let getcompdata = {};
            getcompdata['companyId'] = companydtl.companyId;
            getcompdata['businessTypeId'] = companydtl.businessTypeId;
            this._AService.GetcompanyDetails(getcompdata)
              .subscribe(data => {
                if (data.error.errorData == 0) {
                  var companyInfodtl = data.companyDetails;
                  var membershipDtl=data.companyMembershipDetails;
                 // var banchlist = data.companyBranchList;
                  // for (var i = 0; i < banchlist.length; i++) {
                  //   this.cityList.push({'cityId': banchlist[i].cityId, 'cityName':banchlist[i].cityName})
                  // }
                  // //console.log('===',this.cityList);
                  // var citylist=JSON.stringify(this.cityList)
                  // localStorage.setItem('CityName',citylist);
                  localStorage.setItem('BusinessTypeId', companyInfodtl.businessTypeId);
                  localStorage.setItem('contactNo', companyInfodtl.contactNo);
                  localStorage.setItem('address', companyInfodtl.regAddressLineOne+' '+companyInfodtl.regAddressLineOne+' '+companyInfodtl.districtName+' '+companyInfodtl.stateName+' '+companyInfodtl.countryName);
                  localStorage.setItem('companyname', companyInfodtl.companyName);
                  localStorage.setItem('email', companyInfodtl.email);
                  localStorage.setItem('panNo', companyInfodtl.panNo);
                  localStorage.setItem('businessTypeName',companyInfodtl.businessTypeName);
                  this.notifications.create('Done', 'Login successful',
                    NotificationType.Bare, { theClass: 'outline primary', timeOut: 6000, showProgressBar: false });
                  this.buttonDisabled = false;
                  var processStep=companydtl.firstLoginFlag;
                  // alert(processStep);
                  debugger;
                  localStorage.setItem('membershipstatus',membershipDtl.status)
                  if(membershipDtl.status==1){
                    if(processStep==0){
                      this.router.navigate([environment.adminRoot+'/'+'account/sessionmngpref']);
                    }
                    else if(processStep==1){
                    
                      this.router.navigate([environment.adminRoot+'/'+'account/productpref']);
                    }
                    else if(processStep==2){
                      this.router.navigate([environment.adminRoot+'/'+'tradefloor/Dashboard']);
                      //this.router.navigate([environment.adminRoot]);
                    }
                  }
                  else{
                    this.router.navigate([environment.adminRoot+'/'+'payments/pendingpayment']);
                  }
                  
                 
                }
              })
          }


        }
        else {
          this.buttonDisabled = false;
          this.buttonState = '';
          this.notifications.create('Error', data.error.errorMsg,
            NotificationType.Bare, { theClass: 'outline primary', timeOut: 6000, showProgressBar: false });

        }

      });

    // .catch((error) => {
    //   this.buttonDisabled = false;
    //   this.buttonState = '';
    //   this.notifications.create('Error', error.message,
    //     NotificationType.Bare, { theClass: 'outline primary', timeOut: 6000, showProgressBar: false });
    // });


    // this.authService.signIn(this.loginForm.value).then(user => {
    //   this.router.navigate([environment.adminRoot]);
    // }).catch((error) => {
    //   this.buttonDisabled = false;
    //   this.buttonState = '';
    //   this.notifications.create('Error', error.message,
    //     NotificationType.Bare, { theClass: 'outline primary', timeOut: 6000, showProgressBar: false });
    // });
  }
  

  getbusinesstype() {
    this._AService.getBusinesstype()
      .subscribe(data => {
        this.allbusinessType = data;

      })
  }
  UpdateMargin(){
    let marginInput={};
    var companyID = localStorage.getItem('CompanyID');
    marginInput['companyId']=companyID;
   
    this._AService.UpdateMarginFlag(marginInput)
      .subscribe(data => {
       console.log('---------',data);
       

      })
  }
}
