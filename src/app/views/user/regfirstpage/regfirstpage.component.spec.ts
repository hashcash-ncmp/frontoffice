import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegfirstpageComponent } from './regfirstpage.component';

describe('RegfirstpageComponent', () => {
  let component: RegfirstpageComponent;
  let fixture: ComponentFixture<RegfirstpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegfirstpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegfirstpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
