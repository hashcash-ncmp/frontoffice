import { Component, OnInit } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiService } from 'src/app/data/api.service';

import { environment } from 'src/environments/environment';
import * as $ from 'jquery';
import {
  Router
} from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NotificationsService, NotificationType } from "angular2-notifications";
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-regfirstpage',
  templateUrl: './regfirstpage.component.html',
  styleUrls: ['./regfirstpage.component.scss']
})
export class RegfirstpageComponent implements OnInit {
  // public companyName = "";
  public CompanyEmail = "";
  public fullname: any = "";
  public phone: any = "";
  clientId: string = '';
  public errormessage: any;
  constructor(private _Service: ApiService, private http: HttpClient, private route: Router,
    private notifications: NotificationsService, private translate: TranslateService,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
  }
  goTootpPage() {
    debugger;
    this.errormessage = "";
    var userobj = {};
    userobj['name'] = this.fullname;
    userobj['tempPhNo'] = this.phone;
    userobj['email'] = this.CompanyEmail;
    if (this.fullname != "" && this.phone != "" && this.CompanyEmail != "") {
      this._Service.GetOtp(userobj)
        .subscribe(response => {
          debugger;
          var result = response;
          this.clientId = result.userResult.clientId;
          console.log(this.clientId);
          if (result.error.errorData == 0) {
            var value = result.userResult;
            localStorage.setItem('tmpMobileno', this.phone);
            localStorage.setItem('otpClintId', this.clientId);
            localStorage.setItem('tmpOtp', value.phNoOtp);
            localStorage.setItem('inregEmail', this.CompanyEmail);
            this.route.navigateByUrl('/regotp');
            //  this.route.navigate([environment.adminRoot+'/'+'account/sessionmngpref']);
          }
          else {
            this.errormessage = result.error.errorMsg;
          }
        })
    }
    else {
      this.errormessage = " Mandatory field missing.";
    }


  }
  ClearError() {
    this.errormessage = "";
  }

  form = new FormGroup({
    name: new FormControl(null, Validators.compose([Validators.required, Validators.pattern("^[a-zA-Z ]{2,30}$")])),
    email: new FormControl(null, Validators.compose([Validators.required, Validators.email])),
    tempPhNo: new FormControl(null, Validators.compose([Validators.required, Validators.pattern("[0-9]{10,10}")]))
  });

  onSubmit(form: FormGroup) {
    if (form.valid) {
      this.spinner.show();
      this._Service.GetOtp(form.value).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            var value = data.userResult;
            localStorage.setItem('tmpMobileno', form.value.tempPhNo);
            localStorage.setItem('otpClintId', value.clientId);
            localStorage.setItem('inregEmail', form.value.email);
            localStorage.setItem('name', form.value.name);
            form.reset();
            this.route.navigateByUrl('/regotp');
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }
    else {
      this.onError("Enter Valid Information!");
    }
  }
  onError(message: string): void {
    this.notifications.create(this.translate.instant('alert.error'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 3000, showProgressBar: true });
  }
}
