import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/data/api.service';

import { environment } from 'src/environments/environment';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NotificationsService, NotificationType } from "angular2-notifications";
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from "ngx-spinner";
import { NUMBER_REGEX } from 'src/main';
@Component({
  selector: 'app-regotp',
  templateUrl: './regotp.component.html',
  styleUrls: ['./regotp.component.scss']
})
export class RegotpComponent implements OnInit {
  public SentOtp: any = "";
  public tempMobile: any = "";
  public errormessage: any = "";
  public companyId: any = "";
  clientId: string = '';
  constructor(private _service: ApiService, private http: HttpClient, private route: Router,
    private notifications: NotificationsService, private translate: TranslateService,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.tempMobile = localStorage.getItem('tmpMobileno');
    var OTP = localStorage.getItem('tmpOtp');
    // alert('Your OTP is  ' + OTP);
    this.clientId = localStorage.getItem('otpClintId');
  }
  verifyOtp() {
    debugger;
    var otpobj = {};
    otpobj['clientId'] = this.clientId;
    otpobj['phNoOtp'] = this.SentOtp;

    if (this.clientId != "" && this.SentOtp != "") {
      this.spinner.show();
      this._service.OTPverify(otpobj)
        .subscribe(response => {
          this.spinner.hide();
          debugger;
          var result = response;
          if (result.flag == 1) {
            var value = result.userResult;
            // var userId=value.userId;
            localStorage.setItem('companyId', value.companyId);
            if (value.regProcessStatus == 1) {
              alert('You are alreay registered,Please login');
              // this.route.navigateByUrl('/');
              window.location.href = "http://52.8.99.117/login";
            }
            else {

              localStorage.setItem('processStep', value.processStep);
              localStorage.setItem('associationflag', value.associationFlag);
              localStorage.setItem('regType', value.companyRegTypeId);

              this.route.navigateByUrl('/register');
            }

          }
          else {
            this.errormessage = 'Invalid OTP Provided.';
          }

        }, error => {
          this.spinner.hide();
        })
    }
    else {
      this.errormessage = " Mandatory field missing.";
    }

  }

  form = new FormGroup({
    clientId: new FormControl(this.clientId),
    phNoOtp: new FormControl(null, Validators.compose([Validators.required, Validators.pattern(NUMBER_REGEX)]))
  });

  verifyOTP(form: FormGroup) {
    if (form.valid) {
      form.value.clientId = localStorage.getItem('otpClintId');
      this.spinner.show();
      this._service.OTPverify(form.value).subscribe(
        data => {
          this.spinner.hide();
          form.reset();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            localStorage.setItem('companyId', data.userResult.companyId);
            if (data.userResult.regProcessStatus == 1) {
              this.onError("Already Registered!");
              setTimeout(() => {
                this.route.navigateByUrl('/login');
              }, 3000);
            }
            else {
              localStorage.setItem('processStep', data.userResult.processStep);
              localStorage.setItem('associationflag', data.userResult.associationFlag);
              localStorage.setItem('regType', data.userResult.companyRegTypeId);
              this.route.navigateByUrl('/register');
            }
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }
    else {
      this.onError("Please Enter Valid OTP!");
    }
  }

  resendOtp() {
    let name = localStorage.getItem("name");
    let email = localStorage.getItem("inregEmail");
    let tempPhNO = localStorage.getItem("tmpMobileno");
    if (name && email && tempPhNO) {
      this.spinner.show();
      this._service.GetOtp({ name: name, email: email, tempPhNo: tempPhNO }).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.onSuccess("OTP sent to " + tempPhNO);
            var value = data.userResult;
            localStorage.setItem('tmpMobileno', tempPhNO);
            localStorage.setItem('otpClintId', value.clientId);
            localStorage.setItem('inregEmail', email);
            localStorage.setItem('name', name);
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }
    else {
      this.onError("Phone No Not Found,Please Enter Details!");
      setTimeout(() => {
        this.route.navigateByUrl('/regfirst');
      }, 3000);
    }
  }
  onError(message: string): void {
    this.notifications.create(this.translate.instant('alert.error'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 3000, showProgressBar: true });
  }
  onSuccess(message: string): void {
    this.notifications.create(this.translate.instant('alert.success'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 3000, showProgressBar: true });
  }
}
