import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegotpComponent } from './regotp.component';

describe('RegotpComponent', () => {
  let component: RegotpComponent;
  let fixture: ComponentFixture<RegotpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegotpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegotpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
