import { Component, ViewChild, OnInit, TemplateRef } from '@angular/core';
import { ApiService } from 'src/app/data/api.service';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
@Component({
  selector: 'app-blank-page',
  templateUrl: './blank-page.component.html'
})
export class BlankPageComponent {
  modalRef: BsModalRef;
  public Password;
  public CPassword;
  public otp: any;
  public passwordMatch: boolean;
  constructor(  private router: Router,public _ApiService: ApiService, private notifications: NotificationsService, private modalService: BsModalService) { }
  confirmPassword(e) {
    if (
      (this.Password != '' && this.Password != undefined) &&
      (this.CPassword != '' && this.CPassword != undefined)
    ) {
      if (this.Password == this.CPassword) {
      
        this.passwordMatch = true;
    
      } else {
        // $('.confirm_password_text').html('Password  Mismatched');
        // $('.confirm_password_text').css('color', 'red');
        this.passwordMatch = false;
        // $('#submit_btn').attr('disabled', true);
      }
    } else {

    }

  }
  SavePassword() {
    var companyID = localStorage.getItem('CompanyID');
    let checkOTPdata = {};
    var companyID = localStorage.getItem('CompanyID');
    checkOTPdata['companyId'] = companyID;
    checkOTPdata['phNoOtp'] = this.otp;
    if (this.otp != null) {
      this._ApiService.OtpCheck(checkOTPdata)
        .subscribe(data => {
          if (data.error.errorData == 0) {
            var pform = {};
            var companyID = localStorage.getItem('CompanyID');
            pform['companyId'] = companyID
            pform['newPassword'] = this.Password;
            this._ApiService.SavePassword(pform)
              .subscribe(data => {
                if (data.error.errorData == 0) {
                  this.notifications.create(
                    'Done',
                    'Password updated Successfully!',
                    NotificationType.Bare,
                    { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
                  );
                  this.modalRef.hide();
                  this.router.navigate(['user/login']);
                }
                else{
                  this.notifications.create(
                    'Error',
                    data.error.errorMsg,
                    NotificationType.Bare,
                    { theClass: 'outline primary', timeOut: 6000, showProgressBar: false }
                  );
                  this.modalRef.hide();
                }
                this.CPassword = "";
                this.Password = "";
                this.passwordMatch = false;
              })



          }
        })
    }
  }



  showOtpModal(template: TemplateRef<any>) {
     this.modalRef = this.modalService.show(
      template, Object.assign({}, { class: 'gray modal-md' })
    );
    let otpdata = {};
    var companyID = localStorage.getItem('CompanyID');
      otpdata['companyId'] = companyID;
    this._ApiService.Otpgenarate(otpdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {

          var result = data.userResult;
        var OTP = result.phNoOtp;
          //   alert('Ckeck OTP on your registered mobile no!');
          alert('Your OTP is ' + OTP);
          this.notifications.create(
            'Done',
            'Ckeck OTP on your registered mobile no!',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );

        }
        console.log();


      })
  }
}
