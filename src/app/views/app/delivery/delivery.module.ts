import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxSpinnerModule } from "ngx-spinner";
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { DeliveryComponent } from './delivery.component'
import { AdddeliveryComponent } from './adddelivery/adddelivery.component'
import { ViewdeliveryComponent } from './viewdelivery/viewdelivery.component'
import { DeliveryRoutingModule } from './delivery-routing.module'
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';

@NgModule({
  declarations: [DeliveryComponent, AdddeliveryComponent, ViewdeliveryComponent],
  imports: [
    CommonModule,
    DeliveryRoutingModule,
    SharedModule,
    LayoutContainersModule,
    FormsModule,
    ReactiveFormsModule,
    SimpleNotificationsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgxSpinnerModule,
    PaginationModule,
    PaginationModule.forRoot(),
    TypeaheadModule.forRoot(),
  ],
  providers: [BsModalService,DatePipe],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DeliveryModule { }
