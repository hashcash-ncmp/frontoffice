import { Component, OnInit, TemplateRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/data/api.service';
import { noop, Observable, Observer, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { RANGES } from 'src/main';

@Component({
  selector: 'app-adddelivery',
  templateUrl: './adddelivery.component.html',
  styleUrls: ['./adddelivery.component.scss']
})
export class AdddeliveryComponent implements OnInit {
  ranges: any = [];
  bsValue = new Date();
  bsRangeValue: Date[];
  //stateId: Array<any> = [];
  tncId: string = '';
  stateList: Array<any> = [];
  modalRef: BsModalRef = null;
  companyId: any = null;
  tcnList: Array<any> = [];
  userType: string = '';
  deliveryDtl: any = {};
  deliveryList: Array<any> = [];
  deliveryRequest: Array<any> = [];
  shipmentList: Array<any> = [];
  billingAddress: Array<any> = [];
  billingAddressId: any = '';
  shippingAddressId: any = '';
  authorizedPersonInfo: any = '';
  idCardNo: any = '';
  idCardType: any = '';
  contactNo: any = '';

  stateId: any = 0;
  productId: number = 0;
  subProductId: number = 0;
  seasonId: number = 0;
  productList: Array<any> = [];
  subProductList: Array<any> = [];
  seasonList: Array<any> = [];
  buyerCompanyId: any = 0;
  sellerCompanyId: any = 0;
  showDeliveryButton: boolean = true;


  constructor(private http: HttpClient, private spinner: NgxSpinnerService, private service: ApiService,
    private notifications: NotificationsService, private translate: TranslateService,
    private modalService: BsModalService) {
    //this.bsRangeValue = [this.bsValue, this.bsValue];
    this.ranges = RANGES;
  }

  ngOnInit(): void {
    this.userType = localStorage.getItem('BusinessTypeId');
    this.companyId = localStorage.getItem('CompanyID');
    this.GetAllProducts();
    this.getStateList();
    this.getshipmentlist();
    this.companyInfodata();
    this.getAllCompanyByComId();
  }

  openModal(template: TemplateRef<any>) {
    if (this.deliveryList[0] != null ? this.deliveryList[0].status == 1 : false)
      this.checkBalanceforQty();
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'modal-xl' });
  }

  formatDate(date: Date) {
    return date.getFullYear() + "-" + (date.getMonth() >= 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-" + (date.getDate() > 9 ? date.getDate() : "0" + date.getDate());
  }

  getAllCompanyByComId() {
    this.buyerCompanyId = 0;
    this.sellerCompanyId = 0;
    const url = this.service.WEBSERVICE + '/selection/getAllCompanyByComId';
    this.suggestions$ = new Observable((observer: Observer<string>) => {
      observer.next(this.search);
    }).pipe(
      switchMap((query: string) => {
        if (query) {
          this.spinner.show();
          return this.http.post<DataSourceEnvelop>(
            url, { searchKeyword: query, companyId: this.companyId, flag: "buyer" }).pipe(
              map((data: DataSourceEnvelop) => { this.spinner.hide(); return data && data.allCompany || []; }),
              tap(() => noop, err => {
                this.spinner.hide();
                this.errorMessage = err && err.message || 'Something goes wrong';
              })
            );
        }
        return of([]);
      })
    );
  }

  getStateList() {
    this.spinner.show();
    this.service.getStateList({}).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.stateList = data.stateList != null ? data.stateList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  GetAllProducts() {
    this.deliveryDtl = {};
    this.deliveryList = [];
    this.spinner.show();
    this.service.GetAllProducts({ companyId: this.companyId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.productList = data.productList != null ? data.productList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  GetAllSubProducts(productId: number) {
    this.deliveryDtl = {};
    this.seasonId = 0;
    this.deliveryList = [];
    this.subProductId = 0;
    this.subProductList = [];
    this.spinner.show();
    this.service.GetAllSubProducts({ companyId: this.companyId, productId: productId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.subProductList = data.subProductList != null ? data.subProductList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getSeason() {
    this.deliveryDtl = {};
    this.seasonId = 0;
    this.deliveryList = [];
    this.seasonId = 0;
    this.spinner.show();
    this.service.getSeason({ productId: this.productId, subProductId: this.subProductId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.seasonList = data.seasonList != null ? data.seasonList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getTcnList() {
    this.deliveryDtl = {};
    this.deliveryList = [];
    this.tncId = '';
    this.tcnList = [];
    if (this.bsRangeValue[0] != null && this.bsRangeValue[1] != null && this.productId != 0 && this.subProductId != 0 && this.seasonId != 0 && this.stateId != 0 && this.buyerCompanyId > 0 && this.sellerCompanyId > 0) {
      this.spinner.show();
      this.service.getTcnList({ flag: "buyer", sellerCompanyId: this.sellerCompanyId, buyerCompanyId: this.companyId, toDate: this.formatDate(this.bsRangeValue[1]), productId: this.productId, subProductId: this.subProductId, fromDate: this.formatDate(this.bsRangeValue[0]), companyId: this.companyId, seasonId: this.seasonId, stateId: this.stateId }).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.tcnList = data.tcnList != null ? data.tcnList : [];
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }
  }

  getViewByTcn(tcnNo: string) {
    this.showDeliveryButton = true;
    this.deliveryRequest = [];
    if (this.tncId != null && this.tncId.length > 0 && this.bsRangeValue[0] != null && this.bsRangeValue[1] != null && this.productId != 0 && this.subProductId != 0 && this.seasonId != 0 && this.stateId != 0) {
      this.spinner.show();
      this.service.getViewByTcn({ tcnNo: tcnNo, pageNo: 1, noOfItemsPerPage: 1000, toDate: this.formatDate(this.bsRangeValue[1]), productId: this.productId, subProductId: this.subProductId, fromDate: this.formatDate(this.bsRangeValue[0]), companyId: this.companyId, seasonId: this.seasonId, stateId: this.stateId }).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.deliveryDtl = {};
            this.deliveryList = [];
            this.onError(data['error'].errorMsg);
          } else {
            this.deliveryDtl = data.deliveryValue != null ? data.deliveryValue : {};
            this.deliveryList = data.deliveryViewDetailsList != null ? data.deliveryViewDetailsList : [];
            this.deliveryList.forEach(object => {
              if (object.status == 0) {
                this.deliveryRequest.push({
                  allotmentDtlId: object.allotmentDtlId,
                  regionId: object.regionId,
                  zoneId: object.zoneId,
                  stationId: object.stationId,
                  quantity: object.quantity,
                  unit: object.unitId,
                  lotNo: object.lotNo,
                  partyLotNo: object.partyLotNo,
                  storageLocation: object.storageLocation,
                  allotmentDate: this.deliveryDtl.allotmentDate != null ? (this.deliveryDtl.allotmentDate.split(' ')[0] != null ? this.deliveryDtl.allotmentDate.split(' ')[0] : this.deliveryDtl.allotmentDate) : this.deliveryDtl.allotmentDate,
                  authorizedPersonInfo: this.authorizedPersonInfo,
                  idCardNo: this.idCardNo,
                  idCardType: this.idCardType,
                  contactNo: this.contactNo,
                  prNo: object.pressRunningNo
                });
              }
            });
          }
        }, error => {
          this.spinner.hide();
          this.deliveryDtl = {};
          this.deliveryList = [];
          this.onError(error.message);
          console.log(error)
        }
      );
    }
  }

  getshipmentlist() {
    this.shippingAddressId = '';
    this.shipmentList = [];
    this.spinner.show();
    this.service.getshipmentlist(this.companyId).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.shipmentList = data.shipmentLocationList != null ? data.shipmentLocationList : [];
          this.shippingAddressId = this.shipmentList[0] != null ? (this.shipmentList[0].shipmentId != null ? this.shipmentList[0].shipmentId : '') : '';
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  companyInfodata() {
    this.billingAddressId = '';
    this.billingAddress = [];
    let param = new FormData();
    param.append('companyId', this.companyId);
    this.spinner.show();
    this.service.getcompanylist(this.companyId).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.billingAddress = data.companyBranchList != null ? data.companyBranchList : [];
          this.billingAddressId = this.billingAddress[0] != null ? (this.billingAddress[0].comBranchId != null ? this.billingAddress[0].comBranchId : '') : '';
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  checkBalanceforQty() {
    this.showDeliveryButton = false;
    let qtySum = 0;
    this.deliveryRequest.forEach(obj => {
      qtySum = qtySum + obj.quantity;
    });
    let param = {};
    param['tcnNo'] = this.tncId;
    param['quantity'] = qtySum;
    this.spinner.show();
    this.service.checkBalanceforQty(param).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.showDeliveryButton = true;
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  addRemoveObject(isChecked: any, object: any) {
    if (isChecked && object.status == 1) {
      this.deliveryRequest.push({
        allotmentDtlId: object.allotmentDtlId,
        regionId: object.regionId,
        zoneId: object.zoneId,
        stationId: object.stationId,
        quantity: object.quantity,
        unit: object.unitId,
        lotNo: object.lotNo,
        partyLotNo: object.partyLotNo,
        storageLocation: object.storageLocation,
        allotmentDate: object.allotmentDate != null ? (object.allotmentDate.split(' ')[0] != null ? object.allotmentDate.split(' ')[0] : object.allotmentDate) : object.allotmentDate,
        authorizedPersonInfo: this.authorizedPersonInfo,
        idCardNo: this.idCardNo,
        idCardType: this.idCardType,
        contactNo: this.contactNo,
        prNo: object.pressRunningNo
      });
    } else if (!isChecked) {
      this.deliveryRequest.splice(this.deliveryRequest.findIndex(function (i) {
        return i.allotmentDtlId == object.allotmentDtlId;
      }), 1);
    }
  }

  setValue() {
    this.deliveryRequest.forEach(element => {
      element.authorizedPersonInfo = this.authorizedPersonInfo;
      element.idCardNo = this.idCardNo;
      element.idCardType = this.idCardType;
      element.contactNo = this.contactNo;
    });
  }

  addDeliveryAddress() {
    if (this.billingAddressId > 0 && this.shippingAddressId > 0 && this.tncId != null && this.tncId.length > 0 &&
      this.deliveryRequest.length > 0 && this.authorizedPersonInfo != null && this.authorizedPersonInfo.length > 0 &&
      this.idCardNo != null && this.idCardNo.length > 0 && this.idCardType != null && this.idCardType.length > 0 &&
      this.contactNo != null && this.contactNo.length > 0 && this.deliveryDtl.productId > 0 && this.deliveryDtl.subProductId > 0 && this.seasonId > 0 && this.stateId > 0) {
      let param = {
        billingAddressId: this.billingAddressId,
        shippingAddressId: this.shippingAddressId,
        tcnNo: this.tncId,
        companyId: this.companyId,
        deliveryRequest: this.deliveryRequest,
        productId: this.deliveryDtl.productId,
        subProductId: this.deliveryDtl.subProductId,
        seasonId: this.seasonId,
        stateId: this.stateId
      };
      this.spinner.show();
      this.service.addDeliveryAddress(param).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            //this.onSuccess(data['error'].errorMsg);
            this.mailSellerBuyer();
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    } else {
      this.onError('Please Enter Valid Info!');
    }
  }

  mailSellerBuyer() {
    let html = '', allotmentIds = '';
    html += '<table>'
    html += '<tbody>'
    html += '<tr>'
    html += '<td><b>Buyer Company Name&nbsp;:&nbsp;</b>' + this.deliveryDtl.buyerCompanyName + '</td>'
    html += '<td><b>Product Name &nbsp;:&nbsp;</b>' + this.deliveryDtl.productName + '</td>'
    html += '<td><b>Sub Product Name&nbsp;:&nbsp;</b>' + this.deliveryDtl.subProductName + '</td>'
    html += '<td><b>Season Year&nbsp;:&nbsp;</b>' + this.deliveryDtl.seasonYear + '</td>'
    html += '</tr>'
    html += '<tr>'
    html += '<td><b>Seller Company Name&nbsp;:&nbsp;</b>' + this.deliveryDtl.sellerCompanyName + '</td>'
    html += '<td><b>AllotmentNo&nbsp;:&nbsp;</b>' + this.deliveryDtl.allotmentNo + '</td>'
    html += '<td><b>Trade Timestamp&nbsp;:&nbsp;</b>' + (this.deliveryDtl.tradeTimestamp)
    html += '</td>'
    html += '<td><b>TCN&nbsp;:&nbsp;</b>' + this.deliveryDtl.tcn + '</td>'
    html += '</tr>'
    html += '<tr>'
    html += '<td colspan="2">'
    html += '<b>Billing Address&nbsp;:&nbsp;</b>' + this.getSelectedBillingAddress().addressLineOne + ',' + this.getSelectedBillingAddress().addressLineTwo + ',' + this.getSelectedBillingAddress().cityName + ',' + this.getSelectedBillingAddress().districtName + ',' + this.getSelectedBillingAddress().pinCode + ',' + this.getSelectedBillingAddress().stateName + ',' + this.getSelectedBillingAddress().countryName
    html += '</td>'
    html += '<td colspan="2">'
    html += '<b>Shipping Address&nbsp;:&nbsp;</b>' + this.getSelectedShippingAddress().addressLineOne + ',' + this.getSelectedShippingAddress().addressLineTwo + ',' + this.getSelectedShippingAddress().cityName + ',' + this.getSelectedShippingAddress().districtName + ',' + this.getSelectedShippingAddress().pinCode + ',' + this.getSelectedShippingAddress().stateName + ',' + this.getSelectedShippingAddress().countryName
    html += '</td>'
    html += '</tr>'
    html += '<tr>'
    html += '<td>'
    html += '<b>Authorized Person Info&nbsp;:&nbsp;</b>' + this.authorizedPersonInfo
    html += '</td>'
    html += '<td>'
    html += '<b>ID Card No&nbsp;:&nbsp;</b>' + this.idCardNo
    html += '</td>'
    html += '<td>'
    html += '<b>ID Card Type&nbsp;:&nbsp;</b>' + this.idCardType
    html += '</td>'
    html += '<td>'
    html += '<b>Contact No&nbsp;:&nbsp;</b>' + this.contactNo
    html += '</td>'
    html += '</tr>'
    html += '</tbody>'
    html += '</table>'
    html += '<table>'
    html += '<thead>'
    html += '<tr>'
    html += '<th>Location</th>'
    html += '<th>Quantity</th>'
    html += '<th>Unit</th>'
    html += '<th>Lot No</th>'
    html += '<th>Party Lot No</th>'
    html += '<th>Storage Location</th>'
    html += '<th>Press Running No</th>'
    html += '</tr>'
    html += '</thead>'
    html += '<tbody>'
    for (let i = 0; i < this.deliveryRequest.length; i++) {
      html += '<tr>'
      html += '<td>' + this.getSelectedDelivery(this.deliveryRequest[i].allotmentDtlId).locationAddress + '</td>'
      html += '<td>' + this.getSelectedDelivery(this.deliveryRequest[i].allotmentDtlId).quantity + '</td>'
      html += '<td>' + this.getSelectedDelivery(this.deliveryRequest[i].allotmentDtlId).unitName + '</td>'
      html += '<td>' + this.getSelectedDelivery(this.deliveryRequest[i].allotmentDtlId).lotNo + '</td>'
      html += '<td>' + this.getSelectedDelivery(this.deliveryRequest[i].allotmentDtlId).partyLotNo + '</td>'
      html += '<td>' + this.getSelectedDelivery(this.deliveryRequest[i].allotmentDtlId).storageLocation + '</td>'
      html += '<td>' + this.getSelectedDelivery(this.deliveryRequest[i].allotmentDtlId).pressRunningNo + '</td>'
      html += '</tr>'
      allotmentIds += this.deliveryRequest[i].allotmentDtlId + ',';
    }
    html += '</tbody>'
    html += '</table>'
    if (allotmentIds.length > 0)
      allotmentIds = allotmentIds.substring(0, allotmentIds.length - 1);
    console.log(html);
    this.spinner.show();
    this.service.mailSellerBuyer({ fileContent: html, tcnNo: this.tncId, companyId: this.companyId, allotmentDtlIds: allotmentIds }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.onSuccess('Success');
          this.modalRef.hide();
          this.deliveryDtl = {};
          this.deliveryList = [];
          this.billingAddressId = '';
          this.shippingAddressId = '';
          this.authorizedPersonInfo = '';
          this.tncId = '';
          this.idCardNo = '';
          this.idCardType = '';
          this.contactNo = '';
          this.bsRangeValue = [this.bsValue, this.bsValue];
          this.productId = 0;
          this.subProductId = 0;
          this.subProductList = [];
          this.seasonList = [];
          this.seasonId = 0;
          this.stateId = 0;
          this.tcnList = [];
          this.deliveryRequest = [];
          this.buyerCompanyId = 0;
          this.sellerCompanyId = 0;
          (document.getElementById("searchBar") as HTMLInputElement).value = '';
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getSelectedBillingAddress(): any {
    let obj = {};
    for (let i = 0; i < this.billingAddress.length; i++) {
      if (this.billingAddress[i].comBranchId == this.billingAddressId) {
        obj = this.billingAddress[i];
        break;
      }
    }
    return obj;
  }

  getSelectedShippingAddress(): any {
    let obj = {};
    for (let i = 0; i < this.shipmentList.length; i++) {
      if (this.shipmentList[i].shipmentId == this.shippingAddressId) {
        obj = this.shipmentList[i];
        break;
      }
    }
    return obj;
  }

  getSelectedDelivery(id: number): any {
    let obj = {};
    for (let i = 0; i < this.deliveryList.length; i++) {
      if (this.deliveryList[i].allotmentDtlId == id) {
        obj = this.deliveryList[i];
        break;
      }
    }
    return obj;
  }

  reset() {
    this.authorizedPersonInfo = '';
    this.idCardNo = '';
    this.idCardType = '';
    this.contactNo = '';
  }

  onError(message: string): void {
    this.notifications.create(this.translate.instant('alert.error'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 3000, showProgressBar: true });
  }

  onSuccess(message: string): void {
    this.notifications.create(this.translate.instant('alert.success'),
      this.translate.instant(message),
      NotificationType.Success, { timeOut: 3000, showProgressBar: true });
  }
  search: string;
  suggestions$: Observable<DataSourceType[]>;
  errorMessage: string;
  onSelect(event: TypeaheadMatch): void {
    this.buyerCompanyId = event.item.companyId;
    this.sellerCompanyId = event.item.companyId;
  }
}
interface DataSourceType {
  companyId: any;
  companyName: any;
}
interface DataSourceEnvelop {
  allCompany: DataSourceType[];
}
