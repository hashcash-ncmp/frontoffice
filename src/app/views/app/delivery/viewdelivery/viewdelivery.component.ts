import { Component, OnInit, TemplateRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/data/api.service';
import { noop, Observable, Observer, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { DatePipe } from '@angular/common';
import { RANGES } from 'src/main';
@Component({
  selector: 'app-viewdelivery',
  templateUrl: './viewdelivery.component.html',
  styleUrls: ['./viewdelivery.component.scss']
})
export class ViewdeliveryComponent implements OnInit {
  hashUrl=null;
  ranges: any = [];
  bsValue = new Date();
  bsRangeValue: Date[];
  //stateId: Array<any> = [];
  tncId: string = '';
  stateList: Array<any> = [];
  modalRef: BsModalRef = null;
  companyId: any = null;
  tcnList: Array<any> = [];
  userType: string = '';
  deliveryDtl: any = {};
  deliveryList: Array<any> = [];
  deliveryRequest: Array<any> = [];
  shipmentList: Array<any> = [];
  billingAddress: any = null;
  billingAddressId: any = '';
  shippingAddressId: any = '';
  authorizedPersonInfo: any = '';
  idCardNo: any = '';
  idCardType: any = '';
  contactNo: any = '';

  stateId: any = 0;
  productId: number = 0;
  subProductId: number = 0;
  seasonId: number = 0;
  productList: Array<any> = [];
  subProductList: Array<any> = [];
  seasonList: Array<any> = [];
  buyerCompanyId: any = 0;
  sellerCompanyId: any = 0;
  deliveryNoList: Array<any> = [];
  deliveryNo: any = 0;


  constructor(private datePipe: DatePipe, private http: HttpClient, private spinner: NgxSpinnerService, private service: ApiService,
    private notifications: NotificationsService, private translate: TranslateService,
    private modalService: BsModalService) {
    //this.bsRangeValue = [this.bsValue, this.bsValue];    
    this.ranges = RANGES;
    this.hashUrl=service.BLOCK_CHAIN_URL;
  }

  ngOnInit(): void {
    this.userType = localStorage.getItem('BusinessTypeId');
    this.companyId = localStorage.getItem('CompanyID');
    this.GetAllProducts();
    this.getStateList();
    this.getAllCompanyByComId();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'modal-xl' });
  }

  formatDate(date: Date) {
    return date.getFullYear() + "-" + (date.getMonth() >= 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-" + (date.getDate() > 9 ? date.getDate() : "0" + date.getDate());
  }

  getAllCompanyByComId() {
    this.buyerCompanyId = 0;
    this.sellerCompanyId = 0;
    const url = this.service.WEBSERVICE + '/selection/getAllCompanyByComId';
    this.suggestions$ = new Observable((observer: Observer<string>) => {
      observer.next(this.search);
    }).pipe(
      switchMap((query: string) => {
        if (query) {
          this.spinner.show();
          return this.http.post<DataSourceEnvelop>(
            url, { searchKeyword: query, companyId: this.companyId, flag: "seller" }).pipe(
              map((data: DataSourceEnvelop) => { this.spinner.hide(); return data && data.allCompany || []; }),
              tap(() => noop, err => {
                this.spinner.hide();
                this.errorMessage = err && err.message || 'Something goes wrong';
              })
            );
        }
        return of([]);
      })
    );
  }

  getStateList() {
    this.spinner.show();
    this.service.getStateList({}).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.stateList = data.stateList != null ? data.stateList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  GetAllProducts() {
    this.deliveryDtl = {};
    this.deliveryList = [];
    this.spinner.show();
    this.service.GetAllProducts({ companyId: this.companyId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.productList = data.productList != null ? data.productList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  GetAllSubProducts(productId: number) {
    this.deliveryDtl = {};
    this.seasonId = 0;
    this.deliveryList = [];
    this.subProductId = 0;
    this.subProductList = [];
    this.spinner.show();
    this.service.GetAllSubProducts({ companyId: this.companyId, productId: productId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.subProductList = data.subProductList != null ? data.subProductList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getSeason() {
    // this.deliveryDtl = {};
    // this.seasonId = 0;
    // this.deliveryList = [];
    // this.seasonId = 0;
    // this.spinner.show();
    // this.service.getSeason({ productId: this.productId, subProductId: this.subProductId }).subscribe(
    //   data => {
    //     this.spinner.hide();
    //     if (data['error'].errorData != '0') {
    //       this.onError(data['error'].errorMsg);
    //     } else {
    //       this.seasonList = data.seasonList != null ? data.seasonList : [];
    //     }
    //   }, error => {
    //     this.spinner.hide();
    //     this.onError(error.message);
    //     console.log(error)
    //   }
    // );
  }

  getTcnList() {
    this.deliveryDtl = {};
    this.deliveryList = [];
    this.tncId = '';
    this.tcnList = [];
    if (this.sellerCompanyId > 0 && this.buyerCompanyId > 0 && this.bsRangeValue[0] != null && this.bsRangeValue[1] != null && this.productId != 0 && this.subProductId != 0 && this.stateId != 0) {
      this.spinner.show();
      this.service.getTcnList({ flag: "seller", sellerCompanyId: this.companyId, buyerCompanyId: this.buyerCompanyId, toDate: this.formatDate(this.bsRangeValue[1]), productId: this.productId, subProductId: this.subProductId, fromDate: this.formatDate(this.bsRangeValue[0]), companyId: this.companyId, stateId: this.stateId }).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.tcnList = data.tcnList != null ? data.tcnList : [];
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }
  }

  getDeliveryNoListByTcn(tcnNo: string) {
    // this.deliveryDtl = {};
    // this.deliveryList = [];
    // this.deliveryNo = '';
    // this.deliveryNoList = [];
    // this.spinner.show();
    // this.service.getDeliveryNoListByTcn({ tcnNo: tcnNo }).subscribe(
    //   data => {
    //     this.spinner.hide();
    //     if (data['error'].errorData != '0') {
    //       this.onError(data['error'].errorMsg);
    //     } else {
    //       this.deliveryNoList = data.deliveryNoList != null ? data.deliveryNoList : [];
    //     }
    //   }, error => {
    //     this.spinner.hide();
    //     this.onError(error.message);
    //     console.log(error)
    //   }
    // );
  }

  getViewByTcn() {
    if (this.tncId != null && this.tncId.length > 0 && this.bsRangeValue[0] != null && this.bsRangeValue[1] != null && this.productId != 0 && this.subProductId != 0 && this.stateId != 0) {
      this.spinner.show();
      this.service.getApproveTcnForDelivery({ tcnNo: this.tncId, pageNo: 1, noOfItemsPerPage: 1000, toDate: this.formatDate(this.bsRangeValue[1]), productId: this.productId, subProductId: this.subProductId, fromDate: this.formatDate(this.bsRangeValue[0]), companyId: this.companyId, stateId: this.stateId }).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.deliveryDtl = {};
            this.deliveryRequest = [];
            this.onError(data['error'].errorMsg);
          } else {
            this.deliveryDtl = data.deliveryValue != null ? data.deliveryValue : {};
            this.deliveryList = data.deliveryViewDetailsList != null ? data.deliveryViewDetailsList : [];
          }
        }, error => {
          this.spinner.hide();
          this.deliveryDtl = {};
          this.deliveryRequest = [];
          this.onError(error.message);
          console.log(error)
        }
      );
    }
  }

  printPdf(index) {
    let html = '<head><style> table { font-family: arial, sans-serif; border-collapse: collapse; width: 100%; text-align:center } td, th { border: 1px solid #dddddd; text-align: left; padding: 8px; }</style></head>';
    html += '<table>'
    html += '<tbody>'
    html += '<tr><td colspan=4 style="text-align:center;v-align:center;padding-top:1px;padding-bottom:1px;"><h2>Swayam</h2></td></tr>'
    html += '<tr><td colspan=4 style="text-align:center;v-align:center;padding-top:1px;padding-bottom:1px;"><h5>Delivery Receipt</h5></td></tr>'
    html += '<tr>'
    html += '<td><b>Product Name &nbsp;:&nbsp;</b>' + this.deliveryDtl.productName + '</td>'
    html += '<td><b>Sub Product Name&nbsp;:&nbsp;</b>' + this.deliveryDtl.subProductName + '</td>'
    html += '<td><b>Buyer Company&nbsp;:&nbsp;</b>' + this.deliveryDtl.buyerCompanyName + '</td>'
    html += '<td><b>Seller Company&nbsp;:&nbsp;</b>' + this.deliveryDtl.sellerCompanyName + '</td>'
    html += '</tr>'
    html += '<tr>'
    html += '<td><b>TCN&nbsp;:&nbsp;</b>' + this.deliveryDtl.tcn + '</td>'
    html += '<td><b>Trade Timestamp&nbsp;:&nbsp;</b>' + this.datePipe.transform(this.deliveryDtl.tradeTimestamp, 'MMM d, y h:mm:ss a') + '</td>'
    html += '<td><b>Location&nbsp;:&nbsp;</b>' + this.deliveryDtl.location + '</td>'
    html += '<td><b>Allotment No&nbsp;:&nbsp;</b>' + this.deliveryDtl.allotmentNo + '</td>'
    html += '</tr>'
    html += '<tr>'
    html += '<td><b>Trade Rate&nbsp;:&nbsp;</b>' + this.deliveryDtl.trade_rate + '</td>'
    html += '<td><b>Trade Qty&nbsp;:&nbsp;</b>' + this.deliveryDtl.trade_quantity + '</td>'
    html += '<td><b>Pending Qty&nbsp;:&nbsp;</b>' + this.deliveryDtl.pendingQty + '</td>'
    html += '<td></td>'
    html += '</tr>'
    html += '</tbody>'
    html += '</table>'
    html += '<table>'
    html += '<thead>'
    html += '<tr>'
    html += '<th>Do ID</th>'
    html += '<th>Do Date</th>'
    html += '<th>Do Qty</th>'
    html += '<th>Actual Delivery</th>'
    html += '<th>Pending Delivery</th>'
    html += '<th>Expected Delivery Date</th>'
    html += '<th>Status</th>'
    html += '</tr>'
    html += '</thead>'
    html += '<tbody>'
    // for (let i = 0; i < this.deliveryList.length; i++) {
    html += '<tr>'
    html += '<td>' + this.deliveryList[index].doId + '</td>'
    html += '<td>' + this.datePipe.transform(this.deliveryList[index].doDate, 'MMM d, y h:mm:ss a') + '</td>'
    html += '<td>' + this.deliveryList[index].doQty + '</td>'
    html += '<td>' + this.deliveryList[index].actualDeliveryQty + '</td>'
    html += '<td>' + this.deliveryList[index].pendingDeliveryQty + '</td>'
    html += '<td>' + this.datePipe.transform(this.deliveryList[index].expectedDeliveryDate, 'MMM d, y h:mm:ss a') + '</td>'
    html += '<td>' + this.deliveryList[index].deliveryStatus + '</td>'
    html += '</tr>'
    // }
    html += '</tbody>'
    html += '</table>'
    this.spinner.show();
    this.service.generatePdf(html, this.deliveryList[index].doId + '_Delivery.pdf').then((data) => {
      this.spinner.hide();
      console.log(data)
    }).catch((error) => {
      this.spinner.hide();
      console.log(error)
    })
  }
  onError(message: string): void {
    this.notifications.create(this.translate.instant('alert.error'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 3000, showProgressBar: true });
  }

  onSuccess(message: string): void {
    this.notifications.create(this.translate.instant('alert.success'),
      this.translate.instant(message),
      NotificationType.Success, { timeOut: 3000, showProgressBar: true });
  }
  search: string;
  suggestions$: Observable<DataSourceType[]>;
  errorMessage: string;
  onSelect(event: TypeaheadMatch): void {
    this.buyerCompanyId = event.item.companyId;
    this.sellerCompanyId = event.item.companyId;
  }
}
interface DataSourceType {
  companyId: any;
  companyName: any;
}
interface DataSourceEnvelop {
  allCompany: DataSourceType[];
}
