import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeliveryComponent } from './delivery.component'
import { AdddeliveryComponent } from './adddelivery/adddelivery.component'
import { ViewdeliveryComponent } from './viewdelivery/viewdelivery.component'
const routes: Routes = [
  {
    path: '', component: DeliveryComponent,
    children: [
      { path: '', redirectTo: 'delivery', pathMatch: 'full' },
      { path: 'delivery', component: DeliveryComponent },
      { path: 'adddelivery', component: AdddeliveryComponent },
      { path: 'viewdelivery', component: ViewdeliveryComponent }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliveryRoutingModule { }
