import { Component, OnInit, TemplateRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/data/api.service';
import { noop, Observable, Observer, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { RANGES } from 'src/main';

@Component({
  selector: 'app-addshipment',
  templateUrl: './addshipment.component.html',
  styleUrls: ['./addshipment.component.scss']
})
export class AddshipmentComponent implements OnInit {
  ranges: any = [];
  bsValue = new Date();
  bsRangeValue: Date[];
  tncId: string = '';
  stateList: Array<any> = [];
  modalRef: BsModalRef = null;
  companyId: any = null;
  tcnList: Array<any> = [];
  stateId: any = 0;
  productId: number = 0;
  subProductId: number = 0;
  seasonId: number = 0;
  productList: Array<any> = [];
  subProductList: Array<any> = [];
  seasonList: Array<any> = [];
  deliveryNoList: Array<any> = [];
  deliveryNo: any = 0;
  shipmentValue: any = {};
  shipmentViewDetailsList: Array<any> = [];
  buyerCompanyId: any = 0;
  sellerCompanyId: any = 0;
  shipmentPackagingCode: any = null;
  shipmentPricingCode: any = null;
  gstPercent: any = 0;
  tradePrice: any = 0;
  hsnCode: any = null;
  total: any = 0;
  roundTotal: any = 0;
  isEnable: boolean = false;
  docArray: Array<number> = [];
  fileArray: Array<File> = [];
  shipmentList: Array<any> = [];
  billingAddress: any = null;
  pricingConversionRate: any = 0;
  pricingUnitCode: any = null;
  checkBoxArray: Array<any> = [];

  constructor(private http: HttpClient, private spinner: NgxSpinnerService, private service: ApiService,
    private notifications: NotificationsService, private translate: TranslateService,
    private modalService: BsModalService) {
    //this.bsRangeValue = [this.bsValue, this.bsValue];  
    this.ranges = RANGES;
  }

  ngOnInit(): void {
    this.companyId = localStorage.getItem('CompanyID');
    this.GetAllProducts();
    this.getStateList();
    this.getAllCompanyByComId();
  }

  showPreview(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'modal-xl' });
  }

  form = new FormGroup({
    companyId: new FormControl('', Validators.required),
    deliveryNo: new FormControl('', Validators.required),
    tcnNo: new FormControl('', Validators.required),
    documentId: new FormControl('', Validators.required),
    vehicleNo: new FormControl('', Validators.required),
    transportDocNo: new FormControl('', Validators.required),
    shippingAddressId: new FormControl('', Validators.required),
    billingAddressId: new FormControl('', Validators.required),
    dispatchFromId: new FormControl('', Validators.required),
    shipToId: new FormControl('', Validators.required),
    netValue: new FormControl('', Validators.required),
    hsnCode: new FormControl('', Validators.required),
    productId: new FormControl('', Validators.required),
    subProductId: new FormControl('', Validators.required),
    documentDate: new FormControl('', Validators.required),
    transportDocDate: new FormControl('', Validators.required),
    shipmentDocMasterId: new FormControl('', Validators.required),
    shipmentRequests: new FormArray([], Validators.required)
  });

  get shipmentRequests(): FormArray {
    return this.form.get('shipmentRequests') as FormArray;
  }

  addShipmentRow(id: number, bag: any) {
    this.shipmentRequests.push(new FormGroup({
      deliveryShipmentId: new FormControl(id, Validators.required),
      noOfBags: new FormControl('', Validators.required),
      shipmentPackagingCode: new FormControl(this.shipmentPackagingCode, Validators.required),
      quantity: new FormControl('', Validators.required),
      shipmentPricingCode: new FormControl(this.shipmentPricingCode, Validators.required),
      unitPrice: new FormControl(this.tradePrice, Validators.required),
      taxRate: new FormControl(this.gstPercent, Validators.required),
      taxableValue: new FormControl('', Validators.required),
      otherCharge: new FormControl(0),
      totalValue: new FormControl(''),
      roundOffTotal: new FormControl('', Validators.required),
      hsnCode: new FormControl({ value: this.hsnCode, disabled: true }),
      taxValue: new FormControl({ value: '', disabled: true })
    }));
  }

  removeShipmentRow(index: number) {
    if (index > 0) {
      this.shipmentRequests.removeAt(index);
    }
  }

  calculateValue(index: number) {
    let control = this.shipmentRequests.controls[index];
    let quantity = control.get('quantity').value;
    let otherCharge = control.get('otherCharge').value;
    control.get('taxableValue').setValue(Number(Number(quantity) * Number(this.tradePrice) * Number(this.pricingConversionRate)) || "0.00");
    control.get('taxValue').setValue(Number(
      Number(control.get('taxableValue').value) * (Number(control.get('taxRate').value) / 100)) || "0.00");
    control.get('totalValue').setValue(Number(Number(control.get('taxableValue').value) + Number(control.get('taxValue').value) + Number(otherCharge)) || "0.00");
    control.get('roundOffTotal').setValue(Math.ceil(control.get('totalValue').value) || "0.00");
    this.calaulateTotal();
  }

  calaulateTotal() {
    this.total = 0;
    this.roundTotal = 0;
    this.shipmentRequests.controls.forEach(element => {
      this.total += Number((element as AbstractControl).get('totalValue').value);
      this.roundTotal += Number((element as AbstractControl).get('roundOffTotal').value);
    });
    this.roundTotal = Math.ceil(Number(this.roundTotal));
    this.form.get('netValue').setValue(this.total);
  }

  formatDate(date: Date) {
    return date.getFullYear() + "-" + (date.getMonth() >= 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-" + (date.getDate() > 9 ? date.getDate() : "0" + date.getDate());
  }

  getAllCompanyByComId() {
    this.reset();
    this.buyerCompanyId = 0;
    this.sellerCompanyId = 0;
    const url = this.service.WEBSERVICE + '/selection/getAllCompanyByComId';
    this.suggestions$ = new Observable((observer: Observer<string>) => {
      observer.next(this.search);
    }).pipe(
      switchMap((query: string) => {
        if (query) {
          this.spinner.show();
          return this.http.post<DataSourceEnvelop>(
            url, { searchKeyword: query, companyId: this.companyId, flag: "seller" }).pipe(
              map((data: DataSourceEnvelop) => { this.spinner.hide(); return data && data.allCompany || []; }),
              tap(() => noop, err => {
                this.spinner.hide();
                this.errorMessage = err && err.message || 'Something goes wrong';
              })
            );
        }
        return of([]);
      })
    );
  }


  getTradeInfoByproductTcn() {
    this.reset();
    this.isEnable = false;
    this.spinner.show();
    this.service.getTradeInfoByproductTcn({ productId: this.productId, subProductId: this.subProductId, tcnNo: this.tncId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          let tradeInfo = data.tradeInfo != null ? data.tradeInfo : {};
          this.shipmentPackagingCode = tradeInfo.shipmentPackagingCode;
          this.shipmentPricingCode = tradeInfo.shipmentPricingCode;
          this.gstPercent = tradeInfo.gstPercent;
          this.tradePrice = tradeInfo.tradePrice;
          this.hsnCode = tradeInfo.hsnCode;
          this.pricingConversionRate = tradeInfo.pricingConversionRate;
          this.pricingUnitCode = tradeInfo.pricingUnitCode;
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getStateList() {
    this.stateId = 0;
    this.stateList = [];
    this.reset();
    this.spinner.show();
    this.service.getStateList({}).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.stateList = data.stateList != null ? data.stateList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  GetAllProducts() {
    this.productId = 0;
    this.productList = [];
    this.reset();
    //this.tradeInfo = {};
    this.spinner.show();
    this.service.GetAllProducts({ companyId: this.companyId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.productList = data.productList != null ? data.productList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  GetAllSubProducts(productId: number) {
    this.reset();
    this.seasonId = 0;
    this.subProductId = 0;
    this.subProductList = [];
    this.seasonList = [];
    this.spinner.show();
    this.service.GetAllSubProducts({ companyId: this.companyId, productId: productId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.subProductList = data.subProductList != null ? data.subProductList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getSeason() {
    this.reset();
    this.seasonId = 0;
    this.seasonList = [];
    this.spinner.show();
    this.service.getSeason({ productId: this.productId, subProductId: this.subProductId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.seasonList = data.seasonList != null ? data.seasonList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getTcnList() {
    this.reset();
    this.tncId = '';
    this.tcnList = [];
    this.deliveryNo = '0';
    this.deliveryNoList = [];
    if (this.bsRangeValue[0] != null && this.bsRangeValue[1] != null && this.productId != 0 && this.subProductId != 0 && this.seasonId != 0 && this.stateId != 0 && this.buyerCompanyId > 0 && this.sellerCompanyId > 0) {
      this.spinner.show();
      this.service.getTcnListForShipments({ flag: "seller", sellerCompanyId: this.companyId, buyerCompanyId: this.buyerCompanyId, toDate: this.formatDate(this.bsRangeValue[1]), productId: this.productId, subProductId: this.subProductId, fromDate: this.formatDate(this.bsRangeValue[0]), companyId: this.companyId, seasonId: this.seasonId, stateId: this.stateId }).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.tcnList = data.tcnList != null ? data.tcnList : [];
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }
  }

  getDeliveryNoListByTcn(tcnNo: string) {
    this.deliveryNo = '';
    this.deliveryNoList = [];
    this.reset();
    this.spinner.show();
    this.service.getDeliveryNoListByTcn({ tcnNo: tcnNo }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.deliveryNoList = data.deliveryNoList != null ? data.deliveryNoList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getDeliveryDetailsByDeliverNo(deliveryNo: string) {
    this.isEnable = false;
    this.shipmentRequests.clear();
    this.form.reset();
    this.fileArray = [];
    this.docArray = [];
    this.shipmentValue = {};
    this.shipmentViewDetailsList = [];
    this.checkBoxArray = [];

    this.spinner.show();
    this.service.getDeliveryDetailsByDeliverNo({ pageNo: 1, noOfItemsPerPage: 1000, deliveryNo: deliveryNo, companyId: this.companyId, tcnNo: this.tncId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.shipmentValue = data.shipmentValue != null ? data.shipmentValue : {};
          this.shipmentViewDetailsList = data.shipmentViewDetailsList != null ? data.shipmentViewDetailsList : [];
          if (this.shipmentPackagingCode && this.shipmentPricingCode && this.gstPercent &&
            this.tradePrice && this.hsnCode && this.shipmentValue && this.shipmentViewDetailsList &&
            this.shipmentViewDetailsList.length > 0 && this.pricingConversionRate && this.pricingUnitCode) {
            this.form.get('companyId').setValue(this.companyId);
            this.form.get('tcnNo').setValue(this.tncId);
            this.form.get('deliveryNo').setValue(this.deliveryNo);
            this.form.get('hsnCode').setValue(this.hsnCode);
            this.form.get('productId').setValue(this.productId);
            this.form.get('subProductId').setValue(this.subProductId);
            this.form.get('shippingAddressId').setValue(this.shipmentValue.shippingAddressId);
            this.form.get('billingAddressId').setValue(this.shipmentValue.billingAddressId);
            this.form.get('dispatchFromId').setValue(this.companyId);
            this.form.get('shipToId').setValue(this.buyerCompanyId);
            // this.shipmentViewDetailsList.forEach(i => {
            //   this.addShipmentRow(i.deliveryShipmentId, i.quantity);
            // });
            this.isEnable = true;
            // this.companyInfodata();
            // this.getshipmentlist();
          } else {
            this.onInfo("Delivery Details Not Found!");
          }
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  setFile(index: number, fileId: number, file: File) {
    if (file != null) {
      this.fileArray[index] = file;
      this.docArray[index] = fileId;
      let arr = [];
      this.docArray.forEach(element => {
        if (element > 0)
          arr.push(element);
      });
      this.form.get('shipmentDocMasterId').setValue(arr.toString());
    }
    else {
      this.fileArray.splice(index, 1);
      this.docArray.splice(index, 1);
      let arr = [];
      this.docArray.forEach(element => {
        if (element > 0)
          arr.push(element);
      });
      this.form.get('shipmentDocMasterId').setValue(arr.toString());
    }
  }

  addShipmentInformation() {
    //console.log(JSON.stringify(this.form.value));
    if (this.form.valid && this.fileArray[0]) {
      this.form.get('transportDocDate').setValue(this.formatDate(new Date(this.form.get('transportDocDate').value)));
      this.form.get('documentDate').setValue(this.formatDate(new Date(this.form.get('documentDate').value)));
      let formData = new FormData();
      this.fileArray.forEach(i => {
        if (i != null)
          formData.append("files", i);
      });
      formData.append("data", JSON.stringify(this.form.value));
      this.spinner.show();
      this.service.addShipmentInformation(formData).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.modalRef.hide();
            this.onSuccess(data['error'].errorMsg);
            this.reset();
            this.bsValue = new Date();
            this.productId = 0;
            this.subProductId = 0;
            this.subProductList = [];
            this.seasonId = 0;
            this.seasonList = [];
            this.stateId = 0;
            this.tncId = '';
            this.tcnList = [];
            this.deliveryNo = 0;
            this.deliveryNoList = [];
            (document.getElementById("searchBar") as HTMLInputElement).value = '';
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }
  }

  companyInfodata() {
    this.billingAddress = null;
    let param = new FormData();
    param.append('companyId', this.companyId);
    this.spinner.show();
    this.service.companyInfodata(param).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.billingAddress = data;
          //this.form.get('billingAddressId').setValue(this.billingAddress != null ? (this.billingAddress.companyId != null ? this.billingAddress.companyId : '') : '');
          //this.form.get('dispatchFromId').setValue(this.billingAddress != null ? (this.billingAddress.companyId != null ? this.billingAddress.companyId : '') : '');
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getshipmentlist() {
    this.shipmentList = [];
    this.spinner.show();
    this.service.getshipmentlist(this.companyId).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.shipmentList = data.shipmentLocationList != null ? data.shipmentLocationList : [];
          //this.form.get('shippingAddressId').setValue(this.shipmentList[0] != null ? (this.shipmentList[0].shipmentId != null ? this.shipmentList[0].shipmentId : '') : '');
          //this.form.get('shipToId').setValue(this.shipmentList[0] != null ? (this.shipmentList[0].shipmentId != null ? this.shipmentList[0].shipmentId : '') : '');
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  checkVariationsForBags(bags: any, deliveryShipmentId: any, control: AbstractControl) {
    let quantity = this.getQuantity(deliveryShipmentId);
    if (quantity > 0 && bags > 0) {
      this.spinner.show();
      this.service.checkVariationsForBags({
        productId: this.productId, subProductId: this.subProductId,
        quantity: quantity, noOfBags: bags, deliveryShipmentId: deliveryShipmentId
      }).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
            control.setValue('');
          } else {

          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }
  }

  addRemoveObject(isChecked: any, index: any, deliveryShipmentId: any, quantity: any) {
    if (isChecked) {
      this.addShipmentRow(deliveryShipmentId, quantity);
    } else {
      this.shipmentRequests.removeAt(index);
    }
    this.calaulateTotal();
  }

  reset() {
    this.shipmentRequests.clear();
    this.form.reset();
    this.shipmentList = [];
    this.billingAddress = {};
    this.isEnable = false;
    this.fileArray = [];
    this.docArray = [];
    this.shipmentPackagingCode = null;
    this.shipmentPricingCode = null;
    this.gstPercent = 0;
    this.tradePrice = 0;
    this.hsnCode = null;
    this.pricingConversionRate = 0;
    this.pricingUnitCode = null;
    this.shipmentValue = {};
    this.shipmentViewDetailsList = [];
    this.checkBoxArray = [];
  }

  getQuantity(deliveryShipmentId: any): number {
    let quantity = 0;
    for (let x = 0; x < this.shipmentViewDetailsList.length; x++) {
      if (deliveryShipmentId == this.shipmentViewDetailsList[x].deliveryShipmentId) {
        quantity = this.shipmentViewDetailsList[x].quantity;
        break;
      }
    }
    return quantity;
  }

  onError(message: string): void {
    this.notifications.create(this.translate.instant('alert.error'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 3000, showProgressBar: true });
  }

  onSuccess(message: string): void {
    this.notifications.create(this.translate.instant('alert.success'),
      this.translate.instant(message),
      NotificationType.Success, { timeOut: 3000, showProgressBar: true });
  }

  onInfo(message: string): void {
    this.notifications.create(this.translate.instant('alert.warning'),
      this.translate.instant(message),
      NotificationType.Info, { timeOut: 3000, showProgressBar: true });
  }


  search: string;
  suggestions$: Observable<DataSourceType[]>;
  errorMessage: string;
  onSelect(event: TypeaheadMatch): void {
    this.buyerCompanyId = event.item.companyId;
    this.sellerCompanyId = event.item.companyId;
  }
}
interface DataSourceType {
  companyId: any;
  companyName: any;
}
interface DataSourceEnvelop {
  allCompany: DataSourceType[];
}

