import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShipmentComponent } from "./shipment.component"
import { AddshipmentComponent } from "./addshipment/addshipment.component"
import { ViewshipmentComponent } from "./viewshipment/viewshipment.component"

const routes: Routes = [
  {
    path: '', component: ShipmentComponent,
    children: [
      { path: '', redirectTo: 'addshipment', pathMatch: 'full' },
      { path: 'addshipment', component: AddshipmentComponent },
      { path: 'viewshipment', component: ViewshipmentComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShipmentRoutingModule { }
