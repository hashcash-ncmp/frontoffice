import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/data/api.service';
import { noop, Observable, Observer, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { DatePipe } from '@angular/common';
import { RANGES } from 'src/main';

@Component({
  selector: 'app-viewshipment',
  templateUrl: './viewshipment.component.html',
  styleUrls: ['./viewshipment.component.scss']
})
export class ViewshipmentComponent implements OnInit {
  hashUrl=null;
  ranges: any = [];
  url: string = null;
  bsValue = new Date();
  bsRangeValue: Date[];
  tncId: string = '';
  stateList: Array<any> = [];
  companyId: any = null;
  tcnList: Array<any> = [];
  stateId: any = 0;
  productId: number = 0;
  subProductId: number = 0;
  seasonId: number = 0;
  productList: Array<any> = [];
  subProductList: Array<any> = [];
  seasonList: Array<any> = [];
  deliveryNoList: Array<any> = [];
  deliveryNo: any = 0;
  buyerCompanyId: number = 0;
  sellerCompanyId: number = 0;
  shipmentNo: any = 0;
  shipmentNoList: Array<any> = [];
  userType: any = null;
  shipmentViewDetailsList: Array<any> = [];
  shipmentValue: any = {};
  shipmentDocumentList: Array<any> = [];


  constructor(private datePipe: DatePipe, private http: HttpClient, private spinner: NgxSpinnerService, private service: ApiService,
    private notifications: NotificationsService, private translate: TranslateService) {
    //this.bsRangeValue = [this.bsValue, this.bsValue];
    this.url = service.WEBSERVICE;
    this.ranges = RANGES;
    this.hashUrl=service.BLOCK_CHAIN_URL;
  }

  ngOnInit(): void {
    this.userType = localStorage.getItem('BusinessTypeId');
    this.companyId = localStorage.getItem('CompanyID');
    this.GetAllProducts();
    this.getStateList();
    this.getAllCompanyByComId();
  }

  getAllCompanyByComId() {
    this.buyerCompanyId = 0;
    this.sellerCompanyId = 0;
    const url = this.service.WEBSERVICE + '/selection/getAllCompanyByComId';
    this.suggestions$ = new Observable((observer: Observer<string>) => {
      observer.next(this.search);
    }).pipe(
      switchMap((query: string) => {
        if (query) {
          this.spinner.show();
          return this.http.post<DataSourceEnvelop>(
            url, { searchKeyword: query, companyId: this.companyId, flag: "buyer" }).pipe(
              map((data: DataSourceEnvelop) => { this.spinner.hide(); return data && data.allCompany || []; }),
              tap(() => noop, err => {
                this.spinner.hide();
                this.errorMessage = err && err.message || 'Something goes wrong';
              })
            );
        }
        return of([]);
      })
    );
  }

  getStateList() {
    this.stateId = 0;
    this.stateList = [];
    this.tncId = '';
    this.tcnList = [];
    this.deliveryNo = '0';
    this.deliveryNoList = [];
    this.shipmentNo = 0;
    this.shipmentNoList = [];
    this.spinner.show();
    this.service.getStateList({}).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.stateList = data.stateList != null ? data.stateList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  GetAllProducts() {
    this.productId = 0;
    this.productList = [];
    this.spinner.show();
    this.service.GetAllProducts({ companyId: this.companyId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.productList = data.productList != null ? data.productList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  GetAllSubProducts(productId: number) {
    this.subProductId = 0;
    this.subProductList = [];
    this.seasonId = 0;
    this.seasonList = [];
    this.tncId = '';
    this.tcnList = [];
    this.deliveryNo = '0';
    this.deliveryNoList = [];
    this.shipmentNo = 0;
    this.shipmentNoList = [];
    this.spinner.show();
    this.service.GetAllSubProducts({ companyId: this.companyId, productId: productId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.subProductList = data.subProductList != null ? data.subProductList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getSeason() {
    // this.seasonId = 0;
    // this.seasonList = [];
    // this.tncId = '';
    // this.tcnList = [];
    // this.deliveryNo = '0';
    // this.deliveryNoList = [];
    // this.shipmentNo = 0;
    // this.shipmentNoList = [];
    // this.spinner.show();
    // this.service.getSeason({ productId: this.productId, subProductId: this.subProductId }).subscribe(
    //   data => {
    //     this.spinner.hide();
    //     if (data['error'].errorData != '0') {
    //       this.onError(data['error'].errorMsg);
    //     } else {
    //       this.seasonList = data.seasonList != null ? data.seasonList : [];
    //     }
    //   }, error => {
    //     this.spinner.hide();
    //     this.onError(error.message);
    //     console.log(error)
    //   }
    //);
  }

  getTcnList() {
    this.tncId = '';
    this.tcnList = [];
    this.deliveryNo = '0';
    this.deliveryNoList = [];
    this.shipmentNo = 0;
    this.shipmentNoList = [];
    if (this.bsRangeValue[0] != null && this.bsRangeValue[1] != null && this.productId != 0 && this.subProductId != 0 && this.stateId != 0 && this.buyerCompanyId > 0 && this.sellerCompanyId > 0) {
      this.spinner.show();
      this.service.getTcnListForShipments({ flag: "buyer", sellerCompanyId: this.companyId, buyerCompanyId: this.buyerCompanyId, toDate: this.formatDate(this.bsRangeValue[1]), productId: this.productId, subProductId: this.subProductId, fromDate: this.formatDate(this.bsRangeValue[0]), companyId: this.companyId, stateId: this.stateId }).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.tcnList = data.tcnList != null ? data.tcnList : [];
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }
  }

  getDeliveryNoListByTcn(tcnNo: string) {
    // this.deliveryNo = '';
    // this.deliveryNoList = [];
    // this.shipmentNo = 0;
    // this.shipmentNoList = [];
    // this.spinner.show();
    // this.service.getDeliveryNoListByTcn({ tcnNo: tcnNo }).subscribe(
    //   data => {
    //     this.spinner.hide();
    //     if (data['error'].errorData != '0') {
    //       this.onError(data['error'].errorMsg);
    //     } else {
    //       this.deliveryNoList = data.deliveryNoList != null ? data.deliveryNoList : [];
    //     }
    //   }, error => {
    //     this.spinner.hide();
    //     this.onError(error.message);
    //     console.log(error)
    //   }
    // );
  }

  getShipmentNoListByDeliveryNO(deliveryNo: string) {
    // this.shipmentNo = 0;
    // this.shipmentNoList = [];
    // this.spinner.show();
    // this.service.getShipmentNoListByDeliveryNO({ deliveryNo: deliveryNo }).subscribe(
    //   data => {
    //     this.spinner.hide();
    //     if (data['error'].errorData != '0') {
    //       this.onError(data['error'].errorMsg);
    //     } else {
    //       this.shipmentNoList = data.shipmentNoList != null ? data.shipmentNoList : [];
    //     }
    //   }, error => {
    //     this.spinner.hide();
    //     this.onError(error.message);
    //     console.log(error)
    //   }
    // );
  }

  getConfmShippmentDetails() {
    if (this.tncId != null && this.tncId.length > 0 && this.bsRangeValue[0] != null && this.bsRangeValue[1] != null && this.productId != 0 && this.subProductId != 0 && this.stateId != 0 && this.buyerCompanyId > 0 && this.sellerCompanyId > 0) {
      let param = {
        productId: this.productId,
        subProductId: this.subProductId,
        tcnNo: this.tncId,
        stateId: this.stateId,
        // seasonId: this.seasonId,
        fromDate: this.formatDate(this.bsRangeValue[0]),
        toDate: this.formatDate(this.bsRangeValue[1]),
        pageNo: 1,
        noOfItemsPerPage: 1000,
        companyId: this.companyId,
        // deliveryNo: this.deliveryNo,
        // shipmentNo: this.shipmentNo,
        flag: "seller"
      }
      this.spinner.show();
      this.service.getConfmShippmentDetails(param).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.shipmentViewDetailsList = [];
            this.shipmentValue = {};
            this.shipmentDocumentList = [];
            this.onError(data['error'].errorMsg);
          } else {
            this.shipmentViewDetailsList = data.shipmentViewDetailsList != null ? data.shipmentViewDetailsList : [];
            this.shipmentValue = data.shipmentValue != null ? data.shipmentValue : {};
            this.shipmentDocumentList = data.shipmentDocumentList != null ? data.shipmentDocumentList : [];
          }
        }, error => {
          this.spinner.hide();
          this.shipmentViewDetailsList = [];
          this.shipmentDocumentList = [];
          this.shipmentValue = {};
          this.onError(error.message);
          console.log(error)
        }
      );
    }
  }

  printPdf(index) {
    let html = '<head><style> table { font-family: arial, sans-serif; border-collapse: collapse; width: 100%; text-align:center } td, th { border: 1px solid #dddddd; text-align: left; padding: 8px; }</style></head>';
    html += '<table>'
    html += '<tbody>'
    html += '<tr><td colspan=4 style="text-align:center;v-align:center;padding-top:1px;padding-bottom:1px;"><h2>Swayam</h2></td></tr>'
    html += '<tr><td colspan=4 style="text-align:center;v-align:center;padding-top:1px;padding-bottom:1px;"><h5>Shipment Receipt</h5></td></tr>'
    html += '<tr>'
    html += '<td><b>ProductName:&nbsp;</b>&nbsp;' + this.shipmentValue.productName + '</td>'
    html += '<td><b>SubProductName:&nbsp;</b>&nbsp;' + this.shipmentValue.subProductName + '</td>'
    html += '<td><b>Buyer Name:&nbsp;</b>&nbsp;' + this.shipmentValue.buyerCompanyName + '</td>'
    html += '<td><b>Seller Name:&nbsp;</b>&nbsp;' + this.shipmentValue.sellerCompanyName + '</td>'
    html += '</tr>'
    html += '<tr>'
    html += '<td><b>Tcn:&nbsp;</b>&nbsp;' + this.shipmentValue.tcn + '</td>'
    html += '<td><b>Trade TimeStamp:&nbsp;</b>&nbsp;' + this.datePipe.transform(this.shipmentValue.tradeTimestamp, 'MMM d, y h:mm:ss a') + '</td>'
    html += '<td><b>Location:&nbsp;</b>&nbsp;' + this.shipmentValue.location + '</td>'
    html += '<td><b>Allotment No:&nbsp;</b>&nbsp;' + this.shipmentValue.allotmentNo + '</td>'
    html += '</tr>'
    html += '<tr>'
    html += '<td><b>Trade Rate:&nbsp;</b>&nbsp;' + this.shipmentValue.tradePrice + '</td>'
    html += '<td><b>Trade Qty:&nbsp;</b>&nbsp;' + this.shipmentValue.tradeQuantity + '</td>'
    html += '<td><b>Pending Qty:&nbsp;</b>&nbsp;' + this.shipmentValue.pendingQuantity + '</td>'
    html += '<td></td>'
    html += '</tr>'
    html += '</tbody>'
    html += '</table>'
    html += '<table>'
    html += '<thead>'
    html += '<tr>'
    html += '<th>Shipment ID</th>'
    html += '<th>Delivery No</th>'
    html += '<th>Shpiment Date</th>'
    html += '<th>Document No</th>'
    html += '<th>Doc Date</th>'
    html += '<th>NoOfBags</th>'
    html += '<th>Quantity</th>'
    html += '<th>Value</th>'
    html += '<th>Tax</th>'
    html += '<th>Other Charges</th>'
    html += '<th>Net Value</th>'
    html += '</tr>'
    html += '</thead>'
    html += '<tbody>'
    // for (let i = 0; i < this.shipmentViewDetailsList.length; i++) {
    html += '<tr>'
    html += '<td>' + this.shipmentViewDetailsList[index].shipmentId + '</td>'
    html += '<td>' + this.shipmentViewDetailsList[index].deliveryNo + '</td>'
    html += '<td>' + this.datePipe.transform(this.shipmentViewDetailsList[index].shipmentDate, 'MMM d, y h:mm:ss a') + '</td>'
    html += '<td>' + this.shipmentViewDetailsList[index].documentNo + '</td>'
    html += '<td>' + this.datePipe.transform(this.shipmentViewDetailsList[index].documentDate, 'MMM d, y h:mm:ss a') + '</td>'
    html += '<td>' + this.shipmentViewDetailsList[index].noOfBags + '</td>'
    html += '<td>' + this.shipmentViewDetailsList[index].quantity + '</td>'
    html += '<td>' + this.shipmentViewDetailsList[index].value + '</td>'
    html += '<td>' + this.shipmentViewDetailsList[index].tax + '</td>'
    html += '<td>' + this.shipmentViewDetailsList[index].otherCharge + '</td>'
    html += '<td>' + this.shipmentViewDetailsList[index].netValue + '</td>'
    html += '</tr>'
    // }
    html += '</tbody>'
    html += '</table>'
    this.spinner.show();
    this.service.generatePdf(html, this.shipmentViewDetailsList[index].shipmentId + '_Shipment.pdf').then((data) => {
      this.spinner.hide();
      console.log(data)
    }).catch((error) => {
      this.spinner.hide();
      console.log(error)
    })
  }

  formatDate(date: Date) {
    return date.getFullYear() + "-" + (date.getMonth() >= 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-" + (date.getDate() > 9 ? date.getDate() : "0" + date.getDate());
  }

  onError(message: string): void {
    this.notifications.create(this.translate.instant('alert.error'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 3000, showProgressBar: true });
  }

  onSuccess(message: string): void {
    this.notifications.create(this.translate.instant('alert.success'),
      this.translate.instant(message),
      NotificationType.Success, { timeOut: 3000, showProgressBar: true });
  }

  search: string;
  suggestions$: Observable<DataSourceType[]>;
  errorMessage: string;
  onSelect(event: TypeaheadMatch): void {
    this.buyerCompanyId = event.item.companyId;
    this.sellerCompanyId = event.item.companyId;
  }
}
interface DataSourceType {
  companyId: any;
  companyName: any;
}
interface DataSourceEnvelop {
  allCompany: DataSourceType[];
}
