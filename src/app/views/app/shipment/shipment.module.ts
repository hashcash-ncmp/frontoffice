import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxSpinnerModule } from "ngx-spinner";
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ShipmentRoutingModule } from './shipment-routing.module';
import { ShipmentComponent } from './shipment.component';
import { AddshipmentComponent } from './addshipment/addshipment.component';
import { ViewshipmentComponent } from './viewshipment/viewshipment.component';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';


@NgModule({
  declarations: [ShipmentComponent, AddshipmentComponent, ViewshipmentComponent],
  imports: [
    CommonModule,
    ShipmentRoutingModule,
    SharedModule,
    LayoutContainersModule,
    FormsModule,
    ReactiveFormsModule,
    SimpleNotificationsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgxSpinnerModule,
    PaginationModule,
    PaginationModule.forRoot(),
    TypeaheadModule.forRoot(),
  ],
  providers: [BsModalService,DatePipe],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShipmentModule { }
