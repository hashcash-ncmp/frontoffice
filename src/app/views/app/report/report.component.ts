import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/data/api.service';
import { TranslateService } from '@ngx-translate/core';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { RANGES } from 'src/main';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
  ranges: any = [];
  bsValue = new Date();
  bsRangeValue: Date[];
  companyId: string = '';
  reportId: number = 0;
  companyList: Array<any> = [];
  reportCategoriesList: Array<any> = [];
  reportAllValues: Array<any> = [];
  headerList: Array<any> = [];
  pageNo: number = 1;
  pageSize: number = 20;
  totalCount: number = 0;
  url: string = null;

  constructor(private spinner: NgxSpinnerService, private service: ApiService, private notifications: NotificationsService, private translate: TranslateService) {
    //this.bsRangeValue = [this.bsValue, this.bsValue];
    this.companyId = localStorage.getItem('CompanyID');
    this.url = service.REPORT_WEBSERVICE;
    this.ranges = RANGES;
  }

  ngOnInit(): void {
    this.getReportCategories();
  }

  getReportCategories() {
    this.companyList = [];
    this.spinner.show();
    this.service.getReportCategories().subscribe(data => {
      this.spinner.hide();
      if (data['error'].errorData != '0') {
        this.onError(data['error'].errorMsg);
      } else {
        this.reportCategoriesList = data.reportCategoriesList != null ? data.reportCategoriesList : [];
      }
    }, error => {
      this.spinner.hide();
      this.onError(error.message);
      console.log(error)
    });
  }

  getAllReports(pageNo: number) {
    if (this.reportId > 0 && this.companyId != null && this.companyId.length > 0 && this.bsRangeValue[0] != null && this.bsRangeValue[1] != null) {
      this.pageNo = pageNo;
      this.pageSize = 20;
      this.spinner.show();
      this.service.getAllReports({ companyId: this.companyId, reportCode: this.reportId, formDate: this.formatDate(this.bsRangeValue[0]), toDate: this.formatDate(this.bsRangeValue[1]), pageNo: pageNo, noOfItemsPerPage: 20 }).subscribe(data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.reportAllValues = [];
          this.totalCount = 0;
          this.headerList = [];
          this.onError(data['error'].errorMsg);
        } else {
          this.reportAllValues = data.reportAllValues != null ? data.reportAllValues : [];
          this.totalCount = data.totalCount != null ? data.totalCount : 0;
          this.headerList = Object.keys(this.reportAllValues[0] != null ? this.reportAllValues[0] : {});
        }
      }, error => {
        this.reportAllValues = [];
        this.totalCount = 0;
        this.headerList = [];
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      })
    }
  }

  formatDate(date: Date) {
    return date.getFullYear() + "-" + (date.getMonth() >= 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-" + (date.getDate() > 9 ? date.getDate() : "0" + date.getDate());
  }

  onError(message: string): void {
    this.notifications.create(this.translate.instant('alert.error'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 3000, showProgressBar: true });
  }
}
