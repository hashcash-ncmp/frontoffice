import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router) { }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        var membershipstatus=  localStorage.getItem('membershipstatus');
        if (membershipstatus=="1") {
            // logged in so return true
            return true;
        }
        else{
           
          //  return false;
        this.router.navigate([environment.adminRoot+'/'+'payments/pendingpayment']);
        
        }
        // not logged in so redirect to login page with the return url
     //   this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    
    }
}