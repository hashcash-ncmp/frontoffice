import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradepaymentComponent } from './tradepayment.component';

describe('TradepaymentComponent', () => {
  let component: TradepaymentComponent;
  let fixture: ComponentFixture<TradepaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradepaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradepaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
