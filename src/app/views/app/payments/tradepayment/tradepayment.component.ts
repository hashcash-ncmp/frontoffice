import { Component, OnInit, TemplateRef } from '@angular/core';
import { ApiService } from 'src/app/data/api.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { RANGES } from 'src/main';
import { NotificationsService, NotificationType } from 'angular2-notifications';

@Component({
  selector: 'app-tradepayment',
  templateUrl: './tradepayment.component.html',
  styleUrls: ['./tradepayment.component.scss']
})
export class TradepaymentComponent implements OnInit {
  dropdownList = [];
  selectedItems = [];
  updateBalance: any;;
  public deliveryOrdValue: any;
  public netPayble: any;
  public modalRef: BsModalRef;
  public allproducts = [];
  public allcommodity = [];
  public paymentdtl: any;
  public transactionId: any;
  public commissionId: any = 0;
  public commissionList = [];
  public companyDtl: any;
  public matchingDtl: any;
  public paymentDtl = [];
  public commissionDtl = [];
  public tradepayDtl = [];
  public BankDtl = [];
  public invoiceList = [];
  bsValue = new Date();
  bsRangeValue: Date[];
  ranges: any = [];
  public productId: string = "";
  public subProductId: string = "";
  public Tradelimit: any = "";
  public errormessage: any;
  public tcnNo: string = "";
  public paymenttype: string = "";
  public counterCompanyId: string = "";
  public paymenttypeId: any = "";
  // productId: any;
  public PaymentTypes: any;
  public CompanyNames: any;
  public TcnAll: any;
  public PError: any = "";
  public errorMessage: any = "";
  public errorMsgforDate: any = "";
  public allShipmentNo = [];
  public Invoiceno: any;
  public dropdownSettings = {};
  public ckeckallVal: boolean = true;
  public exchangeName: any;
  public exchangeAddress: any;
  public exchangeEmail: any;
  public exchangeMobile: any;
 

  constructor(private _ApiService: ApiService, private modalService: BsModalService, private spinner: NgxSpinnerService, private notifications: NotificationsService) {
    // this.bsRangeValue = [this.bsValue, this.bsValue];
    this.ranges = RANGES;
  }

  ngOnInit(): void {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'shipmentHdrId',
      textField: 'shipmentNo',
      unSelectAllText: 'UnSelect All',
      enableCheckAll: false,
      itemsShowLimit: 10,
      allowSearchFilter: false
    };

    document.getElementById('tbl-space').style.display = 'none'
    this.GetAllProduct();
    this.GetpaymentTypes();
    this.getInvoiceList();
  }

  GetAllProduct() {
    this.PError = "";
    let offerinput = {};
    var companyID = localStorage.getItem('CompanyID');
    offerinput['companyId'] = companyID;
    this.spinner.show();

    this._ApiService.Getallproduct(offerinput)
      .subscribe(data => {
        this.spinner.hide();
        if (data.error.errorData == 0) {
          this.allproducts = data.productList;
          // console.log('ppppppppp', data.productList);

        }
      })
  }

  getcommodityProduct(event) {
    //this.allcommodity = [];
    this.PError = "";
    localStorage.removeItem("ProductID");
    localStorage.removeItem("SubProductID");
    localStorage.removeItem("stateID");
    localStorage.setItem('ProductID', event);
    let commodityinput = {};
    var companyID = localStorage.getItem('CompanyID');
    commodityinput['productId'] = event;
    this.spinner.show();
    this._ApiService.Getcommodities(commodityinput)
      .subscribe(data => {
        this.spinner.hide();
        if (data.error.errorData == 0) {
          this.allcommodity = data.subProductList;
          //console.log('ccccccccccccc', data);
          this.CheckValFlag();
        }
      })
  }
  GetpaymentTypes() {
    this.spinner.show();
    this._ApiService.GetPaymentTypes()
      .subscribe(data => {
        this.spinner.hide();
        if (data.error.errorData == 0) {
          this.PaymentTypes = data.paymentTypeList;
          // console.log('ppppppppp', data.productList);

        }
        else {

        }
      })
  }
  GetcompanyNames() {
    var companyID = localStorage.getItem('CompanyID');
    if (companyID != null && this.subProductId != null && this.bsRangeValue[0] != null && this.bsRangeValue[1] != null) {
      this.errorMsgforDate = "";
      let companydata = {};
      companydata['companyId'] = companyID;
      companydata['productId'] = this.productId;
      companydata['subProductId'] = this.subProductId;
      companydata['fromDate'] = this.formatDate(this.bsRangeValue[0]);
      companydata['toDate'] = this.formatDate(this.bsRangeValue[1]);
      this.spinner.show();
      this._ApiService.GetPCompanyName(companydata)
        .subscribe(data => {
          this.spinner.hide();
          if (data.error.errorData == 0) {
            this.CompanyNames = data.paymentFilterList;
            // console.log('ppppppppp', data.productList);
            this.CheckValFlag();
          }
          else {

          }
        })
    }



  }
  GetpaymentTcn(event) {
    this.errorMessage = "";
    let paymentdata = {};
    var companyID = localStorage.getItem('CompanyID');
    paymentdata['companyId'] = companyID;
    paymentdata['productId'] = this.productId;
    paymentdata['subProductId'] = this.subProductId;
    paymentdata['fromDate'] = this.formatDate(this.bsRangeValue[0]);
    paymentdata['toDate'] = this.formatDate(this.bsRangeValue[1]);
    paymentdata['counterCompanyId'] = event;
    this.spinner.show();
    this._ApiService.GetPaymentTcn(paymentdata)
      .subscribe(data => {
        this.spinner.hide();
        if (data.error.errorData == 0) {
          this.TcnAll = data.paymentFilterList;
          // console.log('ppppppppp', data.productList);
          this.CheckValFlag();
        }
        else {

        }
      })


  }
  checkdaterange() {
    if (this.bsRangeValue === null) {
      this.errorMsgforDate = "Select Date range."
    }
    else {

    }
  }
  checkProductId() {
    if (this.productId == "") {
      this.PError = "Select Product first";
      this.CheckValFlag();
    }
  }
  chanckcompanyname() {
    this.errorMsgforDate = '';
    if (this.counterCompanyId === "") {
      this.errorMessage = "Select Company Name first";
    }
    else {

    }
  }
  GetTradepDtlByPaymentType() {
    this.tradepayDtl = [];
    this.commissionList = [];
    this.spinner.show();
    document.getElementById('error-Invmsg').style.display = 'none';
    document.getElementById('error-Badjustmsg').style.display = 'none';
    document.getElementById('error-Emdmsg').style.display = 'none';
    document.getElementById('error-Blmsg').style.display = 'none';
    if (this.paymenttypeId == 4) {
      let paymentInput = {};
      var companyID = localStorage.getItem('CompanyID');
      paymentInput['companyId'] = companyID;
      paymentInput['tcnNo'] = this.tcnNo;
      paymentInput['paymentType'] = this.paymenttypeId;
      paymentInput['productId'] = this.productId
      paymentInput['subProductId'] = this.subProductId;
      paymentInput['shipmentNo'] = this.invoiceList;
      this._ApiService.getTradePaymentDtl(paymentInput)
        .subscribe(data => {
          this.spinner.hide();
          if (data.error.errorData == 0) {
            //  if (this.paymenttypeId == 1&& data.tradePaymentList!="") {
            document.getElementById('tbl-space').style.display = 'block';
            document.getElementById('payment-tbl').style.display = 'none';
            document.getElementById('preview-btn').style.display = 'none';
            document.getElementById('balancepayment-tbl').style.display = 'none';
            document.getElementById('balancepayment-btn').style.display = 'none';
            document.getElementById('balanceadjustment-tbl').style.display = 'none';
            document.getElementById('adjustment-btn').style.display = 'none';
            document.getElementById('InvoicePayment-tbl').style.display = 'block';
            //  document.getElementById('InvoicePreview-btn').style.display = 'block';
            this.tradepayDtl = data.tradePaymentList;

            console.log(this.tradepayDtl)
            for (var i = 0; i < this.tradepayDtl.length; i++) {
              if (this.tradepayDtl[0].status != 7) {
                document.getElementById('InvoicePreview-btn').style.display = 'block';
              }
              else {
                document.getElementById('error-Invmsg').style.display = 'block';

              }
            }
            this.commissionList = data.commisionList;
            //   console.log('ppppppppp', this.paymentdtl);
          }
          else {
            this.notifications.create(
              'Error',
              data.error.errorMsg,
              NotificationType.Error,
              { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
            );
          }
        })
      //   this.getInvoiceList();
    }
    else {
      let paymentInput = {};
      var companyID = localStorage.getItem('CompanyID');
      paymentInput['companyId'] = companyID;
      paymentInput['tcnNo'] = this.tcnNo;
      paymentInput['paymentType'] = this.paymenttypeId;
      paymentInput['productId'] = this.productId
      paymentInput['subProductId'] = this.subProductId;
      this._ApiService.getTradePaymentDtl(paymentInput)
        .subscribe(data => {
          this.spinner.hide();
          if (data.error.errorData == 0) {
            if (this.paymenttypeId == 1 && data.tradePaymentList != "") {
              document.getElementById('tbl-space').style.display = 'block';
              document.getElementById('InvoicePayment-tbl').style.display = 'none';
              document.getElementById('InvoicePreview-btn').style.display = 'none';
              document.getElementById('balancepayment-tbl').style.display = 'none';
              document.getElementById('balancepayment-btn').style.display = 'none';
              document.getElementById('balanceadjustment-tbl').style.display = 'none';
              document.getElementById('adjustment-btn').style.display = 'none';
              document.getElementById('refund-tbl').style.display = 'none';
              document.getElementById('refund-btn').style.display = 'none';
              document.getElementById('payment-tbl').style.display = 'block';

              this.tradepayDtl = data.tradePaymentList;
              this.commissionList = data.commisionList;
              for (var i = 0; i < this.tradepayDtl.length; i++) {
                if (this.tradepayDtl[0].status != 7) {
                  document.getElementById('preview-btn').style.display = 'block';
                }
                else {
                  document.getElementById('error-Emdmsg').style.display = 'block';
                }
              }
              // this.transactionId = this.paymentdtl[0].transactionId;
              // console.log('ppppppppp', this.paymentdtl);
            }
            else if (this.paymenttypeId == 2 && data.tradePaymentList != "") {
              document.getElementById('tbl-space').style.display = 'block';
              document.getElementById('payment-tbl').style.display = 'none';
              document.getElementById('preview-btn').style.display = 'none';
              document.getElementById('InvoicePayment-tbl').style.display = 'none';
              document.getElementById('InvoicePreview-btn').style.display = 'none';
              document.getElementById('balanceadjustment-tbl').style.display = 'none';
              document.getElementById('adjustment-btn').style.display = 'none';
              document.getElementById('refund-tbl').style.display = 'none';
              document.getElementById('refund-btn').style.display = 'none';
              document.getElementById('balancepayment-tbl').style.display = 'block';

              this.tradepayDtl = data.tradePaymentList;
              this.commissionList = data.commisionList;
              for (var i = 0; i < this.tradepayDtl.length; i++) {
                if (this.tradepayDtl[0].status != 7) {
                  document.getElementById('balancepayment-btn').style.display = 'block';
                }
                else {
                  document.getElementById('error-Blmsg').style.display = 'block';
                }
              }
              ///  console.log('ppppppppp', this.paymentdtl);
            }
            else if (this.paymenttypeId == 3 && data.tradePaymentList != "") {
              document.getElementById('tbl-space').style.display = 'block';
              document.getElementById('payment-tbl').style.display = 'none';
              document.getElementById('preview-btn').style.display = 'none';
              document.getElementById('InvoicePayment-tbl').style.display = 'none';
              document.getElementById('InvoicePreview-btn').style.display = 'none';
              document.getElementById('balancepayment-tbl').style.display = 'none';
              document.getElementById('balancepayment-btn').style.display = 'none';
              document.getElementById('refund-tbl').style.display = 'none';
              document.getElementById('refund-btn').style.display = 'none';
              document.getElementById('balanceadjustment-tbl').style.display = 'block';

              this.tradepayDtl = data.tradePaymentList;
              this.commissionList = data.commisionList;
              for (var i = 0; i < this.tradepayDtl.length; i++) {
                if (this.tradepayDtl[0].status != 7) {
                  document.getElementById('adjustment-btn').style.display = 'block';
                }
                else {
                  document.getElementById('error-Badjustmsg').style.display = 'block';
                }
              }
              ///  console.log('ppppppppp', this.paymentdtl);
            }
            else if (this.paymenttypeId == 5 && data.tradePaymentList != "") {
              document.getElementById('tbl-space').style.display = 'block';
              document.getElementById('payment-tbl').style.display = 'none';
              document.getElementById('preview-btn').style.display = 'none';
              document.getElementById('InvoicePayment-tbl').style.display = 'none';
              document.getElementById('InvoicePreview-btn').style.display = 'none';
              document.getElementById('balanceadjustment-tbl').style.display = 'none';
              document.getElementById('adjustment-btn').style.display = 'none';
              document.getElementById('refund-tbl').style.display = 'block';
              document.getElementById('refund-btn').style.display = 'block';
              document.getElementById('refund-btn').style.display = 'none';
              this.tradepayDtl = data.tradePaymentList;
              this.commissionList = data.commisionList;
              for (var i = 0; i < this.tradepayDtl.length; i++) {
                if (this.tradepayDtl[0].status != 7) {
                  document.getElementById('preview-btn').style.display = 'block';
                }
                else {
                  document.getElementById('error-msg').style.display = 'block';
                }
              }
              //  console.log('ppppppppp', this.paymentdtl);

            }
            else {

            }
          }
          else if (data.error.errorData == 1) {
            document.getElementById('tbl-space').style.display = 'none';
            document.getElementById('payment-tbl').style.display = 'none';
            document.getElementById('preview-btn').style.display = 'none';
            document.getElementById('InvoicePayment-tbl').style.display = 'none';
            document.getElementById('InvoicePreview-btn').style.display = 'none';
            document.getElementById('balanceadjustment-tbl').style.display = 'none';
            document.getElementById('adjustment-btn').style.display = 'none';
            document.getElementById('refund-tbl').style.display = 'none';
            document.getElementById('refund-btn').style.display = 'none';
            document.getElementById('error-msg').style.display = 'none';
            this.notifications.create(
              'Error',
              data.error.errorMsg,
              NotificationType.Error,
              { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
            );
          }
        })
    }
  }
  UpdateBalancePayment(tId, deliveryQty) {
    let updatebalanceInput = {};
    this.deliveryOrdValue = "";
    this.netPayble = ""
    var companyID = localStorage.getItem('CompanyID');
    updatebalanceInput['companyId'] = companyID;
    updatebalanceInput['tcnNo'] = this.tcnNo;
    updatebalanceInput['txnId'] = tId;
    updatebalanceInput['deliveryQuantity'] = deliveryQty;
    this._ApiService.UpdatebalencePmt(updatebalanceInput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.updateBalance = data.tradePaymentList;
          this.deliveryOrdValue = this.updateBalance[0].deliveryOrderValue;
          this.netPayble = this.updateBalance[0].netPayable;
          console.log('value------------------', data.tradePaymentList);

        }
      })
  }
  checkquantityOffer(event, ID) {
    //this.errormessage = "";
    let Inputcheckdata = {};
    // var productID = this.offerForm.get('productId').value;
    // var subProductID = this.offerForm.get('commodityId').value;
    var companyID = localStorage.getItem('CompanyID');
    Inputcheckdata['companyId'] = companyID;
    Inputcheckdata['productId'] = this.productId;
    Inputcheckdata['subProductId'] = this.subProductId;
    this._ApiService.GetTradeLimit(Inputcheckdata)
      .subscribe(data => {
        if (data.error.errorData == 0 && data.error.errorMsg != 'No data!') {
          if (event > 0) {
            this.Tradelimit = data.tradeLimit.tradeLimit;
            let HasReminder = event % this.Tradelimit;
            if (HasReminder == 0) {
              this.errormessage = "";
              this.UpdateBalancePayment(ID, event);
            }
            else {
              this.errormessage = "Exceed Trade Limit. Available Trade limit is" + ' ' + this.Tradelimit;
            }
          }
          else {
            // this.offerForm.get('Quantity').setValue('');
            this.errormessage = "Quantity can not be 0 or nagative";
          }
          // if (event > 0) {
          //   if (event >= this.Tradelimit && HasReminder == 0) {
          //     this.errormessage = "";
          //   }
          //   else {
          //     // (event < 0 || event < this.Tradelimit || HasReminder != 0)
          //     this.errormessage = "Quantity can not be 0 or less than trade limit and product of 100."
          //   }
          // }
          // else{

          //   this.offerForm.get('Quantity').setValue('');
          //   this.errormessage = "Quantity can not be  0 or negative."
          // }
        }
        else {
          if (event <= 0) {
            //this.offerForm.get('Quantity').setValue('');
            this.errormessage = "Quantity can not be  0 or negative."
          }
          else {
            this.errormessage = data.error.errorMsg;
          }

        }
      })

  }
  CheckValFlag() {
    if (this.paymenttypeId == 4) {

      this.getInvoiceList();
    }
    else {
      this.allShipmentNo = [];
      document.getElementById('invoice-list').style.display = 'none';
    }
    if (this.tcnNo != "" && this.paymenttypeId != "" && this.productId != "" && this.subProductId != "") {
      this.ckeckallVal = false;
      //alert(this.ckeckallVal);
    }
  }
  getInvoiceList() {
    let inputInvoice = {};
    var companyID = localStorage.getItem('CompanyID');
    inputInvoice['companyId'] = companyID;
    inputInvoice['tcnNo'] = this.tcnNo;
    this._ApiService.GetpaymentInvoiceno(inputInvoice)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          document.getElementById('invoice-list').style.display = 'block';
          console.log(data);
          this.allShipmentNo = data.shipmentNoList;
        }

      })
  }
  onItemDeSelect(event) {
    //console.log('', item);
    var temp = [];
    //  temp = this.tempval.split(',');
    for (var i = 0; i < this.invoiceList.length; i++) {
      var bId = event.shipmentNo.toString();
      if (this.invoiceList[i] == bId) {
        var found = this.invoiceList.includes(bId);
        if (found == true) {
          this.invoiceList.splice(i, 1);
        }
      }
      //   this.tempval = temp.toString();
      console.log('$$$$$$$$', this.invoiceList);
    }
  }
  onSelectAll(event) {

  }
  onItemSelect(event) {
    // if (this.invoiceList == null) {
    console.log('&&&&&&&&&&&&&', event);

    this.invoiceList.push(event.shipmentNo);
    // }
    // else {
    //   this.tempval = item.businessTypeId + ',' + this.tempval;
    // }
  }
  GetPreviewData() {
    this.Getexchangeaddress();
    let previewInput = {};
    var companyID = localStorage.getItem('CompanyID');
    previewInput['companyId'] = companyID;
    previewInput['paymentList'] = this.tradepayDtl;
    previewInput['commisionList'] = this.commissionList;
    this._ApiService.createPAdvicePreview(previewInput)
      .subscribe(data => {
        if (data.error.errorData == 0) {


          console.log('GetPreviewData', data);
          this.companyDtl = data.companyDetails;
          this.matchingDtl = data.matchedCompanyDetails;
          this.paymentDtl = data.tradePaymentList;
          this.commissionDtl = data.commisionList;
          this.BankDtl = data.bankDetails;
          let html = '';
          html += '<html><head></head><body>'
          html += '<style>';
          html += '.invoice-box {';
          html += '    max-width: 800px;';
          html += '    margin: auto;';
          html += '    padding: 30px;';
          html += '    border: 1px solid #eee;';
          html += '    box-shadow: 0 0 10px rgba(0, 0, 0, .15);';
          html += '    font-size: 10px;';
          html += '    line-height: 24px;';
          html += "    font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;";
          html += '    color: #555;}';

          html += '.invoice-box table {';
          html += '    width: 100%;';
          html += '    line-height: inherit;';
          html += '    text-align: left;';
          html += '}';

          html += '.invoice-box table td {';
          html += '    padding: 5px;';
          html += '    vertical-align: top;';
          html += '}';

          html += '.invoice-box table tr td:nth-child(2) {';
          html += '    text-align: right;';
          html += '}';

          html += '.invoice-box table tr.top table td {';
          html += '    padding-bottom: 20px;';
          html += '}';

          html += '.invoice-box table tr.top table td.title {';
          html += '    font-size: 15px;';
          html += '    line-height: 45px;';
          html += '    color: #333;';
          html += '}';

          html += '.invoice-box table tr.information table td {';
          html += '    padding-bottom: 40px;';
          html += '}';

          html += '.invoice-box table tr.heading td {';
          html += '    background: #eee;';
          html += '    border-bottom: 1px solid #ddd;';
          html += '    font-weight: bold;';
          html += '}';

          html += '.invoice-box table tr.details td {';
          html += '    padding-bottom: 20px;';
          html += '}';

          html += '.invoice-box table tr.item td{';
          html += '    border-bottom: 1px solid #eee;';
          html += '}';
          html += '';
          html += '.invoice-box table tr.item.last td {';
          html += '    border-bottom: none;';
          html += '}';

          html += '.invoice-box table tr.total td:nth-child(2) {';
          html += '    border-top: 2px solid #eee;';
          html += '    font-weight: bold;';
          html += '}';

          html += '@media only screen and (max-width: 600px) {';
          html += '    .invoice-box table tr.top table td {';
          html += '        width: 100%;';
          html += '        display: block;';
          html += '        text-align: center;';
          html += '    }';

          html += '    .invoice-box table tr.information table td {';
          html += '        width: 100%;';
          html += '        display: block;';
          html += '        text-align: center;';
          html += '    }';
          html += '}';

          html += '.rtl {';
          html += '    direction: rtl;';
          html += "    font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;";
          html += '}';

          html += ' .rtl table {';
          html += '    text-align: right;';
          html += ' }';

          html += '  .rtl table tr td:nth-child(2) {';
          html += '      text-align: left;';
          html += '  }';
          html += '  </style>';


          html += '<div class="invoice-box">';
          html += '<table cellpadding="0" cellspacing="0">';
          html += '<tr class="top">';
          html += ' <td colspan="2">';
          html += '<table>';
          html += '<tr>';
          html += '<td class="title"> ADVICE';

          html += '</td>';

          html += '<td>Trade Payment Advice #: ' + this.paymentDtl[0].adviceNo;
          if (this.paymenttypeId != 1) {
            html += '<br>Commission Advice:' + this.commissionDtl[0].adviceNo;
          }
          html += '</td>';
          html += ' </tr>';
          html += '</table>';
          html += '</td>';
          html += '</tr>';

          html += '<tr class="information">';
          html += '<td colspan="2">';
          html += '<table>';

          html += '<tr>';
          html += '<td>SELLER <br>Company Name : ' + this.companyDtl.companyName + '<br> Pan No : ' + this.companyDtl.panNo + '<br> Contact No : ' + this.matchingDtl.contactNo + '<br> Address :' + this.companyDtl.regAddressLineOne + ',' + this.companyDtl.regAddressLineTwo + ',' + this.companyDtl.cityName + ',' + this.companyDtl.districtName + '' + this.companyDtl.stateName + ',' + this.companyDtl.countryName;
          html += '</td>';

          html += '<td>BUYER <br> Company Name : ' + this.matchingDtl.companyName + '<br> Pan No  : ' + this.matchingDtl.panNo + '<br> Contact No : ' + this.matchingDtl.contactNo + '<br> Address : ' + this.matchingDtl.regAddressLineOne + +this.matchingDtl.regAddressLineTwo;
          this.matchingDtl.cityName + this.matchingDtl.districtName + this.matchingDtl.stateName
            + this.matchingDtl.countryName;
          html += '</td>';
          html += '</tr>';
          html += '</table>';
          html += '</td>';
          html += '</tr>';
          html += '<table>';
          html += '<tr class="heading">';
          html += '<td>Payment Type Name</td>';
          html += ' <td>TCN No</td>';
          html += ' <td>TradeQuantity</td>';
          html += ' <td>TradePrice</td>';
          html += ' <td>Emd Date</td>';
          html += ' <td>Emd Value</td>';
          html += ' <td>Trade Date</td>';
          html += ' <td>Contract Value</td>';

          html += '</tr>';
          html += '<tr>';
          for (var i = 0; i < this.tradepayDtl.length; i++) {
            html += '<tr>';
            html += ' <td>' + this.tradepayDtl[i].paymentTypeName + '</td>';
            html += ' <td>' + this.tradepayDtl[i].tcn + '</td>';
            html += ' <td>' + this.tradepayDtl[i].tradeQuantity + '</td>';
            html += ' <td>' + parseFloat(this.tradepayDtl[i].tradePrice).toFixed(2) + '</td>';
            html += ' <td>' + this.tradepayDtl[i].emdDate + '</td>';
            html += ' <td>' + parseFloat(this.tradepayDtl[i].emdValue).toFixed(2) + '</td>';
            html += ' <td>' + this.tradepayDtl[i].tradeDate + '</td>';
            html += ' <td>' + parseFloat(this.tradepayDtl[i].contractValue).toFixed(2) + '</td>';

            html += '</tr>';


            html += '</table>'

            html += '<tr class="item">';
            html += '<td>Total Amount(In Rupees)';
            //  html += this.invoiceDescription
            html += '</td>';

            html += '<td>';
            html += this.tradepayDtl[i].totalAmount
          }
          html += '</td>';
          html += '</tr>';
          html += '<tr class="total">';
          html += '<td></td>';
          //            html += '<td> GST: ' + parseFloat(this.invoiceGST).toFixed(2) + '   </td>';
          html += '</tr>';
          html += '<tr class="total">';
          html += '<td></td>';
          //            html += '<td> Total: ' + (parseFloat(this.invoiceAmount) + parseFloat(this.invoiceGST)).toFixed(2) + '   </td>';
          html += '</tr>';
          html += '</table>';
          html += '<table>';
          if (this.paymenttypeId != 1) {
            html += '<tr class="heading">';
            html += ' <td>TCN No</td>';
            html += ' <td>BuyerCompanyName</td>';
            html += ' <td>SellerCompanyName</td>';
            html += ' <td>Commission Fee</td>';
            html += ' <td>GST Amount</td>';
            html += '</tr>';
            html += '<tr>';
            for (var i = 0; i < this.commissionDtl.length; i++) {
              html += '<tr>';
              html += ' <td>' + this.commissionDtl[i].tcn + '</td>';
              html += ' <td>' + this.commissionDtl[i].buyerCompanyName + '</td>';
              html += ' <td>' + this.commissionDtl[i].sellerCompanyName + '</td>';
              html += ' <td>' + this.commissionDtl[i].commisionFeeReceived + '</td>';
              html += ' <td>' + this.commissionDtl[i].gstAmount + '</td>';
              html += '</tr>'


              html += '</table>'

              html += '<tr class="item">';
              html += '<td>Total Amount(In Rupees)';
              //  html += this.invoiceDescription
              html += '</td>';

              html += '<td>&nbsp;&nbsp;&nbsp;';
              html += this.commissionDtl[i].totalAmount
            }
            html += '</td>';
            html += '</tr>';
            html += '<tr class="total">';
            html += '<td></td>';
          }
          //            html += '<td> GST: ' + parseFloat(this.invoiceGST).toFixed(2) + '   </td>';
          html += '</tr>';
          html += '<tr class="total">';
          html += '<td></td>';
          //            html += '<td> Total: ' + (parseFloat(this.invoiceAmount) + parseFloat(this.invoiceGST)).toFixed(2) + '   </td>';
          html += '</tr>';
          html += '</table>';
          html += '<tr class="information">';
          html += '<td colspan="2">';
          html += '<table>';
          html += '<tr>';
          //  html += '<td> Bank Name : ' + this.invoiceBankName + '<br> Beneficiary : ' + this.invoiceBankBeneficiaryName + '<br> A/C No : ' + this.invoiceBankAccountNo + ' <br> IFSC : ' + this.invoiceBankIfscCode + ' <br> Type : ' + this.invoiceBankAccountType;
          html += '</td>';

          //html += '<td>Acme Corp.<br> John Doe<br> john@example.com';
          html += '</td>';
          html += '</tr>';
          html += '</table>';
          html += '</td>';
          html += '</tr>';
          html += '</div>';
          html += '</body>';
          console.log(html);

          let html2 = '';
          html2 += '<html><head></head><body>'
          html2 += '<style>';
          html2 += '.invoice-box {';
          html2 += '    max-width: 800px;';
          html2 += '    margin: auto;';
          html2 += '    padding: 30px;';
          html2 += '    border: 1px solid #eee;';
          html2 += '    box-shadow: 0 0 10px rgba(0, 0, 0, .15);';
          html2 += '    font-size: 16px;';
          html2 += '    line-height: 24px;';
          html2 += "    font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;";
          html2 += '    color: #555;}';

          html2 += '.invoice-box table {';
          html2 += '    width: 100%;';
          html2 += '    line-height: inherit;';
          html2 += '    text-align: left;';
          html2 += '}';

          html2 += '.invoice-box table td {';
          html2 += '    padding: 5px;';
          html2 += '    vertical-align: top;';
          html2 += '}';

          html2 += '.invoice-box table tr td:nth-child(2) {';
          html2 += '    text-align: right;';
          html2 += '}';

          html2 += '.invoice-box table tr.top table td {';
          html2 += '    padding-bottom: 20px;';
          html2 += '}';

          html2 += '.invoice-box table tr.top table td.title {';
          html2 += '    font-size: 15px;';
          html2 += '    line-height: 45px;';
          html2 += '    color: #333;';
          html2 += '}';

          html2 += '.invoice-box table tr.information table td {';
          html2 += '    padding-bottom: 40px;';
          html2 += '}';

          html2 += '.invoice-box table tr.heading td {';
          html2 += '    background: #eee;';
          html2 += '    border-bottom: 1px solid #ddd;';
          html2 += '    font-weight: bold;';
          html2 += '}';

          html2 += '.invoice-box table tr.details td {';
          html2 += '    padding-bottom: 20px;';
          html2 += '}';

          html2 += '.invoice-box table tr.item td{';
          html2 += '    border-bottom: 1px solid #eee;';
          html2 += '}';
          html2 += '';
          html2 += '.invoice-box table tr.item.last td {';
          html2 += '    border-bottom: none;';
          html2 += '}';

          html2 += '.invoice-box table tr.total td:nth-child(2) {';
          html2 += '    border-top: 2px solid #eee;';
          html2 += '    font-weight: bold;';
          html2 += '}';

          html2 += '@media only screen and (max-width: 600px) {';
          html2 += '    .invoice-box table tr.top table td {';
          html2 += '        width: 100%;';
          html2 += '        display: block;';
          html2 += '        text-align: center;';
          html2 += '    }';

          html2 += '    .invoice-box table tr.information table td {';
          html2 += '        width: 100%;';
          html2 += '        display: block;';
          html2 += '        text-align: center;';
          html2 += '    }';
          html2 += '}';

          html2 += '.rtl {';
          html2 += '    direction: rtl;';
          html2 += "    font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;";
          html2 += '}';

          html2 += ' .rtl table {';
          html2 += '    text-align: right;';
          html2 += ' }';

          html2 += '  .rtl table tr td:nth-child(2) {';
          html2 += '      text-align: left;';
          html2 += '  }';
          html2 += '  </style>';


          html2 += '<div class="invoice-box">';
          html2 += '<table cellpadding="0" cellspacing="0">';
          html2 += '<tr class="top">';
          html2 += ' <td colspan="2">';
          html2 += '<table>';
          html2 += '<tr>';
          html2 += '<td class="title"> ADVICE';

          html2 += '</td>';

          html2 += '<td>Trade Payment Advice #: ' + this.tradepayDtl[0].adviceNo;
          if (this.paymenttypeId != 1) {
            html += '<br>Commission Advice:' + this.commissionDtl[0].adviceNo;
          }
          html2 += '</td>';
          html2 += ' </tr>';
          html2 += '</table>';
          html2 += '</td>';
          html2 += '</tr>';

          html2 += '<tr class="information">';
          html2 += '<td colspan="2">';
          html2 += '<table>';
          html2 += '<tr>';
          html2 += '<td>SELLER <br>Company Name : ' + this.companyDtl.companyName + '<br> Pan No : ' + this.companyDtl.panNo + '<br> Contact No : ' + this.matchingDtl.contactNo + '<br> Address :' + this.companyDtl.regAddressLineOne + ',' + this.companyDtl.regAddressLineTwo + ',' + this.companyDtl.cityName + ',' + this.companyDtl.districtName + '' + this.companyDtl.stateName + ',' + this.companyDtl.countryName;
          html2 += '</td>';

          html2 += '<td>BUYER <br> Company Name : ' + this.matchingDtl.companyName + '<br> Pan No  : ' + this.matchingDtl.panNo + '<br> Contact No : ' + this.matchingDtl.contactNo + '<br> Address : ' + this.matchingDtl.regAddressLineOne + +this.matchingDtl.regAddressLineTwo;
          this.matchingDtl.cityName + this.matchingDtl.districtName + this.matchingDtl.stateName
            + this.matchingDtl.countryName;
          html2 += '</td>';
          html2 += '</tr>';
          html2 += '</table>';
          html2 += '</td>';
          html2 += '</tr>';
          html2 += '<table>';
          html2 += '<tr class="heading">';
          html2 += '<td>Payment Type Name</td>';
          html2 += ' <td>TCN No</td>';
          html2 += ' <td>TradeQuantity</td>';
          html2 += ' <td>Trade Amount</td>';
          html2 += ' <td>Emd Value</td>';
          html2 += ' <td>Trade Date</td>';
          html2 += ' <td>Contract Value</td>';
          html2 += ' <td>Matched Company Name</td>';
          html2 += '</tr>';
          html2 += '<tr>';
          for (var i = 0; i < this.tradepayDtl.length; i++) {
            html2 += '<tr>';
            html2 += ' <td>' + this.tradepayDtl[i].paymentTypeName + '</td>';
            html2 += ' <td>' + this.tradepayDtl[i].tcn + '</td>';
            html2 += ' <td>' + this.tradepayDtl[i].tradeQuantity + '</td>';
            html2 += ' <td>' + this.tradepayDtl[i].tradePrice + '</td>';
            html2 += ' <td>' + this.tradepayDtl[i].tradeAmount + '</td>';
            html2 += ' <td>' + this.tradepayDtl[i].emdValue + '</td>';
            html2 += ' <td>' + this.tradepayDtl[i].emdValue + '</td>';
            html2 += ' <td>' + this.tradepayDtl[i].tradeDate + '</td>';
            html2 += ' <td>' + this.tradepayDtl[i].contractValue + '</td>';
            html2 += ' <td>' + this.tradepayDtl[i].matchedCompanyName + '</td>';
            html2 += '</tr>';


            html2 += '</table>'

            html2 += '<tr class="item">';
            html2 += '<td>Total Amount(In Rupees)';
            //  html += this.invoiceDescription
            html2 += '</td>';

            html2 += '<td>';
            html2 += this.tradepayDtl[i].totalAmount
          }
          html2 += '</td>';
          html2 += '</tr>';
          html2 += '<tr class="total">';
          html2 += '<td></td>';
          //            html += '<td> GST: ' + parseFloat(this.invoiceGST).toFixed(2) + '   </td>';
          html2 += '</tr>';
          html2 += '<tr class="total">';
          html2 += '<td></td>';
          //            html += '<td> Total: ' + (parseFloat(this.invoiceAmount) + parseFloat(this.invoiceGST)).toFixed(2) + '   </td>';
          html2 += '</tr>';
          html2 += '</table>';
          html2 += '<table>';
          html2 += '<tr class="heading">';
          html2 += ' <td>TCN No</td>';
          html2 += ' <td>BuyerCompanyName</td>';
          html2 += ' <td>SellerCompanyName</td>';
          html2 += ' <td>Commission Fee</td>';
          html2 += ' <td>GST Amount</td>';
          html2 += '</tr>';
          if (this.paymenttypeId != 1) {
            html2 += '<tr>';

            for (var i = 0; i < this.commissionDtl.length; i++) {
              html2 += '<tr>';
              html2 += ' <td>' + this.commissionDtl[i].tcn + '</td>';
              html2 += ' <td>' + this.commissionDtl[i].buyerCompanyName + '</td>';
              html2 += ' <td>' + this.commissionDtl[i].sellerCompanyName + '</td>';
              html2 += ' <td>' + this.commissionDtl[i].commisionFeeReceived + '</td>';
              html2 += ' <td>' + this.commissionDtl[i].gstAmount + '</td>';
              html2 += '</tr>'


              html2 += '</table>'

              html2 += '<tr class="item">';
              html2 += '<td>Total Amount(In Rupees)';
              //  html += this.invoiceDescription
              html2 += '</td>';

              html2 += '<td>&nbsp;&nbsp;&nbsp;';
              html2 += this.commissionDtl[i].totalAmount
            }
            html2 += '</td>';
            html2 += '</tr>';
          }
          html2 += '<tr class="total">';
          html2 += '<td></td>';
          //            html += '<td> GST: ' + parseFloat(this.invoiceGST).toFixed(2) + '   </td>';
          html2 += '</tr>';
          html2 += '<tr class="total">';
          html2 += '<td></td>';
          //            html += '<td> Total: ' + (parseFloat(this.invoiceAmount) + parseFloat(this.invoiceGST)).toFixed(2) + '   </td>';
          html2 += '</tr>';
          html2 += '</table>';
          html2 += '<tr class="information">';
          html2 += '<td colspan="2">';
          html2 += '<table>';
          html2 += '<tr>';
          //  html += '<td> Bank Name : ' + this.invoiceBankName + '<br> Beneficiary : ' + this.invoiceBankBeneficiaryName + '<br> A/C No : ' + this.invoiceBankAccountNo + ' <br> IFSC : ' + this.invoiceBankIfscCode + ' <br> Type : ' + this.invoiceBankAccountType;
          html2 += '</td>';

          //html += '<td>Acme Corp.<br> John Doe<br> john@example.com';
          html2 += '</td>';
          html2 += '</tr>';
          html2 += '</table>';
          html2 += '</td>';
          html2 += '</tr>';
          html2 += '</div>';
          html2 += '</body>';
          // console.log(html);
          let advicemailInput = {}
          var companyID = localStorage.getItem('CompanyID');
          var emailID = localStorage.getItem('email');
          advicemailInput['companyId'] = companyID;
          advicemailInput['emailId'] = emailID; //'dhritienator@gmail.com';
          advicemailInput['fileContent'] = html;
          advicemailInput['trdPaymentAdviceNo'] = this.tradepayDtl[0].adviceNo;
          if (this.paymenttypeId != 1) {
            advicemailInput['trdCommAdviceNo'] = this.commissionDtl[0].adviceNo;
          }

          advicemailInput['commFileContent'] = html2;
          this._ApiService.SendMailtoBuyerSeller(advicemailInput)
            .subscribe(data => {
              if (data.error.errorData == 0) {
                console.log('*******************', data);
                this.notifications.create(
                  'Done',
                  'Mail has been sent',
                  NotificationType.Success,
                  { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
                );

              }
              else {
                this.notifications.create(
                  'Error',
                  data.error.errorMsg,
                  NotificationType.Error,
                  { theClass: 'outline primary', timeOut: 6000, showProgressBar: false }
                );
              }
            })
        }
        else {
          this.notifications.create(
            'Error',
            data.error.errorMsg,
            NotificationType.Success,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }
      })

  }
  Getexchangeaddress() {
    this._ApiService.Getexchangedetails()
      .subscribe(data => {
        var exchangedtl = data.adminExchangeDetails;
        this.exchangeName = exchangedtl.exchangeName;
        this.exchangeAddress = exchangedtl.address;
        this.exchangeEmail = exchangedtl.email;
        this.exchangeMobile = exchangedtl.mobile;

      })
  }
  SendAdviceMail() {


  }
  GeneratePreview(template: TemplateRef<any>) {
    this.GetPreviewData();
    setTimeout(() => this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'modal-xl' }), 1000);

  }
  GeneratePreviewInvoicePayment(template: TemplateRef<any>) {
    this.GetPreviewData();

    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'modal-xl' });
  }

  formatDate(date: Date) {
    return date.getFullYear() + "-" + (date.getMonth() >= 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-" + (date.getDate() > 9 ? date.getDate() : "0" + date.getDate());
  }
}
