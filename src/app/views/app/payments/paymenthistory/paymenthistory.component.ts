import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/data/api.service';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { RANGES } from 'src/main';
@Component({
  selector: 'app-paymenthistory',
  templateUrl: './paymenthistory.component.html',
  styleUrls: ['./paymenthistory.component.scss']
})
export class PaymenthistoryComponent implements OnInit {
  hashUrl=null;
  maxDate: Date = new Date();
  public fromdate: string = '';
  public toDate: string = '';
  public PaymentType: string = '';
  public ProductType: string = '';
  public Tcn: string = '';
  public paymentdtl = [];
  public PaymentsTyp: any;
  public notrequired: boolean = false;
  ranges: any = [];
  bsValue = new Date();
  bsRangeValue: Date[];
  fdatecheck: boolean = false;
  tdatecheck: boolean = false;
  typecheck: boolean = false;
  currentDate: any = new Date().toISOString().slice(0, 10);
  currentYear: any = new Date().getFullYear();
  currentMonth: any = (new Date().getMonth()) + 1;
  currentDay: any = new Date().getDate();
  public maxdate: any = this.currentYear + '-' + this.currentMonth + '_' + this.currentDay;  //'2018-12-31';
  visibility:boolean=false;
  constructor(private router: Router, private notifications: NotificationsService, private spinner: NgxSpinnerService, public _ApiService: ApiService) {
    this.ranges = RANGES;
    this.hashUrl=_ApiService.BLOCK_CHAIN_URL;
   }

  ngOnInit(): void {
    this.Getpaymenttype();
    // document.getElementById('bank-tbl').style.display = 'none';
    this.CheckCompanyId();
  }
  CheckCompanyId() {
    var companyID = localStorage.getItem('CompanyID');
    if (!companyID) {
      this.router.navigate(['user/login']);
      localStorage.removeItem('CompanyID');
    }
  }
  Getpaymenttype() {
    this.spinner.show();
    this._ApiService.GetpaymentType()
      .subscribe(data => {
        this.spinner.hide();
        if (data.error.errorData == 0) {
          this.PaymentsTyp = data.paymentCategoryList;
          console.log('----------', data);

        }
      })
  }
  GetaymentHistory() {
    var companyID = localStorage.getItem('CompanyID');
    this.fromdate = this.formatDate(this.bsRangeValue[0]);
    this.toDate = this.formatDate(this.bsRangeValue[1]);
    if (this.fromdate != null && this.toDate != null && companyID != "" && this.PaymentType) {
    let conpayform = {};
    conpayform['companyId'] = companyID;
    conpayform['startDate'] = this.fromdate,
      conpayform['endDate'] = this.toDate,
      conpayform['paymentCategoryId'] = this.PaymentType;
      this.spinner.show();
      this._ApiService.paymenthistory(conpayform)
        .subscribe(data => {
          this.spinner.hide();
          if (data.error.errorData == 0) {
            this.fdatecheck = false;
            this.tdatecheck = false;
            this.typecheck = false;
            // document.getElementById('bank-tbl').style.display = 'block';
            this.visibility=true;
            this.paymentdtl = data.paymentHistoryList;

          }
          else {
            this.notifications.create(
              'Done',
              data.error.errorMsg,
              NotificationType.Bare,
              { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
            );
          }

        })
    }
    else if (this.fromdate == "") {
      this.fdatecheck = true;
    }
    else if (this.toDate == "") {
      this.tdatecheck = true;
    }
    else if (this.PaymentType == "") {
      this.typecheck = true;
    }
    else {
      this.typecheck = false;
      this.fdatecheck = false;
      this.tdatecheck = false;
    }
  }

  GetpaymentType(event) {
    var paymentId = event;
    if (paymentId == 1) {
      this.notrequired = true;
    }
    else {
      this.notrequired = false;

    }

  }

  formatDate(date: Date) {
    return date.getFullYear() + "-" + (date.getMonth() >= 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-" + (date.getDate() > 9 ? date.getDate() : "0" + date.getDate());
  }
}
