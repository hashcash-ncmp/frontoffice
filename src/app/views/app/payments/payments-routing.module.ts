import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentsComponent } from './payments.component';
import { PendingpaymentsComponent } from './pendingpayments/pendingpayments.component';
import { ConfirmpaymentsComponent } from './confirmpayments/confirmpayments.component';
import { PaymenthistoryComponent } from './paymenthistory/paymenthistory.component';
import { PaymentdepositComponent } from './paymentdeposit/paymentdeposit.component';
import { TradepaymentComponent } from './tradepayment/tradepayment.component';
const routes: Routes = [
  {
    path: '', component: PaymentsComponent,
    children: [
      { path: '', redirectTo: 'pendingpayment', pathMatch: 'full' },
      { path: 'pendingpayment', component: PendingpaymentsComponent },
      { path: 'confirmpayment', component: ConfirmpaymentsComponent },
      { path: 'PaymentHistory', component: PaymenthistoryComponent },
      { path: 'deposit', component: PaymentdepositComponent },
      { path: 'tardepayment', component: TradepaymentComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentsRoutingModule { }
