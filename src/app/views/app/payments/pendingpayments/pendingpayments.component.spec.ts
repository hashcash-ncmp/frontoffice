import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingpaymentsComponent } from './pendingpayments.component';

describe('PendingpaymentsComponent', () => {
  let component: PendingpaymentsComponent;
  let fixture: ComponentFixture<PendingpaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingpaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingpaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
