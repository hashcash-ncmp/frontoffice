import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/data/api.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { RANGES } from 'src/main';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-pendingpayments',
  templateUrl: './pendingpayments.component.html',
  styleUrls: ['./pendingpayments.component.scss']
})
export class PendingpaymentsComponent implements OnInit {
  maxDate: Date = new Date();
  public fromdate: string = '';
  public toDate: string = '';
  public PaymentType: string = '';
  public ProductType: string = '';
  public Tcn: any;
  public paymetdtl = [];
  public bankDetailsList = [];
  public PaymentsTyp: any;
  public notrequired: boolean = false;
  modalRef: BsModalRef;
  PaymentID: any;
  amount: any;
  fdatecheck: boolean = false;
  tdatecheck: boolean = false;
  typecheck: boolean = false;
  bsValue = new Date();
  bsRangeValue: Date[];
  ranges: any = [];
  visibility: boolean = false;

  //  currentDate: any = new Date().toISOString().slice(0, 10);
  //  currentYear: any = new Date().getFullYear();
  //  currentMonth: any = (new Date().getMonth()) + 1;
  //   currentDay: any = new Date().getDate();

  //  '{year:currentYear,month:currentMonth,day:currentDay}'
  updateForm: FormGroup = new FormGroup({
    acknowledgementId: new FormControl('', Validators.required),
    bankId: new FormControl('', Validators.required),
    amount: new FormControl(''),
    paydate: new FormControl('', Validators.required),

  });
  constructor(private router: Router, private notifications: NotificationsService, private spinner: NgxSpinnerService, private _ApiService: ApiService, private modalService: BsModalService, private translate: TranslateService) {
    this.ranges = RANGES;
  }
  ngOnInit(): void {
    this.Getpaymenttype();
    // document.getElementById('payment-tbl').style.display = 'none';
    this.visibility = false;
    this.CheckCompanyId();

  }
  CheckCompanyId() {
    var companyID = localStorage.getItem('CompanyID');
    if (!companyID) {
      this.router.navigate(['user/login']);
      localStorage.removeItem('CompanyID');
    }
  }
  Getpaymenttype() {
    this.spinner.show();
    this._ApiService.GetpaymentType()
      .subscribe(data => {
        this.spinner.hide();
        if (data.error.errorData == 0) {
          this.PaymentsTyp = data.paymentCategoryList;
          // console.log('----------', data);
        }
      })
  }
  GetpaymentType(event) {
    var paymentId = event;
    if (paymentId == 1 || paymentId == 2) {
      this.notrequired = true;
    }
    else {
      this.notrequired = false;

    }

  }
  GetpendingPayment() {
    this.paymetdtl = [];
    let paypentdata = {};
    var companyID = localStorage.getItem('CompanyID');
    this.fromdate = this.formatDate(this.bsRangeValue[0]);
    this.toDate = this.formatDate(this.bsRangeValue[1]);
    if (this.fromdate != "" && this.toDate != "" && companyID != "" && this.PaymentType) {
      paypentdata['companyId'] = companyID;//'128';// 
      paypentdata['startDate'] = this.fromdate;//"2020-03-01";
      paypentdata['endDate'] = this.toDate; //
      paypentdata['paymentCategoryId'] = this.PaymentType;
      this.fdatecheck = false;
      this.tdatecheck = false;
      this.typecheck = false;
      this.spinner.show();
      this._ApiService.GetPendingpaymentType(paypentdata)
        .subscribe(data => {
          this.spinner.hide();
          if (data.error.errorData == 0 && data.error.errorMsg != 'No data!') {
            //document.getElementById('payment-tbl').style.display = 'block';
            this.visibility = true;
            this.paymetdtl = data.paymentHistoryList;
            console.log('----------dhriti', data);

          }
          else {
            // document.getElementById('payment-tbl').style.display = 'none';
            this.visibility = false;
            this.notifications.create(
              'Done',
              data.error.errorMsg,
              NotificationType.Bare,
              { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
            );
          }
        })
    }
    else if (this.fromdate == "") {
      this.fdatecheck = true;
    }
    else if (this.toDate == "") {
      this.tdatecheck = true;
    }
    else if (this.PaymentType == "") {
      this.typecheck = true;
    }
    else {
      this.typecheck = false;
      this.fdatecheck = false;
      this.tdatecheck = false;
    }
  }
  openpaymentModal(id, amnt, template) {
    this.modalRef = this.modalService.show(
      template, Object.assign({}, { class: 'gray modal-lg' })
    );
    this.PaymentID = id;

    this.amount = amnt;

    this.GetBankList();
  }
  GetBankList() {
    this.bankDetailsList = [];

    var companyID = localStorage.getItem('CompanyID');
    this._ApiService.BankBranchList(companyID)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.bankDetailsList = data.bankDetailsList;
          console.log('============', this.bankDetailsList);


        }
      })

  }
  UpdatePayment(form: any) {
    //console.log('------------', form);
    //{"paymentId":1, "paymentAcknowledgementId":"123QWE", "paymentBankId":"1", "amount":"100.98", "paymentDate":"2021-02-17"}
    let paydtlform = {};
    paydtlform['paymentId'] = this.PaymentID;
    paydtlform['paymentAcknowledgement'] = form.value.acknowledgementId;
    paydtlform['paymentBankId'] = form.value.bankId;
    paydtlform['amount'] = form.value.amount;
    paydtlform['paymentDate'] = form.value.paydate;
    this._ApiService.Updatepayment(paydtlform)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.bankDetailsList = data.bankDetailsList;
          console.log('============', this.bankDetailsList);
          this.modalRef.hide();
          this.notifications.create(
            'Done',
            'Payment Updated successfully!will take few days to approve from admin',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
          this.GetpendingPayment();
        }
        else {
          this.notifications.create(
            'Done',
            data.error.errorMsg,
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }
      })
  }

  formatDate(date: Date) {
    return date.getFullYear() + "-" + (date.getMonth() >= 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-" + (date.getDate() > 9 ? date.getDate() : "0" + date.getDate());
  }

  generatePaymentLink(paymentDtl) {
    let obj = {};
    obj["orderId"] = paymentDtl.billNo;
    obj["orderAmount"] = 11;  //paymentDtl.netPayable;
    obj["orderCurrency"] = "INR";
    obj["orderNote"] = "Payment For " + paymentDtl.paymentCategoryDesc + " Payment To " + paymentDtl.paymentTo;
    obj["customerEmail"] = localStorage.getItem("email");
    obj["customerName"] = localStorage.getItem("companyname");
    obj["customerPhone"] = localStorage.getItem("contactNo");
    obj["returnUrl"] = this._ApiService.WEBSERVICE + "/cashfreepayment/notify?paymentId=" + paymentDtl.paymentId+"&companyId="+localStorage.getItem('CompanyID')+"&paymentType="+paymentDtl.paymentCategoryId;
    obj["notifyUrl"] = "";
    obj["paymentType"] = paymentDtl.paymentCategoryId;
    obj["companyId"] = localStorage.getItem('CompanyID');
    obj["commissionAmount"]=paymentDtl.commAmount>0?2:0;
    this.spinner.show();
    this._ApiService.generatePaymentLink(obj).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          window.location.href = data.paymentLink;
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  onError(message: string): void {
    this.notifications.create(this.translate.instant('alert.error'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 3000, showProgressBar: true });
  }

  onSuccess(message: string): void {
    this.notifications.create(this.translate.instant('alert.success'),
      this.translate.instant(message),
      NotificationType.Success, { timeOut: 3000, showProgressBar: true });
  }
}
