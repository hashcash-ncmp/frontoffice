import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NotificationsService, SimpleNotificationsModule } from "angular2-notifications";
import { PaymentsRoutingModule } from './payments-routing.module';
import { PaymentsComponent } from './payments.component';
import { PendingpaymentsComponent } from './pendingpayments/pendingpayments.component';
import { ConfirmpaymentsComponent } from './confirmpayments/confirmpayments.component';
import { PaymenthistoryComponent } from './paymenthistory/paymenthistory.component';
import { PaymentdepositComponent } from './paymentdeposit/paymentdeposit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { TradepaymentComponent } from './tradepayment/tradepayment.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [PaymentsComponent, PendingpaymentsComponent, ConfirmpaymentsComponent, PaymenthistoryComponent, PaymentdepositComponent, TradepaymentComponent],
  imports: [
    SharedModule,
    LayoutContainersModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PaymentsRoutingModule,
    SimpleNotificationsModule.forRoot(),
    ReactiveFormsModule,
    NgxSpinnerModule,
    BsDatepickerModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot()

  ],
  providers: [BsModalService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PaymentsModule { }
