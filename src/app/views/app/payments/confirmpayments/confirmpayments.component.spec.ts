import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmpaymentsComponent } from './confirmpayments.component';

describe('ConfirmpaymentsComponent', () => {
  let component: ConfirmpaymentsComponent;
  let fixture: ComponentFixture<ConfirmpaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmpaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmpaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
