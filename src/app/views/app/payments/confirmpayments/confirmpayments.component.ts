import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/data/api.service';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { RANGES } from 'src/main';

@Component({
  selector: 'app-confirmpayments',
  templateUrl: './confirmpayments.component.html',
  styleUrls: ['./confirmpayments.component.scss']
})
export class ConfirmpaymentsComponent implements OnInit {
  maxDate: Date = new Date();
  public fromdate: string = '';
  public toDate: string = '';
  public PaymentType: string = '';
  public ProductType: string = '';
  public Tcn: string = '';
  public confirmpaymentdtl = [];
  public PaymentsTyp: string = '';
  fdatecheck: boolean = false;
  tdatecheck: boolean = false;
  typecheck: boolean = false;
  public notrequired: boolean = true;
  ranges: any = [];
  bsValue = new Date();
  bsRangeValue: Date[];
  visibility:boolean=false;
  constructor(private router: Router, private notifications: NotificationsService, private spinner: NgxSpinnerService, public _ApiService: ApiService) {
    this.ranges = RANGES;
  }

  ngOnInit(): void {
    this.Getpaymenttype();
    // document.getElementById('bank-tbl').style.display = 'none';
    this.CheckCompanyId();
  }
  CheckCompanyId() {
    var companyID = localStorage.getItem('CompanyID');
    if (!companyID) {
      this.router.navigate(['user/login']);
      localStorage.removeItem('CompanyID');
    }
  }
  Getpaymenttype() {
    this.spinner.show();
    this._ApiService.GetpaymentType()
      .subscribe(data => {
        this.spinner.hide();
        if (data.error.errorData == 0) {
          this.PaymentsTyp = data.paymentCategoryList;
          //  console.log('----------',data);

        }
      })
  }
  Getconfirmpayments() {
    debugger;
    //{"companyId":61,"status":1,
    //"startDate":"2020-03-01","endDate":"2021-03-01",
    //"paymentType":"Membership Fee"}
    this.fromdate = this.formatDate(this.bsRangeValue[0]);
    this.toDate = this.formatDate(this.bsRangeValue[1]);
    var companyID = localStorage.getItem('CompanyID');
    if (this.fromdate != null && this.toDate != null  && companyID != "" && this.PaymentType) {
      let conpayform = {};
      conpayform['companyId'] = companyID;//'61';
      conpayform['startDate'] = this.fromdate,//"2020-03-01"//
        conpayform['endDate'] = this.toDate,//"2021-03-01"//
        conpayform['paymentCategoryId'] = this.PaymentType;
      this.spinner.show();
      this.typecheck = false;
      this.fdatecheck = false;
      this.tdatecheck = false;
      this._ApiService.Confirmpayment(conpayform)
        .subscribe(data => {
          this.spinner.hide();
          if (data.error.errorData == 0) {
            // document.getElementById('bank-tbl').style.display = 'block';
            this.visibility=true;
            this.confirmpaymentdtl = data.paymentHistoryList;
            console.log('--------', this.confirmpaymentdtl);

          }
          else {
            this.notifications.create(
              'Done',
              data.error.errorMsg,
              NotificationType.Bare,
              { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
            );
          }

        })

    }

    else if (this.fromdate == "") {
      this.fdatecheck = true;
    }
    else if (this.toDate == "") {
      this.tdatecheck = true;
    }
    else if (this.PaymentType == "") {
      this.typecheck = true;
    }
    else {
      this.typecheck = false;
      this.fdatecheck = false;
      this.tdatecheck = false;
    }

  }
  GetpaymentType(event) {
    var paymentId = event;
    if (paymentId == 1 || paymentId == 2) {
      this.notrequired = true;
    }
    else {
      this.notrequired = false;

    }

  }

  formatDate(date: Date) {
    return date.getFullYear() + "-" + (date.getMonth() >= 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-" + (date.getDate() > 9 ? date.getDate() : "0" + date.getDate());
  }
}
