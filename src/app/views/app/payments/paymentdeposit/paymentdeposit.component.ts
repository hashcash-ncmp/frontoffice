import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/data/api.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { DECIMAL_REGEX, NUMBER_REGEX } from 'src/main';
@Component({
  selector: 'app-paymentdeposit',
  templateUrl: './paymentdeposit.component.html',
  styleUrls: ['./paymentdeposit.component.scss']
})
export class PaymentdepositComponent implements OnInit {
  paymentMode = 'none';
  type: string = 'none';
  companyId: any = 0;
  productList: Array<any> = [];
  MproductList: Array<any> = [];
  productIdListForPgd: Array<any> = [];
  productIdListForMargin: Array<any> = [];
  subProductListForPgd: Array<any> = [];
  subProductListForMargin: Array<any> = [];
  subProductListForWithdrawal: Array<any> = [];
  tradeLimitListForPgd: Array<any> = [];
  tradeLimitListForMargin: Array<any> = [];
  pgdAdvice: any = null;
  public contactNo;
  public address;
  public companyname;
  public email;
  public exchangeName: any;
  public exchangeAddress: any;
  public exchangeEmail: any;
  public exchangeMobile: any;
  public Errmessage: any;
  public valueArry = [];
  modalRef: BsModalRef;
  public Marginamount: any;
  public Netamount: any;
  public Munit: any;
  public Marginquantity: any;
  constructor(private router: Router, private spinner: NgxSpinnerService, private service: ApiService,
    private notifications: NotificationsService, private translate: TranslateService, private modalService: BsModalService) { }

  ngOnInit(): void {
    this.companyId = localStorage.getItem('CompanyID');
    this.GetAllProducts();
    this.GetAllMarginProducts();
    this.CheckCompanyId();

  }
  CheckCompanyId() {
    debugger;
    var companyID = localStorage.getItem('CompanyID');
    if (!companyID) {
      // this.router.navigate([environment.adminRoot+'/'+'user/login']);
      localStorage.removeItem('CompanyID');
      this.router.navigate(['/user/login']);
      //localStorage.removeItem('CompanyID');
    }
  }
  GetAllProducts() {
    this.spinner.show();
    this.service.GetAllProducts({ companyId: this.companyId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.productList = data.productList != null ? data.productList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  GetAllSubProducts(productId: number, index: number, type: string) {
    if ("pgd" == type) {
      this.productIdListForPgd[index] = productId;
      this.pgdFormArray.controls[index].get('subProductId').setValue('');
    }
    else if ("margin" == type) {
      this.productIdListForMargin[index] = productId;
      this.marginSettleFormArray.controls[index].get('subProductId').setValue('');
    } else if ("withdraw" == type) {
      this.tradeLimitObjForWithdraw = {};
      this.withdrawlForm.reset();
      this.withdrawlForm.get('productId').setValue(productId);
    }

    this.spinner.show();
    this.service.GetAllSubProducts({ companyId: this.companyId, productId: productId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          let list = data.subProductList != null ? data.subProductList : [];
          if ("pgd" == type)
            this.subProductListForPgd[index] = list;
          else if ("withdraw" == type)
            this.subProductListForWithdrawal = list;
          // else if ("margin" == type)
          // this.subProductListForMargin[index] = list;
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  GetTradeLimitBySubProduct(subProductId: number, index: number, type: string) {
    this.spinner.show();
    this.service.GetTradeLimitBySubProduct({
      companyId: this.companyId, productId: "pgd" == type ? this.productIdListForPgd[index] : this.productIdListForMargin[index],
      subProductId: subProductId
    }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          if (data.tradeLimit == null)
            this.onError('Trade Limit Not Found!');
          let object = data.tradeLimit != null ? data.tradeLimit : {};
          if ("pgd" == type) {
            this.tradeLimitListForPgd[index] = object;
            //this.pgdFormArray.controls[index].get('amount').setValue(object.amount);
            //this.pgdFormArray.controls[index].get('gst').setValue('18');
            //this.pgdFormArray.controls[index].get('amount').disable();
            //this.pgdFormArray.controls[index].get('gst').disable();
            this.pgdFormArray.controls[index].get('quantity').setValidators(Validators.compose([Validators.required, Validators.min(object.tradeLimit), Validators.pattern(DECIMAL_REGEX)]));
            this.pgdFormArray.controls[index].get('unit').setValue(object.tradeUnit);
          }
          // else if ("margin" == type) {
          //   this.tradeLimitListForMargin[index] = object;
          //   this.marginSettleFormArray.controls[index].get('amount').setValue(object.amount);
          //   this.marginSettleFormArray.controls[index].get('gst').setValue('18');
          //   this.marginSettleFormArray.controls[index].get('amount').disable();
          //   this.marginSettleFormArray.controls[index].get('gst').disable();
          //   //this.marginSettleFormArray.controls[index].get('quantity').setValidators(Validators.compose([Validators.required, Validators.min(object.tradeLimit)]));
          //   this.marginSettleFormArray.controls[index].get('unit').setValue(object.tradeUnit);
          // }
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  pgdForm: FormGroup = new FormGroup({
    pgdFormArray: new FormArray([new FormGroup({
      productId: new FormControl('', Validators.required),
      subProductId: new FormControl('', Validators.required),
      quantity: new FormControl('', Validators.compose([Validators.required, Validators.pattern(DECIMAL_REGEX)])),
      amount: new FormControl({ value: '', disabled: true }, Validators.required),
      gst: new FormControl({ value: '', disabled: true }, Validators.required),
      pgdAmt: new FormControl({ value: '', disabled: true }, Validators.required),
      unit: new FormControl('', Validators.required)
    })], Validators.required)
  });
  marginSettleForm: FormGroup = new FormGroup({
    marginSettleFormArray: new FormArray([new FormGroup({
      productId: new FormControl('', Validators.required),
      subProductId: new FormControl('', Validators.required),
      // depositQuantity: new FormControl('', Validators.required),
      quantity: new FormControl('', Validators.required),
      marginAmt: new FormControl({ value: '', disabled: true }, Validators.required),
      gst: new FormControl({ value: '', disabled: true }, Validators.required),
      netAmount: new FormControl({ value: '', disabled: true }, Validators.required),
      unit: new FormControl('', Validators.required)
    })], Validators.required)
  });

  get pgdFormArray(): FormArray {
    return this.pgdForm.get('pgdFormArray') as FormArray;
  }

  addPgdRow() {
    this.pgdFormArray.push(new FormGroup({
      productId: new FormControl('', Validators.required),
      subProductId: new FormControl('', Validators.required),
      quantity: new FormControl('', Validators.required),
      amount: new FormControl({ value: '', disabled: true }, Validators.required),
      gst: new FormControl({ value: '', disabled: true }, Validators.required),
      pgdAmt: new FormControl({ value: '', disabled: true }, Validators.required),
      unit: new FormControl('', Validators.required)
    }));
  }

  removePgdRow(index: number) {
    if (index > 0) {
      this.pgdFormArray.removeAt(index);
      this.subProductListForPgd.splice(index, 1);
      this.productIdListForPgd.splice(index, 1);
      this.tradeLimitListForPgd.splice(index, 1);
    }
  }

  onPgdFormSubmit(form: FormGroup, template: TemplateRef<any>) {
    let totalAmount = 0, totalNetAmount = 0;
    let array = form.get('pgdFormArray') as FormArray;
    for (let control of array.controls) {
      totalAmount = totalAmount + Number(control.get('amount').value);
      totalNetAmount = totalNetAmount + Number(control.get('pgdAmt').value);
    }
    let param = {
      depositHdr: {
        companyId: this.companyId,
        paymentOperationId: 1,
        paymentOperation: 'Deposit',
        paymentCategoryId: 2,
        paymentCategory: 'PGD',
        totalAmount: totalAmount,
        netAmount: totalNetAmount
      },
      pgdDetailList: (form.get('pgdFormArray') as FormArray).getRawValue()
    }
    if (param.depositHdr.totalAmount > 0 && param.depositHdr.netAmount > 0 && form.valid && param.pgdDetailList.length > 0) {
      this.spinner.show();
      this.service.GeneratePGDAdvice(param).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.productIdListForPgd = [];
            this.subProductListForPgd = [];
            this.tradeLimitListForPgd = [];
            array.clear();
            form.reset();
            this.addPgdRow();
            this.onSuccess(data.error.errorMsg);
            this.pgdAdvice = { adviceId: data.depositHdr.adviceId, pgdDetailList: data.pgdDetailList, totalAmount: data.depositHdr.totalAmount, Issudate: data.depositHdr.createdOn };
            this.contactNo = localStorage.getItem('contactNo');
            this.address = localStorage.getItem('address');
            this.companyname = localStorage.getItem('companyname');
            this.email = localStorage.getItem('email');
            this.Getexchangeaddress();

            this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'modal-xl' });
            ////////send mail
            let html = '';
            html += '<html><head></head><body>'
            html += '<style>';
            html += '.invoice-box {';
            html += '    max-width: 800px;';
            html += '    margin: auto;';
            html += '    padding: 30px;';
            html += '    border: 1px solid #eee;';
            html += '    box-shadow: 0 0 10px rgba(0, 0, 0, .15);';
            html += '    font-size: 16px;';
            html += '    line-height: 24px;';
            html += "    font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;";
            html += '    color: #555;}';

            html += '.invoice-box table {';
            html += '    width: 100%;';
            html += '    line-height: inherit;';
            html += '    text-align: left;';
            html += '}';

            html += '.invoice-box table td {';
            html += '    padding: 5px;';
            html += '    vertical-align: top;';
            html += '}';

            html += '.invoice-box table tr td:nth-child(2) {';
            html += '    text-align: right;';
            html += '}';

            html += '.invoice-box table tr.top table td {';
            html += '    padding-bottom: 20px;';
            html += '}';

            html += '.invoice-box table tr.top table td.title {';
            html += '    font-size: 45px;';
            html += '    line-height: 45px;';
            html += '    color: #333;';
            html += '}';

            html += '.invoice-box table tr.information table td {';
            html += '    padding-bottom: 40px;';
            html += '}';

            html += '.invoice-box table tr.heading td {';
            html += '    background: #eee;';
            html += '    border-bottom: 1px solid #ddd;';
            html += '    font-weight: bold;';
            html += '}';

            html += '.invoice-box table tr.details td {';
            html += '    padding-bottom: 20px;';
            html += '}';

            html += '.invoice-box table tr.item td{';
            html += '    border-bottom: 1px solid #eee;';
            html += '}';
            html += '';
            html += '.invoice-box table tr.item.last td {';
            html += '    border-bottom: none;';
            html += '}';

            html += '.invoice-box table tr.total td:nth-child(2) {';
            html += '    border-top: 2px solid #eee;';
            html += '    font-weight: bold;';
            html += '}';

            html += '@media only screen and (max-width: 600px) {';
            html += '    .invoice-box table tr.top table td {';
            html += '        width: 100%;';
            html += '        display: block;';
            html += '        text-align: center;';
            html += '    }';

            html += '    .invoice-box table tr.information table td {';
            html += '        width: 100%;';
            html += '        display: block;';
            html += '        text-align: center;';
            html += '    }';
            html += '}';

            html += '.rtl {';
            html += '    direction: rtl;';
            html += "    font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;";
            html += '}';

            html += ' .rtl table {';
            html += '    text-align: right;';
            html += ' }';

            html += '  .rtl table tr td:nth-child(2) {';
            html += '      text-align: left;';
            html += '  }';
            html += '  </style>';


            html += '<div class="invoice-box">';
            html += '<table cellpadding="0" cellspacing="0">';
            html += '<tr class="top">';
            html += ' <td colspan="2">';
            html += '<table>';
            html += '<tr>';
            html += '<td class="title"> ADVICE';

            html += '</td>';

            html += '<td>Advice #: ' + this.pgdAdvice.adviceId;
            html += '<br>Advice Issuance Date:' + this.pgdAdvice.Issudate;
            html += '</td>';
            html += ' </tr>';
            html += '</table>';
            html += '</td>';
            html += '</tr>';

            html += '<tr class="information">';
            html += '<td colspan="2">';
            html += '<table>';
            html += '<tr>';
            html += '<td>Company Name : ' + this.companyname + '<br> Company Phone : ' + this.contactNo + '<br> Exchange Address :' + this.address;
            html += '</td>';

            html += '<td>   Exchange Name : ' + this.exchangeName + '<br> Exchange Address : ' + this.exchangeAddress + '<br> Exchange Mobile : ' + this.exchangeMobile;
            html += '</td>';
            html += '</tr>';
            html += '</table>';
            html += '</td>';
            html += '</tr>';
            html += '<table>';
            html += '<tr class="heading">';
            html += '<td>Payment Description</td>';
            html += ' <td>Product</td>';
            html += ' <td>Trade Limit</td>';
            html += ' <td>Units</td>';
            html += ' <td>Amount</td>';
            html += '</tr>';
            html += '<tr>';
            for (var i = 0; i < this.pgdAdvice.pgdDetailList.length; i++) {
              html += '<tr>';
              html += ' <td>' + this.pgdAdvice.pgdDetailList[i].productName + '</td>';
              html += ' <td>' + this.pgdAdvice.pgdDetailList[i].quantity + '</td>';
              html += ' <td>' + this.pgdAdvice.pgdDetailList[i].unit + '</td>';
              html += ' <td>' + this.pgdAdvice.pgdDetailList[i].pgdAmt + '</td>';
              html += '</tr>';
            }

            html += '</table>'

            html += '<tr class="item">';
            html += '<td>Total Amount(In Rupees)';
            //  html += this.invoiceDescription
            html += '</td>';

            html += '<td>';
            html += this.pgdAdvice.totalAmount
            html += '</td>';
            html += '</tr>';
            html += '<tr class="total">';
            html += '<td></td>';
            //            html += '<td> GST: ' + parseFloat(this.invoiceGST).toFixed(2) + '   </td>';
            html += '</tr>';
            html += '<tr class="total">';
            html += '<td></td>';
            //            html += '<td> Total: ' + (parseFloat(this.invoiceAmount) + parseFloat(this.invoiceGST)).toFixed(2) + '   </td>';
            html += '</tr>';
            html += '</table>';
            html += '<tr class="information">';
            html += '<td colspan="2">';
            html += '<table>';
            html += '<tr>';
            //  html += '<td> Bank Name : ' + this.invoiceBankName + '<br> Beneficiary : ' + this.invoiceBankBeneficiaryName + '<br> A/C No : ' + this.invoiceBankAccountNo + ' <br> IFSC : ' + this.invoiceBankIfscCode + ' <br> Type : ' + this.invoiceBankAccountType;
            html += '</td>';

            //html += '<td>Acme Corp.<br> John Doe<br> john@example.com';
            html += '</td>';
            html += '</tr>';
            html += '</table>';
            html += '</td>';
            html += '</tr>';
            html += '</div>';
            html += '</body>';
            console.log(html);


            var emailforAdvice = localStorage.getItem('email');
            let input = {
              "companyId": this.companyId,
              "emailId": emailforAdvice,
              "fileContent": html,
              "fileType": "pgd",
              "depositeId": data.depositHdr.depositId
            }
            this.service.GenarateInvoice(input)
              .subscribe(data => {
                if (data.error.errorData == 1) {
                  // this.data.alert(data.message, 'danger');
                  this.notifications.create('Error', data.error.errorMsg,
                    NotificationType.Bare, { theClass: 'outline primary', timeOut: 6000, showProgressBar: false });


                } else {
                  this.notifications.create('Done', data.error.errorMsg,
                    NotificationType.Bare, { theClass: 'outline primary', timeOut: 6000, showProgressBar: false });
                  //this.data.alert('Invoice send to your email', 'success');
                }
              });
            ////////send mail
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    } else {
      this.onError('Please Enter Valid Info!');
    }
  }

  Getexchangeaddress() {
    this.service.Getexchangedetails()
      .subscribe(data => {
        var exchangedtl = data.adminExchangeDetails;
        this.exchangeName = exchangedtl.exchangeName;
        this.exchangeAddress = exchangedtl.address;
        this.exchangeEmail = exchangedtl.email;
        this.exchangeMobile = exchangedtl.mobile;

      })
  }
  setNetAmountForPgd(index: number, qty: number) {
    debugger;
    let tradeLimitObj = this.tradeLimitListForPgd[index];
    if (tradeLimitObj != null && qty > 0 && tradeLimitObj.amount != null && tradeLimitObj.tradeLimit != null) {
      let amount = Number(tradeLimitObj.amount) / Number(tradeLimitObj.tradeLimit);
      let taxableAmount = Number(amount * qty);
      let netAmount = Number(taxableAmount) + Number(taxableAmount * 0.18);
      this.pgdFormArray.controls[index].get('amount').setValue(taxableAmount.toFixed(2));
      this.pgdFormArray.controls[index].get('gst').setValue((taxableAmount * 0.18).toFixed(2));
      this.pgdFormArray.controls[index].get('pgdAmt').setValue(netAmount.toFixed(2));
    } else {
      this.pgdFormArray.controls[index].get('amount').setValue('');
      this.pgdFormArray.controls[index].get('gst').setValue('');
      this.pgdFormArray.controls[index].get('pgdAmt').setValue('');
    }
  }


  get marginSettleFormArray(): FormArray {
    return this.marginSettleForm.get('marginSettleFormArray') as FormArray;
  }

  addMarginSettleRow() {
    this.marginSettleFormArray.push(new FormGroup({
      productId: new FormControl('', Validators.required),
      subProductId: new FormControl('', Validators.required),
      // depositQuantity: new FormControl('', Validators.required),
      // deficitQuantity: new FormControl('', Validators.required),
      // amount: new FormControl({ value: '', disabled: true }, Validators.required),
      quantity: new FormControl('', Validators.required),
      marginAmt: new FormControl({ value: '', disabled: true }, Validators.required),
      gst: new FormControl({ value: '', disabled: true }, Validators.required),
      netAmount: new FormControl({ value: '', disabled: true }, Validators.required),
      unit: new FormControl('', Validators.required)
    }));
  }

  removeMarginSettleRow(index: number) {
    if (index > 0) {
      this.marginSettleFormArray.removeAt(index);
      this.subProductListForMargin.splice(index, 1);
      this.productIdListForMargin.splice(index, 1);
      this.tradeLimitListForMargin.splice(index, 1);
    }
  }

  onMarginSettleFormSubmit(form: FormGroup, template: TemplateRef<any>) {
    //  alert('Comming Soon......')
    let totalAmount = 0, totalNetAmount = 0;
    let array = form.get('marginSettleFormArray') as FormArray;
    for (let control of array.controls) {
      totalAmount = totalAmount + Number(control.get('netAmount').value);
      totalNetAmount = totalNetAmount + Number(control.get('marginAmt').value);
    }
    let param = {
      depositHdr: {
        companyId: this.companyId,
        paymentOperationId: 1,
        paymentOperation: 'Deposit',
        paymentCategoryId: 2,
        paymentCategory: 'Margin Settle',
        totalAmount: totalAmount,
        netAmount: totalNetAmount
      },
      pgdDetailList: (form.get('marginSettleFormArray') as FormArray).getRawValue()
    }
    if (param.depositHdr.totalAmount > 0 && param.depositHdr.netAmount > 0 && form.valid && param.pgdDetailList.length > 0) {
      this.spinner.show();
      this.service.GenerateMarginAdvice(param).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.productIdListForPgd = [];
            this.subProductListForPgd = [];
            this.tradeLimitListForPgd = [];
            array.clear();
            form.reset();
            this.addPgdRow();
            this.onSuccess(data.error.errorMsg);
            this.pgdAdvice = { adviceId: data.depositHdr.adviceId, pgdDetailList: data.pgdDetailList, totalAmount: data.depositHdr.totalAmount, Issudate: data.depositHdr.createdOn };
            this.contactNo = localStorage.getItem('contactNo');
            this.address = localStorage.getItem('address');
            this.companyname = localStorage.getItem('companyname');
            this.email = localStorage.getItem('email');
            //  this.Getexchangeaddress();
            this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'modal-xl' });
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    } else {
      this.onError('Please Enter Valid Info!');
    }
  }

  setNetAmountForMargin(index: number, qty: number) {
    let amount = Number(this.marginSettleFormArray.controls[index].get('amount').value);
    let taxableAmount = Number(amount * qty);
    let netAmount = Number(taxableAmount) + Number(taxableAmount * 0.18);
    this.marginSettleFormArray.controls[index].get('netAmount').setValue(netAmount);
  }

  onError(message: string): void {
    this.notifications.create(this.translate.instant('alert.error'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 3000, showProgressBar: true });
  }

  onSuccess(message: string): void {
    this.notifications.create(this.translate.instant('alert.success'),
      this.translate.instant(message),
      NotificationType.Success, { timeOut: 3000, showProgressBar: true });
  }
  CheckMarginflag(template: TemplateRef<any>) {
    debugger;
    let marginInput = {};
    var companyID = localStorage.getItem('CompanyID');
    marginInput['companyId'] = companyID;
    this.service.checkMarginFlag(marginInput)
      .subscribe(data => {
        //   console.log('flag---------', data);
        if (data['error'].errorData == '0') {
          if (data['error'].errorMsg == 'No Margin Deficit') {
            this.Errmessage = 'No Margin Deficit';
            this.modalRef = this.modalService.show(
              template, Object.assign({}, { class: 'gray modal-md' })
            );

          }
          else if (data['error'].errorMsg == 'There is Margin Deficit!') {
            this.GetMarginValues();
            this.modalRef = this.modalService.show(
              template, Object.assign({}, { class: 'gray modal-md' })
            );
          }
        }
      })
  }
  GetMarginValues() {
    debugger;
    this.valueArry = [];
    let marginInput = {};
    var companyID = localStorage.getItem('CompanyID');
    marginInput['companyId'] = companyID;
    this.service.GetMarginValue(marginInput)
      .subscribe(data => {

        if (data.error.errorData == '0') {
          this.valueArry = data.marginList;
          console.log('flag---------', this.valueArry);
        }

      })
  }
  checkForvalue(i) {
    debugger;
    for (var j = 0; j < this.valueArry.length; j++) {
      //       companyId: 168
      // depositId: 0
      // marginAmt: 100000
      // marginDtlId: 0
      // productId: 7
      // quantity: 2000
      // subProductId: 7
      // unit: "TON"

      var ProductId = this.marginSettleFormArray.controls[i].get('productId').value;
      var SubproductId = this.marginSettleFormArray.controls[i].get('subProductId').value;
      if (this.valueArry[j].productId == ProductId && this.valueArry[j].subProductId == SubproductId) {
        this.Marginquantity = this.valueArry[j].quantity;
        this.Munit = this.valueArry[j].unit;
        this.Marginamount = this.valueArry[j].marginAmt;
        this.Netamount = this.valueArry[j].marginAmt + (this.valueArry[j].marginAmt * 18 / 100);
        this.marginSettleFormArray.controls[i].get('marginAmt').setValue(this.Marginamount);
        this.marginSettleFormArray.controls[i].get('quantity').setValue(this.Marginquantity);
        this.marginSettleFormArray.controls[i].get('netAmount').setValue(this.Netamount);
        this.marginSettleFormArray.controls[i].get('gst').setValue('18%');
        this.marginSettleFormArray.controls[i].get('unit').setValue(this.Munit);

      }

      // this.marginSettleFormArray
      // alert(this.marginSettleFormArray.controls[i].get('productId').value);
      // alert(this.marginSettleFormArray.controls[i].get('subProductId').value);
    }
  }
  GetAllMarginProducts() {
    this.spinner.show();
    this.service.GetAllMarginProducts({ companyId: this.companyId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.MproductList = data.productList != null ? data.productList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  GetAllMarginSubProducts(productId: number, index: number, type: string) {
    if ("pgd" == type) {
      this.productIdListForPgd[index] = productId;
      this.pgdFormArray.controls[index].get('subProductId').setValue('');
    }
    else if ("margin" == type) {
      this.productIdListForMargin[index] = productId;
      this.marginSettleFormArray.controls[index].get('subProductId').setValue('');
    }

    this.spinner.show();
    this.service.GetAllMarginSubProducts({ companyId: this.companyId, productId: productId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          let list = data.subProductList != null ? data.subProductList : [];
          if ("pgd" == type)
            this.subProductListForPgd[index] = list;
          else if ("margin" == type)
            this.subProductListForMargin[index] = list;
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  //withdrawal------------------------//
  withdrawlForm: FormGroup = new FormGroup({
    productId: new FormControl(null, Validators.required),
    subProductId: new FormControl(null, Validators.required),
    unit: new FormControl(null, Validators.required),
    quantity: new FormControl(null, Validators.compose([Validators.required, Validators.pattern(NUMBER_REGEX)])),
    availablePgd: new FormControl(null, Validators.required),
    withdrawlAmount: new FormControl(null, Validators.required),
    availableAmount: new FormControl(null, Validators.required)
  });
  tradeLimitObjForWithdraw = {};
  getAvaliablePgd(subProductId: number) {
    this.resetWithdrawForm();
    this.spinner.show();
    this.service.Getbalance({ companyId: this.companyId, productId: this.withdrawlForm.get('productId').value, subProductId: subProductId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          let list = data.walletInfo != null ? data.walletInfo : [];
          this.withdrawlForm.get('availablePgd').setValue(list[0] != null ? list[0].currentQty : 0);
          this.withdrawlForm.get('availableAmount').setValue(list[0] != null ? list[0].currentPgdAmt : 0);
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }
  GetWithdrawTradeLimit(subProductId: number) {
    this.tradeLimitObjForWithdraw = {};
    this.resetWithdrawForm();
    this.spinner.show();
    this.service.GetTradeLimitBySubProduct({ companyId: this.companyId, productId: this.withdrawlForm.get('productId').value, subProductId: subProductId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          if (data.tradeLimit == null) {
            this.onError('Unit Not Found!');
          } else {
            let object = data.tradeLimit != null ? data.tradeLimit : {};
            this.tradeLimitObjForWithdraw = object;
            this.withdrawlForm.get('quantity').setValidators(Validators.compose([Validators.required, Validators.min(object.tradeLimit), Validators.pattern(NUMBER_REGEX)]));
            this.withdrawlForm.get('unit').setValue(object.tradeUnit);
          }
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }
  resetWithdrawForm() {
    this.withdrawlForm.get('unit').setValue(null);
    this.withdrawlForm.get('quantity').setValue(null);
    this.withdrawlForm.get('availablePgd').setValue(null);
    this.withdrawlForm.get('withdrawlAmount').setValue(null);
    this.withdrawlForm.get('availableAmount').setValue(null);
  }
  calculateAmount(pgd: number, amount: number, limit: number) {
    this.withdrawlForm.get('withdrawlAmount').setValue(0);
    if (pgd % 100 == 0 && limit > 0) {
      if (pgd <= this.withdrawlForm.get('availablePgd').value)
        this.withdrawlForm.get('withdrawlAmount').setValue((amount * pgd) / limit);
      else {
        this.withdrawlForm.get('quantity').setValue(null);
        this.onError('PGD is greater than available pgd!');
      }
    }
    else {
      this.withdrawlForm.get('quantity').setValue(null);
      if (limit != null)
        this.onError('PGD should be multiple of ' + limit + '!');
      else
        this.onError('PGD Details Not Found!');
    }
  }
  onWithdrawlFormSubmit(form: FormGroup) {
    let param = {
      withdrawalHdr: {
        companyId: this.companyId,
        paymentOperationId: 2,
        paymentOperation: 'Withdraw',
        paymentCategoryId: 3,
        paymentCategory: 'Withrawal',
        totalAmount: form.get('withdrawlAmount').value,
        netAmount: form.get('withdrawlAmount').value
      },
      pgdDetailList: [form.value]
    }
    this.spinner.show();
    this.service.generateWithdrawalAdvice(param).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.withdrawlForm.reset();
          this.onSuccess(data.error.errorMsg);
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }
}
