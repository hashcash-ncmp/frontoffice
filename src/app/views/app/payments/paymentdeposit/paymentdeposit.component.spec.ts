import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentdepositComponent } from './paymentdeposit.component';

describe('PaymentdepositComponent', () => {
  let component: PaymentdepositComponent;
  let fixture: ComponentFixture<PaymentdepositComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentdepositComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentdepositComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
