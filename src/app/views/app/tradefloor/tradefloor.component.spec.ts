import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradefloorComponent } from './tradefloor.component';

describe('TradefloorComponent', () => {
  let component: TradefloorComponent;
  let fixture: ComponentFixture<TradefloorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradefloorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradefloorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
