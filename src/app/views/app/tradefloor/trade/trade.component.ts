import { Component, OnInit, OnDestroy, TemplateRef } from '@angular/core';
import { ApiService } from 'src/app/data/api.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';

@Component({
  selector: 'app-trade',
  templateUrl: './trade.component.html',
  styleUrls: ['./trade.component.scss']
})
export class TradeComponent implements OnInit {
  public allproducts = [];
  public allcommodity = [];
  public allvariety = [];
  public grade = [];
  public SavedProd = [];
  public historydata = [];
  public errormessage: any = "";
  public Priceerrormessage: any = "";
  public NodataError: any = "";
  public SellErr: any = "";
  public BuyErr: any = "";
  product: any = "";
  productId: any;
  Ordertype: any;
  NigotiationT: any;
  ProductList: any;
  modalRef: BsModalRef;
  NigotiationFlag: boolean = false;
  public Equantity: any;
  public Eid: any;
  public Eunit: any;
  //public activeT:boolean=true;
  public notrequired: boolean = false;
  public deliverytype: any;
  public paymentMode = [];
  public UnitAll: any;
  public Unitone: any;
  public Unittwo: any;
  public Unitthree: any;
  public Ptermsdata: any;
  public allcity: any;
  //public City: string = '';
  // public CityP: string = '';

  public CityId: string = '';
  public tableHeader: any;
  public tabledata: any;
  public ImageArrayone: any = [];
  public ImageArraytwo: any = [];
  public imagecheck: boolean = false;
  public imgsizecheck: boolean = false;
  public imgsizecheckcrt: boolean = false;
  public BusinessTypeId: any;
  public CityList = [];
  public Canceldata = [];
  public OpenOrder = [];
  public termList = [];
  //public activeTline:boolean=true;
  public Orderdata: any;
  public Orderbook = [];
  public SellOrderbook = [];
  public BuyOrderbook = [];
  public Matchingdata = [];
  public Nigotiationdata = [];
  public theCheckbox: boolean = false;
  orderType: any;
  OrderId: any;
  public OPrice: any;
  public PriceUnit: any;
  public Oquantity: any;
  public QUnit: any;
  // Price: any;
  Quantity: any;
  editOrderForm: FormGroup = new FormGroup({
    Orderprice: new FormControl(''),
    OrderQuantity: new FormControl('', Validators.required),
    ConpanyName: new FormControl(''),
    ProductName: new FormControl(''),
    SubProductName: new FormControl(''),
    // cityState: new FormControl(''),
    state: new FormControl(''),
    region: new FormControl(''),
    district: new FormControl(''),
    city: new FormControl(''),

    EmdPercent: new FormControl(''),
    SeasonYear: new FormControl(''),
    DeliveryType:new FormControl(''),
    VarietyName: new FormControl(''),
    PaymentMode: new FormControl(''),
    PaymentTimeline: new FormControl(''),
    Grade: new FormControl(''),
    OrderDate: new FormControl(''),
    ExpireTime: new FormControl('')
  })
  // editOpenOrderForm: FormGroup = new FormGroup({
  //   OrderQuantity: new FormControl('', Validators.required),
  //   QuantityUnit: new FormControl('', Validators.required),
  // })
  // offerForm: FormGroup = new FormGroup({
  //   term: new FormControl('', Validators.required),
  //   pTerm: new FormControl('', Validators.required),
  //   tradetype: new FormControl('', Validators.required),
  //   productId: new FormControl('', Validators.required),
  //   commodityId: new FormControl('', Validators.required),
  //   varietyId: new FormControl('', Validators.required),
  //   grade: new FormControl('', Validators.required),
  //   harvestDate: new FormControl('', Validators.required),
  //   deliveryType: new FormControl('', Validators.required),
  //   Quantity: new FormControl('', Validators.required),
  //   Qunit: new FormControl('', Validators.required),
  //   Location: new FormControl('', Validators.required),
  //   LocationID: new FormControl('', Validators.required),
  //   price: new FormControl('', Validators.required),
  //   Punit: new FormControl('', Validators.required),
  //   Pmode: new FormControl('', Validators.required),
  //   Ptimeline: new FormControl('', Validators.required),
  //   Pemd: new FormControl(''),
  //   Emdtimeline: new FormControl('', Validators.required),
  //   Pterms: new FormControl('', Validators.required),
  //   Exdate: new FormControl('', Validators.required),

  //   Pimage: new FormControl('', Validators.required),
  //   Qcertificate: new FormControl('', Validators.required)
  // })
  tabs = [
    { title: 'First', content: 'First Tab Content ---------------', active: true },
    { title: 'Second', content: 'Second Tab Content ---------------', },
    { title: 'Third', content: 'Third Tab Content -------------------', removable: true },
    { title: 'Four', content: 'Fourth Tab Content ---------------------------', disabled: true }
  ];
  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();
  minDate = new Date();
  //----------------cart dummy data
  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
  ];
  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  // public lineChartOptions;
  public lineChartOptions: (ChartOptions) = {
    responsive: true,
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: '#e5edef',
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];
  //------------
  public countries = [];
  public states = [];
  public statearray = [];
  public stateId: any;
  public zones = [];
  public region = [];
  public regionarray = [];
  public zonearray = [];
  public City = [];
  public cityarray = [];
  public zoneP: any;
  public stationP: any;
  public zoneFlag: any;
  public stationFlag: any;
  public flagStatematch: boolean = false;
  public flagRegionmatch: boolean = false;
  //--------------
  constructor(private _ApiService: ApiService, private notifications: NotificationsService, private modalService: BsModalService) {
    this.minDate.setDate(this.minDate.getDate() - 1);
    this.maxDate.setDate(this.maxDate.getDate() + 7);
    this.bsRangeValue = [this.bsValue, this.maxDate];
  }

  ngOnInit(): void {

    this.GetsavedProducts();

    this.GetInsightRestdata();
    this.GetNigotiationdata();
    this.Getstate();
    this.productId = 0;
    this.NigotiationT = 1;
    localStorage.setItem('ProcuctId', this.productId);
    localStorage.setItem('OrderType', this.Ordertype);
    localStorage.setItem('Nigotiation', this.NigotiationT);

    this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
    this.CityList = JSON.parse(localStorage.getItem('CityName'));

    this.BuyErr = "";
    this.SellErr = "";
    this.NodataError = "";
    this._ApiService.currentData.subscribe(data => {

      this.Orderdata = data;
      console.log('-------------Dhriti', this.Orderdata);
      
      if (this.Orderdata.e != null) {
        debugger;
        if (this.Orderdata.e == 'orderbook') {
         
       
          this.Orderbook = this.Orderdata.tR;
          for (var i = 0, j = 0, k = 0; i < this.Orderbook.length; i++) {
            if (this.Orderbook[i].orderType == 1) {
              this.BuyErr = "";
              this.BuyOrderbook[j] = this.Orderbook[i];
              this.BuyOrderbook.sort(function (a, b) {
                return a.productName.localeCompare(b.productName) || a.initialQuantity - b.initialQuantity;
              });
              j++;
            }
            if (this.Orderbook[i].orderType == 2) {
              this.SellErr = "";
              this.SellOrderbook[k] = this.Orderbook[i];
              this.SellOrderbook.sort(function (a, b) {
                return a.productName.localeCompare(b.productName) || b.initialQuantity - a.initialQuantity;
              });
              k++;
            }
            if (this.BuyOrderbook == undefined || this.BuyOrderbook.length == 0) {
              this.BuyErr = "No Data !"
            }
            else if (this.SellOrderbook.length == undefined || this.SellOrderbook.length == 0) {
              this.SellErr = "No Data !"
            }
          }
          //    console.log('------------SellOrderbook', this.SellOrderbook);
        }
        else if (this.Orderdata.e == 'matchInterest') {
          this.Matchingdata = this.Orderdata.tR;
        }

      }
      else {
        // this.BuyErr = "No Data !"
        // this.SellErr = "No Data !"

      }
    }
    );
    if (this.BusinessTypeId == 2) {

      //  this.offerForm.get('tradetype').setValue('1');

      localStorage.setItem('OrderType', '2');
    }
    else if (this.BusinessTypeId == 3) {
      //  this.offerForm.get('tradetype').setValue('2');



      localStorage.setItem('OrderType', '1');
    }
    else {

      localStorage.setItem('OrderType', '0');
    }
   this.GetorderRestdata();
    //this._ApiService._connect();
    setTimeout(() => {
      this._ApiService.sendmessagedata();
    }, 5000);
  }

  GetorderRestdata() {
    //  debugger;
    this.BuyErr = "";
    this.BuyErr = "";
    this.NodataError = "";
    this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
    var companyID = localStorage.getItem('CompanyID');
    var PId = localStorage.getItem('ProcuctId');
    var Ordertype = localStorage.getItem('OrderType');
    var Nigotiation = localStorage.getItem('Nigotiation');
    let ordeInput = {};
    ordeInput['companyId'] = companyID;
    ordeInput['pageNo'] = 1;
    ordeInput['noOfItemsPerPage'] = 50;
    ordeInput['productId'] = PId;
    ordeInput['businessTypeId'] = this.BusinessTypeId;
    ordeInput['negotiationTermsId'] = Nigotiation;
    ordeInput['orderType'] = Ordertype;
    this._ApiService.GetorderRest(ordeInput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.Orderbook = data.orderList;
          console.log('ORDERBOOK', this.Orderbook);
          debugger;
          this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
          if (this.BusinessTypeId == 1) {
            debugger;
            if (this.Orderbook != undefined) {
              for (var i = 0, j = 0, k = 0; i < this.Orderbook.length; i++) {
                if (this.Orderbook[i].orderType == 1) {
                  this.BuyErr="";
                  this.BuyOrderbook[j] = this.Orderbook[i];
                  this.BuyOrderbook.sort(function (a, b) {
                    return a.productName.localeCompare(b.productName) || a.initialQuantity - b.initialQuantity;
                  });
                  //console.log('test------', this.BuyOrderbook);
                  j++;
                }
                else {
                  this.SellErr="";
                  this.SellOrderbook[k] = this.Orderbook[i];
                  this.SellOrderbook.sort(function (a, b) {
                    return a.productName.localeCompare(b.productName) || b.initialQuantity - a.initialQuantity;
                  });
                  k++;
                 // console.log('------------SellOrderbook', this.SellOrderbook);
                }


              }
            }

            if (this.BuyOrderbook == undefined || this.BuyOrderbook.length == 0) {

              this.BuyErr = "No Data !"
            }
            if (this.SellOrderbook.length == undefined || this.SellOrderbook.length == 0) {

              this.SellErr = "No Data !"
            }
          }
          else {
            this.Orderbook = data.orderList;
          }

        }
        else if (data.error.errorData == 1) {
          this.NodataError = data.error.errorMsg;
          // console.log('ORDERBOOK', this.NodataError);
        }
      })

  }
  GetInsightRestdata() {

    var companyID = localStorage.getItem('CompanyID');

    let insightInput = {};
    insightInput['companyId'] = companyID;
    insightInput['pageNo'] = 1;
    insightInput['noOfItemsPerPage'] = 40;

    this._ApiService.GetmatchingInterestRest(insightInput)
      .subscribe(data => {
        if (data.error.errorData == 0 && data.error.errorData != 'no data') {
          this.Matchingdata = data.orderList;
          // console.log(data.orderList);
        }
      })

  }
  GetNigotiationdata() {
    var companyID = localStorage.getItem('CompanyID');
    let insightInput = {};
    insightInput['companyId'] = companyID;
    insightInput['pageNo'] = 1;
    insightInput['noOfItemsPerPage'] = 40;
    insightInput['orderType'] = 0;
    this._ApiService.GetNigotiation(insightInput)
      .subscribe(data => {
        if (data.error.errorData == 0 && data.error.errorData != 'no data') {
          this.Nigotiationdata = data.orderList;
          // console.log(data.orderList);
        }
      })

  }

  GetOrder() {
    var orderdata = {};
    var companyID = localStorage.getItem('CompanyID');
    orderdata['companyId'] = companyID;
    this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
    orderdata['businessTypeId'] = this.BusinessTypeId;
    orderdata['pageNo'] = 1;
    orderdata['noOfItemsPerPage'] = 50;
    this._ApiService.Insightdata(orderdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.Orderdata = data.orderList;
          // console.log(data.orderList);
        }
      })
  }
  Getstate() {
    this.zones = [];
    this.City = [];
    this.statearray = [];
    this.states = [];
    this.region = [];
    this.regionarray = [];
    // this.spinner.show();
    this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
    //  var orderType=this.offerForm.get('tradetype').value;
    // this.offerForm.get('state').setValue('');
    //alert(event);
    this._ApiService.getcompanystatedistrictForOrder(this.BusinessTypeId)
      .subscribe(data => {
        this.countries = data.locationList;
        //var temparrray = this.countries;
        console.log('-------------', this.countries);
        for (var i = 0; i < this.countries.length; i++) {
          if (this.countries != null) {

            var found = this.statearray.includes(this.countries[i].stateId);
            if (found == false) {
              this.states.push({ 'stateName': this.countries[i].stateName, 'stateId': this.countries[i].stateId });
            }
            this.statearray.push(this.countries[i].stateId)
          }
        }
        // console.log('-------------', this.states);
        // this.states=data.states;
        //  this.spinner.hide();
      })

  }

  // GetDistrict(data) {

  //   this.zones = [];
  //   this.zonearray = [];
  //   this.City = [];

  //   for (var j = 0; j < this.countries.length; j++) {
  //     if (this.countries[j].stateId == data) {
  //       var foundzone = this.zonearray.includes(this.countries[j].districtId);
  //       if (foundzone == false) {
  //         this.zones.push({ 'districtName': this.countries[j].districtName, 'districtId': this.countries[j].districtId });
  //       }
  //       this.zonearray.push(this.countries[j].districtId);
  //     }
  //   }
  //   console.log('-------------', this.zones);
  // }
  // CheckProductState(state) {
  //   var stateId = state;
  //   let inputData = {};
  //   inputData['productId'] = this.offerForm.get('productId').value;
  //   inputData['subProductId'] = this.offerForm.get('commodityId').value;
  //   inputData['stateId'] = stateId;
  //   this._ApiService.CheckProdState(inputData)
  //     .subscribe(data => {
  //       var response = data;
  //       if (response.error.errorData == 1) {
  //         this.offerForm.get('state').setValue('');
  //         this.notifications.create(
  //           'Error',
  //           response.error.errorMsg,
  //           NotificationType.Error,
  //           { theClass: 'outline primary', timeOut: 6000, showProgressBar: false }
  //         );
  //       }
  //       else {
  //         this.GetRegion(stateId);
  //       }
  //     })

  // }
  GetRegion(data) {
    debugger;
    this.zones = [];
    this.zonearray = [];
    this.region = [];
    this.regionarray = [];
    this.City = [];
    this.cityarray = [];

    localStorage.setItem('stateID', data)
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].stateId == data) {
        var foundzone = this.regionarray.includes(this.countries[j].regionId);
        if (foundzone == false) {
          this.region.push({ 'regionName': this.countries[j].regionName, 'regionId': this.countries[j].regionId });
        }
        this.regionarray.push(this.countries[j].regionId);
      }

    }

  }
  GetDistrictForRegion(data) {
    debugger;
    if (data != 0) {
      this.zones = [];
      this.zonearray = [];
      this.City = [];
      for (var j = 0; j < this.countries.length; j++) {
        if (this.countries[j].regionId == data) {
          var foundzone = this.zonearray.includes(this.countries[j].districtId);
          if (foundzone == false) {
            this.zones.push({ 'districtName': this.countries[j].districtName, 'districtId': this.countries[j].districtId });
          }
          this.zonearray.push(this.countries[j].districtId);
        }
      }
    }

    console.log('-------------', this.zones);
  }
  Getcity(data) {
    this.City = [];
    this.cityarray = [];
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].districtId == data) {
        var foundcity = this.cityarray.includes(this.countries[j].cityId);
        if (foundcity == false) {
          this.City.push({ 'cityName': this.countries[j].cityName, 'cityId': this.countries[j].cityId });
        }
        this.cityarray.push(this.countries[j].cityId);
      }
    }

  }
  OpenEditmodal(template, order) {
    debugger;
    this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
    console.log('OOOOOOOOOOOOOOOOO', order);
    this.zoneFlag = "";
    this.stationFlag = "";
    this.flagStatematch = false;
    this.flagRegionmatch = false;
    for (var i = 0; i < this.states.length; i++) {
      if (order.stateId == this.states[i].stateId) {
        this.GetRegion(order.stateId);
        this.flagStatematch = true;
      }

    }
    if (this.flagStatematch == true) {
      for (var j = 0; j < this.region.length; j++) {
        if (order.regionId == this.region[j].regionId) {
          this.flagRegionmatch = true;
          if (order.bidCount == 0) {
            this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'modal-xl' });
            localStorage.setItem('sateid', order.stateId);
            this.OrderId = order.orderId;
            // this.OPrice = price;
            this.PriceUnit = order.priceUnit;
            this.Oquantity = order.currentQuantity;
            this.QUnit = order.quantityUnit;
            this.editOrderForm.get('Orderprice').setValue(order.price);
            this.editOrderForm.get('OrderQuantity').setValue(order.currentQuantity);
            this.editOrderForm.get('ConpanyName').setValue(order.companyName);
            this.editOrderForm.get('ProductName').setValue(order.productName);
            this.editOrderForm.get('SubProductName').setValue(order.subProductName);
            this.editOrderForm.get('state').setValue(order.stateName);
            this.editOrderForm.get('region').setValue(order.regionName);
            var regonId = order.regionId;
            this.zoneFlag = order.districtId;
            this.stationFlag = order.cityId;
            //  alert(this.zoneFlag + "" + this.stationFlag);
            if (order.districtId == 0) {
              setTimeout(() => this.GetDistrictForRegion(regonId), 300);
              this.zoneP = order.districtId;

            }
            else {
              //
              this.zoneP = order.districtId;
              this.editOrderForm.get('district').setValue(order.districtName);
            }


            if (order.cityId == 0) {
              setTimeout(() => this.Getcity(order.districtId), 400);
              this.stationP = order.cityId;
            
            }
            else {
              this.stationP = order.cityId;
              this.editOrderForm.get('city').setValue(order.cityName);
            }
            // this.editOrderForm.get('district').setValue(order.districtName);
            // this.editOrderForm.get('city').setValue(order.cityName);

            this.editOrderForm.get('EmdPercent').setValue(order.emdPercent);
            this.editOrderForm.get('SeasonYear').setValue(order.seasonYear);
            this.editOrderForm.get('DeliveryType').setValue(order.deliveryType);
            this.editOrderForm.get('VarietyName').setValue(order.varietyName);
            this.editOrderForm.get('PaymentMode').setValue(order.paymentMode);
            this.editOrderForm.get('PaymentTimeline').setValue(order.paymentTimeline);
            this.editOrderForm.get('Grade').setValue(order.grade);
            this.editOrderForm.get('OrderDate').setValue(order.orderDate);
            this.editOrderForm.get('ExpireTime').setValue(order.expireTime);
            this.orderType = order.orderType;
            this.GetTablebyGrade(order.productId,order.subProductId,order.varietyName);
            if (this.NigotiationFlag == true) {
              this.editOrderForm.controls['Orderprice'].disable();
            }
            else if (this.NigotiationFlag == false) {
              this.editOrderForm.controls['Orderprice'].enable();
            }
          }
          else {
            this.notifications.create(
              'Done',
              'Offer already placed !!',
              NotificationType.Bare,
              { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
            );
            // alert('Offer already placed');
          }

        }

      }
      if (this.flagRegionmatch == false) {
        this.notifications.create(
          'Done',
          'Region does not match with your company location !!',
          NotificationType.Bare,
          { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
        );
      }
    }
    else {
      this.notifications.create(
        'Done',
        'State does not match with your company location !!',
        NotificationType.Bare,
        { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
      );
    }

  }
  GetTablebyGrade(productId, subProductId, varietyName) {
    let gradespdata = {}
    var PId = productId;// localStorage.getItem('ProductID');
    var subPId = subProductId;//localStorage.getItem('SubProductID');
    var vId = varietyName;// localStorage.getItem('VarietyID');
    gradespdata['pageNo'] = 1;
    gradespdata['noOfItemsPerPage'] = 10;
    gradespdata['productId'] = PId;
    gradespdata['subProductId'] = subPId;
    gradespdata['varietyName'] = vId;

    var stateId = localStorage.getItem('sateid');
    gradespdata['stateId'] = stateId;
    this._ApiService.gradespecification(gradespdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          // document.getElementById('grade-table').style.display = 'block';
          console.log('ggggggggg', data);
          this.tabledata = data.productGradeSpecDtl;
          this.tableHeader = Object.keys(this.tabledata[0]);
          console.log('=====', this.tableHeader);
        }
      })
  }
  checkquantity(event) {

    this.errormessage = "";

    if (event <= this.Oquantity && event > 0) {

      this.errormessage = "";
      // alert(this.errormessage);
    }
    else {
      this.errormessage = "Quantity can not be 0 or greater than order quantity ."
      // alert(this.errormessage);
    }
  }
  checkprice(event) {
    if (event <= this.OPrice && event > 0) {

      this.Priceerrormessage = "";

    }
    else {
      this.Priceerrormessage = "Price can not be 0 or greater than order price ."

    }
  }
  Editorder(form: any) {
      debugger;
    console.log('vvvvvvvvvvv', form.value);
    var orderdata = {};
    var companyID = localStorage.getItem('CompanyID');
    orderdata['companyId'] = companyID;
    orderdata['orderId'] = this.OrderId;
    if (form.value.Orderprice == undefined) {
      orderdata['bidPrice'] = this.OPrice;
    }
    else {
      orderdata['bidPrice'] = form.value.Orderprice;
    }

    orderdata['bidQuantity'] = form.value.OrderQuantity;
    var BusinesstypeId = localStorage.getItem('BusinessTypeId');
    orderdata['businessTypeId'] = BusinesstypeId;
    
    if (BusinesstypeId == '3' && this.editOrderForm.get('district').value != "" && this.editOrderForm.get('city').value != "") {
      if (this.errormessage == '' && this.Priceerrormessage == '') {
        orderdata['districtId'] =this.zoneP;
        orderdata['cityId'] = this.stationP;
        this._ApiService.Editdata(orderdata)
          .subscribe(data => {
            if (data.error.errorData == 0) {
              this.modalRef.hide();
              this.GetOrder();

              // console.log(data.orderList);
              this.notifications.create(
                'Done',
                'Order has been placed .',
                NotificationType.Success,
                { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
              );
            }
            else {
              this.modalRef.hide();
              this.notifications.create('Error',
                data.error.errorMsg,
                NotificationType.Error, { timeOut: 3000, showProgressBar: false });
              // this.notifications.create(
              //   'Error',
              //   data.error.errorMsg,
              //   NotificationType.Bare,
              //   { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
              // );
            }
          })
      } else {

      }
    }
    else if (BusinesstypeId != '3') {
      if (this.errormessage == '' && this.Priceerrormessage == '') {
        orderdata['districtId'] =this.zoneP;
        orderdata['cityId'] = this.stationP;
        this._ApiService.Editdata(orderdata)
          .subscribe(data => {
            if (data.error.errorData == 0) {
              this.modalRef.hide();
              this.GetOrder();

              // console.log(data.orderList);
              this.notifications.create(
                'Done',
                'Order has been placed .',
                NotificationType.Success,
                { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
              );
            }
            else {
              this.modalRef.hide();
              this.notifications.create('Error',
                data.error.errorMsg,
                NotificationType.Error, { timeOut: 3000, showProgressBar: false });
              // this.notifications.create(
              //   'Error',
              //   data.error.errorMsg,
              //   NotificationType.Bare,
              //   { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
              // );
            }
          })
      } else {

      }
    }
    else{
      this.modalRef.hide();
      this.notifications.create(
        'Done',
        'Zone and station is required .',
        NotificationType.Bare,
        { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
      );
    }


  }
  // EditOpenorderModal(template, id, quantity, unit, PId, SubPId) {
  //   this.Equantity = quantity;
  //   this.Eid = id;
  //   this.Eunit = unit
  //   this.editOpenOrderForm.get('OrderQuantity').setValue(this.Equantity);
  //   // this.editOpenOrderForm.get('QuantityUnit').setValue(this.Eunit);

  //   localStorage.setItem('ProductID', PId);
  //   localStorage.setItem('SubProductID', SubPId);
  //   this.GeSpecificUnit();
  //   this.modalRef = this.modalService.show(
  //     template, Object.assign({}, { class: 'gray modal-md' })
  //   );
  // }
  UpdateOrder(form: any) {
    //  debugger;
    let updateInputdata = {};
    var companyID = localStorage.getItem('CompanyID');
    updateInputdata['companyId'] = companyID;
    updateInputdata['orderId'] = this.Eid;
    // alert(this.editOpenOrderForm.get('OrderQuantity').value);
    if (form.value.OrderQuantity == undefined) {
      updateInputdata['quantity'] = form.value.OrderQuantity;
    }
    else {
      updateInputdata['quantity'] = this.Equantity;
    }
    updateInputdata['quantityUnit'] = this.Eunit;


    this._ApiService.OrderUpdate(updateInputdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.modalRef.hide();
          this.GetOpenOrder();
          // console.log(data.orderList);
          this.notifications.create(
            'Done',
            'Order updated successfully .',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }
      })



  }
  GetsavedProducts() {

    var productdata = {};
    var CompanyID = localStorage.getItem('CompanyID');

    productdata['companyId'] = CompanyID;
    this._ApiService.getselectedProducts(productdata)
      .subscribe(data => {

        this.ProductList = data.selectedProductList;

        for (var i = 0; i < this.ProductList.length; i++) {
          if (this.ProductList[i].isActive != 0) {
            this.SavedProd.push({ 'prodId': this.ProductList[i].productId, 'ProdName': this.ProductList[i].productName })
          }
        }

        console.log('---------999', this.SavedProd);

      })
  }
  SearchCityByname(Cityname) {

    let citydata = {};
    citydata['searchKeyword'] = Cityname;
    this._ApiService.searchCity(citydata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.allcity = data.cityList;
          console.log('----', this.allcity);

        }
      })
  }
  myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
  }
  filterFunction() {

    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput1");
    filter = input.value.toUpperCase();
    if (this.BusinessTypeId == 3) {
      this.allcity = this.CityList;
    }
    else {
      this.SearchCityByname(filter);
      var div = document.getElementById("myDropdown");
      a = div.getElementsByTagName("a");
      for (i = 0; i < a.length; i++) {
        var txtValue = a[i].textContent || a[i].innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          a[i].style.display = "";
        } else {
          a[i].style.display = "none";
        }
      }
    }
  }

  myFunctionP() {
    document.getElementById("myDropdownP").classList.toggle("show");
  }
  filterFunctionP() {

    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput2");
    filter = input.value.toUpperCase();

  }
  // getAsset(event) {
  //   //  debugger;
  //   //   alert(event.cityName+''+ event.cityId)
  //   this.City = event.cityName;
  //   this.CityId = event.cityId;
  //   document.getElementById("myDropdown").classList.toggle("show");

  // }
  getProduct(event) {
    //   alert(event.cityName+''+ event.cityId)
    this.product = event.ProdName;
    this.productId = event.prodId;
    localStorage.setItem('ProcuctId', this.productId);
    document.getElementById("myDropdownP").classList.toggle("show");
    this._ApiService.unsubscribe();
    this.Orderdata = "";
    this.Matchingdata = [];
    setTimeout(() => {
      this._ApiService.sendmessagedata();
    }, 3000);

  }
  GetOrderType(event) {
    console.log('----------', event.target.id);

    if (event.target.id == 'on') {
      this.Orderbook = null;
      localStorage.setItem('OrderType', '1');
      this._ApiService.unsubscribe();
      setTimeout(() => {
        this._ApiService.sendmessagedata();
      }, 3000);

    }
    else if (event.target.id == 'na') {
      this.Orderbook = null;
      localStorage.setItem('OrderType', '2');
      this._ApiService.unsubscribe();
      setTimeout(() => {
        this._ApiService.sendmessagedata();
      }, 3000);

    }
    else {
      this.Orderbook = null;
      localStorage.setItem('OrderType', '0');
      this._ApiService.unsubscribe();
      setTimeout(() => {
        this._ApiService.sendmessagedata();
      }, 3000);
    }



  }
  Getserch(event) {
    // debugger;
    if (event == false) {
      this.Orderbook = [];
      this.BuyOrderbook = [];
      this.SellOrderbook = [];
      this.BuyErr = "";
      this.SellErr = "";
      this.NigotiationFlag = false;
      localStorage.setItem('Nigotiation', '1');
      this._ApiService.unsubscribe();
      // this.Orderbook = null;
      this.GetorderRestdata();
      setTimeout(() => {
        this._ApiService.sendmessagedata();
      }, 3000);
    }
    else if (event == true) {
      this.Orderbook = [];
      this.BuyOrderbook = [];
      this.SellOrderbook = [];
      this.BuyErr = "";
      this.SellErr = "";
      this.NigotiationFlag = true;
      localStorage.setItem('Nigotiation', '2');
      this._ApiService.unsubscribe();
      //  this.Orderbook = null;
      this.GetorderRestdata();
      setTimeout(() => {
        this._ApiService.sendmessagedata();
      }, 3000);
    }
  }
  GetAllProduct() {
    let offerinput = {};
    var companyID = localStorage.getItem('CompanyID');
    offerinput['companyId'] = companyID;
    this._ApiService.Getallproduct(offerinput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.allproducts = data.productList;
          // console.log('ppppppppp', data.productList);

        }
      })
  }
  getcommodityProduct(event) {

    console.log(event);
    localStorage.setItem('ProductID', event);
    let commodityinput = {};
    var companyID = localStorage.getItem('CompanyID');
    commodityinput['productId'] = event;
    this._ApiService.Getcommodities(commodityinput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.allcommodity = data.subProductList;
          //console.log('ccccccccccccc', data);
        }
      })
  }
  Getallviriety(event) {
    // console.log(event);Canceldata
    var commodityId = event;
    localStorage.setItem('SubProductID', event);
    let varietyinput = {};
    var ProductID = localStorage.getItem('ProductID');
    var companyID = localStorage.getItem('CompanyID');
    varietyinput['companyId'] = companyID;
    varietyinput['productId'] = ProductID;
    varietyinput['subProductId'] = event;
    this._ApiService.Getvariety(varietyinput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.allvariety = data.productVarietyList;
          //console.log('vvvvvvvvvvvvv', data);
        }
      })
  }

  // SubmitOffer(form: any) {
  //   // form-data(not json)
  //   // String order = {"negotiationTermsId":1,
  //   // "orderType":1,
  //   // "companyId":122,
  //   // "productId":7,
  //   // "subProductId":8,
  //   // "varietyName":"PN23",
  //   // "grade":"PN23",
  //   // "quantity":200,
  //   // "quantityUnit":"QUINTAL",
  //   // "price":100.5,
  //   // "priceUnit":"KG",
  //   // "expiryDurationHours":12,
  //   // "harvestedMonthYear":"11/2018",
  //   // "deliveryTypeId":1,
  //   // "paymentModeId":3,
  //   // "cityId":3307,
  //   // "cityName":"Kharkutta",
  //   // "paymentTimeline":20,
  //   // "emdPercent":5.2,
  //   // "emdTimeline":10}

  //   // Multipart productImage (jpg/jpeg/png)
  //   // Multipart qualityCertificate (doc/docx/pdf)




  //   let offrtdata = new FormData();
  //   var offerdtl = {};
  //   if (this.theCheckbox == false) {
  //     offerdtl['negotiationTermsId'] = "1";
  //   }
  //   else {
  //     offerdtl['negotiationTermsId'] = "2";
  //   }

  //   var companyID = localStorage.getItem('CompanyID');
  //   offerdtl['companyId'] = companyID;
  //   offerdtl['orderType'] = form.value.tradetype;
  //   offerdtl['productId'] = form.value.productId;
  //   offerdtl['subProductId'] = form.value.commodityId;
  //   offerdtl['varietyName'] = form.value.varietyId;
  //   offerdtl['grade'] = form.value.grade;
  //   offerdtl['quantity'] = form.value.Quantity;
  //   offerdtl['quantityUnit'] = form.value.Qunit;
  //   offerdtl['price'] = form.value.price;
  //   offerdtl['priceUnit'] = form.value.Punit;
  //   offerdtl['expiryDurationHours'] = form.value.Exdate;
  //   offerdtl['harvestedMonthYear'] = form.value.harvestDate;
  //   offerdtl['deliveryTypeId'] = form.value.deliveryType;
  //   offerdtl['paymentModeId'] = form.value.Pmode;
  //   offerdtl['cityId'] = this.CityId;
  //   offerdtl['cityName'] = this.City;
  //   offerdtl['paymentTimeline'] = form.value.Ptimeline;
  //   offerdtl['emdPercent'] = form.value.Pemd;
  //   offerdtl['emdTimeline'] = form.value.Emdtimeline;
  //   offerdtl['paymentTerms'] = form.value.pTerm;
  //   console.log('---------', JSON.stringify(offerdtl));
  //   if (this.ImageArrayone != "" && this.ImageArraytwo != "") {
  //     var testfile = this.ImageArrayone[0].files[0];
  //     var testfiletwo = this.ImageArraytwo[0].files[0];
  //   }
  //   else {
  //     testfile = "";
  //     testfiletwo = "";
  //   }


  //   offrtdata.append('order ', JSON.stringify(offerdtl));
  //   offrtdata.append('productImage ', testfile);
  //   offrtdata.append('qualityCertificate  ', testfiletwo);

  //   if (companyID != "" && form.value.tradetype != '' && form.value.commodityId != '' && form.value.varietyId != '' && form.value.grade != '' && form.value.Quantity != '' && form.value.Qunit != '' && form.value.price != '' && form.value.Punit != '' && form.value.Exdate != '' && form.value.harvestDate != '' && form.value.deliveryType != '' && form.value.Pmode != '' && form.value.Emdtimeline != '' && this.City != "" && this.CityId != '') {
  //     this._ApiService.createOrder(offrtdata)
  //       .subscribe(data => {
  //         if (data.error.errorData == 0) {
  //           var orderId = data.orderId;
  //           this.GetOpenOrder();
  //           this.notifications.create(
  //             'Done',
  //             'Order Created Successfully',
  //             NotificationType.Bare,
  //             { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
  //           );
  //           this.offerForm.reset();
  //           this.GetAllProduct();
  //           this.Getdeliverytype();
  //           this.GetPaymentmode();
  //           this.GetOpenOrder();
  //           //    this.Getgrade("");
  //           this.allcommodity = [];
  //           this.allvariety = [];
  //           this.grade = [];
  //           this.GeSpecificUnit();
  //           document.getElementById('grade-table').style.display = 'none';
  //         }
  //         else {
  //           this.notifications.create('Error', data.error.errorMsg,
  //             NotificationType.Bare, { theClass: 'outline primary', timeOut: 6000, showProgressBar: false });

  //         }

  //       })
  //   }
  //   else {
  //     this.notifications.create(
  //       'Error',
  //       'Mandatory field missing',
  //       NotificationType.Bare,
  //       { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
  //     );
  //   }



  // }

  Getdeliverytype() {
    this._ApiService.DeliveryType()
      .subscribe(data => {
        if (data.error.errorData == 0) {

          this.deliverytype = data.deliveryList;
        }
      })
  }
  GeSpecificUnit() {
    var productID = localStorage.getItem('ProductID');
    var subProductID = localStorage.getItem('SubProductID');
    let unitdata = {};
    unitdata['productId'] = productID;
    unitdata['subProductId'] = subProductID;
    this._ApiService.Getunit(unitdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.UnitAll = data.productUnits;
          console.log('uuuuuuuu', this.UnitAll.purchasingMaxUnitCode);
          this.Unitone = this.UnitAll.purchasingMaxUnitCode;
          this.Unittwo = this.UnitAll.purchasingMinUnitCode;
          this.Unitthree = this.UnitAll.shipmentCode;
        }
      })

  }
  GetPaymentmode() {
    this._ApiService.PaymentType()
      .subscribe(data => {
        if (data.error.errorData == 0) {

          this.paymentMode = data.paymentModeList;

        }
      })
  }
  // GetpayPterms(ID) {

  //   if (ID == 3) {
  //     //  this.offerForm.get('Pemd').([]);
  //     this.offerForm.controls['Pemd'].enable();
  //     this.offerForm.controls['Emdtimeline'].enable();

  //   }
  //   else {
  //     this.offerForm.controls['Pemd'].disable();
  //     this.offerForm.controls['Emdtimeline'].disable();

  //   }
  //   let ptermdata = {};
  //   ptermdata['paymentModeId'] = ID;
  //   this._ApiService.Pterms(ptermdata)
  //     .subscribe(data => {
  //       if (data.error.errorData == 0) {
  //         this.Ptermsdata = data.termsList;
  //         this.termList = data.paymentTermList;
  //         console.log('-------------Ptermsdata', this.termList.length);
  //       }
  //     })

  // }
  GetOpenOrder() {
    var orderdata = {};
    var companyID = localStorage.getItem('CompanyID');
    orderdata['companyId'] = companyID;
    orderdata['pageNo'] = 1;
    orderdata['noOfItemsPerPage'] = 50;
    var BusinesstypeId = localStorage.getItem('BusinessTypeId');
    orderdata['businessTypeId'] = BusinesstypeId;

    this._ApiService.Opendeals(orderdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.OpenOrder = data.orderList;
          console.log('---------------', this.OpenOrder);
        }

      })
  }
  Gethistorydata() {
    var companyID = localStorage.getItem('CompanyID');
    let tadedata = {};
    tadedata['companyId'] = companyID;
    tadedata['pageNo'] = 1;
    tadedata['noOfItemsPerPage'] = 20;
    this._ApiService.Tradehistory(tadedata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.historydata = data.tradeHistoryList;

        }
      })
  }
  ApproveOrder(Id) {
    // alert(Id)
    var companyID = localStorage.getItem('CompanyID');
    let ordrdata = {};
    ordrdata['companyId'] = companyID;
    ordrdata['bidId'] = Id;
    this._ApiService.Approvorder(ordrdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.GetInsightRestdata();
          this.notifications.create(
            'Done',
            'Order Approved',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }
      })
  }
  RejectOrder(Id) {
    var companyID = localStorage.getItem('CompanyID');
    let ordrdata = {};
    ordrdata['companyId'] = companyID;
    ordrdata['bidId'] = Id;
    this._ApiService.Rejectorder(ordrdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.GetInsightRestdata();
          this.notifications.create(
            'Done',
            'Order Rejected',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }
        else {
          this.notifications.create(
            'Error',
            data.error.errorMsg,
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }
      })
  }
  // Cancelorder(orderID) {
  //   var orderdata = {};
  //   var companyID = localStorage.getItem('CompanyID');
  //   orderdata['companyId'] = companyID;
  //   orderdata['orderId'] = orderID;

  //   this._ApiService.Ordercancel(orderdata)
  //     .subscribe(data => {
  //       if (data.error.errorData == 0) {

  //         console.log(data);
  //         this.GetOpenOrder();
  //         this.GetCancelOrder();
  //         this.notifications.create(
  //           'Done',
  //           'Order Canceled Successfully',
  //           NotificationType.Bare,
  //           { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
  //         );
  //       }
  //     })

  // }
  // GetCancelOrder() {
  //   var orderdata = {};
  //   var companyID = localStorage.getItem('CompanyID');
  //   orderdata['companyId'] = companyID;
  //   orderdata['pageNo'] = 1;
  //   orderdata['noOfItemsPerPage'] = 50;
  //   this._ApiService.Canceldeals(orderdata)
  //     .subscribe(data => {
  //       if (data.error.errorData == 0) {
  //         if (data.error.errorData.errorMsg == "no data") {
  //           this.Canceldata = [];
  //         }
  //         else {
  //           this.Canceldata = data.orderList;
  //         }

  //         console.log('cancel', data.orderList);
  //       }
  //     })
  // }

  ngOnDestroy() {
    this._ApiService.unsubscribe();
    // this._ApiService._disconnect();
  }

}
