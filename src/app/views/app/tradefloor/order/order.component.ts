import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { ValueTransformer } from '@angular/compiler/src/util';
import { ApiService } from 'src/app/data/api.service';
import { NotificationsService, NotificationType } from 'angular2-notifications';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  public allproducts = [];
  public allcommodity = [];
  public allvariety = [];
  public grade = [];
  public SavedProd = [];
  public historydata = [];
  public termList = [];
  public City: string = '';
  public errormessage: any = "";
  public Priceerrormessage: any = "";
  public theCheckbox: boolean = false;
  product: any = "";
  productId: any;
  Ordertype: any;
  NigotiationT: any;
  ProductList: any;
  public tableHeader: any;
  public tabledata: any;
  NigotiationFlag: boolean = false;
  public Equantity: any;
  public Eid: any;
  public Eunit: any;
  public notrequired: boolean = false;
  public deliverytype: any;
  public paymentMode = [];
  public UnitAll: any;
  public Unitone: any;
  public Unittwo: any;
  public Unitthree: any;
  public Ptermsdata: any;
  public ImageArrayone: any = [];
  public ImageArraytwo: any = [];
  public imagecheck: boolean = false;
  public imgsizecheck: boolean = false;
  public imgsizecheckcrt: boolean = false;
  public BusinessTypeId: any;
  form: FormGroup;
  // Data: Array<any> = [
  //   { name: 'Pear', value: 'pear' },
  //   { name: 'Plum', value: 'plum' },
  //   { name: 'Kiwi', value: 'kiwi' },
  //   { name: 'Apple', value: 'apple' },
  //   { name: 'Lime', value: 'lime' }
  // ];
  offerForm: FormGroup = new FormGroup({
    term: new FormControl('', Validators.required),
    pTerm: new FormControl('', Validators.required),
    tradetype: new FormControl('', Validators.required),
    productId: new FormControl('', Validators.required),
    commodityId: new FormControl('', Validators.required),
    varietyId: new FormControl('', Validators.required),
    grade: new FormControl('', Validators.required),
    harvestDate: new FormControl('', Validators.required),
    deliveryType: new FormControl('', Validators.required),
    Quantity: new FormControl('', Validators.required),
    Qunit: new FormControl('', Validators.required),
    Location: new FormControl('', Validators.required),
    LocationID: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required),
    Punit: new FormControl('', Validators.required),
    Pmode: new FormControl('', Validators.required),
    Ptimeline: new FormControl('', Validators.required),
    Pemd: new FormControl(''),
    Emdtimeline: new FormControl('', Validators.required),
    Pterms: new FormControl('', Validators.required),
    Exdate: new FormControl('', Validators.required),

    Pimage: new FormControl('', Validators.required),
    Qcertificate: new FormControl('', Validators.required)
  })
  constructor(private fb: FormBuilder,private _ApiService: ApiService,private notifications: NotificationsService) {
    this.form = this.fb.group({
      checkArray: this.fb.array([], [Validators.required]),
     
    })
  }

  
  ngOnInit(): void {

  //   this.GetAllProduct();
  //   this.Getdeliverytype();
  //   this.GetPaymentmode();
     
  //   this.productId = 0;

  //   this.NigotiationT = 1;
  //   localStorage.setItem('ProcuctId', this.productId);
  //   localStorage.setItem('OrderType', this.Ordertype);
  //   localStorage.setItem('Nigotiation', this.NigotiationT);
  //   //this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
  //   // this.GetOrder();
  //   this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
  // //  this.CityList = JSON.parse(localStorage.getItem('CityName'));
  //   //console.log('BBBBB', this.CityList);
  
  //   if (this.BusinessTypeId == 2) {

  //     this.offerForm.get('tradetype').setValue('1');
  //     document.getElementById('tradetwo').style.display = 'none';
  //     document.getElementById('lbl-tradetwo').style.display = 'none';
  //     document.getElementById('sell').style.display = 'none';
  //     document.getElementById('na').style.display = 'none';
  //     document.getElementById('buy').style.display = 'none';
  //     document.getElementById('on').style.display = 'none';
  //     document.getElementById('all').style.display = 'none';
  //     document.getElementById('off').style.display = 'none';
  //     document.getElementById('tradeone').style.display = 'block';
  //     document.getElementById('lbl-tradeone').style.display = 'block';
  //     localStorage.setItem('OrderType', '2');
  //   }
  //   else if (this.BusinessTypeId == 3) {
  //     this.offerForm.get('tradetype').setValue('2');
  //     document.getElementById('lbl-tradeone').style.display = 'none';
  //     document.getElementById('tradeone').style.display = 'none';
  //     document.getElementById('sell').style.display = 'none';
  //     document.getElementById('na').style.display = 'none';
  //     document.getElementById('buy').style.display = 'none';
  //     document.getElementById('on').style.display = 'none';
  //     document.getElementById('all').style.display = 'none';
  //     document.getElementById('off').style.display = 'none';
  //     document.getElementById('tradetwo').style.display = 'block';
  //     document.getElementById('lbl-tradetwo').style.display = 'block';
  //     localStorage.setItem('OrderType', '1');
  //   }
  //   else {
  //     document.getElementById('tradeone').style.display = 'block';
  //     document.getElementById('tradetwo').style.display = 'block';
  //     document.getElementById('sell').style.display = 'block';
  //     document.getElementById('buy').style.display = 'block';
  //     document.getElementById('all').style.display = 'block';
  //     localStorage.setItem('OrderType', '0');
  //   }
   
  }
 
 
  // GetOrder() {

  //   var orderdata = {};
  //   var companyID = localStorage.getItem('CompanyID');
  //   orderdata['companyId'] = companyID;
  //   this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
  //   orderdata['businessTypeId'] = this.BusinessTypeId;
  //   orderdata['pageNo'] = 1;
  //   orderdata['noOfItemsPerPage'] = 50;
  //   this._ApiService.Insightdata(orderdata)
  //     .subscribe(data => {
  //       if (data.error.errorData == 0) {
  //         this.Orderdata = data.orderList;
  //         // console.log(data.orderList);
  //       }
  //     })
  // }
  
  // checkquantity(event) {

  //   this.errormessage = "";

  //   if (event <= this.Oquantity && event > 0) {

  //     this.errormessage = "";
  //     alert(this.errormessage);
  //   }
  //   else {
  //     this.errormessage = "Quantity can not be < 0 or greater than order price ."
  //     alert(this.errormessage);
  //   }
  // }
  // checkprice(event) {
  //   if (event > 0) {

  //     this.Priceerrormessage = "";

  //   }
  //   else {
  //     this.Priceerrormessage = "Price can not be < 0 or negative."

  //   }
  // }
  // Editorder(form: any) {
  //   debugger;
  //   console.log('vvvvvvvvvvv', form.value);
  //   var orderdata = {};
  //   var companyID = localStorage.getItem('CompanyID');
  //   orderdata['companyId'] = companyID;
  //   orderdata['orderId'] = this.OrderId;
  //   if (form.value.Orderprice == undefined) {
  //     orderdata['bidPrice'] = this.OPrice;
  //   }
  //   else {
  //     orderdata['bidPrice'] = form.value.Orderprice;
  //   }

  //   orderdata['bidQuantity'] = form.value.OrderQuantity;
  //   this._ApiService.Editdata(orderdata)
  //     .subscribe(data => {
  //       if (data.error.errorData == 0) {
  //         this.modalRef.hide();
  //         this.GetOrder();
  //         // console.log(data.orderList);
  //         this.notifications.create(
  //           'Done',
  //           'Order has been placed .',
  //           NotificationType.Bare,
  //           { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
  //         );
  //       }
  //     })
  // }
  // EditOpenorderModal(template, id, quantity, unit, PId, SubPId) {
  //   this.Equantity = quantity;
  //   this.Eid = id;
  //   this.Eunit = unit
  //   this.editOpenOrderForm.get('OrderQuantity').setValue(this.Equantity);
  //   // this.editOpenOrderForm.get('QuantityUnit').setValue(this.Eunit);

  //   localStorage.setItem('ProductID', PId);
  //   localStorage.setItem('SubProductID', SubPId);
  //   this.GeSpecificUnit();
  //   this.modalRef = this.modalService.show(
  //     template, Object.assign({}, { class: 'gray modal-md' })
  //   );
  // }
  // UpdateOrder(form: any) {
  //   debugger;
  //   let updateInputdata = {};
  //   var companyID = localStorage.getItem('CompanyID');
  //   updateInputdata['companyId'] = companyID;
  //   updateInputdata['orderId'] = this.Eid;
  //   // alert(this.editOpenOrderForm.get('OrderQuantity').value);
  //   if (form.value.OrderQuantity == undefined) {
  //     updateInputdata['quantity'] = form.value.OrderQuantity;
  //   }
  //   else {
  //     updateInputdata['quantity'] = this.Equantity;
  //   }
  //   updateInputdata['quantityUnit'] = this.Eunit;


  //   this._ApiService.OrderUpdate(updateInputdata)
  //     .subscribe(data => {
  //       if (data.error.errorData == 0) {
  //         this.modalRef.hide();
  //         this.GetOpenOrder();
  //         // console.log(data.orderList);
  //         this.notifications.create(
  //           'Done',
  //           'Order updated successfully .',
  //           NotificationType.Bare,
  //           { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
  //         );
  //       }
  //     })



  // }
  // GetsavedProducts() {

  //   var productdata = {};
  //   var CompanyID = localStorage.getItem('CompanyID');

  //   productdata['companyId'] = CompanyID;
  //   this._ApiService.getselectedProducts(productdata)
  //     .subscribe(data => {

  //       this.ProductList = data.selectedProductList;

  //       for (var i = 0; i < this.ProductList.length; i++) {
  //         if (this.ProductList[i].isActive != 0) {
  //           this.SavedProd.push({ 'prodId': this.ProductList[i].productId, 'ProdName': this.ProductList[i].productName })
  //         }
  //       }

  //       console.log('---------999', this.SavedProd);

  //     })
  // }
  // SearchCityByname(Cityname) {

  //   let citydata = {};
  //   citydata['searchKeyword'] = Cityname;
  //   this._ApiService.searchCity(citydata)
  //     .subscribe(data => {
  //       if (data.error.errorData == 0) {
  //         this.allcity = data.cityList;
  //         console.log('----', this.allcity);

  //       }
  //     })
  // }
  
  // filterFunctionP() {

  //   var input, filter, ul, li, a, i;
  //   input = document.getElementById("myInput2");
  //   filter = input.value.toUpperCase();

  // }
  // getAsset(event) {
  //   debugger;
  //   //   alert(event.cityName+''+ event.cityId)
  //   this.City = event.cityName;
  //   this.CityId = event.cityId;
  //   document.getElementById("myDropdown").classList.toggle("show");

  // }
  // getProduct(event) {

  //   //   alert(event.cityName+''+ event.cityId)
  //   this.product = event.ProdName;
  //   this.productId = event.prodId;
  //   localStorage.setItem('ProcuctId', this.productId);
  //   document.getElementById("myDropdownP").classList.toggle("show");

  // }
 
  Getserch(event) {
    debugger;
    if (event == false) {

      this.NigotiationFlag = false;
      localStorage.setItem('Nigotiation', '1');
     
    }
    else if (event == true) {

      this.NigotiationFlag = true;
      localStorage.setItem('Nigotiation', '2');
      this._ApiService.unsubscribe();
    
    }
  }
  GetAllProduct() {
    let offerinput = {};
    var companyID = localStorage.getItem('CompanyID');
    offerinput['companyId'] = companyID;
    this._ApiService.Getallproduct(offerinput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.allproducts = data.productList;
          // console.log('ppppppppp', data.productList);

        }
      })
  }
  getcommodityProduct(event) {

    console.log(event);
    localStorage.setItem('ProductID', event);
    let commodityinput = {};
    var companyID = localStorage.getItem('CompanyID');
    commodityinput['productId'] = event;
    this._ApiService.Getcommodities(commodityinput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.allcommodity = data.subProductList;
          //console.log('ccccccccccccc', data);
        }
      })
  }
  Getallviriety(event) {
    // console.log(event);Canceldata
    var commodityId = event;
    localStorage.setItem('SubProductID', event);
    let varietyinput = {};
    var ProductID = localStorage.getItem('ProductID');
    var companyID = localStorage.getItem('CompanyID');
    varietyinput['companyId'] = companyID;
    varietyinput['productId'] = ProductID;
    varietyinput['subProductId'] = event;
    this._ApiService.Getvariety(varietyinput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.allvariety = data.productVarietyList;
          //console.log('vvvvvvvvvvvvv', data);
        }
      })
  }

  SubmitOffer(form: any) {
   

    let offrtdata = new FormData();
    var offerdtl = {};
    if (this.theCheckbox == false) {
      offerdtl['negotiationTermsId'] = "1";
    }
    else {
      offerdtl['negotiationTermsId'] = "2";
    }

    var companyID = localStorage.getItem('CompanyID');
    offerdtl['companyId'] = companyID;
    offerdtl['orderType'] = form.value.tradetype;
    offerdtl['productId'] = form.value.productId;
    offerdtl['subProductId'] = form.value.commodityId;
    offerdtl['varietyName'] = form.value.varietyId;
    offerdtl['grade'] = form.value.grade;
    offerdtl['quantity'] = form.value.Quantity;
    offerdtl['quantityUnit'] = form.value.Qunit;
    offerdtl['price'] = form.value.price;
    offerdtl['priceUnit'] = form.value.Punit;
    offerdtl['expiryDurationHours'] = form.value.Exdate;
    offerdtl['harvestedMonthYear'] = form.value.harvestDate;
    offerdtl['deliveryTypeId'] = form.value.deliveryType;
    offerdtl['paymentModeId'] = form.value.Pmode;
    // offerdtl['cityId'] = this.CityId;
    // offerdtl['cityName'] = this.City;
    offerdtl['paymentTimeline'] = form.value.Ptimeline;
    offerdtl['emdPercent'] = form.value.Pemd;
    offerdtl['emdTimeline'] = form.value.Emdtimeline;
    offerdtl['paymentTerms'] = form.value.pTerm;
    console.log('---------', JSON.stringify(offerdtl));
    if (this.ImageArrayone != "" && this.ImageArraytwo != "") {
      var testfile = this.ImageArrayone[0].files[0];
      var testfiletwo = this.ImageArraytwo[0].files[0];
    }
    else {
      testfile = "";
      testfiletwo = "";
    }


    offrtdata.append('order ', JSON.stringify(offerdtl));
    offrtdata.append('productImage ', testfile);
    offrtdata.append('qualityCertificate  ', testfiletwo);

    if (companyID != "" && form.value.tradetype != '' && form.value.commodityId != '' && form.value.varietyId != '' && form.value.grade != '' && form.value.Quantity != '' && form.value.Qunit != '' && form.value.price != '' && form.value.Punit != '' && form.value.Exdate != '' && form.value.harvestDate != '' && form.value.deliveryType != '' && form.value.Pmode != '' && form.value.Emdtimeline != '' && this.City != "" ) {
      this._ApiService.createOrder(offrtdata)
        .subscribe(data => {
          if (data.error.errorData == 0) {
            var orderId = data.orderId;
           
            this.notifications.create(
              'Done',
              'Order Created Successfully',
              NotificationType.Bare,
              { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
            );
            this.offerForm.reset();
            this.GetAllProduct();
            this.Getdeliverytype();
            this.GetPaymentmode();
          
            this.Getgrade("");
            this.allcommodity = [];
            this.allvariety = [];
            this.grade = [];
            this.GeSpecificUnit();
            document.getElementById('grade-table').style.display = 'none';
          }
          else {
            this.notifications.create('Error', data.error.errorMsg,
              NotificationType.Bare, { theClass: 'outline primary', timeOut: 6000, showProgressBar: false });

          }

        })
    }
    else {
      this.notifications.create(
        'Error',
        'Mandatory field missing',
        NotificationType.Bare,
        { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
      );
    }



  }
  CheckUploadImage = (e) => {
    this.imagecheck = false;
    this.ImageArrayone = [];
    let files = e.target.files;
    let length = e.target.files.length
    let fileArr = [];
    let isValid = false;
    //console.log('************START*************')
    //console.log(this.companyKycDocArray)
    if (length > 0) {
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        console.log(file)
        if (file.type === 'image/jpeg' || file.type === 'image/bmp') {

          if (file.size <= 1048576) {
            fileArr.push(file);
            isValid = true
            this.imgsizecheck = false;
          } else {
            this.imgsizecheck = true;
            // $(identifier).siblings('.text-danger').show();
          }
        } else {
          this.imgsizecheck = true;
          // $(identifier).siblings('.text-danger').show();
        }
      }

      if (isValid) {
        let obj = {}
        // obj['id'] = id;
        obj['files'] = fileArr
        this.ImageArrayone.push(obj);
      }


    }

  }
  CheckUploadCirtificate = (e) => {

    this.imagecheck = false;
    this.ImageArraytwo = [];
    let files = e.target.files;
    let length = e.target.files.length
    let fileArr = [];
    let isValid = false;
    //console.log('************START*************')
    //console.log(this.companyKycDocArray)
    if (length > 0) {
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        console.log(file)
        if (file.type === 'application/pdf' || file.type === 'application/msword' || file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {

          if (file.size <= 1048576) {
            fileArr.push(file);
            isValid = true
            this.imgsizecheckcrt = false;
          } else {
            this.imgsizecheckcrt = true;

          }
        } else {
          this.imgsizecheckcrt = true;

        }
      }

      if (isValid) {
        let obj = {}
        // obj['id'] = id;
        obj['files'] = fileArr
        this.ImageArraytwo.push(obj);
      }


    }

  }
  Getterms(event) {

  }
  
  Getgrade(event) {

    var varietyId = event;
    localStorage.setItem('VarietyID', event);
    var gradeInput = {}
    gradeInput['varietyName'] = varietyId;
    this._ApiService.Getgrade(gradeInput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.grade = data.productSpecsList;
          this.GeSpecificUnit();
          //console.log('vvvvvvvvvvvvv', data);
        }
      })
    this.GetTablebyGrade();
  }
  GetTablebyGrade() {
    let gradespdata = {}
    var PId = localStorage.getItem('ProductID');
    var subPId = localStorage.getItem('SubProductID');
    var vId = localStorage.getItem('VarietyID');
    gradespdata['pageNo'] = 1;
    gradespdata['noOfItemsPerPage'] = 10;
    gradespdata['productId'] = PId;
    gradespdata['subProductId'] = subPId;
    gradespdata['varietyName'] = vId
    this._ApiService.gradespecification(gradespdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          document.getElementById('grade-table').style.display = 'block';
          console.log('ggggggggg', data);
          this.tabledata = data.productGradeSpecDtl;
          this.tableHeader = Object.keys(this.tabledata[0]);
    
          console.log('=====', this.tabledata);



        }
      })
  }
  Getdeliverytype() {
    this._ApiService.DeliveryType()
      .subscribe(data => {
        if (data.error.errorData == 0) {

          this.deliverytype = data.deliveryList;
        }
      })
  }
  GeSpecificUnit() {
    var productID = localStorage.getItem('ProductID');
    var subProductID = localStorage.getItem('SubProductID');
    let unitdata = {};
    unitdata['productId'] = productID;
    unitdata['subProductId'] = subProductID;
    this._ApiService.Getunit(unitdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.UnitAll = data.productUnits;
          console.log('uuuuuuuu', this.UnitAll.purchasingMaxUnitCode);
          this.Unitone = this.UnitAll.purchasingMaxUnitCode;
          this.Unittwo = this.UnitAll.purchasingMinUnitCode;
          this.Unitthree = this.UnitAll.shipmentCode;
        }
      })

  }
  GetPaymentmode() {
    this._ApiService.PaymentType()
      .subscribe(data => {
        if (data.error.errorData == 0) {

          this.paymentMode = data.paymentModeList;

        }
      })
  }
  GetpayPterms(ID) {

    if (ID == 3) {
      //  this.offerForm.get('Pemd').([]);
      this.offerForm.controls['Pemd'].enable();
      this.offerForm.controls['Emdtimeline'].enable();

    }
    else {
      this.offerForm.controls['Pemd'].disable();
      this.offerForm.controls['Emdtimeline'].disable();

    }
    let ptermdata = {};
    ptermdata['paymentModeId'] = ID;
    this._ApiService.Pterms(ptermdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.Ptermsdata = data.termsList;
          this.termList = data.paymentTermList;
          console.log('-------------Ptermsdata', this.termList.length);
        }
      })

  }
 
  
  // ngOnDestroy() {
  //   this._ApiService.unsubscribe();
  //   this._ApiService._disconnect();
  // }
}
