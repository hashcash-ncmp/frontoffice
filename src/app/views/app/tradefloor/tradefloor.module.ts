import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { TradefloorComponent } from './tradefloor.component';
import { OrderComponent } from '../tradefloor/order/order.component';
import{TradefloorRoutingModule} from '../tradefloor/tradefloor.routing';
import { NotificationsService, SimpleNotificationsModule } from "angular2-notifications";
import { OfferComponent } from './offer/offer.component';
import{FormsModule}  from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { TabsModule, TabsetConfig } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule, BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { BsDropdownModule,BsDropdownConfig } from 'ngx-bootstrap/dropdown';
import { MarketinsightsComponent } from './marketinsights/marketinsights.component';
import { TradeComponent } from './trade/trade.component';
import { ChartsModule } from 'ng2-charts';
import { TradehistoryComponent } from './tradehistory/tradehistory.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DashboardComponent } from './dashboard/dashboard.component';
// import { BrowserModule } from '@angular/platform-browser';



@NgModule({
  declarations: [TradefloorComponent, OrderComponent, OfferComponent, MarketinsightsComponent, TradeComponent, TradehistoryComponent, DashboardComponent],
  imports: [
    SharedModule,
    LayoutContainersModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TradefloorRoutingModule,
    SimpleNotificationsModule.forRoot(),
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    BsDropdownModule,ChartsModule,
    NgxSpinnerModule
    
  ],
  providers: [
    TabsetConfig,BsDatepickerConfig,BsDropdownConfig]
})
export class TradefloorModule { }
