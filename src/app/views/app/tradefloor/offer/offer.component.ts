import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { ValueTransformer } from '@angular/compiler/src/util';
import { ApiService } from 'src/app/data/api.service';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import * as $ from 'jquery';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.scss']
})
export class OfferComponent implements OnInit {
  public allproducts = [];
  public allcommodity = [];
  public allvariety = [];
  public grade = [];
  public SavedProd = [];
  public historydata = [];
  public termList = [];
  public OpenOrder = [];
  public Canceldata = [];
  // public Nigotiationdata = [];
  public Matchingdata = [];
  public walletbalance: any = "";
  modalRef: BsModalRef;
  public cencelId: any;
  public errormessage: any = "";
  public Priceerrormessage: any = "";
  public theCheckbox: boolean = false;
  public OPrice: any;
  product: any = "";
  productId: any;
  Ordertype: any;
  NigotiationT: any;
  ProductList: any;
  public Tradelimit: any;
  public tableHeader: any;
  public tabledata: any;
  NigotiationFlag: boolean = false;
  public Equantity: any;
  public Eid: any;
  public Eunit: any;
  public notrequired: boolean = false;
  public deliverytype: any;
  public paymentMode = [];
  public UnitAll: any;
  public Unitone: any;
  public Unittwo: any;
  public Unitthree: any;
  public Ptermsdata: any;
  public ImageArrayone: any = [];
  public ImageArraytwo: any = [];
  public imagecheck: boolean = false;
  public imgsizecheck: boolean = false;
  public imgsizecheckcrt: boolean = false;
  public BusinessTypeId: any;
  public price: any;
  public OrderId: any;
  public PriceUnit: any;
  public Oquantity: any;
  public QUnit: any;
  public orderType: any;
  //form: FormGroup;
  public countries = [];
  public states = [];
  public statearray = [];
  public stateId: any;
  public zones = [];
  public region = [];
  public regionarray = [];
  public zonearray = [];
  public City = [];
  public cityarray = [];
  public Seasons = [];
  public TradeLimitbyID: any;
  public EpayMode: any;
  public NodataError: any = "";
  public SellErr: any = "";
  public BuyErr: any = "";
  public Orderdata: any;
  public Orderbook = [];
  public SellOrderbook = [];
  public BuyOrderbook = [];
  public zoneP: any;
  public stationP: any;
  public zoneFlag: any;
  public stationFlag: any;
  public flagStatematch: boolean = false;
  public flagRegionmatch: boolean = false;
  //----------------cart dummy data
  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
  ];
  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions = {
    responsive: true,
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'brown',
      backgroundColor: 'white',
    },
  ];
  public lineChartColorstwo: Color[] = [
    {
      borderColor: 'brown',
      backgroundColor: 'white',
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  //------------
  editOrderForm: FormGroup = new FormGroup({
    Orderprice: new FormControl(''),
    OrderQuantity: new FormControl('', Validators.required),
    ConpanyName: new FormControl(''),
    ProductName: new FormControl(''),
    ProductId: new FormControl(''),
    SubProductName: new FormControl(''),
    SubProductId: new FormControl(''),
    // cityState: new FormControl(''),
    state: new FormControl(''),
    region: new FormControl(''),
    district: new FormControl(''),
    city: new FormControl(''),

    EmdPercent: new FormControl(''),
    SeasonYear: new FormControl(''),
    DeliveryType: new FormControl(''),
    VarietyName: new FormControl(''),
    PaymentMode: new FormControl(''),
    PaymentTimeline: new FormControl(''),
    Grade: new FormControl(''),
    OrderDate: new FormControl(''),
    ExpireTime: new FormControl('')
  })
  editOpenOrderForm: FormGroup = new FormGroup({

    price: new FormControl('', Validators.required),
    Punit: new FormControl('', Validators.required),
    OrderQuantity: new FormControl('', Validators.required),
    QuantityUnit: new FormControl('', Validators.required),
    nTerm: new FormControl('', Validators.required),
    deliveryType: new FormControl('', Validators.required),
    Pmode: new FormControl('', Validators.required),
    Ptimeline: new FormControl('', Validators.required),
    Pemd: new FormControl(''),
    Emdtimeline: new FormControl(''),
    pTerm: new FormControl('', Validators.required),
    OqualityType: new FormControl('', Validators.required),

    // term: new FormControl('', Validators.required),
    // tradetype: new FormControl('', Validators.required),
    // productId: new FormControl('', Validators.required),
    // commodityId: new FormControl('', Validators.required),
    // varietyId: new FormControl('', Validators.required),
    // grade: new FormControl('', Validators.required),
    // seasonId: new FormControl('', Validators.required),
    // Quantity: new FormControl('', Validators.required),
    // Qunit: new FormControl('', Validators.required),


    // LocationID: new FormControl('', Validators.required),




    // 
    // Pterms: new FormControl('', Validators.required),
    // Exdate: new FormControl('', Validators.required),
    // Pimage: new FormControl('', Validators.required),
    // Qcertificate: new FormControl('', Validators.required),
    // state: new FormControl('', Validators.required),
    // district: new FormControl('', Validators.required),
    // region: new FormControl('', Validators.required),
    // city: new FormControl('', Validators.required),

  })
  offerForm: FormGroup = new FormGroup({
    term: new FormControl('', Validators.required),
    pTerm: new FormControl('', Validators.required),
    tradetype: new FormControl('', Validators.required),
    productId: new FormControl('', Validators.required),
    commodityId: new FormControl('', Validators.required),
    varietyId: new FormControl('', Validators.required),
    grade: new FormControl('', Validators.required),
    seasonId: new FormControl('', Validators.required),
    deliveryType: new FormControl('', Validators.required),
    Quantity: new FormControl('', Validators.required),
    Qunit: new FormControl('', Validators.required),
    OqualityType: new FormControl('', Validators.required),
    // LocationID: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required),
    Punit: new FormControl('', Validators.required),
    Pmode: new FormControl('', Validators.required),
    Ptimeline: new FormControl('', Validators.required),
    Pemd: new FormControl(''),
    Emdtimeline: new FormControl('', Validators.required),
    Pterms: new FormControl('', Validators.required),
    Exdate: new FormControl('', Validators.required),
    Pimage: new FormControl('', Validators.required),
    Qcertificate: new FormControl('', Validators.required),
    state: new FormControl('', Validators.required),
    district: new FormControl('', Validators.required),
    region: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required)

  })
  constructor(private spinner: NgxSpinnerService, private modalService: BsModalService, private fb: FormBuilder, private _ApiService: ApiService, private notifications: NotificationsService) {
    // this.form = this.fb.group({
    //   checkArray: this.fb.array([], [Validators.required]),

    // })

  }


  ngOnInit(): void {
    this.GetsavedProducts();
    this.GetAllProduct();
    this.Getdeliverytype();
    this.GetPaymentmode();
    this.Getstate();
    this.GetOpenOrder();
    this.GetCancelOrder();
    this.Gethistorydata();
    this.GetorderRestdata();
    this.GetMatchingRestdata();
    // this.GetNigotiationdata();
    //this.GetSeason();
    this.productId = 0;
    this.NigotiationT = 1;
    localStorage.setItem('ProcuctId', this.productId);
    localStorage.setItem('OrderType', this.Ordertype);
    localStorage.setItem('Nigotiation', this.NigotiationT);

    //this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
    // this.GetOrder();
    this.BusinessTypeId = localStorage.getItem('BusinessTypeId');

    //  this.CityList = JSON.parse(localStorage.getItem('CityName'));
    //console.log('BBBBB', this.CityList);
    this._ApiService.currentData.subscribe(data => {

      this.Orderdata = data;
      console.log('-------------Dhriti', this.Orderdata);

      if (this.Orderdata.e != null) {
       
        if (this.Orderdata.e == 'orderbook') {

          this.Orderbook = this.Orderdata.tR;
          for (var i = 0, j = 0, k = 0; i < this.Orderbook.length; i++) {
            if (this.Orderbook[i].orderType == 1) {
              this.BuyErr = "";
              this.BuyOrderbook[j] = this.Orderbook[i];
              this.BuyOrderbook.sort(function (a, b) {
                return a.productName.localeCompare(b.productName) || a.initialQuantity - b.initialQuantity;
              });
              j++;
            }
            if (this.Orderbook[i].orderType == 2) {
              this.SellErr = "";
              this.SellOrderbook[k] = this.Orderbook[i];
              this.SellOrderbook.sort(function (a, b) {
                return a.productName.localeCompare(b.productName) || b.initialQuantity - a.initialQuantity;
              });
              k++;
            }
            if (this.BuyOrderbook == undefined || this.BuyOrderbook.length == 0) {
              this.BuyErr = "No Data !"
            }
            else if (this.SellOrderbook.length == undefined || this.SellOrderbook.length == 0) {
              this.SellErr = "No Data !"
            }
          }
          //    console.log('------------SellOrderbook', this.SellOrderbook);
        }
        else if (this.Orderdata.e == 'matchInterest') {
          this.Matchingdata = this.Orderdata.tR;
        }

      }
      else {
        // this.BuyErr = "No Data !"
        // this.SellErr = "No Data !"

      }
    }
    );

    if (this.BusinessTypeId == 2) {
      this.offerForm.get('tradetype').setValue('1');
      document.getElementById('tradetwo').style.display = 'none';
      document.getElementById('lbl-tradetwo').style.display = 'none';
      document.getElementById('tradeone').style.display = 'block';
      document.getElementById('lbl-tradeone').style.display = 'block';
      localStorage.setItem('OrderType', '2');

    }
    else if (this.BusinessTypeId == 3) {
      this.offerForm.get('tradetype').setValue('2');
      document.getElementById('lbl-tradeone').style.display = 'none';
      document.getElementById('tradeone').style.display = 'none';
      document.getElementById('tradetwo').style.display = 'block';
      document.getElementById('lbl-tradetwo').style.display = 'block';
      localStorage.setItem('OrderType', '1');

    }
    else {
      document.getElementById('tradeone').style.display = 'block';
      document.getElementById('tradetwo').style.display = 'block';
      localStorage.setItem('OrderType', '0');
    }
    this.GetorderRestdata();
    //this._ApiService._connect();
    setTimeout(() => {
      this._ApiService.sendmessagedata();
    }, 5000);
  }
  GetsavedProducts() {

    var productdata = {};
    var CompanyID = localStorage.getItem('CompanyID');
    productdata['companyId'] = CompanyID;
    this._ApiService.getselectedProducts(productdata)
      .subscribe(data => {
        this.ProductList = data.selectedProductList;
        for (var i = 0; i < this.ProductList.length; i++) {
          if (this.ProductList[i].isActive != 0) {
            this.SavedProd.push({ 'prodId': this.ProductList[i].productId, 'ProdName': this.ProductList[i].productName })
          }
        }

        console.log('---------999', this.SavedProd);

      })
  }
  getProduct(event) {
    //   alert(event.cityName+''+ event.cityId)
    // this.product = event.ProdName;
    this.productId = event;
    localStorage.setItem('ProcuctId', this.productId);
    //  document.getElementById("myDropdownP").classList.toggle("show");
    this._ApiService.unsubscribe();
    this.Orderdata = "";
    this.Matchingdata = [];
    setTimeout(() => {
      this._ApiService.sendmessagedata();
    }, 3000);

  }
  Getstate() {

    this.zones = [];
    this.City = [];
    this.statearray = [];
    this.states = [];
    this.region = [];
    this.regionarray = [];
    this.spinner.show();
    this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
    //  var orderType=this.offerForm.get('tradetype').value;
    this.offerForm.get('state').setValue('');
    //alert(event);
    this._ApiService.getcompanystatedistrictForOrder(this.BusinessTypeId)
      .subscribe(data => {
        this.countries = data.locationList;
        //var temparrray = this.countries;
        console.log('-------------', this.countries);
        for (var i = 0; i < this.countries.length; i++) {
          if (this.countries != null) {

            var found = this.statearray.includes(this.countries[i].stateId);
            if (found == false) {
              this.states.push({ 'stateName': this.countries[i].stateName, 'stateId': this.countries[i].stateId });
            }
            this.statearray.push(this.countries[i].stateId)
          }
        }
        // console.log('-------------', this.states);
        // this.states=data.states;
        this.spinner.hide();
      })

  }
  GetorderRestdata() {
    //  debugger;
    this.BuyErr = "";
    this.BuyErr = "";
    this.NodataError = "";
    this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
    var companyID = localStorage.getItem('CompanyID');
    var PId = localStorage.getItem('ProcuctId');
    var Ordertype = localStorage.getItem('OrderType');
    var Nigotiation = localStorage.getItem('Nigotiation');
    let ordeInput = {};
    ordeInput['companyId'] = companyID;
    ordeInput['pageNo'] = 1;
    ordeInput['noOfItemsPerPage'] = 50;
    ordeInput['productId'] = PId;
    ordeInput['businessTypeId'] = this.BusinessTypeId;
    ordeInput['negotiationTermsId'] = Nigotiation;
    ordeInput['orderType'] = Ordertype;
    this._ApiService.GetorderRest(ordeInput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.Orderbook = data.orderList;
          console.log('ORDERBOOK', this.Orderbook);
        //  debugger;
          this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
          if (this.BusinessTypeId == 1) {
           // debugger;
            if (this.Orderbook != undefined) {
              for (var i = 0, j = 0, k = 0; i < this.Orderbook.length; i++) {
                if (this.Orderbook[i].orderType == 1) {
                  this.BuyErr = "";
                  this.BuyOrderbook[j] = this.Orderbook[i];
                  this.BuyOrderbook.sort(function (a, b) {
                    return a.productName.localeCompare(b.productName) || a.initialQuantity - b.initialQuantity;
                  });
                  //console.log('test------', this.BuyOrderbook);
                  j++;
                }
                else {
                  this.SellErr = "";
                  this.SellOrderbook[k] = this.Orderbook[i];
                  this.SellOrderbook.sort(function (a, b) {
                    return a.productName.localeCompare(b.productName) || b.initialQuantity - a.initialQuantity;
                  });
                  k++;
                  // console.log('------------SellOrderbook', this.SellOrderbook);
                }


              }
            }

            if (this.BuyOrderbook == undefined || this.BuyOrderbook.length == 0) {

              this.BuyErr = "No Data !"
            }
            if (this.SellOrderbook.length == undefined || this.SellOrderbook.length == 0) {

              this.SellErr = "No Data !"
            }
          }
          else {
            this.Orderbook = data.orderList;
          }

        }
        else if (data.error.errorData == 1) {
          this.NodataError = data.error.errorMsg;
          // console.log('ORDERBOOK', this.NodataError);
        }
      })

  }
  CheckProductState(state) {
    debugger;
    var stateId = state;
    let inputData = {};
    inputData['productId'] = this.offerForm.get('productId').value;
    inputData['subProductId'] = this.offerForm.get('commodityId').value;
    inputData['stateId'] = stateId;
    this._ApiService.CheckProdState(inputData)
      .subscribe(data => {
        var response = data;
        if (response.error.errorData == 1) {
          this.offerForm.get('state').setValue('');
          this.notifications.create(
            'Error',
            response.error.errorMsg,
            NotificationType.Error,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: false }
          );
        }
        else {
          this.GetRegion(stateId);
        }
      })

  }
  GetRegion(data) {
    debugger;
    this.zones = [];
    this.zonearray = [];
    this.region = [];
    this.regionarray = [];
    this.City = [];
    this.cityarray = [];
    //this.stateId="";
    // this.stateId= data;
    localStorage.setItem('stateID', data)
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].stateId == data) {
        var foundzone = this.regionarray.includes(this.countries[j].regionId);
        if (foundzone == false) {
          this.region.push({ 'regionName': this.countries[j].regionName, 'regionId': this.countries[j].regionId });
        }
        this.regionarray.push(this.countries[j].regionId);
      }

    }
    this.Getallviriety();
    if (this.region[0].regionName == 'NA') {
      this.GetDistrict(data);
    }
    // else{
    //   this.GetDistrict(data);
    // }
  }
  GetDistrict(data) {

    this.zones = [];
    this.zonearray = [];
    this.City = [];

    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].stateId == data) {
        var foundzone = this.zonearray.includes(this.countries[j].districtId);
        if (foundzone == false) {
          this.zones.push({ 'districtName': this.countries[j].districtName, 'districtId': this.countries[j].districtId });
        }
        this.zonearray.push(this.countries[j].districtId);
      }
    }
    console.log('-------------', this.zones);
  }
  GetDistrictForRegion(data) {


    if (data != 0) {
      this.zones = [];
      this.zonearray = [];
      this.City = [];
      for (var j = 0; j < this.countries.length; j++) {
        if (this.countries[j].regionId == data) {
          var foundzone = this.zonearray.includes(this.countries[j].districtId);
          if (foundzone == false) {
            this.zones.push({ 'districtName': this.countries[j].districtName, 'districtId': this.countries[j].districtId });
          }
          this.zonearray.push(this.countries[j].districtId);
        }
      }
    }

    console.log('-------------', this.zones);
  }
  Getcity(data) {
    this.City = [];
    this.cityarray = [];
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].districtId == data) {
        var foundcity = this.cityarray.includes(this.countries[j].cityId);
        if (foundcity == false) {
          this.City.push({ 'cityName': this.countries[j].cityName, 'cityId': this.countries[j].cityId });
        }
        this.cityarray.push(this.countries[j].cityId);
      }
    }

  }
  GetSeason() {
    let Soasonparam = {};
    var PId = localStorage.getItem('ProductID');
    var subPId = localStorage.getItem('SubProductID');
    Soasonparam['productId'] = PId;
    Soasonparam['subProductId'] = subPId;
    this._ApiService.GetSeason(Soasonparam)
      .subscribe(data => {
        this.Seasons = data.seasonList;
      })
  }


  Getserch(event) {

    if (event == false) {
      this.Orderbook = [];
      this.BuyOrderbook = [];
      this.SellOrderbook = [];
      this.BuyErr = "";
      this.SellErr = "";
      this.NigotiationFlag = false;
      localStorage.setItem('Nigotiation', '1');
      this._ApiService.unsubscribe();
      // this.Orderbook = null;
      this.GetorderRestdata();
      setTimeout(() => {
        this._ApiService.sendmessagedata();
      }, 3000);


    }
    else if (event == true) {
      this.Orderbook = [];
      this.BuyOrderbook = [];
      this.SellOrderbook = [];
      this.BuyErr = "";
      this.SellErr = "";
      this.NigotiationFlag = true;
      localStorage.setItem('Nigotiation', '2');
      this._ApiService.unsubscribe();
      // this.Orderbook = null;
      this.GetorderRestdata();
      setTimeout(() => {
        this._ApiService.sendmessagedata();
      }, 3000);
    }

  }
  GetAllProduct() {
    let offerinput = {};
    var companyID = localStorage.getItem('CompanyID');
    offerinput['companyId'] = companyID;
    this._ApiService.Getallproduct(offerinput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.allproducts = data.productList;
          // console.log('ppppppppp', data.productList);

        }
      })
  }
  getcommodityProduct(event) {
    //this.allcommodity = [];
    // var orderType=  this.offerForm.get('tradetype').value;
    // this.Getstate(orderType);
    this.allvariety = [];
    this.grade = [];
    this.tabledata = [];
    this.tableHeader = [];
    // this.states = [];
    // this.statearray = [];
    // this.region = [];
    // this.regionarray = []
    // this.zones = [];
    // this.zonearray = []
    // this.City = [];
    this.Seasons = [];
    this.allvariety = [];
    localStorage.removeItem("ProductID");
    localStorage.removeItem("SubProductID");
    localStorage.removeItem("stateID");
    this.offerForm.get('varietyId').setValue('');
    //  this.stateId="";
    //  console.log(event);
    localStorage.setItem('ProductID', event);
    setTimeout(() => {
      this._ApiService.sendmessagedata();
    }, 3000);
    let commodityinput = {};
    var companyID = localStorage.getItem('CompanyID');
    commodityinput['productId'] = event;
    this._ApiService.Getcommodities(commodityinput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.allcommodity = data.subProductList;
          //console.log('ccccccccccccc', data);


        }
      })
  }
  GetSubproductId(event) {
    // var orderType=  this.offerForm.get('tradetype').value;
    // this.Getstate(orderType);
    // var commodityId = event;
    localStorage.setItem('SubProductID', event);
    this.GetSeason();
    this.TradeLimitbyID = '';
    this.Getbalance();
    this.allvariety = [];
    this.grade = [];
    this.tabledata = [];
    this.tableHeader = [];
    // this.states = [];
    // this.statearray = [];
    // this.region = [];
    // this.regionarray = []
    // this.zones = [];
    // this.zonearray = []
    // this.City = [];
    this.Seasons = [];
    this.allvariety = [];
    var stateId = this.offerForm.get('state').value;
    // alert(stateId);
    if (stateId != "") {
      localStorage.setItem('stateID', stateId);
      this.Getallviriety();
    }
    // this.offerForm.get('state').setValue('');
  }
  Getallviriety() {
    // console.log(event);Canceldata
    let varietyinput = {};
    var ProductID = localStorage.getItem('ProductID');
    var companyID = localStorage.getItem('CompanyID');
    var SubproductId = localStorage.getItem('SubProductID');
    varietyinput['companyId'] = companyID;
    varietyinput['productId'] = ProductID;
    varietyinput['subProductId'] = SubproductId;
    var stateId = localStorage.getItem('stateID');
    varietyinput['stateId'] = stateId;
    this._ApiService.Getvariety(varietyinput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.allvariety = data.productVarietyList;
          //console.log('vvvvvvvvvvvvv', data);
        }
      })
  }

  SubmitOffer(form: any) {
    debugger;
    if (this.errormessage == "") {
      let offrtdata = new FormData();
      var offerdtl = {};
      if (this.theCheckbox == false) {
        offerdtl['negotiationTermsId'] = "1";
      }
      else {
        offerdtl['negotiationTermsId'] = "2";
      }

      var companyID = localStorage.getItem('CompanyID');
      offerdtl['companyId'] = companyID;
      offerdtl['orderType'] = form.value.tradetype;
      offerdtl['productId'] = form.value.productId;
      offerdtl['subProductId'] = form.value.commodityId;
      offerdtl['varietyName'] = form.value.varietyId;
      offerdtl['grade'] = form.value.grade;
      offerdtl['quantity'] = form.value.Quantity;
      offerdtl['quantityUnit'] = this.Unitone;
      offerdtl['price'] = form.value.price;
      offerdtl['priceUnit'] = this.Unittwo;
      offerdtl['expiryDurationHours'] = form.value.Exdate;
      // offerdtl['harvestedMonthYear'] = form.value.harvestDate;
      offerdtl['deliveryTypeId'] = form.value.deliveryType;
      offerdtl['paymentModeId'] = form.value.Pmode;
      offerdtl['stateId'] = form.value.state;
      if (form.value.district != '') {
        offerdtl['districtId'] = form.value.district;
      }
      else {
        offerdtl['districtId'] = '0';
      }
      offerdtl['regionId'] = form.value.region;
      if (form.value.city != '') {
        offerdtl['cityId'] = form.value.city;
      }
      else {
        offerdtl['cityId'] = '0';
      }

      offerdtl['cityName'] = '0';
      offerdtl['paymentTimeline'] = form.value.Ptimeline;
      offerdtl['emdPercent'] = form.value.Pemd;
      offerdtl['emdTimeline'] = form.value.Emdtimeline;
      offerdtl['paymentTerms'] = form.value.pTerm;
      offerdtl['orderQualityType'] = form.value.OqualityType;
      offerdtl['seasonId'] = form.value.seasonId;
      var BusinesstypeId = localStorage.getItem('BusinessTypeId');
      offerdtl['businessTypeId'] = BusinesstypeId;
      console.log('---------', JSON.stringify(offerdtl));
      if (this.ImageArrayone != "" && this.ImageArraytwo != "") {
        var testfile = this.ImageArrayone[0].files[0];
        var testfiletwo = this.ImageArraytwo[0].files[0];
      }
      else {
        testfile = "";
        testfiletwo = "";
      }
      offrtdata.append('order ', JSON.stringify(offerdtl));
      offrtdata.append('productImage ', testfile);
      offrtdata.append('qualityCertificate  ', testfiletwo);

      if (this.BusinessTypeId == 3) {
        if (companyID != "" && form.value.tradetype != '' && form.value.commodityId != '' && form.value.varietyId != '' && form.value.grade != '' && form.value.Quantity != '' && this.Unitone != '' && form.value.price != '' && this.Unittwo != '' && form.value.Exdate != '' && form.value.deliveryType != '' && form.value.Pmode != '' && form.value.seasonId != '' && form.value.state != null && form.value.district != null && form.value.city != null) {
          this._ApiService.createOrder(offrtdata)
            .subscribe(data => {
              if (data.error.errorData == 0) {
                var orderId = data.orderId;

                this.notifications.create(
                  'Done',
                  'Order Created Successfully',
                  NotificationType.Success,
                  { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
                );
                this.offerForm.reset();
                this.GetAllProduct();
                this.Getdeliverytype();
                this.GetPaymentmode();
                this.GetOpenOrder();
                this.Getgrade("");
                this.allcommodity = [];
                this.allvariety = [];
                this.grade = [];
                this.GeSpecificUnit();
                document.getElementById('grade-table').style.display = 'none';
              }
              else {
                this.notifications.create('Error', data.error.errorMsg,
                  NotificationType.Error, { theClass: 'outline primary', timeOut: 6000, showProgressBar: false });

              }

            })
        }
        else {
          this.notifications.create(
            'Error',
            'Mandatory field missing',
            NotificationType.Error,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: false }
          );
        }
      }
      else {
        if (companyID != "" && form.value.tradetype != '' && form.value.commodityId != '' && form.value.varietyId != '' && form.value.grade != '' && form.value.Quantity != '' && this.Unitone != '' && form.value.price != '' && this.Unittwo != '' && form.value.Exdate != '' && form.value.deliveryType != '' && form.value.Pmode != '' && form.value.seasonId != '' && form.value.state != null) {
          this._ApiService.createOrder(offrtdata)
            .subscribe(data => {
              if (data.error.errorData == 0) {
                var orderId = data.orderId;

                this.notifications.create(
                  'Done',
                  'Order Created Successfully',
                  NotificationType.Success,
                  { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
                );
                this.offerForm.reset();
                this.GetAllProduct();
                this.Getdeliverytype();
                this.GetPaymentmode();
                this.GetOpenOrder();
                this.Getgrade("");
                this.allcommodity = [];
                this.allvariety = [];
                this.grade = [];
                this.GeSpecificUnit();
                document.getElementById('grade-table').style.display = 'none';
              }
              else {
                this.notifications.create('Error', data.error.errorMsg,
                  NotificationType.Bare, { theClass: 'outline primary', timeOut: 6000, showProgressBar: false });

              }

            })
        }
        else {
          this.notifications.create(
            'Error',
            'Mandatory field missing',
            NotificationType.Error,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: false }
          );
        }
      }
      // else if(form.value.tradetype === ""){
      //   this.notifications.create(
      //     'Error',
      //     'Select trade type .',
      //     NotificationType.Bare,
      //     { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
      //   );
      // }
    }
    else {
      this.notifications.create(
        'Error',
        this.errormessage,
        NotificationType.Error,
        { theClass: 'outline primary', timeOut: 6000, showProgressBar: false }
      );
    }

  }
  CheckUploadImage = (e) => {
    this.imagecheck = false;
    this.ImageArrayone = [];
    let files = e.target.files;
    let length = e.target.files.length
    let fileArr = [];
    let isValid = false;
    //console.log('************START*************')
    //console.log(this.companyKycDocArray)
    if (length > 0) {
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        console.log(file)
        if (file.type === 'image/jpeg' || file.type === 'image/bmp') {

          if (file.size <= 1048576) {
            fileArr.push(file);
            isValid = true
            this.imgsizecheck = false;
          } else {
            this.imgsizecheck = true;
            // $(identifier).siblings('.text-danger').show();
          }
        } else {
          this.imgsizecheck = true;
          // $(identifier).siblings('.text-danger').show();
        }
      }

      if (isValid) {
        let obj = {}
        // obj['id'] = id;
        obj['files'] = fileArr
        this.ImageArrayone.push(obj);
      }


    }

  }
  CheckUploadCirtificate = (e) => {
    this.imagecheck = false;
    this.ImageArraytwo = [];
    let files = e.target.files;
    let length = e.target.files.length
    let fileArr = [];
    let isValid = false;
    //console.log('************START*************')
    //console.log(this.companyKycDocArray)
    if (length > 0) {
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        console.log(file)
        if (file.type === 'application/pdf' || file.type === 'application/msword' || file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {

          if (file.size <= 1048576) {
            fileArr.push(file);
            isValid = true
            this.imgsizecheckcrt = false;
          } else {
            this.imgsizecheckcrt = true;

          }
        } else {
          this.imgsizecheckcrt = true;

        }
      }

      if (isValid) {
        let obj = {}
        // obj['id'] = id;
        obj['files'] = fileArr
        this.ImageArraytwo.push(obj);
      }


    }

  }
  Getterms(event) {
    // alert(event);
    //this.Getstate(event);
  }

  Getgrade(event) {

    var varietyId = event;
    localStorage.setItem('VarietyID', event);
    var gradeInput = {}
    gradeInput['varietyName'] = varietyId;
    var stateId = localStorage.getItem('stateID');
    gradeInput['stateId'] = stateId;
    this._ApiService.Getgrade(gradeInput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.grade = data.productSpecsList;
          this.GeSpecificUnit();
          //console.log('vvvvvvvvvvvvv', data);
        }
      })
    // this.GetTablebyGrade();
  }
  GetTablebyGrade(template: TemplateRef<any>) {
    let gradespdata = {}
    var PId = localStorage.getItem('ProductID');
    var subPId = localStorage.getItem('SubProductID');
    var vId = localStorage.getItem('VarietyID');
    gradespdata['pageNo'] = 1;
    gradespdata['noOfItemsPerPage'] = 10;
    gradespdata['productId'] = PId;
    gradespdata['subProductId'] = subPId;
    gradespdata['varietyName'] = vId;
    var stateId = localStorage.getItem('stateID');
    gradespdata['stateId'] = stateId;
    this._ApiService.gradespecification(gradespdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {

          // document.getElementById('grade-table').style.display = 'block';
          console.log('ggggggggg', data);
          // this.tabledata = data.productGradeSpecDtl;
          // this.tableHeader = Object.keys(this.tabledata[0]);
          for (var i = 0; i < data.productGradeSpecDtl.length; i++) {
            var gradeval = this.offerForm.get('grade').value;
            if (data.productGradeSpecDtl[i].Grade != gradeval) {
              data.productGradeSpecDtl.splice(i, 1);
              this.tabledata = data.productGradeSpecDtl//JSON.parse(data.productGradeSpecDtl[i]);
              this.tableHeader = Object.keys(this.tabledata[0]);
            }
          }
          this.modalRef = this.modalService.show(
            template, Object.assign({}, { class: 'gray modal-lg' })
          );
          console.log('=====', this.tabledata);



        }
      })
  }
  GetTablebyGrade_order(productId, subProductId, varietyName) {
    let gradespdata = {}
    var PId = productId;// localStorage.getItem('ProductID');
    var subPId = subProductId;//localStorage.getItem('SubProductID');
    var vId = varietyName;// localStorage.getItem('VarietyID');
    gradespdata['pageNo'] = 1;
    gradespdata['noOfItemsPerPage'] = 10;
    gradespdata['productId'] = PId;
    gradespdata['subProductId'] = subPId;
    gradespdata['varietyName'] = vId;

    var stateId = localStorage.getItem('sateid');
    gradespdata['stateId'] = stateId;
    this._ApiService.gradespecification(gradespdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          // document.getElementById('grade-table').style.display = 'block';
          console.log('ggggggggg', data);
          this.tabledata = data.productGradeSpecDtl;
          this.tableHeader = Object.keys(this.tabledata[0]);
          console.log('=====', this.tableHeader);
        }
      })
  }
  Getdeliverytype() {
    this._ApiService.DeliveryType()
      .subscribe(data => {
        if (data.error.errorData == 0) {

          this.deliverytype = data.deliveryList;
          console.log('this.deliverytype', this.deliverytype);

        }
      })
  }
  GeSpecificUnit() {
    debugger;
    var productID = localStorage.getItem('ProductID');
    var subProductID = localStorage.getItem('SubProductID');
    let unitdata = {};
    unitdata['productId'] = productID;
    unitdata['subProductId'] = subProductID;
    this._ApiService.Getunit(unitdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.UnitAll = data.productUnits;
          console.log('uuuuuuuu', this.UnitAll.purchasingMaxUnitCode);
          this.Unitone = this.UnitAll.quantityUnitCode;
          this.Unittwo = this.UnitAll.pricingUnitCode;
          // this.Unitthree = this.UnitAll.shipmentCode;
        }
      })

  }
  GetPaymentmode() {
    this._ApiService.PaymentType()
      .subscribe(data => {
        if (data.error.errorData == 0) {

          this.paymentMode = data.paymentModeList;

        }
      })
  }
  GetpayPterms(ID) {
    if (ID == 3) {
      //  this.offerForm.get('Pemd').([]);
      this.offerForm.controls['Pemd'].enable();
      this.offerForm.controls['Emdtimeline'].enable();

    }
    else {
      this.offerForm.controls['Pemd'].disable();
      this.offerForm.controls['Emdtimeline'].disable();

    }
    let ptermdata = {};
    ptermdata['paymentModeId'] = ID;
    this._ApiService.Pterms(ptermdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.Ptermsdata = data.termsList;
          this.termList = data.paymentTermList;
          //   console.log('-------------Ptermsdata', this.termList.length);
        }
      })

  }
  GetOpenOrder() {
    var orderdata = {};
    var companyID = localStorage.getItem('CompanyID');
    orderdata['companyId'] = companyID;
    orderdata['pageNo'] = 1;
    orderdata['noOfItemsPerPage'] = 50;
    var BusinesstypeId = localStorage.getItem('BusinessTypeId');
    orderdata['businessTypeId'] = BusinesstypeId;
    this._ApiService.Opendeals(orderdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.OpenOrder = data.orderList;
          console.log('---------------', this.OpenOrder);
        }

      })
  }
  Gethistorydata() {
    var companyID = localStorage.getItem('CompanyID');
    let tadedata = {};
    tadedata['companyId'] = companyID;
    tadedata['pageNo'] = 1;
    tadedata['noOfItemsPerPage'] = 5;
    this._ApiService.Tradehistory(tadedata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.historydata = data.tradeHistoryList;

        }
      })
  }

  Cancelorder(template, orderID) {
    this.modalRef = this.modalService.show(
      template, Object.assign({}, { class: 'gray modal-md' })
    );

    this.cencelId = orderID;
  }
  ConfirmCancelOrder() {
    var orderdata = {};
    var companyID = localStorage.getItem('CompanyID');
    orderdata['companyId'] = companyID;
    orderdata['orderId'] = this.cencelId;

    this._ApiService.Ordercancel(orderdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {

          console.log(data);
          this.GetOpenOrder();
          this.GetCancelOrder();
          this.notifications.create(
            'Done',
            'Order Canceled Successfully',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
          this.modalRef.hide();
        }
      })

  }
  GetCancelOrder() {
    var orderdata = {};
    var companyID = localStorage.getItem('CompanyID');
    orderdata['companyId'] = companyID;
    orderdata['pageNo'] = 1;
    orderdata['noOfItemsPerPage'] = 5;
    var BusinesstypeId = localStorage.getItem('BusinessTypeId');
    orderdata['businessTypeId'] = BusinesstypeId;

    this._ApiService.Canceldeals(orderdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          if (data.error.errorData.errorMsg == "no data") {
            this.Canceldata = [];
          }
          else {
            this.Canceldata = data.orderList;
          }

          console.log('cancel', data.orderList);
        }
      })
  }
  checkquantityOffer(event) {
    debugger;
    this.errormessage = "";
    let Inputcheckdata = {};
    var productID = this.offerForm.get('productId').value;
    var subProductID = this.offerForm.get('commodityId').value;
    var companyID = localStorage.getItem('CompanyID');
    Inputcheckdata['companyId'] = companyID;
    Inputcheckdata['productId'] = productID;
    Inputcheckdata['subProductId'] = subProductID;
    this._ApiService.GetTradeLimit(Inputcheckdata)
      .subscribe(data => {
        if (data.error.errorData == 0 && data.error.errorMsg != 'No data!') {
          if (event > 0) {
            this.Tradelimit = data.tradeLimit.tradeLimit;
            let HasReminder = event % this.Tradelimit;
            if (HasReminder == 0) {
              this.errormessage = "";
            }
            else {
              this.errormessage = "Exceed Trade Limit. Available Trade limit is" + ' ' + this.Tradelimit;
            }
          }
          else {
            this.offerForm.get('Quantity').setValue('');
            this.errormessage = "Quantity can not be 0 or nagative";
          }
          // if (event > 0) {
          //   if (event >= this.Tradelimit && HasReminder == 0) {
          //     this.errormessage = "";
          //   }
          //   else {
          //     // (event < 0 || event < this.Tradelimit || HasReminder != 0)
          //     this.errormessage = "Quantity can not be 0 or less than trade limit and product of 100."
          //   }
          // }
          // else{

          //   this.offerForm.get('Quantity').setValue('');
          //   this.errormessage = "Quantity can not be  0 or negative."
          // }
        }
        else {
          if (event <= 0) {
            this.offerForm.get('Quantity').setValue('');
            this.errormessage = "Quantity can not be  0 or negative."
          }
          else {
            this.errormessage = data.error.errorMsg;
          }

        }
      })

  }
  checkquantity(event) {
    debugger;
    this.errormessage = "";
    let Inputcheckdata = {};
    var productID = this.editOrderForm.get('ProductId').value;
    var subProductID = this.editOrderForm.get('SubProductId').value;
    var companyID = localStorage.getItem('CompanyID');
    Inputcheckdata['companyId'] = companyID;
    Inputcheckdata['productId'] = productID;
    Inputcheckdata['subProductId'] = subProductID;
    this._ApiService.GetTradeLimit(Inputcheckdata)
      .subscribe(data => {
        if (data.error.errorData == 0 && data.error.errorMsg != 'No data!') {
          if (event > 0) {
            this.Tradelimit = data.tradeLimit.tradeLimit;
            let HasReminder = event % this.Tradelimit;
            if (HasReminder == 0) {
              this.errormessage = "";
            }
            else {
              this.errormessage = "Exceed Trade Limit. Available Trade limit is" + ' ' + this.Tradelimit;
            }
          }
          else {
            this.offerForm.get('Quantity').setValue('');
            this.errormessage = "Quantity can not be 0 or nagative";
          }
          // if (event > 0) {
          //   if (event >= this.Tradelimit && HasReminder == 0) {
          //     this.errormessage = "";
          //   }
          //   else {
          //     // (event < 0 || event < this.Tradelimit || HasReminder != 0)
          //     this.errormessage = "Quantity can not be 0 or less than trade limit and product of 100."
          //   }
          // }
          // else{

          //   this.offerForm.get('Quantity').setValue('');
          //   this.errormessage = "Quantity can not be  0 or negative."
          // }
        }
        else {
          if (event <= 0) {
            this.offerForm.get('Quantity').setValue('');
            this.errormessage = "Quantity can not be  0 or negative."
          }
          else {
            this.errormessage = data.error.errorMsg;
          }

        }
      })

  }
  checkprice(event) {

    if (event > 0) {

      this.Priceerrormessage = "";

    }
    else {
      this.offerForm.get('price').setValue('');
      this.Priceerrormessage = "Price can not be  0 or negative."

    }
  }
  EditOpenorderModal(template, order) {
    //-----------------------------------//
    // console.log('ooooooooooooooo', order);
    debugger;
    this.Eid = "";
    // this.Equantity = quantity;
    this.Eid = order.orderId;

    this.editOpenOrderForm.get('price').setValue(order.price);
    this.editOpenOrderForm.get('Punit').setValue(order.priceUnit);
    this.editOpenOrderForm.get('OrderQuantity').setValue(order.initialQuantity);
    this.editOpenOrderForm.get('QuantityUnit').setValue(order.quantityUnit);
    this.editOpenOrderForm.get('nTerm').setValue(order.negotiationTermsId);
    this.editOpenOrderForm.get('deliveryType').setValue(order.deliveryTypeId);
    this.editOpenOrderForm.get('Pmode').setValue(order.paymentModeId);
    if (order.paymentModeId != "") {
      // this.GetpayPterms(order.paymentModeId);
      this.EpayMode = order.paymentModeId;
      this.GetpayPtermsEdit(this.EpayMode);
    }
    this.editOpenOrderForm.get('Ptimeline').setValue(order.paymentTimeline);
    this.editOpenOrderForm.get('Pemd').setValue(order.emdPercent);
    this.editOpenOrderForm.get('Emdtimeline').setValue(order.emdTimeline);
    this.editOpenOrderForm.get('pTerm').setValue(order.paymentTerms);
    this.editOpenOrderForm.get('OqualityType').setValue(order.orderQualityType);
    //   localStorage.setItem('ProductID', order);
    //  localStorage.setItem('SubProductID', SubPId);

    //  this.GeSpecificUnit();

    setTimeout(() => {

      //  this.editOpenOrderForm.get('QuantityUnit').setValue(this.Unitone);

      this.modalRef = this.modalService.show(
        template, Object.assign({}, { class: 'gray modal-md' })
      );
    }, 1000);

  }
  GetpayPtermsEdit(ID) {

    if (ID == 3 || this.EpayMode == 3) {
      //  this.offerForm.get('Pemd').([]);
      this.editOpenOrderForm.controls['Pemd'].enable();
      this.editOpenOrderForm.controls['Emdtimeline'].enable();

    }
    else {
      this.editOpenOrderForm.controls['Pemd'].disable();
      this.editOpenOrderForm.controls['Emdtimeline'].disable();

    }
    let ptermdata = {};
    ptermdata['paymentModeId'] = ID;
    this._ApiService.Pterms(ptermdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.Ptermsdata = data.termsList;
          this.termList = data.paymentTermList;
          //console.log('-------------Ptermsdata', this.termList.length);
        }
      })

  }
  UpdateOrder(form: any) {
    debugger;
    let updateInputdata = {};
    var companyID = localStorage.getItem('CompanyID');
    updateInputdata['companyId'] = companyID;
    updateInputdata['quantity'] = form.value.OrderQuantity;
    updateInputdata['quantityUnit'] = form.value.QuantityUnit;
    updateInputdata['price'] = form.value.price;
    updateInputdata['priceUnit'] = form.value.Punit;
    updateInputdata['negotiationTermsId'] = form.value.nTerm;
    updateInputdata['deliveryTypeId'] = form.value.deliveryType;
    updateInputdata['paymentModeId'] = form.value.Pmode;
    updateInputdata['paymentTimeline'] = form.value.Ptimeline;
    updateInputdata['emdPercent'] = form.value.Pemd;
    updateInputdata['emdPercent'] = this.editOpenOrderForm.get('Pemd').value;
    updateInputdata['emdTimeline'] = form.value.Emdtimeline;
    updateInputdata['emdTimeline'] = this.editOpenOrderForm.get('Emdtimeline').value;;
    updateInputdata['paymentTerms'] = form.value.pTerm;
    updateInputdata['orderQualityType'] = form.value.OqualityType;
    updateInputdata['orderId'] = this.Eid;

    if (form.value.OrderQuantity == undefined) {
      updateInputdata['quantity'] = form.value.OrderQuantity;
    }
    else {
      updateInputdata['quantity'] = this.editOpenOrderForm.get('OrderQuantity').value;
    }
    updateInputdata['quantityUnit'] = this.editOpenOrderForm.get('QuantityUnit').value;


    this._ApiService.OrderUpdate(updateInputdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.modalRef.hide();
          this.GetOpenOrder();
          // console.log(data.orderList);
          this.notifications.create(
            'Done',
            'Order updated successfully .',
            NotificationType.Success,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }
        else {
          this.modalRef.hide();
          this.notifications.create(
            'Error',
            data.error.errorMsg,
            NotificationType.Error,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: false }
          );
        }
      })



  }
  Getbalance() {
    let inputdata = {};
    var productID = localStorage.getItem('ProductID');
    var subProductID = localStorage.getItem('SubProductID');
    var companyID = localStorage.getItem('CompanyID');
    inputdata['companyId'] = companyID;
    inputdata['productId'] = productID;
    inputdata['subProductId'] = subProductID;
    this._ApiService.Getbalance(inputdata)
      .subscribe(data => {
        if (data.error.errorData == 0 && data.error.errorMsg != 'no data') {
          document.getElementById('wallet-lbl').style.display = 'block';
          this.walletbalance = data.walletInfo[0];
          this.TradeLimitbyID = this.walletbalance.currentQty
          console.log('*******balance', data.walletInfo);
        }


      })
  }
  OpenEditmodal(template, order) {
    debugger;
    this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
    console.log('OOOOOOOOOOOOOOOOO', order);
    this.zoneFlag = "";
    this.stationFlag = "";
    this.flagStatematch = false;
    this.flagRegionmatch = false;
    for (var i = 0; i < this.states.length; i++) {
      if (order.stateId == this.states[i].stateId) {
        this.GetRegion(order.stateId);
        this.flagStatematch = true;
      }

    }
    if (this.flagStatematch == true) {
      for (var j = 0; j < this.region.length; j++) {
        if (order.regionId == this.region[j].regionId) {
          this.flagRegionmatch = true;
          if (order.ownBidCount == 0) {
            this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'modal-xl' });
            localStorage.setItem('sateid', order.stateId);
            this.OrderId = order.orderId;
            // this.OPrice = 
            this.PriceUnit = order.priceUnit;
            this.Oquantity = order.currentQuantity;
            this.QUnit = order.quantityUnit;
            this.editOrderForm.get('Orderprice').setValue(order.price);
            this.editOrderForm.get('OrderQuantity').setValue(order.currentQuantity);
            this.editOrderForm.get('ConpanyName').setValue(order.companyName);
            this.editOrderForm.get('ProductName').setValue(order.productName);
            this.editOrderForm.get('ProductId').setValue(order.productId);
            this.editOrderForm.get('SubProductName').setValue(order.subProductName);//
            this.editOrderForm.get('SubProductId').setValue(order.subProductId);
            this.editOrderForm.get('state').setValue(order.stateName);
            this.editOrderForm.get('region').setValue(order.regionName);
            var regonId = order.regionId;
            this.zoneFlag = order.districtId;
            this.stationFlag = order.cityId;
            //  alert(this.zoneFlag + "" + this.stationFlag);
            if (order.districtId == 0) {
              setTimeout(() => this.GetDistrictForRegion(regonId), 300);
              this.zoneP = order.districtId;

            }
            else {
              //
              this.zoneP = order.districtId;
              this.editOrderForm.get('district').setValue(order.districtName);
            }


            if (order.cityId == 0) {
              setTimeout(() => this.Getcity(order.districtId), 400);
              this.stationP = order.cityId;

            }
            else {
              this.stationP = order.cityId;
              this.editOrderForm.get('city').setValue(order.cityName);
            }
            // this.editOrderForm.get('district').setValue(order.districtName);
            // this.editOrderForm.get('city').setValue(order.cityName);

            this.editOrderForm.get('EmdPercent').setValue(order.emdPercent);
            this.editOrderForm.get('SeasonYear').setValue(order.seasonYear);
            this.editOrderForm.get('DeliveryType').setValue(order.deliveryType);
            this.editOrderForm.get('VarietyName').setValue(order.varietyName);
            this.editOrderForm.get('PaymentMode').setValue(order.paymentMode);
            this.editOrderForm.get('PaymentTimeline').setValue(order.paymentTimeline);
            this.editOrderForm.get('Grade').setValue(order.grade);
            this.editOrderForm.get('OrderDate').setValue(order.orderDate);
            this.editOrderForm.get('ExpireTime').setValue(order.expireTime);
            this.orderType = order.orderType;
            this.GetTablebyGrade_order(order.productId, order.subProductId, order.varietyName);
            if (this.NigotiationFlag == true) {
              this.editOrderForm.controls['Orderprice'].disable();
            }
            else if (this.NigotiationFlag == false) {
              this.editOrderForm.controls['Orderprice'].enable();
            }
          }
          else {
            this.notifications.create(
              'Done',
              'Offer already placed !!',
              NotificationType.Bare,
              { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
            );
            // alert('Offer already placed');
          }

        }

      }
      if (this.flagRegionmatch == false) {
        this.notifications.create(
          'Done',
          'Region does not match with your company location !!',
          NotificationType.Bare,
          { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
        );
      }
    }
    else {
      this.notifications.create(
        'Done',
        'State does not match with your company location !!',
        NotificationType.Bare,
        { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
      );
    }

  }
  Editorder(form: any) {
    debugger;
    console.log('vvvvvvvvvvv', form.value);
    var orderdata = {};
    var companyID = localStorage.getItem('CompanyID');
    orderdata['companyId'] = companyID;
    orderdata['orderId'] = this.OrderId;
    if (form.value.Orderprice == undefined) {
      orderdata['bidPrice'] = this.OPrice;
    }
    else {
      orderdata['bidPrice'] = form.value.Orderprice;
    }

    orderdata['bidQuantity'] = form.value.OrderQuantity;
    var BusinesstypeId = localStorage.getItem('BusinessTypeId');
    orderdata['businessTypeId'] = BusinesstypeId;

    if (BusinesstypeId == '3' && this.editOrderForm.get('district').value != "" && this.editOrderForm.get('city').value != "") {
      if (this.errormessage == '' && this.Priceerrormessage == '') {
        orderdata['districtId'] = this.zoneP;
        orderdata['cityId'] = this.stationP;
        this._ApiService.Editdata(orderdata)
          .subscribe(data => {
            if (data.error.errorData == 0) {
              this.modalRef.hide();
              this.GetOrder();
              this.GetOpenOrder();
              setTimeout(() => {
                this._ApiService.sendmessagedata();
              }, 5000);
              // console.log(data.orderList);
              this.notifications.create(
                'Done',
                'Order has been placed .',
                NotificationType.Success,
                { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
              );
            }
            else {
              this.modalRef.hide();
              this.notifications.create('Error',
                data.error.errorMsg,
                NotificationType.Error, { timeOut: 3000, showProgressBar: false });
              // this.notifications.create(
              //   'Error',
              //   data.error.errorMsg,
              //   NotificationType.Bare,
              //   { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
              // );
            }
          })
      } else {

      }
    }
    else if (BusinesstypeId != '3') {
      if (this.errormessage == '' && this.Priceerrormessage == '') {
        orderdata['districtId'] = this.zoneP;
        orderdata['cityId'] = this.stationP;
        this._ApiService.Editdata(orderdata)
          .subscribe(data => {
            if (data.error.errorData == 0) {
              this.modalRef.hide();
              this.GetOrder();
              this.GetOpenOrder();
              setTimeout(() => {
                this._ApiService.sendmessagedata();
              }, 5000);
              // console.log(data.orderList);
              this.notifications.create(
                'Done',
                'Order has been placed .',
                NotificationType.Success,
                { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
              );
            }
            else {
              this.modalRef.hide();
              this.notifications.create('Error',
                data.error.errorMsg,
                NotificationType.Error, { timeOut: 3000, showProgressBar: false });
              // this.notifications.create(
              //   'Error',
              //   data.error.errorMsg,
              //   NotificationType.Bare,
              //   { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
              // );
            }
          })
      } else {

      }
    }
    else {
      this.modalRef.hide();
      this.notifications.create(
        'Done',
        'Zone and station is required .',
        NotificationType.Bare,
        { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
      );
    }


  }
  GetOrder() {
    var orderdata = {};
    var companyID = localStorage.getItem('CompanyID');
    orderdata['companyId'] = companyID;
    this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
    orderdata['businessTypeId'] = this.BusinessTypeId;
    orderdata['pageNo'] = 1;
    orderdata['noOfItemsPerPage'] = 50;
    this._ApiService.Insightdata(orderdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.Orderdata = data.orderList;
          // console.log(data.orderList);
        }
      })
  }
  // GetNigotiationdata() {
  //   var companyID = localStorage.getItem('CompanyID');
  //   let insightInput = {};
  //   insightInput['companyId'] = companyID;
  //   insightInput['pageNo'] = 1;
  //   insightInput['noOfItemsPerPage'] = 40;
  //   insightInput['orderType'] = 0;
  //   this._ApiService.GetNigotiation(insightInput)
  //     .subscribe(data => {
  //       if (data.error.errorData == 0 && data.error.errorData != 'no data') {
  //         this.Nigotiationdata = data.orderList;
  //         // console.log(data.orderList);
  //       }
  //     })

  // }
  GetMatchingRestdata() {
    var companyID = localStorage.getItem('CompanyID');
    let insightInput = {};
    insightInput['companyId'] = companyID;
    insightInput['pageNo'] = 1;
    insightInput['noOfItemsPerPage'] = 40;
    this._ApiService.GetmatchingInterestRest(insightInput)
      .subscribe(data => {
        if (data.error.errorData == 0 && data.error.errorData != 'no data') {
          this.Matchingdata = data.orderList;
          // console.log(data.orderList);
        }
      })

  }
  ApproveOrder(Id) {
    // alert(Id)
    var companyID = localStorage.getItem('CompanyID');
    let ordrdata = {};
    ordrdata['companyId'] = companyID;
    ordrdata['bidId'] = Id;
    this._ApiService.Approvorder(ordrdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.GetMatchingRestdata();
          this.GetOpenOrder();
          setTimeout(() => {
            this._ApiService.sendmessagedata();
          }, 5000);
          this.notifications.create(
            'Done',
            'Order Approved',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }
      })
  }
  RejectOrder(Id) {
    var companyID = localStorage.getItem('CompanyID');
    let ordrdata = {};
    ordrdata['companyId'] = companyID;
    ordrdata['bidId'] = Id;
    this._ApiService.Rejectorder(ordrdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.GetMatchingRestdata();
          this.notifications.create(
            'Done',
            'Order Rejected',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }
        else {
          this.notifications.create(
            'Error',
            data.error.errorMsg,
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }
      })
  }

  ngOnDestroy() {
    this._ApiService.unsubscribe();
    //  this._ApiService._disconnect();
  }
}
