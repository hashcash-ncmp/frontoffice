import { Component, OnInit, OnDestroy, TemplateRef } from '@angular/core';
import { ApiService } from 'src/app/data/api.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { ThemeService } from 'ng2-charts';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-tradehistory',
  templateUrl: './tradehistory.component.html',
  styleUrls: ['./tradehistory.component.scss']
})
export class TradehistoryComponent implements OnInit {
hashUrl=null;
public historydata:any;
public historydataTemp:any;
public OpenOrder = [];
public Canceldata=[];
  constructor(private _ApiService: ApiService, private notifications: NotificationsService, private modalService: BsModalService) {
    this.hashUrl=_ApiService.BLOCK_CHAIN_URL;
  }

  ngOnInit(): void {
    
   this.Gethistorydata();
   this.GetOpenOrder();
   this.GetCancelOrder();
    this._ApiService.currentData.subscribe(data => {
      console.log('history',data);
      
     this.historydataTemp = data;
    if (this.historydataTemp.e != null) {
      if (this.historydataTemp.e == 'tradeHistory') {

        this.historydata = this.historydataTemp.tH;
        console.log('------------Orderbook', this.historydata);
      }
      // else if (this.Orderdata.e == 'matchInterest') {
      //   this.Matchingdata = this.Orderdata.tR;
      // }

    }
    else {
     
    }
     
    }
    );
   //this._ApiService._connect();
    setTimeout(() => {
      this._ApiService.sendmessageHistorydata();
    }, 3000);
  }
  Gethistorydata(){
    var companyID = localStorage.getItem('CompanyID');
    let tadedata={};
    tadedata['companyId']=companyID;
    tadedata['pageNo']=1;
    tadedata['noOfItemsPerPage']=20;
    this._ApiService.Tradehistory(tadedata)
    .subscribe(data=>{
      if (data.error.errorData == 0) {
        this.historydata=data.tradeHistoryList;

      }
    })
  }
  GetOpenOrder() {
    var orderdata = {};
    var companyID = localStorage.getItem('CompanyID');
    orderdata['companyId'] = companyID;
    orderdata['pageNo'] = 1;
    orderdata['noOfItemsPerPage'] = 20;
    var BusinesstypeId=localStorage.getItem('BusinessTypeId');
    orderdata['businessTypeId'] = BusinesstypeId;
    this._ApiService.Opendeals(orderdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.OpenOrder = data.orderList;
          console.log('---------------', this.OpenOrder);
        }

      })
  }
  GetCancelOrder() {
    var orderdata = {};
    var companyID = localStorage.getItem('CompanyID');
    orderdata['companyId'] = companyID;
    orderdata['pageNo'] = 1;
    orderdata['noOfItemsPerPage'] = 50;
    var BusinesstypeId=localStorage.getItem('BusinessTypeId');
    orderdata['businessTypeId'] = BusinesstypeId;
    
    this._ApiService.Canceldeals(orderdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          if (data.error.errorData.errorMsg == "no data") {
            this.Canceldata = [];
          }
          else {
            this.Canceldata = data.orderList;
          }

          console.log('cancel', data.orderList);
        }
      })
  }
  ngOnDestroy() {
    this._ApiService.unsubscribeHistorydata();
    //this._ApiService._disconnect();
  }
}
