import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{OrderComponent} from '../tradefloor/order/order.component';
import{TradefloorComponent} from '../tradefloor/tradefloor.component';
import { OfferComponent } from './offer/offer.component';
import { MarketinsightsComponent } from './marketinsights/marketinsights.component';
import { TradeComponent } from './trade/trade.component';
import { TradehistoryComponent } from './tradehistory/tradehistory.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
    {
        path: '', component: TradefloorComponent,
        children: [
            { path: '', redirectTo: 'order', pathMatch: 'full' },
            { path: 'order', component: OrderComponent },
            { path: 'market-insights', component: MarketinsightsComponent },
            { path: 'Dashboard', component: DashboardComponent },
            { path: 'offer', component: OfferComponent },
             { path: 'trade', component: TradeComponent },
            { path: 'history', component: TradehistoryComponent },
            // { path: 'bankinginfo', component: BankinginfoComponent },
            // { path: 'companyinfo', component: CompanyinfoComponent },
            // { path: 'productpref', component: ProductprefComponent },
            // { path: 'sessionmngpref', component: BlankPageComponent },
            // { path: 'addbank', component: AddbankComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TradefloorRoutingModule { }