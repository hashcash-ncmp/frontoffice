import { Component, OnInit } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { ApiService } from 'src/app/data/api.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-marketinsights',
  templateUrl: './marketinsights.component.html',
  styleUrls: ['./marketinsights.component.scss']
})
export class MarketinsightsComponent implements OnInit {
  public Orderdata: any;
  public BusinessTypeId: any;
  orderType: any;
  modalRef: BsModalRef;
  OrderId: any;
  Price: any;
  Quantity: any;
  marketdata: any;
  watchlist: any;
  socketdata: any;
  public SavedProd = [];
  public ProductList: any;
  product: any = "";
  productId: any = "";
  editOrderForm: FormGroup = new FormGroup({
    Orderprice: new FormControl('', Validators.required),
    OrderQuantity: new FormControl('', Validators.required),
  })
  //----------------cart dummy data
  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];

  //------------
  constructor(private _ApiService: ApiService, private notifications: NotificationsService, private modalService: BsModalService) { }

  ngOnInit(): void {
    this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
    // this.GetOrder();
    this.GetMarketdepth();
    this.GetWatchList('0');
    this.GetsavedProducts();
    this._ApiService.currentData.subscribe(data => {
      console.log('watchlist', data);
      this.socketdata = data;
      if (this.socketdata.e != null) {
        if (this.socketdata.e == 'watchList') {
          this.watchlist = this.watchlist.wL;
          console.log('------------watchList', this.watchlist);
        }
        else if (this.socketdata.e == 'marketTrade') {
          this.marketdata = this.socketdata.tH;
          console.log('------------watchList', this.marketdata);
        }
      }
      else {
        // this.watchlist = [];
        // this.marketdata=[];
      }

    })
    // if(this.watchlist!=undefined){
    //   this._ApiService.UnsunscribemessageWatchlistdata();
    // }

    setTimeout(() => {
      this._ApiService.sendmessageWatchlistdata();
      this._ApiService.sendmessageMarkettdata();
    }, 3000);
  }
  getProduct(event) {
    this.product = event.ProdName;
    this.productId = event.prodId;
    // alert(this.productId);
    localStorage.setItem('ProcuctId', this.productId);
    document.getElementById("myDropdownP").classList.toggle("show");
    this.GetWatchList(this.productId);
  }
  // GetOrder() {
  //   var orderdata = {};
  //   var companyID = localStorage.getItem('CompanyID');
  //   orderdata['companyId'] = companyID;
  //   orderdata['businessTypeId'] = this.BusinessTypeId;
  //   orderdata['pageNo'] = 1;
  //   orderdata['noOfItemsPerPage'] = 50;
  //   this._ApiService.Insightdata(orderdata)
  //     .subscribe(data => {
  //       if (data.error.errorData == 0) {
  //         this.Orderdata = data.orderList;
  //         // console.log(data.orderList);
  //       }
  //     })
  // }
  // OpenEditmodal(template, Id, price, quantity,orderType) {
  //   this.modalRef = this.modalService.show(
  //     template, Object.assign({}, { class: 'gray modal-md' })
  //   );
  //   this.OrderId = Id;
  //   this.editOrderForm.get('Orderprice').setValue(price) ;
  //   this.editOrderForm.get('OrderQuantity').setValue(quantity) ;
  //   this.orderType=orderType;
  // }
  // Editorder(form:any) {
  //  //console.log('vvvvvvvvvvv',form.value);
  //      var orderdata = {};
  //   var companyID = localStorage.getItem('CompanyID');
  //   orderdata['companyId'] = companyID;
  //   orderdata['orderId'] = this.OrderId;
  //   orderdata['bidPrice'] = form.value.Orderprice;
  //   orderdata['bidQuantity'] =form.value.OrderQuantity;
  //   this._ApiService.Editdata(orderdata)
  //     .subscribe(data => {
  //       if (data.error.errorData == 0) {
  //          this.modalRef.hide();
  //          this.GetOrder();
  //         // console.log(data.orderList);
  //         this.notifications.create(
  //           'Done',
  //           'Order has been placed .',
  //           NotificationType.Bare,
  //           { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
  //         );
  //       }
  //     })
  // }
  GetsavedProducts() {

    var productdata = {};
    var CompanyID = localStorage.getItem('CompanyID');

    productdata['companyId'] = CompanyID;
    this._ApiService.getselectedProducts(productdata)
      .subscribe(data => {

        this.ProductList = data.selectedProductList;

        for (var i = 0; i < this.ProductList.length; i++) {
          if (this.ProductList[i].isActive != 0) {
            this.SavedProd.push({ 'prodId': this.ProductList[i].productId, 'ProdName': this.ProductList[i].productName })
          }
        }

        console.log('---------999', this.SavedProd);

      })
  }
  myFunctionP() {
    document.getElementById("myDropdownP").classList.toggle("show");
  }
  filterFunctionP() {

    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput2");
    filter = input.value.toUpperCase();

  }
  GetMarketdepth() {
    this._ApiService.GetMarketTrade()
      .subscribe(data => {
        this.marketdata = data.tradeList;
      })
  }
  GetWatchList(PId) {
    debugger
    this.watchlist = [];
    this._ApiService.GetWatchList(PId)
      .subscribe(data => {
        this.watchlist = data.watchList;
      })
  }
  ngOnDestroy() {
    this._ApiService.UnsunscribemessageWatchlistdata();
    //this._ApiService._disconnect();
  }

}
