import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketinsightsComponent } from './marketinsights.component';

describe('MarketinsightsComponent', () => {
  let component: MarketinsightsComponent;
  let fixture: ComponentFixture<MarketinsightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarketinsightsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketinsightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
