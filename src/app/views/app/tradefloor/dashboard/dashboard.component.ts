import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/data/api.service';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  watchlist: any;
  marketdata:any;
   //----------------cart dummy data
   public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
  ];
  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions= {
    responsive: true,
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgb(134,199,243)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  //------------

  constructor(private _ApiService:ApiService) { }

  ngOnInit(): void {
  }
  GetWatchList(PId) {
    debugger
    this.watchlist = [];
    this._ApiService.GetWatchList(PId)
      .subscribe(data => {
        this.watchlist = data.watchList;
      })
  }
  GetMarketdepth() {
    this._ApiService.GetMarketTrade()
      .subscribe(data => {
        this.marketdata = data.tradeList;
      })
  }
}
