import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClaimComponent } from './claim.component';

import { ClaimrequestComponent } from './claimrequest/claimrequest.component';
import { ClaimstatusComponent } from './claimstatus/claimstatus.component';

const routes: Routes = [{
  path: '', component: ClaimComponent,
  children: [
    { path: '', redirectTo: 'claimrequest', pathMatch: 'full' },
    { path: 'claimrequest', component: ClaimrequestComponent },
    { path: 'claimstatus', component: ClaimstatusComponent }]
  
}
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClaimRoutingModule { }
