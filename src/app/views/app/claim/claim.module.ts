import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
// import { SharedModule } from 'src/app/shared/shared.module';
// import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
// import { BsModalService } from 'ngx-bootstrap/modal';
// import { NotificationsService, SimpleNotificationsModule } from "angular2-notifications";
import { ClaimRoutingModule } from './claim-routing.module';
import { ClaimComponent } from './claim.component';
import { ClaimrequestComponent } from './claimrequest/claimrequest.component';
import { ClaimstatusComponent } from './claimstatus/claimstatus.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NotificationsService, SimpleNotificationsModule } from "angular2-notifications";
@NgModule({
  declarations: [ClaimComponent, ClaimrequestComponent, ClaimstatusComponent],
  imports: [
    CommonModule,
    ClaimRoutingModule,
    BsDatepickerModule.forRoot(),
    FormsModule,ReactiveFormsModule,
    SimpleNotificationsModule.forRoot()
  ]
})
export class ClaimModule { }
