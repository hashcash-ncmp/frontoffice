import { Component, OnInit,TemplateRef} from '@angular/core';
import { ApiService } from 'src/app/data/api.service';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-claimstatus',
  templateUrl: './claimstatus.component.html',
  styleUrls: ['./claimstatus.component.scss']
})
export class ClaimstatusComponent implements OnInit {
  public bsValue = new Date();
  public bsRangeValue: Date[];
  public Tradelimit: any;
  public tableHeader: any;
  public claimData = [];
  public viewData=[];
  modalRef: BsModalRef;
  constructor(private _ApiService: ApiService,private notifications: NotificationsService,private modalService: BsModalService) {
    this.bsRangeValue = [this.bsValue, this.bsValue];

  }

  ngOnInit(): void {
    //this.GetclaimView();
    //this.tableHeader=[{'header':'ClaimID'},{'header':'Claim Request Type'},{'header':'Claim To (Company Name)'},{'header':'Claim Amount'},{'header':'Request Issued Date'},{'header':'Resolution Date'},{'header':'Resolution Attachment'}]
    //  this.tableHeader=[{'header':'ClaimID'},{'header':'Claim Request Type'}]
  }
  GetclaimView() {
    debugger;
    if(this.bsRangeValue!=null){
      let claimInput = {};
      claimInput['queryType'] = 1;
      claimInput['fromDate'] = this.bsRangeValue[0];
      claimInput['toDate'] = this.bsRangeValue[1];
      this._ApiService.viewClaim(claimInput)
        .subscribe(data => {
          if (data.error.errorData == 0&&data.error.errorMsg!='no data..') {
            this.claimData = data.getAllClaimInfo;
  
          }
          else{
            this.notifications.create(
              'Done',
              data.error.errorMsg,
              NotificationType.Bare,
              { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
            );
          }
        })
    }
  
  }
  ViewClaim(requestId,claimModal: TemplateRef<any>){
    let claimInput = {};
     claimInput['requestId'] = requestId;
      this._ApiService.GetClaimInfo(claimInput)
        .subscribe(data => {
          if (data.error.errorData == 0&&data.error.errorMsg!='no data..') {
            this.viewData = data.getClaimViewTab;
            this.modalRef = this.modalService.show(
              claimModal, Object.assign({}, { class: 'gray modal-md' })
            );
          }
          else{
            this.notifications.create(
              'Done',
              data.error.errorMsg,
              NotificationType.Bare,
              { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
            );
          }
        })
  }
}