import { Component, OnInit, TemplateRef } from '@angular/core';
import { ApiService } from 'src/app/data/api.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NotificationsService, NotificationType } from 'angular2-notifications';

@Component({
  selector: 'app-claimrequest',
  templateUrl: './claimrequest.component.html',
  styleUrls: ['./claimrequest.component.scss']
})
export class ClaimrequestComponent implements OnInit {
  public bsValue = new Date();
  public bsRangeValue: Date[];
  allproducts = [];
  allcommodity = [];
  claimData = [];
  public states = [];
  public statearray = [];
  public countries = [];
  public issueList=[];
  public issueIdList=[];
  public state: string="";
  public BusinessTypeId: any;
  public tcnNo:any;
  public ImageArray: any = [];
  subProductId: string = "";
  productId: string = "";
  modalRef: BsModalRef;
  claimForm: FormGroup = new FormGroup({
    issueType: new FormControl('', Validators.required),
    issueId: new FormControl('', Validators.required),
    message:new FormControl('', Validators.required),
    Image:new FormControl('', Validators.required),
    priority:new FormControl('', Validators.required)
  })
  constructor(private _ApiService: ApiService, private notifications: NotificationsService, private modalService: BsModalService) {
    this.bsRangeValue = [this.bsValue, this.bsValue];
  }

  ngOnInit(): void {
    this.GetAllProduct();
    this.Getstate();
  }
  GetcompanyNames() {

  }
  Getstate() {
    this.statearray = [];
    this.states = [];
    this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
    this._ApiService.getcompanystatedistrictForOrder(this.BusinessTypeId)
      .subscribe(data => {
        this.countries = data.locationList;
        //var temparrray = this.countries;
        // console.log('-------------', this.countries);
        for (var i = 0; i < this.countries.length; i++) {
          if (this.countries != null) {
            var found = this.statearray.includes(this.countries[i].stateId);
            if (found == false) {
              this.states.push({ 'stateName': this.countries[i].stateName, 'stateId': this.countries[i].stateId });
            }
            this.statearray.push(this.countries[i].stateId)
          }
        }

      })

  }
  Getclaimdata() {
    let claiminput = {};
    var companyID = localStorage.getItem('CompanyID');
    claiminput['companyId'] = companyID;
    claiminput['fromDate'] = this.bsRangeValue[0];
    claiminput['toDate'] = this.bsRangeValue[1];
    claiminput['productId'] = this.productId;
    claiminput['subProductId'] = this.subProductId;
    claiminput['stateId'] = this.state;
    this._ApiService.getClaimData(claiminput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.claimData = data.addAllLists;
          console.log('ppppppppp', data.productList);

        }
        else {

        }
      })
  }
  GetAllProduct() {

    let offerinput = {};
    var companyID = localStorage.getItem('CompanyID');
    offerinput['companyId'] = companyID;
    this._ApiService.Getallproduct(offerinput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.allproducts = data.productList;
          // console.log('ppppppppp', data.productList);

        }
      })
  }
  RaiseclainFlag(claimModal: TemplateRef<any>,tcn) {
    let claimInput = {};
    var companyID = localStorage.getItem('CompanyID');
    claimInput['companyId'] = companyID;
    claimInput['productId'] = this.productId;
    claimInput['subProductId'] = this.subProductId;
    claimInput['tcnNo'] = tcn;
    this.tcnNo=tcn;
    this._ApiService.RaiseClaimConfirmation(claimInput)
    .subscribe(data=>{
      if(data.error.errorData==0){
        var claimFlag=data.raiseClaimId;
        //if(claimFlag==1){
          this.GetIssuetypelist();
          this.modalRef = this.modalService.show(
            claimModal, Object.assign({}, { class: 'gray modal-md' })
          );
      //  }
      
      }
      else{
        this.notifications.create(
          'Done',
          data.error.errorMsg,
          NotificationType.Bare,
          { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
        );
      }
     
    })
  }
 GetIssuetypelist(){
   debugger;
  this._ApiService.GetissuetypeList()
  .subscribe(data=>{
    if(data.error.errorData==0){
      this.issueList=data.addAllLists;
      console.log('-----------',data.addAllLists);
    }
        
  })
 }
 GetIssueIdDtl(event){
   debugger;
   let IssueInput={};
   var companyID = localStorage.getItem('CompanyID');
   IssueInput['issueTypeId']=event;
   IssueInput['tcnNo']=this.tcnNo;
   IssueInput['companyId']=companyID;
  this._ApiService.GetIssueId(IssueInput)
  .subscribe(data=>{
    if(data.error.errorData==0){
      this.issueIdList=data.issueIds;
      console.log('-----------',data.issueIds);
    }
        
  }) 
 }
 Submitclaim(form:any){
   debugger;
  let claimInput={};
  var companyID = localStorage.getItem('CompanyID');
  claimInput['companyId']=companyID;
  claimInput['issueTypeId']=form.value.issueType;
  claimInput['issueId']=form.value.issueId;
  claimInput['message']=form.value.message;
  claimInput['priority']=form.value.priority;
  claimInput['queryType']=1;
   let claimInputfile=new FormData();
  claimInputfile.append('data',JSON.stringify(claimInput));
  claimInputfile.append('claimImage',this.ImageArray[0].files[0]);
 this._ApiService.SubmitClaim(claimInputfile)
 .subscribe(data=>{
   if(data.error.errorData==0){
    
     console.log('-----------',data.error.errorMsg);
     this.modalRef.hide();
   }
   else{
    this.modalRef.hide();
   }
       
 })
 }
  getcommodityProduct(event) {
    localStorage.removeItem("ProductID");
    localStorage.removeItem("SubProductID");
    localStorage.removeItem("stateID");
    localStorage.setItem('ProductID', event);
    let commodityinput = {};
    var companyID = localStorage.getItem('CompanyID');
    commodityinput['productId'] = event;
    this._ApiService.Getcommodities(commodityinput)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.allcommodity = data.subProductList;
          //console.log('ccccccccccccc', data);

        }
      })
  }
  ImageUpload() {

  }
  CheckUploadfile = (e) => {
    debugger;
   //this.imagecheck = false;
    this.ImageArray = [];
    let files = e.target.files;
    let length = e.target.files.length
    let fileArr = [];
    let isValid = false;
  
    if (length > 0) {
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        console.log(file)
        if (file.type === 'application/pdf' || file.type === 'application/msword' || file.type === 'image/jpeg' || file.type === 'image/bmp' ||
          file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {

          if (file.size <= 1048576) {
            fileArr.push(file);
            isValid = true
          } else {
        
          }
        } else {
        
        }
      }
     
      if (isValid) {
        let obj = {}
        // obj['id'] = id;
        obj['files'] = fileArr
        this.ImageArray.push(obj);
      }
    
    }

  }
}
