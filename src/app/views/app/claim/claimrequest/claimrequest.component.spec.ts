import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimrequestComponent } from './claimrequest.component';

describe('ClaimrequestComponent', () => {
  let component: ClaimrequestComponent;
  let fixture: ComponentFixture<ClaimrequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaimrequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimrequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
