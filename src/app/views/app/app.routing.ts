import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { BlankPageComponent } from './blank-page/blank-page.component';
import{AuthGuard} from '../app/auth.guard';


const routes: Routes = [
    {
        path: '', component: AppComponent,
        children: [
            { path: '', pathMatch: 'full',canActivate: [AuthGuard],redirectTo: 'account' },
            { path: 'vien', loadChildren: () => import('./vien/vien.module').then(m => m.VienModule) },
            { path: 'second-menu', loadChildren: () => import('./second-menu/second-menu.module').then(m => m.SecondMenuModule) },
            { path: 'blank-page', component: BlankPageComponent },
            { path: 'account',canActivate: [AuthGuard],loadChildren: () => import('./account/account.module').then(m => m.AccountModule) },
            { path: 'payments', loadChildren: () => import('./payments/payments.module').then(m => m.PaymentsModule) },
            { path: 'marketinsights',canActivate: [AuthGuard],component: BlankPageComponent },
            { path: 'tradefloor',canActivate: [AuthGuard], loadChildren: () => import('./tradefloor/tradefloor.module').then(m => m.TradefloorModule) },
            { path: 'delivery',canActivate: [AuthGuard], loadChildren: () => import('./delivery/delivery.module').then(m => m.DeliveryModule) },
            { path: 'managewallate',canActivate: [AuthGuard] , component: BlankPageComponent },
            { path: 'qualityassuarance',canActivate: [AuthGuard], loadChildren: () => import('./qualityassurance/qualityassurance.module').then(m => m.QualityassuranceModule) },
            { path: 'shipment',canActivate: [AuthGuard] , component: BlankPageComponent },
            { path: 'supportandclaims',canActivate: [AuthGuard] , component: BlankPageComponent },
            { path: 'shipments',canActivate: [AuthGuard], loadChildren: () => import('./shipment/shipment.module').then(m => m.ShipmentModule) },
            { path: 'claim',canActivate: [AuthGuard], loadChildren: () => import('./claim/claim.module').then(m => m.ClaimModule) },
            { path: 'report',canActivate: [AuthGuard], loadChildren: () => import('./report/report.module').then(m => m.ReportModule) }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
