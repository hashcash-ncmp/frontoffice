import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/data/api.service';
// import { FormsModule } from '@angular/forms';
import { NotificationsService, NotificationType } from 'angular2-notifications';
@Component({
  selector: 'app-mywallet',
  templateUrl: './mywallet.component.html',
  styleUrls: ['./mywallet.component.scss']
})
export class MywalletComponent implements OnInit {
  public walletdata: any;
  constructor(private notifications: NotificationsService, private _ApiService: ApiService) { }

  ngOnInit(): void {
    this.showmywallet();
  }
  showmywallet() {
    var companyID = localStorage.getItem('CompanyID');
    let walletInput = {};
    walletInput['companyId'] = companyID;
    this._ApiService.Getmywallet(walletInput)
      .subscribe(data => {
        this.walletdata = data.walletInfo;
      })
  }

}