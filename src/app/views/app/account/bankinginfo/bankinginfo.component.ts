import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, ViewChild, OnInit, TemplateRef } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ApiService } from 'src/app/data/api.service';
import { NotificationsService, NotificationType } from "angular2-notifications";
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-bankinginfo',
  templateUrl: './bankinginfo.component.html',
})
export class BankinginfoComponent implements OnInit {
  @ViewChild('bankInfoForm') loginForm: NgForm;
  modalRef: BsModalRef;
  flag: boolean = true;
  public RecordedBankNumber: any;
  public accountNo: any;
  public branchAddress: any;
  public branchname: any;
  public IfcCode: any;
  public accHoldername: any;
  public BankName: any;

  //public flagforAccnoChange: boolean = true;
  public comBankID: any;
  public companyId: any;
  public otp: any;
  public ImageArray: any = [];
  public imagecheck: boolean = false;
  public imgsizecheck: boolean = false;
  bankinginfo = {
    accountHolderName: '',
    accountNumber: '',
    bankName: '',

    branchAddress: '',
    ifscCode: '',
    branchname: '',
    accoutnType: '',
    city: '',
    sate: ''
  };
  constructor(private translate: TranslateService,private spinner: NgxSpinnerService,private notifications: NotificationsService, private _ApiService: ApiService, private modalService: BsModalService) { }

  ngOnInit(): void {
    this.GetBankInfo();
  }

  makeEditable() {
    this.flag = false;
    // this.modalRef = this.modalService.show(
    //   template, Object.assign({}, { class: 'gray modal-md' })
    // );
  }

  GetBankInfo() {
    let bankInfoform = new FormData();
    var companyID = localStorage.getItem('CompanyID');
    bankInfoform.append('companyId', companyID);
    this._ApiService.BankingInfodata(bankInfoform)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.bankinginfo.accountNumber = data.accountNo;
          this.bankinginfo.accountHolderName = data.accountHolderName;
          this.bankinginfo.bankName = data.bankName;
          this.bankinginfo.branchAddress = data.branchAddress;
          this.bankinginfo.ifscCode = data.ifscCode;
          this.RecordedBankNumber = data.accountNo;
          this.comBankID = data.companyBankId;
          this.companyId = data.companyId;
          this.bankinginfo.branchname = data.branchName;
          this.bankinginfo.accoutnType = data.accountType;
          this.bankinginfo.branchAddress = data.branchAddress;
          this.bankinginfo.sate = data.state;
          this.bankinginfo.city = data.city;

        }

      })

  }
  // matchAccountNo(number) {
  //   if (number != this.RecordedBankNumber) {
  //     this.flagforAccnoChange = false;
  //   }
  //   else {
  //     this.flagforAccnoChange = true;
  //   }

  // }
  showOtpModal(template: TemplateRef<any>) {
    debugger;
    this.modalRef = this.modalService.show(
      template, Object.assign({}, { class: 'gray modal-md' })
    );
    let otpdata = {};
    var companyID = localStorage.getItem('CompanyID');
    otpdata['companyId'] = companyID;
    this._ApiService.Otpgenarate(otpdata)
      .subscribe(data => {
        if (data.error.errorData == 0) {

          var result = data.userResult;
          var OTP= result.phNoOtp;
          //   alert('Ckeck OTP on your registered mobile no!');
          alert('Your OTP is ' + OTP);
          this.notifications.create(
            'Done',
            'Ckeck OTP on your registered mobile no!',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );

        }
        console.log();


      })
  }
  CheckUploadfile = (e) => {
    debugger;
    //  let identifier = '#companyDoc' + id;
    //   $(identifier).siblings('.text-danger').hide();
    this.imagecheck = false;
    this.ImageArray = [];
    let files = e.target.files;
    let length = e.target.files.length
    let fileArr = [];
    let isValid = false;
    //console.log('************START*************')
    //console.log(this.companyKycDocArray)
    if (length > 0) {
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        console.log(file)
        if (file.type === 'application/pdf' || file.type === 'application/msword' || file.type === 'image/jpeg' || file.type === 'image/bmp' ||
          file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {

          if (file.size <= 1048576) {
            fileArr.push(file);
            isValid = true
            this.imgsizecheck = false;
          } else {
            this.imgsizecheck = true;
            // $(identifier).siblings('.text-danger').show();
          }
        } else {
          this.imgsizecheck = true;
          // $(identifier).siblings('.text-danger').show();
        }
      }
      // let index = this.companyKycDocArray.findIndex(x => x['id'] === id);
      // console.log(index)
      //   if (index !== -1) {
      //     this.companyKycDocArray.splice(index, 1);
      //   }
      // } else {
      //   $(identifier).siblings('.text-danger').show();
      // }
      // console.log(isValid)
      if (isValid) {
        let obj = {}
        // obj['id'] = id;
        obj['files'] = fileArr
        this.ImageArray.push(obj);
      }
      //   var companyID = localStorage.getItem('CompanyID');
      //   let payload = new FormData();
      //   payload.append('companyId ', companyID);
      // //  payload.append('photoDoc',this.ImageArray);
      //  let testfile=this.ImageArray[0].files[0];
      //  console.log('---------',testfile);
      //  payload.append('photoDoc', testfile);

      // for (let j = 0; j < this.ImageArray[0].files[0].length; j++) {
      //   let file =  this.ImageArray[0].files[0][j];
      //   payload.append('files', file);
      // }
      // console.log(this.companyKycDocArray)
      //console.log('************END*************')

      // if (this.ImageArray != '') {
      //   this._ApiService.SaveprofileImage(payload)
      //     .subscribe(data => {

      //     })
      // }
      // else {
      //   this.imagecheck=true;
      //   this.notifications.create(
      //     'Done',
      //     'Email updated Successfully!',
      //     NotificationType.Bare,
      //     { theClass: 'Contact No or emailId can not be blank', timeOut: 6000, showProgressBar: true }
      //   );

      // }

    }

  }
  ImageUpload() {
    debugger;
    if (this.ImageArray != '') {
      var companyID = localStorage.getItem('CompanyID');
      let payload = new FormData();
      payload.append('companyId ', companyID);
      //  payload.append('photoDoc',this.ImageArray);
      let testfile = this.ImageArray[0].files[0];
      console.log('---------', testfile);
      payload.append('photoDoc', testfile);

      this._ApiService.SaveprofileImage(payload)
        .subscribe(data => {
          this.ImageArray=[];
          this.notifications.create(
            'Done',
            'Image Uploaded Successfully!',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );

        })
    }
    else {
      this.imagecheck = true;
      this.notifications.create(
        'Done',
        'file can not be blank',
        NotificationType.Bare,
        { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
      );

    }
  }
  UpdatebankInfo() {
    var companyID = localStorage.getItem('CompanyID');
    let checkOTPdata = {};
    var companyID = localStorage.getItem('CompanyID');
    checkOTPdata['companyId'] = companyID;
    checkOTPdata['phNoOtp'] = this.otp;
    if (this.otp != null) {
      this._ApiService.OtpCheck(checkOTPdata)
        .subscribe(data => {
          if (data.error.errorData == 0) {
            let bankInfo = {};
            bankInfo['companyBankId'] = this.comBankID;
            bankInfo['companyId'] = this.companyId;
            if (this.BankName != '' && this.BankName != undefined) {
              bankInfo['bankName'] = this.BankName
            }
            else {
              bankInfo['bankName'] = this.bankinginfo.bankName;
            }
            if (this.accountNo != '' && this.accountNo != undefined) {
              bankInfo['accountNo'] = this.accountNo;
            }
            else {
              bankInfo['accountNo'] = this.bankinginfo.accountNumber;
            }

            if (this.branchname != '' && this.branchname != undefined) {
              bankInfo['branchName'] = this.branchname;
            }
            else {
              bankInfo['branchName'] = this.bankinginfo.branchname;
            }
            if (this.IfcCode != '' && this.IfcCode != undefined) {
              bankInfo['ifscCode'] = this.IfcCode;
            }
            else {
              bankInfo['ifscCode'] = this.bankinginfo.ifscCode;
            }

            if (this.branchAddress != '' && this.branchAddress != undefined) {
              bankInfo['branchAddress'] = this.branchAddress;
            }
            else {
              bankInfo['branchAddress'] = this.bankinginfo.branchAddress;
            }
            if (this.accHoldername != '' && this.accHoldername != undefined) {
              bankInfo['accountHolderName'] = this.accHoldername;
            }
            else {
              bankInfo['accountHolderName'] = this.bankinginfo.accountHolderName;
            }
            bankInfo['accountType'] = this.bankinginfo.accoutnType;
            bankInfo['state'] = this.bankinginfo.sate;
            bankInfo['city'] = this.bankinginfo.city;

            this._ApiService.BankingInfoSave(bankInfo)
              .subscribe(data => {
                if (data.error.errorData == 0) {
                  this.modalRef.hide();

                  this.notifications.create(
                    'Done',
                    'Bank Information updated Successfully!',
                    NotificationType.Bare,
                    { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
                  );
                  this.GetBankInfo();
                  this.flag = true;
                }
              })

          }
        })
    }
  }
  bankForm = new FormGroup({
    accountNo: new FormControl(null, Validators.required),
    ifscCode: new FormControl(null, Validators.required)
  });
  companyID=localStorage.getItem("CompanyID");
  openEditBankModal(template:TemplateRef<any>){
    this.modalRef=this.modalService.show(template,{backdrop:'static'});
  }  
  editBank() {
    let accountNo = this.bankForm.get('accountNo').value;
    let ifscCode = this.bankForm.get('ifscCode').value;
    if (accountNo && ifscCode) {
      this.spinner.show();
      this._ApiService.verifyBankAccountData({
        companyId: this.companyID, accountNo: accountNo, ifscCode: ifscCode
      }).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.bankForm.reset();
            this.onError(data['error'].errorMsg);
          } else {
            this.modalRef.hide();
            this.bankForm.reset();
            this.onSuccess("Bank Verified Successfully");
            this.GetBankInfo();
            this._ApiService.updateVendor({companyId:this.companyID}).subscribe();            
          }
        }, error => {
          this.bankForm.reset();
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }else{
      this.onError("Enter Valid Info!");
    }
  }
  onError(message: string): void {
    this.notifications.create(this.translate.instant('alert.error'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 5000, showProgressBar: true });
  }
  onSuccess(message: string): void {
    this.notifications.create(this.translate.instant('alert.success'),
      this.translate.instant(message),
      NotificationType.Success, { timeOut: 5000, showProgressBar: true });
  }

}
