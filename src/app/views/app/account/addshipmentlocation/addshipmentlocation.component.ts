import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ApiService } from 'src/app/data/api.service';
import { NotificationsService, NotificationType } from 'angular2-notifications';
@Component({
  selector: 'app-addshipmentlocation',
  templateUrl: './addshipmentlocation.component.html',
  styleUrls: ['./addshipmentlocation.component.scss']
})
export class AddshipmentlocationComponent implements OnInit {
  public ShipmentlocationList = [];
  public editShipmentLocationlist = [];
  public AddshipmentInfo = [];
  public UpdateShipmentInfo = [];
  modalRef: BsModalRef;
  public countries = [];
  public states = [];
  public statearray = [];
  public zones = [];
  public zonearray = [];
  public City = [];
  public cityarray = [];
  public region = [];
  public regionarray = [];
  public deleteShipBranchId: any;
  public BusinessTypeId:any;
  public shipmentflag: boolean = false;
  currentDate: any = new Date().toISOString().slice(0, 10);
  currentYear: any = new Date().getFullYear();
  currentMonth: any = (new Date().getMonth()) + 1;
  // currentDay: any = new Date().getDate();
  addshipmentBranchForm: FormGroup = new FormGroup({
    branchname: new FormControl('', Validators.required),
    addresslineone: new FormControl('', Validators.required),
    addresslinetwo: new FormControl('',),
    state: new FormControl('', Validators.required),
    region:new FormControl('', Validators.required),
    district: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    pinCode: new FormControl('', Validators.required),
    contactNo: new FormControl('', Validators.required),


  });
  editShipmentBranchForm: FormGroup = new FormGroup({
    branchname: new FormControl('', Validators.required),
    shipmentId: new FormControl('', Validators.required),
    addresslineone: new FormControl('', Validators.required),
    addresslinetwo: new FormControl('',),
    state: new FormControl('', Validators.required),
    region:new FormControl('', Validators.required),
    district: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    pinCode: new FormControl('', Validators.required),
    contactNo: new FormControl('', Validators.required),
    // gstinNo: new FormControl('', Validators.required),
    // gstinIssueDate: new FormControl('', Validators.required),
    // comBranchId: new FormControl('')
  });
  constructor(private _ApiService: ApiService, private modalService: BsModalService, private notifications: NotificationsService) { }

  ngOnInit(): void {
    this.GetShipmentlist();
    this.Getstate();
    // this.companylocationList;
  }
  addShipmentDetailsModal(template: TemplateRef<any>) {
    debugger;
    if (this.ShipmentlocationList != null) {
      if (this.ShipmentlocationList.length >= 5) {
        this.shipmentflag = true;
      }
      else {
        this.shipmentflag = false;
        this.modalRef = this.modalService.show(
          template, Object.assign({}, { class: 'gray modal-lg' })
        );
      }

    }
    else {
      this.shipmentflag = false;

    }


  }
  EditComhModal(index, template: TemplateRef<any>) {

    debugger;
    this.editShipmentLocationlist = this.ShipmentlocationList[index];

   // console.log('----+++++', this.editShipmentLocationlist);

    this.modalRef = this.modalService.show(
      template, Object.assign({}, { class: 'gray modal-lg' })
    );
    this.Getstate();

    if (this.ShipmentlocationList[index].districtId != "") {

      setTimeout(() => this.GetDistrict(this.ShipmentlocationList[index].stateId), 100);
    }
    if (this.ShipmentlocationList[index].districtId != "") {

      setTimeout(() => this.Getcity(this.ShipmentlocationList[index].districtId), 300);
    }

  }
  // Getstate() {
  //   this.zones = [];
  //   this.City = [];
  //   this.statearray = [];
  //   this._ApiService.getcompanystatedistrict()
  //     .subscribe(data => {
  //       this.countries = data.locationList;
  //       //var temparrray = this.countries;
  //       console.log('-------------', this.countries);
  //       for (var i = 0; i < this.countries.length; i++) {
  //         if (this.countries != null) {

  //           var found = this.statearray.includes(this.countries[i].stateId);
  //           if (found == false) {
  //             this.states.push({ 'stateName': this.countries[i].stateName, 'stateId': this.countries[i].stateId });
  //           }
  //           this.statearray.push(this.countries[i].stateId)
  //         }
  //       }
  //       // console.log('-------------', this.states);
  //       // this.states=data.states;
  //     })

  // }
  // GetZone(data) {
  //   this.zones = [];
  //   this.City = [];
  //   this.zonearray = [];
  //   for (var j = 0; j < this.countries.length; j++) {
  //     if (this.countries[j].stateId == data) {
  //       var foundzone = this.zonearray.includes(this.countries[j].districtId);
  //       if (foundzone == false) {
  //         this.zones.push({ 'districtName': this.countries[j].districtName, 'districtId': this.countries[j].districtId });
  //       }
  //       this.zonearray.push(this.countries[j].districtId);
  //     }
  //   }
  //   console.log('-------------', this.zones);
  // }
  // Getcity(data) {

  //   this.City = [];
  //   this.cityarray = [];
  //   for (var j = 0; j < this.countries.length; j++) {
  //     if (this.countries[j].districtId == data) {
  //       var foundcity = this.cityarray.includes(this.countries[j].cityId);
  //       if (foundcity == false) {
  //         this.City.push({ 'cityName': this.countries[j].cityName, 'cityId': this.countries[j].cityId });
  //       }
  //       this.cityarray.push(this.countries[j].cityId);
  //     }
  //   }

  // }
  
  Getstate() {

    this.zones = [];
    this.City = [];
    this.statearray = [];
    this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
    this._ApiService.getcompanystatedistrict()
      .subscribe(data => {
        this.countries = data.locationList;
        //var temparrray = this.countries;
        console.log('-------------', this.countries);
        for (var i = 0; i < this.countries.length; i++) {
          if (this.countries != null) {

            var found = this.statearray.includes(this.countries[i].stateId);
            if (found == false) {
              this.states.push({ 'stateName': this.countries[i].stateName, 'stateId': this.countries[i].stateId });
            }
            this.statearray.push(this.countries[i].stateId)
          }
        }
        // console.log('-------------', this.states);
        // this.states=data.states;
      })

  }
  GetRegion(data) {
    debugger;
    this.zones = [];
    this.zonearray = [];
    this.region = [];
    this.regionarray = [];
    this.City = [];
    this.cityarray = [];
    //this.stateId="";
    // this.stateId= data;
    localStorage.setItem('stateID', data)
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].stateId == data) {
        var foundzone = this.regionarray.includes(this.countries[j].regionId);
        if (foundzone == false) {
          this.region.push({ 'regionName': this.countries[j].regionName, 'regionId': this.countries[j].regionId });
        }
        this.regionarray.push(this.countries[j].regionId);
      }

    }
  
    if (this.region[0].regionName == 'NA') {
      this.GetDistrict(data);
    }
    // else{
    //   this.GetDistrict(data);
    // }
  }
  GetDistrict(data) {

    this.zones = [];
    this.zonearray = [];
    this.City = [];

    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].stateId == data) {
        var foundzone = this.zonearray.includes(this.countries[j].districtId);
        if (foundzone == false) {
          this.zones.push({ 'districtName': this.countries[j].districtName, 'districtId': this.countries[j].districtId });
        }
        this.zonearray.push(this.countries[j].districtId);
      }
    }
    console.log('-------------', this.zones);
  }
  GetDistrictForRegion(data) {
    if (data != 0) {
      this.zones = [];
      this.zonearray = [];
      this.City = [];
      for (var j = 0; j < this.countries.length; j++) {
        if (this.countries[j].regionId == data) {
          var foundzone = this.zonearray.includes(this.countries[j].districtId);
          if (foundzone == false) {
            this.zones.push({ 'districtName': this.countries[j].districtName, 'districtId': this.countries[j].districtId });
          }
          this.zonearray.push(this.countries[j].districtId);
        }
      }
    }

    console.log('-------------', this.zones);
  }
  Getcity(data) {
    this.City = [];
    this.cityarray = [];
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].districtId == data) {
        var foundcity = this.cityarray.includes(this.countries[j].cityId);
        if (foundcity == false) {
          this.City.push({ 'cityName': this.countries[j].cityName, 'cityId': this.countries[j].cityId });
        }
        this.cityarray.push(this.countries[j].cityId);
      }
    }

  }
  GetShipmentlist() {
    var companyID = localStorage.getItem('CompanyID');
    this._ApiService.getshipmentlist(companyID)
      .subscribe(data => {
        if (data.error.errorData == 0 && data.error.errorMsg != 'No data!') {
          this.ShipmentlocationList = data.shipmentLocationList;
          console.log('============', this.ShipmentlocationList);
          if (this.ShipmentlocationList.length >= 5) {
            this.shipmentflag = true;
          }
          else {
            this.shipmentflag = false;
          }
        }

      })
  }
  addShipmentBranchDetails(form: any) {
    debugger;
    this.AddshipmentInfo = [];
    var addshipmentInfo = {};
    var companyID = localStorage.getItem('CompanyID');
    addshipmentInfo['companyId'] = companyID;
    addshipmentInfo['branchName'] = form.value.branchname;
    addshipmentInfo['addressLineOne'] = form.value.addresslineone;
    addshipmentInfo['addressLineTwo'] = form.value.addresslinetwo;
    addshipmentInfo['countryId'] = '1';
    addshipmentInfo['stateId'] = form.value.state;
    addshipmentInfo['regionId'] = form.value.region;
    addshipmentInfo['districtId'] = form.value.district;
    addshipmentInfo['cityId'] = form.value.city;
    addshipmentInfo['pinCode'] = form.value.pinCode;
    addshipmentInfo['contactNo'] = form.value.contactNo;
    this.AddshipmentInfo.push(addshipmentInfo);
    //  console.log('999999999999',addcomInfo);
    this._ApiService.AddShipmentBranch(this.AddshipmentInfo)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.modalRef.hide();
          this.GetShipmentlist();

          this.addshipmentBranchForm.reset();
          this.Getstate();
          this.notifications.create(
            'Done',
            'Branch added successfully!!',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }
        if (this.ShipmentlocationList.length >= 5) {
          this.shipmentflag = true;
        }
        else {
          this.shipmentflag = false;
        }
      })
  }
  editShipmentDetailsModal(form: any) {
    this.UpdateShipmentInfo = [];
    var UpdateInfo = {};
    var companyID = localStorage.getItem('CompanyID');

    UpdateInfo['shipmentId'] = form.value.shipmentId;
    UpdateInfo['companyId'] = companyID;
    UpdateInfo['branchName'] = form.value.branchname;
    UpdateInfo['addressLineOne'] = form.value.addresslineone;
    UpdateInfo['addressLineTwo'] = form.value.addresslinetwo;
    UpdateInfo['countryId'] = '1';
    UpdateInfo['stateId'] = form.value.state;
    UpdateInfo['regionId'] = form.value.region;
    UpdateInfo['districtId'] = form.value.district;
    UpdateInfo['cityId'] = form.value.city;
    UpdateInfo['pinCode'] = form.value.pinCode;
    UpdateInfo['contactNo'] = form.value.contactNo;
    this.UpdateShipmentInfo.push(UpdateInfo);
    this._ApiService.UpdateshipmentBranch(this.UpdateShipmentInfo)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.modalRef.hide();
          this.notifications.create(
            'Done',
            'Branch updated successfully!!',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }

      })
  }

  DeletealertModal(template: TemplateRef<any>, ID) {
    this.modalRef = this.modalService.show(
      template, Object.assign({}, { class: 'gray modal-lg' })
    );
    this.deleteShipBranchId = ID;
  }
  DelecompanyModal() {
    let comform = {};
    comform['shipmentId'] = this.deleteShipBranchId;
    var companyID = localStorage.getItem('CompanyID');
    comform['companyId'] = companyID;
    this._ApiService.ShipmenLocationtDelete(comform)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.modalRef.hide();
          this.notifications.create(
            'Done',
            'Branch deleted successfully!!',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
          this.GetShipmentlist();


        }
      })
    if (this.ShipmentlocationList.length >= 5) {
      this.shipmentflag = true;
    }
    else {
      this.shipmentflag = false;
    }
  }

}
