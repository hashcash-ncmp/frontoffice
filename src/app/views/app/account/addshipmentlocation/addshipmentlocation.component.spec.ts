import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddshipmentlocationComponent } from './addshipmentlocation.component';

describe('AddshipmentlocationComponent', () => {
  let component: AddshipmentlocationComponent;
  let fixture: ComponentFixture<AddshipmentlocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddshipmentlocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddshipmentlocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
