import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/data/api.service';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { NotificationsService, NotificationType } from 'angular2-notifications';

import { ValueTransformer } from '@angular/compiler/src/util';

@Component({
  selector: 'app-productpref',
  templateUrl: './productpref.component.html'
})
export class ProductprefComponent implements OnInit {
  // public productPref = [];
  // public productInfo: any[] = [{
  //   productId: '',
  //   isActive:''
  // }]
  // public pList = [];
  // form: FormGroup;
  // get ordersFormArray() {
  //   return this.form.controls.orders as FormArray;
  // }

  // ProductForm: FormGroup = new FormGroup({
  //   productId: new FormControl(''),
  //   isActive: new FormControl(''),

  // });
  // productPref = [
  //   ['Bombay Rava / Semolina or Chiroti rava - 1 kg', 'Wheat rava / samba rava / bansi rava - 1 kg', 'Idli rava - 1 kg(optional)', 'Rice rava - 500 gms'],
  //   ['Vermicelli / Semiya - 1 packet big', 'Instant rice sevai - 1 big packet', 'Sago / Javvarisi - 1 / 2 kg', 'Tamarind - 1 / 2 kg'],
  //   ['Turmeric powder - 100 gms', 'Sugar - 1 kg', 'Jaggery - 1/2 kg', 'Steamed rice or Raw rice/Sona masoori - 5-7 kgs'],
  //   ['Idli rice/Boiled rice/Salem rice - 5-7 kgs', 'High quality raw rice for Pongal - 1 kg', 'Dosa rice ( optional) - 2 kgs', 'Basmati rice - 1 to 2 kgs'],
  //   ['Brown rice(optional) - 1 kgs', 'Pressed rice / Poha(thick or thin) - 1 kgs', 'Wheat flour - 2 kgs(North Indians, please increase atta to 5 kgs and reduce rice)', 'Maida - 1 / 2 kg(For cakes and snacks)'],
  //   ['Biscuti', 'Ragi flour - 1 kg', 'Millets varieties & oats - 1 / 2 kg each', 'Rice flour - 1 / 2 kg']
  // ];


  form: FormGroup;
  // Data: Array<any> = [
  //   { name: 'Pear', value: 'pear' },
  //   { name: 'Plum', value: 'plum' },
  //   { name: 'Kiwi', value: 'kiwi' },
  //   { name: 'Apple', value: 'apple' },
  //   { name: 'Lime', value: 'lime' }
  // ];
  public pList = [];
  public Data=[];
  public prodcheck:boolean;
  constructor(private fb: FormBuilder,private _ApiService: ApiService,private notifications: NotificationsService) {
    this.form = this.fb.group({
      checkArray: this.fb.array([], [Validators.required]),
     
    })
  }
  // constructor(private notifications: NotificationsService, private formBuilder: FormBuilder, private _ApiService: ApiService) {
  //   this.form = this.formBuilder.group({
  //     orders: new FormArray([], [Validators.required])
  //   });

  // }

  ngOnInit(): void {
    //  this.GetAllproduct();
      this.GetsavedProducts();
     // this.form.get('checkArray').focu;
      // this.form.controls['checkArray'];
     // this.editOrderForm.controls['Orderprice'].disable();
    }
    GetAllproduct() {
      
      this._ApiService.productpreferencedata()
        .subscribe(data => {
          //console.log('---------',data);
         this.Data = data.productsList;
        
        })
    }
    GetsavedProducts(){
      var productdata={};
      var CompanyID = localStorage.getItem('CompanyID');
     
      productdata['companyId'] = CompanyID;
      this._ApiService.getselectedProducts(productdata)
      .subscribe(data => {
       
        this.Data = data.selectedProductList;
       
         //console.log('---------999',data);
      
      })
    }
    onCheckboxChange(e) {
    
      const checkArray: FormArray = this.form.get('checkArray') as FormArray;
      if (e.target.checked) {
        checkArray.push(new FormControl(e.target.value));
      } else {
        let i: number = 0;
        checkArray.controls.forEach((item: FormControl) => {
          if (item.value == e.target.value) {
            checkArray.removeAt(i);
            return;
          }
          i++;
        });
      }
      
    }
  
    SaveProducts() {
      debugger;
  this.pList=[];
      console.log(this.form.value.checkArray);
      const selectedOrderIds=this.form.value.checkArray;
    //   const selectedOrderIds = this.form.value.checkArray
    //   .map((checked, i) => checked ? this.Data[i].productId : null)
    //   .filter(v => v !== null);
     console.log('9999999999999', selectedOrderIds);
      for (var i = 0; i < this.Data.length; i++) {
  
        var found = selectedOrderIds.includes(this.Data[i].productId.toString());
        if (found == false) {
          this.pList.push({ 'productId': this.Data[i].productId, 'isActive': 0 })
        }
        else {
          this.pList.push({ 'productId': this.Data[i].productId, 'isActive': 1 })
        }
  
      }
    //  console.log('________', this.pList);
      let prodPref = {};
      var CompanyID = localStorage.getItem('CompanyID');
      prodPref['productsList'] = this.pList;
      prodPref['companyId'] = CompanyID;
      this._ApiService.Saveproduct(prodPref)
        .subscribe(data => {
          if (data.error.errorData == 0) {
            this.notifications.create(
              'Done',
              'Product added to preferences Successfully!',
              NotificationType.Bare,
              { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
            );
          //  this.form=null;
           
        //    this.addCheckboxes();
          }
        })
    }
  // GetAllproduct() {

  //   // this.productInfo.push({productId:'',
  //   // isActive:''});
  //   this._ApiService.productpreferencedata()
  //     .subscribe(data => {
  //       //console.log('---------',data);
  //       this.productPref = data.productsList;
  //       // for(var i=0;i<this.productPref.length;i++){
  //       //   this.productInfo.push({productId:'',
  //       //   isActive:''});
  //       // }
  //       this.addCheckboxes();
  //     })
  // }
  // private addCheckboxes() {
  //   this.productPref.forEach(() => this.ordersFormArray.push(new FormControl(false)));
  //   console.log('ch-------------', this.ordersFormArray);

  // }

  // SaveProducts() {
  //   debugger;
  //   // console.log('000000',this.form.value);
  //   // this._ApiService.Saveproduct(this.productInfo)
  //   // .subscribe(data=>{
  //   //   if (data.error.errorData == 0) {
  //   //    alert('done');
  //   //   }
  //   // })
  //   const selectedOrderIds = this.form.value.orders
  //     .map((checked, i) => checked ? this.productPref[i].productId : null)
  //     .filter(v => v !== null);
  //   console.log('9999999999999', selectedOrderIds);
  //   console.log('9999999999999', this.form.value.orders);
  //   for (var i = 0; i < this.productPref.length; i++) {

  //     var found = selectedOrderIds.includes(this.productPref[i].productId);
  //     if (found == false) {
  //       this.pList.push({ 'productId': this.productPref[i].productId, 'isActive': 0 })
  //     }
  //     else {
  //       this.pList.push({ 'productId': this.productPref[i].productId, 'isActive': 1 })
  //     }

  //   }
  //   //console.log('this---', this.pList);
  //   let prodPref = {};
  //   var CompanyID = localStorage.getItem('CompanyID');
  //   prodPref['productsList'] = this.pList;
  //   prodPref['companyId'] = CompanyID;
  //   this._ApiService.Saveproduct(prodPref)
  //     .subscribe(data => {
  //       if (data.error.errorData == 0) {
  //         this.notifications.create(
  //           'Done',
  //           'Product added to preferences Successfully!',
  //           NotificationType.Bare,
  //           { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
  //         );
  //        this.form=null;
         
  //         this.addCheckboxes();
  //       }
  //     })
  // }
}
