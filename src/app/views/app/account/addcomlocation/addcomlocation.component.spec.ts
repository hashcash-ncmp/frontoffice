import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcomlocationComponent } from './addcomlocation.component';

describe('AddcomlocationComponent', () => {
  let component: AddcomlocationComponent;
  let fixture: ComponentFixture<AddcomlocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcomlocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcomlocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
