import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ApiService } from 'src/app/data/api.service';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-addcomlocation',
  templateUrl: './addcomlocation.component.html',
  styleUrls: ['./addcomlocation.component.scss']
})
export class AddcomlocationComponent implements OnInit {
  public companylocationList = [];
  public editComLocationlist = [];
  public AddcompanyInfo = [];
  public UpdatecompanyInfo = [];
  public region=[];
  public regionarray=[];
  modalRef: BsModalRef;
  public countries = [];
  public states = [];
  public statearray = [];
  public zones = [];
  public zonearray = [];
  public City = [];
  public cityarray = [];
  public deletecomBranchId: any;
  public companyflag: boolean = false;
  currentDate: any = new Date().toISOString().slice(0, 10);
  currentYear: any = new Date().getFullYear();
  currentMonth: any = (new Date().getMonth()) + 1;
  public BusinessTypeId:any;
  public temppan;
  public  gsterror;
  addCompanyBranchForm: FormGroup = new FormGroup({
    addresslineone: new FormControl('', Validators.required),
    addresslinetwo: new FormControl('',),
    state: new FormControl('', Validators.required),
    district: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    region:new FormControl('', Validators.required),
    pinCode: new FormControl('', Validators.required),
    contactNo: new FormControl('', Validators.required),
    gstinNo: new FormControl('', Validators.required),
    gstinIssueDate: new FormControl('', Validators.required),

  });
  editCompanyBranchForm: FormGroup = new FormGroup({
    addresslineone: new FormControl('', Validators.required),
    addresslinetwo: new FormControl('',),
    state: new FormControl('', Validators.required),
    district: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    region:new FormControl('', Validators.required),
    pinCode: new FormControl('', Validators.required),
    contactNo: new FormControl('', Validators.required),
    gstinNo: new FormControl('', Validators.required),
    gstinIssueDate: new FormControl('', Validators.required),
    comBranchId: new FormControl('')
  });
  constructor(private _ApiService: ApiService, private modalService: BsModalService, private notifications: NotificationsService,private translate: TranslateService, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.Getcompanylist();
    this.Getstate();
    //this.companylocationList;
  }
  addCompanyDetailsModal(template: TemplateRef<any>) {
   // debugger;
    if (this.companylocationList != null) {
      if (this.companylocationList.length >= 5) {
        this.companyflag = true;
      }
      else{
        this.companyflag = false;
        this.modalRef = this.modalService.show(
          template, Object.assign({}, { class: 'gray modal-lg' })
        );
      }

    }
    else {
      this.companyflag = false;
     
    }
    

  }
  EditComhModal(index, template: TemplateRef<any>) {
    debugger;

    this.editComLocationlist = this.companylocationList[index];

    console.log('----+++++', this.editComLocationlist);

    this.modalRef = this.modalService.show(
      template, Object.assign({}, { class: 'gray modal-lg' })
    );
    this.Getstate();

    if (this.companylocationList[index].districtId != "") {
      setTimeout(() => this.GetRegion(this.companylocationList[index].stateId), 100);

      setTimeout(() => this.GetDistrict(this.companylocationList[index].stateId), 100);
    }
    if (this.companylocationList[index].districtId != "") {

      setTimeout(() => this.Getcity(this.companylocationList[index].districtId), 300);
    }

  }
  // Getstate() {
  //   this.zones = [];
  //   this.City = [];
  //   this.statearray = [];
  //   this._ApiService.getcompanystatedistrict()
  //     .subscribe(data => {
  //       this.countries = data.locationList;
  //       //var temparrray = this.countries;
  //       console.log('-------------', this.countries);
  //       for (var i = 0; i < this.countries.length; i++) {
  //         if (this.countries != null) {

  //           var found = this.statearray.includes(this.countries[i].stateId);
  //           if (found == false) {
  //             this.states.push({ 'stateName': this.countries[i].stateName, 'stateId': this.countries[i].stateId });
  //           }
  //           this.statearray.push(this.countries[i].stateId)
  //         }
  //       }
  //       // console.log('-------------', this.states);
  //       // this.states=data.states;
  //     })

  // }
  // GetZone(data) {
  //   debugger;
  //   //alert(data);
  //   this.zones = [];
  //   this.City = [];
  //   this.zonearray = [];
  //   for (var j = 0; j < this.countries.length; j++) {
  //     if (this.countries[j].stateId == data) {
  //       var foundzone = this.zonearray.includes(this.countries[j].districtId);
  //       if (foundzone == false) {
  //         this.zones.push({ 'districtName': this.countries[j].districtName, 'districtId': this.countries[j].districtId });
  //       }
  //       this.zonearray.push(this.countries[j].districtId);
  //     }
  //   }
  //   console.log('-------------', this.zones);
  // }
  // Getcity(data) {

  //   this.City = [];
  //   this.cityarray = [];
  //   for (var j = 0; j < this.countries.length; j++) {
  //     if (this.countries[j].districtId == data) {
  //       var foundcity = this.cityarray.includes(this.countries[j].cityId);
  //       if (foundcity == false) {
  //         this.City.push({ 'cityName': this.countries[j].cityName, 'cityId': this.countries[j].cityId });
  //       }
  //       this.cityarray.push(this.countries[j].cityId);
  //     }
  //   }

  // }
  Getstate() {

    this.zones = [];
    this.City = [];
    this.statearray = [];
    this.BusinessTypeId = localStorage.getItem('BusinessTypeId');
    this._ApiService.getcompanystatedistrict()
      .subscribe(data => {
        this.countries = data.locationList;
        //var temparrray = this.countries;
        console.log('-------------', this.countries);
        for (var i = 0; i < this.countries.length; i++) {
          if (this.countries != null) {

            var found = this.statearray.includes(this.countries[i].stateId);
            if (found == false) {
              this.states.push({ 'stateName': this.countries[i].stateName, 'stateId': this.countries[i].stateId });
            }
            this.statearray.push(this.countries[i].stateId)
          }
        }
        // console.log('-------------', this.states);
        // this.states=data.states;
      })

  }
  GetRegion(data) {
    debugger;
    this.zones = [];
    this.zonearray = [];
    this.region = [];
    this.regionarray = [];
    this.City = [];
    this.cityarray = [];
    //this.stateId="";
    // this.stateId= data;
    localStorage.setItem('stateID', data)
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].stateId == data) {
        var foundzone = this.regionarray.includes(this.countries[j].regionId);
        if (foundzone == false) {
          this.region.push({ 'regionName': this.countries[j].regionName, 'regionId': this.countries[j].regionId });
        }
        this.regionarray.push(this.countries[j].regionId);
      }

    }
  
    if (this.region[0].regionName == 'NA') {
      this.GetDistrict(data);
    }
    // else{
    //   this.GetDistrict(data);
    // }
  }
  GetDistrict(data) {

    this.zones = [];
    this.zonearray = [];
    this.City = [];

    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].stateId == data) {
        var foundzone = this.zonearray.includes(this.countries[j].districtId);
        if (foundzone == false) {
          this.zones.push({ 'districtName': this.countries[j].districtName, 'districtId': this.countries[j].districtId });
        }
        this.zonearray.push(this.countries[j].districtId);
      }
    }
    console.log('-------------', this.zones);
  }
  GetDistrictForRegion(data) {
    if (data != 0) {
      this.zones = [];
      this.zonearray = [];
      this.City = [];
      for (var j = 0; j < this.countries.length; j++) {
        if (this.countries[j].regionId == data) {
          var foundzone = this.zonearray.includes(this.countries[j].districtId);
          if (foundzone == false) {
            this.zones.push({ 'districtName': this.countries[j].districtName, 'districtId': this.countries[j].districtId });
          }
          this.zonearray.push(this.countries[j].districtId);
        }
      }
    }

    console.log('-------------', this.zones);
  }
  Getcity(data) {
    this.City = [];
    this.cityarray = [];
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].districtId == data) {
        var foundcity = this.cityarray.includes(this.countries[j].cityId);
        if (foundcity == false) {
          this.City.push({ 'cityName': this.countries[j].cityName, 'cityId': this.countries[j].cityId });
        }
        this.cityarray.push(this.countries[j].cityId);
      }
    }

  }
  // CheckGstEdit(no) {
  //   //alert(no);
  //   var Noarray = no.split("");
  //   //alert(noarray);
  //   var temppan = Noarray[2] + Noarray[3] + Noarray[4] + Noarray[5] + Noarray[6] + Noarray[7] + Noarray[8] + Noarray[9] + Noarray[10] + Noarray[11];
  //   // alert(temppan);
  //   var paNo= localStorage.getItem('panNo');
  //   var entityPan = paNo;
  //   if (entityPan != temppan) {
  //     this.notifications.create(
  //       'Error',
  //       'Please enter valid GST no',
  //       NotificationType.Bare,
  //       { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
  //     );
  //     //----------  this.data.alert('Please enter valid GST no', 'danger');
  //   }
  //   else {
  //     this.CheckGstExistance(this.EcomgstNo);
  //   }
  //   //18ALWPG5809L1ZM
  //   //ALWPG5809L
  // }
  CheckGst(no) {
    debugger;
    this.gsterror = "";
    if (no == "") {
     var comGst=this.addCompanyBranchForm.get('gstinNo').value;
      var Noarray = comGst;
    }
    else {
      Noarray = no.split("");
    }

    //alert(noarray);temppan gsterror
    this.temppan = Noarray[2] + Noarray[3] + Noarray[4] + Noarray[5] + Noarray[6] + Noarray[7] + Noarray[8] + Noarray[9] + Noarray[10] + Noarray[11];
    // alert(temppan);
     var paNo= localStorage.getItem('panNo');
    if (paNo != this.temppan) {
      //  this.data.alert('Please enter valid GST no','danger');
      this.gsterror = " GST no doesn't match with pan";
    }
    else {
      // this.comState="";
      //   this.Getstate();

      this.CheckGstExistance(comGst);
    }
   var stateId= this.addCompanyBranchForm.get('state').value;
    this.CheckeStatecode(stateId);
    // if (this.comState == "" && this.comgstNo != "") {
    //   this.data.alert('Please select State', 'warning');
    // }
    // else if (this.comState != "" && this.comgstNo != "") {
    //   var stateID = Noarray[0] + Noarray[1];
    //   if (this.comgstNo == stateID) {

    //   }
    //   else {
    //     this.gsterror = "Wrong GST.Please provide proper GST No ."
    //   }
    // }
    //18ALWPG5809L1ZM
    //ALWPG5809L
  }
  CheckGstExistance(gstNo) {
    if (gstNo != undefined && gstNo != '') {
      //console.log(this.loadmail);
      // this.signupObj.email = this.loadmail;
      //  alert(this.signupObj.email);
      if (gstNo != '' && gstNo != undefined) {
        // wip(1);
        //var emailValue = this.comEmail;
        var gstObj = {};
        gstObj['gstNo'] = gstNo;
        var jsonString = JSON.stringify(gstObj);
        this._ApiService.CheckGst(jsonString)
          .subscribe(response => {
            // wip(0);
            var result = response;
            if (result.error.errorData != '0') {
              this.notifications.create(
                'Error',
                'GST already registered , please try with another GST number.',
                NotificationType.Bare,
                { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
              );
              // this.data.alert(result.error.errorMsg, 'danger');
              //  this.data.alert('GST already registered , please try with another GST number', 'warning');
             // this.comEmail = '';
            } else {
              //if (result.userResult.checkEmailPhoneFlag == 1) {

              //$('#signupInputEmail').focus();
              // } else {

              // }
            }
          }, reason => {
            // wip(0);
            this.notifications.create(
              'Error',
              'Internal Server Error.',
              NotificationType.Bare,
              { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
            );
            //----------   this.data.alert('Internal Server Error', 'danger')
          });

      }
    } else {
      this.notifications.create(
        'Error',
        'Please Provide GST No.',
        NotificationType.Bare,
        { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
      );
      //---------- this.data.alert('Please Provide Email Id', 'warning');
    }

  }
  CheckeStatecode(data) {
    debugger;
    if (data != "") {
      var comGst=this.addCompanyBranchForm.get('gstinNo').value;
      var no = comGst;

      var Noarray = no.split("");
      var StateId = Noarray[0] + Noarray[1];
      if (StateId != data) {
        this.gsterror = "";
        // alert( this.gstmatchError);
        this.gsterror = "GST mismatch with state .Please provide proper GST No .";
      }
      else {
        //this.gsterror = "";
      }
    }
    else {
      // this.gsterror="Please select state.";
      //---------- this.data.alert("Please select state.", 'warning');
      this.notifications.create(
        'Error',
        'Please select state.',
        NotificationType.Bare,
        { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
      );
    }

  }
  Getcompanylist() {
    this.spinner.show();
    debugger;
    this.companylocationList=[];
    var companyID = localStorage.getItem('CompanyID');
    this._ApiService.getcompanylist(companyID)
      .subscribe(data => {
        this.spinner.hide();
        if(data.error.errorData==0){
          this.companylocationList = data.companyBranchList;
          console.log('============', this.companylocationList);
          if (this.companylocationList.length >= 5) {
            this.companyflag = true;
          }
          else{
            this.companyflag = false;
          }
        }
       
       
      })
  }
  addcompanyBranchDetails(form: any) {
    this.AddcompanyInfo = [];
    // console.log('___________',form.value);
    var addcomInfo = {};
    var companyID = localStorage.getItem('CompanyID');
    addcomInfo['companyId'] = companyID;
    addcomInfo['addressLineOne'] = form.value.addresslineone;
    addcomInfo['addressLineTwo'] = form.value.addresslinetwo;
    addcomInfo['countryId'] = '1';
    addcomInfo['stateId'] = form.value.state;
    addcomInfo['regionId'] = form.value.region;
    addcomInfo['districtId'] = form.value.district;
    addcomInfo['cityId'] = form.value.city;
    addcomInfo['gstinNo'] = form.value.gstinNo;
    addcomInfo['gstIssuanceDate'] = form.value.gstinIssueDate;
    addcomInfo['pinCode'] = form.value.pinCode;
    addcomInfo['contractNo'] = form.value.contactNo;
    this.AddcompanyInfo.push(addcomInfo);
    //  console.log('999999999999',addcomInfo);
    this._ApiService.AddCompanyBranch(this.AddcompanyInfo)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.modalRef.hide();
          this.Getcompanylist();
        
          this.addCompanyBranchForm.reset();
          this.Getstate();
          this.notifications.create(
            'Done',
            'Branch added successfully!!',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }
        if (this.companylocationList.length >= 5) {
          this.companyflag = true;
        }
        else{
          this.companyflag = false;
        }
      })
  }
  editBankDetailsModal(form: any) {
    // debugger;
    // console.log('___________',form.value);
    this.UpdatecompanyInfo = [];
    var UpdatecomInfo = {};
    var companyID = localStorage.getItem('CompanyID');
    UpdatecomInfo['companyId'] = companyID;
    UpdatecomInfo['comBranchId'] = companyID;
    UpdatecomInfo['addressLineOne'] = form.value.addresslineone;
    UpdatecomInfo['addressLineTwo'] = form.value.addresslinetwo;
    UpdatecomInfo['countryId'] = '1';
    UpdatecomInfo['stateId'] = form.value.state;
    UpdatecomInfo['regionId'] = form.value.region;
    UpdatecomInfo['districtId'] = form.value.district;
    UpdatecomInfo['cityId'] = form.value.city;
    UpdatecomInfo['gstinNo'] = form.value.gstinNo;
    UpdatecomInfo['gstIssuanceDate'] = form.value.gstinIssueDate;
    UpdatecomInfo['pinCode'] = form.value.pinCode;
    UpdatecomInfo['contractNo'] = form.value.contactNo;

    this.UpdatecompanyInfo.push(UpdatecomInfo);
    ///  console.log('999999999999',UpdatecomInfo);
    this._ApiService.UpdateCompanyBranch(this.UpdatecompanyInfo)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.modalRef.hide();
          this.notifications.create(
            'Done',
            'Branch updated successfully!!',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }

      })
  }
  // CheckGstEdit(no) {
  //   // alert(no);
  //   var Noarray = no.split("");
  //   //alert(noarray);
  //   var temppan = Noarray[2] + Noarray[3] + Noarray[4] + Noarray[5] + Noarray[6] + Noarray[7] + Noarray[8] + Noarray[9] + Noarray[10] + Noarray[11];
  //   // alert(temppan);
  //   var entityPan;//= this.EcompanNo;
  //   if (entityPan != temppan) {
  //     alert('Please enter valid GST no');
  //   }
  //   else {

  //   }
  //   //18ALWPG5809L1ZM
  //   //ALWPG5809L
  // }
  DeletealertModal(template: TemplateRef<any>, ID) {
    
    this.modalRef = this.modalService.show(
      template, Object.assign({}, { class: 'gray modal-lg' })
    );
    this.deletecomBranchId = ID;
  }
  DelecompanyModal() {
    let comform = {};
    comform['comBranchId'] = this.deletecomBranchId;
    var companyID = localStorage.getItem('CompanyID');
    comform['companyId'] = companyID;
    this._ApiService.CompanyDeleteBank(comform)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.modalRef.hide();
          this.Getcompanylist();
          this.notifications.create(
            'Done',
            'Branch deleted successfully!!',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
           
        }
      })
      if (this.companylocationList.length >= 5) {
        this.companyflag = true;
      }
      else{
        this.companyflag = false;
      }
  }
  @ViewChild('gstFile', { static: false })
  gstFileInput: ElementRef;
  gstClientId:string='';
  gstForm = new FormGroup({
    gstNo: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(15), Validators.maxLength(15)])),    
    otp: new FormControl(null, Validators.required),
    gstFile: new FormControl(null, Validators.required)
  });
  validateGstNo(gstNo: string) {  
    if (gstNo != null && gstNo.length == 15) {      
      this.gstClientId = '';      
      this.spinner.show();
      this._ApiService.CheckGst({ gstNo: gstNo,companyId:localStorage.getItem("CompanyID") }).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
            this.gstForm.reset();            
          } else {
            this.gstClientId = data.clientId;
          }
        }, error => {
          this.gstForm.reset();
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }
  }
  saveCompanyBranchDetails() {
    let file=this.gstForm.get('gstFile').value;
    let otp=this.gstForm.get('otp').value;
    let companyId=localStorage.getItem('CompanyID');
    if(file&&otp&&this.gstClientId&&companyId){
      let formData=new FormData();
      formData.append("clientId",this.gstClientId);
      formData.append("uploadFile",file);
      formData.append("companyId",companyId);
      formData.append("otp",otp);
      this.spinner.show();
      this._ApiService.saveCompanyBranchDetails(formData).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.gstForm.reset();
            this.gstClientId='';            
            this.onError(data['error'].errorMsg);
          } else {
            this.modalRef.hide();
            this.gstForm.reset();
            this.gstClientId='';            
            this.Getcompanylist();
            this.onSuccess("Added Successfully");
          }
        }, error => {
          this.gstForm.reset();
          this.gstClientId='';          
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }else{
      this.onError('Please Enter Valid Info!');
    }
  }
  openAddBranchModal(template:TemplateRef<any>){
    this.gstForm.reset();
    this.modalRef=this.modalService.show(template,{ backdrop: 'static', class: 'modal-lg' });
  }
  setFile(file: any, formControl: AbstractControl) {
    formControl.markAsTouched();
    if (file != null)
      formControl.setValue(file);
    else
      formControl.setValue(null);
  }
  onError(message: string): void {
    this.notifications.create(this.translate.instant('alert.error'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 5000, showProgressBar: true });
  }
  onSuccess(message: string): void {
    this.notifications.create(this.translate.instant('alert.success'),
      this.translate.instant(message),
      NotificationType.Success, { timeOut: 5000, showProgressBar: true });
  }
}
