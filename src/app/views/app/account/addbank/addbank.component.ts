import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ApiService } from 'src/app/data/api.service';
import { NotificationsService, NotificationType } from 'angular2-notifications';
//import{PreventDoubleClickDirectiveDirective} from '../../../../../app/shared/directives/prevent-double-click-directive.directive'
@Component({
  selector: 'app-addbank',
  templateUrl: './addbank.component.html'
})
export class AddbankComponent implements OnInit {
  public bankDetailsList = [];
  public savedDetails = [];
  // public bankDetailsList = [
  //   { "companyBankId": 50, "companyId": 43, "bankName": "axisbank", "accountNo": "3225666", "branchName": "axis-behala", "ifscCode": "AXIS001", "accountType": "savings", "accountHolderName": "surya", "branchAddres": null, "state": "wb", "city": "kolkata", "pinCode": 700001, "primaryBankFlag": null },
  //   { "companyBankId": 51, "companyId": 43, "bankName": "sbi", "accountNo": "7999956", "branchName": "sbi-majherhat", "ifscCode": "SBI-001", "accountType": "savings", "accountHolderName": "amit", "branchAddres": null, "state": "wb", "city": "kolkata", "pinCode": 700005, "primaryBankFlag": null }
  // ];
  public bankDetails: any = "";

  modalRef: BsModalRef;
  companyId: any;
  companyBankId: any;
  public countries = [];
  public states = [];
  public statearray = [];
  public zones = [];
  public zonearray = [];
  public City = [];
  public cityarray = [];
  public UpdateBranchInfo = [];
  public setprimeryId: any;
  public deletebankId: any;
  public buttonDisabled: boolean = true;
  addBankForm: FormGroup = new FormGroup({
    accountHolderName: new FormControl('', Validators.required),
    accountNo: new FormControl('', Validators.required),
    accountType: new FormControl('', Validators.required),
    ifscCode: new FormControl('', Validators.required),
    bankName: new FormControl('', Validators.required),
    branchAddress: new FormControl('', Validators.required),
    branchName: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    district: new FormControl('', Validators.required),
    state: new FormControl('', Validators.required),
    pinCode: new FormControl('', Validators.required)
  });
  editBankForm: FormGroup = new FormGroup({
    accountHolderName: new FormControl('', Validators.required),
    accountNo: new FormControl('', Validators.required),
    accountType: new FormControl('', Validators.required),
    ifscCode: new FormControl('', Validators.required),
    bankName: new FormControl('', Validators.required),
    branchName: new FormControl('', Validators.required),
    branchAddress: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    district: new FormControl('', Validators.required),
    state: new FormControl('', Validators.required),
    pinCode: new FormControl('', Validators.required),
    companyId: new FormControl(''),
    companyBankId: new FormControl('')
  });
  constructor(private notifications: NotificationsService, private modalService: BsModalService, private _ApiService: ApiService) { }

  ngOnInit(): void {
    this.GetBankList();
    this.Getstate();
  }
  Getstate() {
    this.zones = [];
    this.City = [];
    this.statearray = [];
    this._ApiService.getcompanystatedistrict()
      .subscribe(data => {
        this.countries = data.locationList;
        //var temparrray = this.countries;
       // console.log('-------------', this.countries);
        for (var i = 0; i < this.countries.length; i++) {
          if (this.countries != null) {

            var found = this.statearray.includes(this.countries[i].stateId);
            if (found == false) {
              this.states.push({ 'stateName': this.countries[i].stateName, 'stateId': this.countries[i].stateId });
            }
            this.statearray.push(this.countries[i].stateId)
          }
        }
        // console.log('-------------', this.states);
        // this.states=data.states;
      })

  }
  GetZone(data) {

    //alert(data);
    this.zones = [];
    this.City = [];
    this.zonearray = [];
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].stateId == data) {
        var foundzone = this.zonearray.includes(this.countries[j].districtId);
        if (foundzone == false) {
          this.zones.push({ 'districtName': this.countries[j].districtName, 'districtId': this.countries[j].districtId });
        }
        this.zonearray.push(this.countries[j].districtId);
      }
    }
  //  console.log('-------------', this.zones);
  }
  Getcity(data) {

    this.City = [];
    this.cityarray = [];
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].districtId == data) {
        var foundcity = this.cityarray.includes(this.countries[j].cityId);
        if (foundcity == false) {
          this.City.push({ 'cityName': this.countries[j].cityName, 'cityId': this.countries[j].cityId });
        }
        this.cityarray.push(this.countries[j].cityId);
      }
    }

  }
  addBankDetailsModal(template: TemplateRef<any>) {
    this.buttonDisabled = false;
    this.modalRef = this.modalService.show(
      template, Object.assign({}, { class: 'gray modal-lg' })
    );
  }

  editBankDetailsModal(index, template: TemplateRef<any>) {
    this.bankDetails = this.bankDetailsList[index];
    // if(this.bankDetails.state!=""){
    //   this.GetZone(this.bankDetails.state);
    // }
    //console.log('pppppppp', this.bankDetails);

    this.modalRef = this.modalService.show(
      template, Object.assign({}, { class: 'gray modal-lg' })
    );
    this.Getstate();

    if (this.bankDetailsList[index].districtId != "") {

      setTimeout(() => this.GetZone(this.bankDetailsList[index].state), 100);
    }
    if (this.bankDetailsList[index].districtId != "") {

      setTimeout(() => this.Getcity(this.bankDetailsList[index].district), 300);
    }
  }

  addBankDetails(form: any) {
    // this.modalRef.hide();
    //  console.log('___________', form.value);
    this.buttonDisabled = true;
    this.UpdateBranchInfo = [];
    var addbankInfo = {};
    var companyID = localStorage.getItem('CompanyID');
    addbankInfo['companyId'] = companyID;
    addbankInfo['bankName'] = form.value.bankName;
    addbankInfo['branchName'] = form.value.branchName;
    addbankInfo['accountNo'] = form.value.accountNo;
    addbankInfo['ifscCode'] = form.value.ifscCode;
    addbankInfo['accountType'] = form.value.accountType;
    addbankInfo['accountHolderName'] = form.value.accountHolderName;
    addbankInfo['branchAddress'] = form.value.branchAddress;
    addbankInfo['state'] = form.value.state;
    addbankInfo['district'] = form.value.district;
    addbankInfo['city'] = form.value.city;
    addbankInfo['pinCode'] = form.value.pinCode;
    this.UpdateBranchInfo.push(addbankInfo);
    //console.log('999999999999', addbankInfo);
    this._ApiService.AddBranch(this.UpdateBranchInfo)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.modalRef.hide();
          this.GetBankList();
          this.addBankForm.reset();
          this.notifications.create(
            'Done',
            'Bank added successfully!!',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }

      })
  }
  makedefaultalert(template: TemplateRef<any>, ID) {

    this.modalRef = this.modalService.show(
      template, Object.assign({}, { class: 'gray modal-lg' })
    );
    this.setprimeryId = ID;
    //  this.makebankdefault(ID);
  }
  makeBankPrimery() {
    let bankform = {};
    bankform['companyBankId'] = this.setprimeryId;
    var companyID = localStorage.getItem('CompanyID');
    bankform['companyId'] = companyID;
    this._ApiService.UpdateBankPrimery(bankform)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.modalRef.hide();
          this.GetBankList();
        }
      })

  }
  DeleteBAnk() {
    let bankform = {};
    bankform['companyBankId'] = this.deletebankId;
    var companyID = localStorage.getItem('CompanyID');
    bankform['companyId'] = companyID;
    this._ApiService.DeleteBank(bankform)
      .subscribe(data => {
        if (data.error.errorData ==0&& data.error.errorMsg!="No Data") {
          this.modalRef.hide();
          this.GetBankList();
          this.notifications.create(
            'Done',
            'Bank deleted successfully!!',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }
        else{
          this.modalRef.hide();
          this.notifications.create(
            'Error',
          data.error.errorMsg,
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }
      })

  }
  DeletealertModal(template: TemplateRef<any>, ID) {
    this.modalRef = this.modalService.show(
      template, Object.assign({}, { class: 'gray modal-lg' })
    );
    this.deletebankId = ID;
  }
  test(){
    //alert('afsfa');
  }
  editBankDetails(form: any) {
    var bankInfo = {};
    bankInfo['companyBankId'] = form.value.companyBankId;
    bankInfo['companyId'] = form.value.companyId;
    bankInfo['bankName'] = form.value.bankName;
    bankInfo['branchName'] = form.value.branchName;
    bankInfo['accountNo'] = form.value.accountNo;
    bankInfo['ifscCode'] = form.value.ifscCode;
    bankInfo['accountType'] = form.value.accountType;
    bankInfo['accountHolderName'] = form.value.accountHolderName;
    bankInfo['branchAddress'] = form.value.branchAddress;
    bankInfo['state'] = form.value.state;
    bankInfo['district'] = form.value.district;
    bankInfo['city'] = form.value.city;
    bankInfo['pinCode'] = form.value.pinCode;
    this.UpdateBranchInfo.push(bankInfo);
   // console.log('999999999999', bankInfo);
    this._ApiService.UpdateBranch(this.UpdateBranchInfo)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.modalRef.hide();
          this.GetBankList();
          this.editBankForm.reset();
          this.notifications.create(
            'Done',
            'Bank updated successfully!!',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        }

      })
  }
  GetBankList() {
    this.bankDetailsList = [];
    this.savedDetails = [];
    var companyID = localStorage.getItem('CompanyID');
    this._ApiService.BankBranchList(companyID)
      .subscribe(data => {
        if (data.error.errorData == 0) {
          this.bankDetailsList = data.bankDetailsList;
          this.savedDetails = data.bankDetailsList;
          // this.companyId=this.bankDetailsList.companyId;
          // this.companyBankId=this.bankDetailsList['companyBankId'];
        }
      })

  }
}
