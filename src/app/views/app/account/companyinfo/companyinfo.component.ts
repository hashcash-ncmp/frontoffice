import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/data/api.service';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-companyinfo',
  templateUrl: './companyinfo.component.html'
})
export class CompanyinfoComponent implements OnInit {
  companyinfo = {
    companyName: '',
    companyRegAddress: '',
    companyRegAddresstwo:'',
    companyCorresAddress: '',
    companyCorresAddresstwo: '',
    panNo:'',
    gstNo:'',
    cinNo:'',
    contactNo: '',
    emailId: '',
    businessType:''
  };
  constructor(public _ApiService:ApiService,private notifications: NotificationsService, private translate: TranslateService) { }

  ngOnInit(): void {
    this.GetCompanyInformation();
  }
  GetCompanyInformation(){
    var conpanyform = new FormData();
    var companyID = localStorage.getItem('CompanyID');
    conpanyform.append('companyId', companyID);
    this._ApiService.companyInfodata(conpanyform)
    .subscribe(data=>{
      if (data.error.errorData == 0) {
      console.log('personal info---------------',data);
      
      this.companyinfo.companyName=data.companyName;
      this.companyinfo.companyRegAddresstwo=data.regAddressLineTwo;
      var addresstwo;
      if(this.companyinfo.companyRegAddresstwo==""){
        addresstwo="";
      }
      else{
        addresstwo=', '+this.companyinfo.companyRegAddresstwo;
      }
      this.companyinfo.companyRegAddress=data.regAddressLineOne+addresstwo+', '+data.cityName +', '+data.districtName+', '+data.stateName+', '+data.countryName+', Pin-'+data.pinCode;
      var corraddresstwo;
      corraddresstwo=data.corrAddressLineTwo;
      if(corraddresstwo==""){
        corraddresstwo="";
      }
      else{
        corraddresstwo=', '+data.corrAddressLineTwo;
      }
      this.companyinfo.companyCorresAddress=data.corrAddressLineOne+corraddresstwo+', Pin-'+data.corrPinCode;
    
      this.companyinfo.panNo=data.panNo;
      this.companyinfo.cinNo=data.cinNo;
      this.companyinfo.gstNo=data.gstNo;
      this.companyinfo.contactNo=data.contactNo;
      this.companyinfo.emailId=data.email;
      this.companyinfo.businessType=data.businessTypeName;
      }
      else{

      }},error => {
       
        this.onError(error.message);
        console.log(error)
      
      
    });

  }
  onError(message: string): void {
    this.notifications.create(this.translate.instant('alert.error'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 3000, showProgressBar: true });
  }

  onSuccess(message: string): void {
    this.notifications.create(this.translate.instant('alert.success'),
      this.translate.instant(message),
      NotificationType.Success, { timeOut: 3000, showProgressBar: true });
  }
}
