import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import{FormsModule}  from '@angular/forms';
import { PersonalinfoComponent } from './personalinfo/personalinfo.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { AccountRoutingModule } from "./account.routing";
import { AccountComponent } from "./account.component";
import { BsModalService } from 'ngx-bootstrap/modal';
import { NotificationsService, SimpleNotificationsModule } from "angular2-notifications";
import { TranslateService } from '@ngx-translate/core';
import { BankinginfoComponent } from './bankinginfo/bankinginfo.component';
import { CompanyinfoComponent } from './companyinfo/companyinfo.component';
import { ProductprefComponent } from './productpref/productpref.component';
import { AddbankComponent } from './addbank/addbank.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AddcomlocationComponent } from './addcomlocation/addcomlocation.component';
import { MywalletComponent } from './mywallet/mywallet.component';
import { AddshipmentlocationComponent } from './addshipmentlocation/addshipmentlocation.component';
import { PreventDoubleClickDirective } from './prevent-double-click.directive';
import { NgxSpinnerModule } from 'ngx-spinner';



@NgModule({
  declarations: [PersonalinfoComponent, AccountComponent, BankinginfoComponent, CompanyinfoComponent, ProductprefComponent, AddbankComponent, AddcomlocationComponent, MywalletComponent, AddshipmentlocationComponent, PreventDoubleClickDirective],
  imports: [
    SharedModule,
    LayoutContainersModule,
    AccountRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SimpleNotificationsModule.forRoot(),
    NgxSpinnerModule
  ],
  exports:[PreventDoubleClickDirective],
  providers: [BsModalService]
})
export class AccountModule { }
