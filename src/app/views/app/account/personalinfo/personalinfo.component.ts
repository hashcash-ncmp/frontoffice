import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ApiService } from 'src/app/data/api.service';
// import { FormsModule } from '@angular/forms';
import { NotificationsService, NotificationType } from 'angular2-notifications';
declare var $;
@Component({
  selector: 'app-personalinfo',
  templateUrl: './personalinfo.component.html'
})
export class PersonalinfoComponent implements OnInit {
  modalRef: BsModalRef;
  personalinfo = {
    name: '',
    email: '',
    contactNo: '',
    addressLine1: '',
    addressLine2: '',
    city: '',
    state: '',
    zip: ''
  };
  membershipinfo = {
    typeName: '',
    description: '',
    fees: '',
    createdOn: '',
    status: ''
  };
  public otp: any;
  public firstName: any;
  public lastName: any;
  public TempcontactNo: any;
  public imagecheck: boolean = false;
  public ImageArray: any = [];
  public blankcheck: boolean = false;
  public Pemail: any;
  public Pcontact: any;
  public MembershipDtl: any;
  constructor(private notifications: NotificationsService, private modalService: BsModalService, private _ApiService: ApiService) { }

  ngOnInit(): void {
    this.GetAuthpersondtl();
  }

  updateEmail() {

  }
  GetAuthpersondtl() {
    debugger;
    var companyID = localStorage.getItem('CompanyID');
    var personalform = new FormData();
    personalform.append('companyId', companyID);
    this._ApiService.Authorisedpersondata(personalform)
      .subscribe(data => {
        // console.log('888888888', data);
        if (data.error.errorData == 0) {
          this.personalinfo.name = data.personalDetails.firstName + " " + data.personalDetails.lastName;
          this.personalinfo.email = data.personalDetails.email;
          this.personalinfo.contactNo = data.personalDetails.contact;
          var panNo = data.personalDetails.panNo;
          var adharno = data.personalDetails.aadharNo;
          var pincode = data.personalDetails.pinCode
          this.MembershipDtl = data.companyMembershipDetails;
          console.log('this.MembershipDtl-------', this.MembershipDtl);
          this.membershipinfo.typeName = this.MembershipDtl.typeName;
          this.membershipinfo.description = this.MembershipDtl.description;
          this.membershipinfo.fees = this.MembershipDtl.fees;
          this.membershipinfo.createdOn = this.MembershipDtl.createdOn;
          this.membershipinfo.status = this.MembershipDtl.status;
          this.personalinfo.addressLine1 = data.personalDetails.addressLineOne + ' ,' + data.personalDetails.addressLineTwo + ' ,' + data.personalDetails.cityName + ' ,' + data.personalDetails.districtName + ' ,' + data.personalDetails.stateName + ' ,' + data.personalDetails.countryName + ' ,' + ' PINCODE- ' + data.personalDetails.pinCode;
          this.firstName = data.personalDetails.firstName;
          this.lastName = data.personalDetails.lastName;
          this.TempcontactNo = data.personalDetails.contact;
          localStorage.setItem('firstName', this.firstName);
          localStorage.setItem('lasttName', this.lastName);
          localStorage.setItem('contactNo', this.TempcontactNo);
        }
      })

  }
  // AddImage() {
  //   let Imageload = new FormData();
  //   // Imageload.append('companyId', this.companyId);
  //   //   Imageload.append('companyId', this.companyId);
  //   // this._ApiService.SaveprofileImage()


  // }

  showOtpModalformail(template: TemplateRef<any>) {
    debugger;

    if (this.Pemail != '' && this.Pemail != undefined) {
      this.blankcheck = false;
      this.modalRef = this.modalService.show(
        template, Object.assign({}, { class: 'gray modal-md' })

      );
   
      let otpdata = {};
      var companyID = localStorage.getItem('CompanyID');
      otpdata['companyId'] = companyID;
      this._ApiService.Otpgenarate(otpdata)
        .subscribe(data => {
          if (data.error.errorData == 0) {

            var result = data.userResult;
            var OTP = result.phNoOtp;
            //   alert('Ckeck OTP on your registered mobile no!');
            alert('Your OTP is' + OTP);
            this.notifications.create(
              'Done',
              'Ckeck OTP on your registered mobile no!',
              NotificationType.Bare,
              { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
            );

          }
          console.log();


        })


    }
    else {
      // this.blankcheck=true;

      this.notifications.create(
        'Done',
        'Please make changes and save!',
        NotificationType.Bare,
        { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
      );
    }

  }
  showOtpModalcontact(template: TemplateRef<any>) {
    debugger;

    if (this.Pcontact != '' && this.Pcontact != undefined) {
      this.blankcheck = false;
      this.modalRef = this.modalService.show(
        template, Object.assign({}, { class: 'gray modal-md' })

      );
      let otpdata = {};
      var companyID = localStorage.getItem('CompanyID');
      otpdata['companyId'] = companyID;
      // otpdata['tempPhNo'] = this.TempcontactNo;
      this._ApiService.Otpgenarate(otpdata)
        .subscribe(data => {
          if (data.error.errorData == 0) {
            var result = data.userResult;
           var OTP = result.phNoOtp;
            //   alert('Ckeck OTP on your registered mobile no!');
            alert('Your OTP is ' +  OTP);
            this.notifications.create(
              'Done',
              'Ckeck OTP on your registered mobile no!',
              NotificationType.Bare,
              { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
            );

          }
          console.log();


        })


    }
    else {
      // this.blankcheck=true;

      this.notifications.create(
        'Done',
        'Please make changes and save!',
        NotificationType.Bare,
        { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
      );
    }

  }
  validateOTP() {
    //alert(this.otp);
    debugger;
    var companyID = localStorage.getItem('CompanyID');
    let checkOTPdata = {};
    var companyID = localStorage.getItem('CompanyID');
    checkOTPdata['companyId'] = companyID;
    checkOTPdata['phNoOtp'] = this.otp;
    if (this.otp != null) {
      this._ApiService.OtpCheck(checkOTPdata)
        .subscribe(data => {
          if (data.error.errorData == 0) {
            var updatedata = {};
            var companyID = localStorage.getItem('CompanyID');
            updatedata['companyId'] = companyID;
            if (this.Pemail != "" && this.Pemail != undefined) {
              updatedata['email'] = this.Pemail;
            }
            else {
              updatedata['email'] = this.personalinfo.email;
            }
            if (this.Pcontact != "" && this.Pcontact != undefined) {
              updatedata['contact'] = this.Pcontact;
            }
            else {
              updatedata['contact'] = this.personalinfo.contactNo;
            }
            //  updatedata['contact'] = this.personalinfo.contactNo;


            this._ApiService.UpdatecontactandEmail(updatedata)
              .subscribe(data => {
                if (data.error.errorData == 0) {
                  this.modalRef.hide();
                  this.notifications.create(
                    'Done',
                    'updated Successfully!',
                    NotificationType.Bare,
                    { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
                  );
                }
              })


          }
        })
    }
  }
  // ImageUpload(e) {
  //   debugger;
  //   let files = e.target.files;
  //   let length = e.target.files.length
  //   let fileArr = [];
  //   var companyID = localStorage.getItem('CompanyID');
  //   let payload = new FormData();
  //   payload.append('companyId ', companyID);
  //   payload.append('companyId ', $(".profileImg")[0].files[0]);
  //   if ($(".profileImg")[0].files[0] != '') {
  //     this._ApiService.SaveprofileImage(payload)
  //       .subscribe(data => {

  //       })
  //   }
  //   else {
  //     this.imagecheck=true;
  //     this.notifications.create(
  //       'Done',
  //       'Email updated Successfully!',
  //       NotificationType.Bare,
  //       { theClass: 'Contact No or emailId can not be blank', timeOut: 6000, showProgressBar: true }
  //     );

  //   }

  // }
  CheckUploadfile = (e) => {
    debugger;
    //  let identifier = '#companyDoc' + id;
    //   $(identifier).siblings('.text-danger').hide();
    this.imagecheck = false;
    this.ImageArray = [];
    let files = e.target.files;
    let length = e.target.files.length
    let fileArr = [];
    let isValid = false;
    //console.log('************START*************')
    //console.log(this.companyKycDocArray)
    if (length > 0) {
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        console.log(file)
        if (file.type === 'application/pdf' || file.type === 'application/msword' || file.type === 'image/jpeg' || file.type === 'image/bmp' ||
          file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {

          if (file.size <= 1048576) {
            fileArr.push(file);
            isValid = true
          } else {
            // $(identifier).siblings('.text-danger').show();
          }
        } else {
          // $(identifier).siblings('.text-danger').show();
        }
      }
      // let index = this.companyKycDocArray.findIndex(x => x['id'] === id);
      // console.log(index)
      //   if (index !== -1) {
      //     this.companyKycDocArray.splice(index, 1);
      //   }
      // } else {
      //   $(identifier).siblings('.text-danger').show();
      // }
      // console.log(isValid)
      if (isValid) {
        let obj = {}
        // obj['id'] = id;
        obj['files'] = fileArr
        this.ImageArray.push(obj);
      }
      //   var companyID = localStorage.getItem('CompanyID');
      //   let payload = new FormData();
      //   payload.append('companyId ', companyID);
      // //  payload.append('photoDoc',this.ImageArray);
      //  let testfile=this.ImageArray[0].files[0];
      //  console.log('---------',testfile);
      //  payload.append('photoDoc', testfile);

      // for (let j = 0; j < this.ImageArray[0].files[0].length; j++) {
      //   let file =  this.ImageArray[0].files[0][j];
      //   payload.append('files', file);
      // }
      // console.log(this.companyKycDocArray)
      //console.log('************END*************')

      // if (this.ImageArray != '') {
      //   this._ApiService.SaveprofileImage(payload)
      //     .subscribe(data => {

      //     })
      // }
      // else {
      //   this.imagecheck=true;
      //   this.notifications.create(
      //     'Done',
      //     'Email updated Successfully!',
      //     NotificationType.Bare,
      //     { theClass: 'Contact No or emailId can not be blank', timeOut: 6000, showProgressBar: true }
      //   );

      // }

    }

  }
  ImageUpload() {
    if (this.ImageArray != '') {
      var companyID = localStorage.getItem('CompanyID');
      let payload = new FormData();
      payload.append('companyId ', companyID);
      //  payload.append('photoDoc',this.ImageArray);
      let testfile = this.ImageArray[0].files[0];
      console.log('---------', testfile);
      payload.append('photoDoc', testfile);

      this._ApiService.SaveprofileImage(payload)
        .subscribe(data => {
          this.notifications.create(
            'Done',
            'Image Uploaded Successfully!',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
          );
        })
    }
    else {
      this.imagecheck = true;
      this.notifications.create(
        'Done',
        'file can not be blank',
        NotificationType.Bare,
        { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
      );

    }
  }
}
