import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountComponent } from './account.component';
import { PersonalinfoComponent } from './personalinfo/personalinfo.component';
import { BankinginfoComponent } from "./bankinginfo/bankinginfo.component";
import { CompanyinfoComponent } from "./companyinfo/companyinfo.component";
import { ProductprefComponent } from "./productpref/productpref.component";
import { BlankPageComponent } from "../blank-page/blank-page.component";
import { AddbankComponent } from './addbank/addbank.component';
import { AddcomlocationComponent } from './addcomlocation/addcomlocation.component';
import { MywalletComponent } from './mywallet/mywallet.component';
import { AddshipmentlocationComponent } from './addshipmentlocation/addshipmentlocation.component';


const routes: Routes = [
    {
        path: '', component: AccountComponent,
        children: [
            { path: '', redirectTo: 'personalinfo', pathMatch: 'full' },
            { path: 'personalinfo', component: PersonalinfoComponent },
            { path: 'bankinginfo', component: BankinginfoComponent },
            { path: 'companyinfo', component: CompanyinfoComponent },
            { path: 'addcompany', component: AddcomlocationComponent },
            { path: 'productpref', component: ProductprefComponent },
            { path: 'sessionmngpref', component: BlankPageComponent },
            // { path: 'addbank', component: AddbankComponent },
            { path: 'mywallet', component: MywalletComponent },
            { path: 'addshipmetbranch', component: AddshipmentlocationComponent }
            
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AccountRoutingModule { }
