import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlankPageComponent } from './blank-page/blank-page.component';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import{OrderComponent} from './tradefloor/order/order.component';
import{FormsModule}  from '@angular/forms';
import { NotificationsService, SimpleNotificationsModule } from "angular2-notifications";
import { PaymentsModule } from './payments/payments.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgSelectModule } from '@ng-select/ng-select';
import{AuthGuard} from '../app/auth.guard';

@NgModule({
  declarations: [BlankPageComponent, AppComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    SharedModule,
    LayoutContainersModule,
    FormsModule,  SimpleNotificationsModule.forRoot(), PaymentsModule,
    SimpleNotificationsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgxSpinnerModule,
    NgSelectModule
  ],
  providers: [AuthGuard],
})
export class AppModule { }

