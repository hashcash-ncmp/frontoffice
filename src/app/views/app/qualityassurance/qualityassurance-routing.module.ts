import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QualityassuranceComponent } from './qualityassurance.component';
import { ReviewselectionandapprovalComponent } from './reviewselectionandapproval/reviewselectionandapproval.component';
import { SelectionandapprovalComponent } from './selectionandapproval/selectionandapproval.component';
import { SelectionandapprovallistComponent } from './selectionandapprovallist/selectionandapprovallist.component';
import { ViewselectionandapprovalComponent } from './viewselectionandapproval/viewselectionandapproval.component';


const routes: Routes = [
  {
    path: '', component: QualityassuranceComponent,
    children: [
      { path: '', redirectTo: 'selectionandapproval', pathMatch: 'full' },
      { path: 'selectionandapproval', component: SelectionandapprovalComponent },
      { path: 'viewselectionandapproval', component: ViewselectionandapprovalComponent },
      { path: 'reviewselectionandapproval', component: ReviewselectionandapprovalComponent },
      { path: 'selectionandapprovallist', component: SelectionandapprovallistComponent },
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QualityassuranceRoutingModule { }
