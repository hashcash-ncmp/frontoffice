import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/data/api.service';
import { noop, Observable, Observer, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { DatePipe } from '@angular/common';
import { RANGES } from 'src/main';

@Component({
  selector: 'app-viewselectionandapproval',
  templateUrl: './viewselectionandapproval.component.html',
  styleUrls: ['./viewselectionandapproval.component.scss']
})
export class ViewselectionandapprovalComponent implements OnInit {
  ranges: any = [];
  bsValue = new Date();
  bsRangeValue: Date[];
  stateId: number = 0;
  tncId: string = '';
  stateList: Array<any> = [];
  selectionAndApprovalList: Array<any> = [];
  allotmentNo: string = null;
  modalRef: BsModalRef = null;
  zoneList: Array<any> = [];
  allotmentNoObject: any = {};
  productId: number = 0;
  subProductId: number = 0;
  seasonId: number = 0;
  companyId: any = null;
  productList: Array<any> = [];
  subProductList: Array<any> = [];
  seasonList: Array<any> = [];
  tcnList: Array<any> = [];
  productUnit: any = null;
  isAddEnable: boolean = false;
  selectionList: any = [];
  regionList: Array<any> = [];
  districtList: Array<any> = [];
  cityList: Array<any> = [];
  isRegionFound = false;
  userType: string = '';
  totalCount = 0;
  pageNo: number = 1;
  pageSize: number = 20;
  selectionValue: any = {};
  buyerCompanyId: any = 0;
  sellerCompanyId: any = 0;

  constructor(private datePipe: DatePipe, private http: HttpClient, private spinner: NgxSpinnerService, private service: ApiService,
    private notifications: NotificationsService, private translate: TranslateService,
    private activatedRoute: ActivatedRoute, private modalService: BsModalService) {
    //this.bsRangeValue = [this.bsValue, this.bsValue];
    this.ranges = RANGES;
  }

  ngOnInit(): void {
    this.userType = localStorage.getItem('BusinessTypeId');
    this.companyId = localStorage.getItem('CompanyID');
    this.GetAllProducts();
    this.getStateList();
    //this.getSeason();
    this.getAllCompanyByComId();
  }

  updateForm = new FormGroup({
    cityId: new FormControl('', Validators.required),
    quantity: new FormControl('', Validators.required),
    lotNo: new FormControl(''),
    partyLotNo: new FormControl(''),
    prNo: new FormControl(''),
    productUnit: new FormControl('', Validators.required),
    storageLocation: new FormControl('', Validators.required),
    regionId: new FormControl('', Validators.required),
    districtId: new FormControl('', Validators.required),
    allotmentDtlId: new FormControl('', Validators.required),
    allotmentNo: new FormControl('', Validators.required),
    status: new FormControl('', Validators.required)
  });

  openModal(template: TemplateRef<any>, object: any) {
    this.updateForm.get('cityId').setValue(object.cityId);
    this.updateForm.get('quantity').setValue(object.quantity);
    this.updateForm.get('productUnit').setValue(object.unitId);
    this.updateForm.get('storageLocation').setValue(object.storageLocation);
    this.updateForm.get('regionId').setValue(object.regionId);
    this.updateForm.get('districtId').setValue(object.districtId);
    this.updateForm.get('allotmentDtlId').setValue(object.allotmentDtlId);
    this.updateForm.get('allotmentNo').setValue(this.selectionValue.allotmentNo);
    this.updateForm.get('status').setValue(object.status);

    if (this.productId == 1 && this.subProductId == 185) {
      this.updateForm.get('lotNo').setValue(object.lotNo);
      this.updateForm.get('partyLotNo').setValue(object.partyLotNo);
      this.updateForm.get('prNo').setValue(object.prNo);

      this.updateForm.get('lotNo').setValidators(Validators.required);
      this.updateForm.get('partyLotNo').setValidators(Validators.required);
      this.updateForm.get('prNo').setValidators(Validators.required);
    } else {
      this.updateForm.get('lotNo').disable();
      this.updateForm.get('partyLotNo').disable();
      this.updateForm.get('prNo').disable();
    }

    if (object.regionId != null && object.regionId > 0)
      this.getRDCList(object.regionId, 'district');

    this.getRDCList(object.districtId, 'city');

    this.productUnit = {
      quantityUnitCode: object.unitName
    }


    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'modal-lg' });
  }

  getAllCompanyByComId() {
    this.buyerCompanyId = 0;
    this.sellerCompanyId = 0;
    const url = this.service.WEBSERVICE + '/selection/getAllCompanyByComId';
    this.suggestions$ = new Observable((observer: Observer<string>) => {
      observer.next(this.search);
    }).pipe(
      switchMap((query: string) => {
        if (query) {
          this.spinner.show();
          return this.http.post<DataSourceEnvelop>(
            url, { searchKeyword: query, companyId: this.companyId, flag: "seller" }).pipe(
              map((data: DataSourceEnvelop) => { this.spinner.hide(); return data && data.allCompany || []; }),
              tap(() => noop, err => {
                this.spinner.hide();
                this.errorMessage = err && err.message || 'Something goes wrong';
              })
            );
        }
        return of([]);
      })
    );
  }


  getRDCList(id: number, type: string) {
    let param = {};
    if (this.userType != null && this.userType == "3")
      param['companyId'] = this.companyId;
    if ("region" == type) {
      param['stateId'] = id;
      this.regionList = [];
      this.districtList = [];
      this.cityList = [];
      this.isRegionFound = false;
    }
    else if ("district" == type) {
      this.districtList = [];
      this.cityList = [];
      param['regionId'] = id;
      //this.updateForm.get('districZoneId').setValue('');
      //this.updateForm.get('cityId').setValue('');
    }
    else if ("city" == type) {
      this.cityList = [];
      param['districtId'] = id;
      //this.updateForm.get('cityId').setValue('');
    }
    this.spinner.show();
    this.service.getRDCList(param).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          if ("region" == type) {
            if (data.regionList != null && data.regionList.length > 0) {
              this.isRegionFound = true;
              this.regionList = data.regionList != null ? data.regionList : [];
            }
            else {
              this.isRegionFound = false;
              this.regionList = [{ regionId: 0, regionName: 'N/A' }];
              this.districtList = data.districtList != null ? data.districtList : [];
            }
          }
          else if ("district" == type && this.isRegionFound)
            this.districtList = data.districtList != null ? data.districtList : [];
          else if ("city" == type)
            this.cityList = data.cityList != null ? data.cityList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  GetAllProducts() {
    this.selectionValue = {};
    this.selectionList = [];
    this.productUnit = null;
    this.spinner.show();
    this.service.GetAllProducts({ companyId: this.companyId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.productList = data.productList != null ? data.productList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  GetAllSubProducts(productId: number) {
    this.selectionValue = {};
    this.seasonId = 0;
    this.selectionList = [];
    this.productUnit = null;
    this.subProductId = 0;
    this.subProductList = [];
    this.spinner.show();
    this.service.GetAllSubProducts({ companyId: this.companyId, productId: productId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.subProductList = data.subProductList != null ? data.subProductList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getStateList() {
    this.selectionValue = {};
    let param = {};
    if (this.userType != null && this.userType == "3")
      param['companyId'] = this.companyId;
    this.selectionList = [];
    this.spinner.show();
    this.service.getStateList(param).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.stateList = data.stateList != null ? data.stateList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getSeason() {
    // this.selectionValue = {};
    // this.seasonId = 0;
    // this.selectionList = [];
    // this.spinner.show();
    // this.service.getSeason({ productId: this.productId, subProductId: this.subProductId }).subscribe(
    //   data => {
    //     this.spinner.hide();
    //     if (data['error'].errorData != '0') {
    //       this.onError(data['error'].errorMsg);
    //     } else {
    //       this.seasonList = data.seasonList != null ? data.seasonList : [];
    //     }
    //   }, error => {
    //     this.spinner.hide();
    //     this.onError(error.message);
    //     console.log(error)
    //   }
    // );
  }

  getSelectionTcn() {
    this.selectionValue = {};
    this.selectionList = [];
    this.tncId = '';
    this.tcnList = [];
    this.isAddEnable = false;
    if (this.bsRangeValue[0] != null && this.bsRangeValue[1] != null && this.productId != 0 && this.subProductId != 0 && this.stateId != 0 && this.buyerCompanyId > 0 && this.sellerCompanyId > 0) {
      let param = {
        toDate: this.formatDate(this.bsRangeValue[1]),
        productId: this.productId,
        subProductId: this.subProductId,
        fromDate: this.formatDate(this.bsRangeValue[0]),
        companyId: this.companyId,
        // seasonId: this.seasonId,
        stateId: this.stateId,
        sellerCompanyId: this.companyId,
        buyerCompanyId: this.buyerCompanyId
      }
      this.spinner.show();
      this.service.getSelectionTcn(param).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.tcnList = data.tcnDetails != null ? data.tcnDetails : [];
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }
    else {
      //this.onError('Please Select Valid Options!');
    }
  }

  formatDate(date: Date) {
    return date.getFullYear() + "-" + (date.getMonth() >= 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-" + (date.getDate() > 9 ? date.getDate() : "0" + date.getDate());
  }

  viewAllSelectionDetails(pageNo: number) {
    this.pageNo = pageNo;
    if (this.tncId != null && this.tncId.length > 0 && this.bsRangeValue[0] != null && this.bsRangeValue[1] != null && this.productId != 0 && this.subProductId != 0 && this.stateId != 0) {
      this.spinner.show();
      this.service.viewAllSelectionDetails({ companyId: this.companyId, productId: this.productId, subProductId: this.subProductId, tcnNo: this.tncId, stateId: this.stateId, fromDate: this.formatDate(this.bsRangeValue[0]), toDate: this.formatDate(this.bsRangeValue[1]), pageNo: this.pageNo, noOfItemsPerPage: this.pageSize }).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.selectionValue = {};
            this.selectionList = [];
            this.onError(data['error'].errorMsg);
          } else {
            this.selectionValue = data.selectionValue != null ? data.selectionValue : {};
            this.selectionList = data.selectionDetailsList != null ? data.selectionDetailsList : [];
            this.totalCount = data.totalCount != null ? data.totalCount : 0;
          }
        }, error => {
          this.spinner.hide();
          this.selectionValue = {};
          this.selectionList = [];
          this.onError(error.message);
          console.log(error)
        }
      );
    }
  }

  updateSelectionDetails(form: NgForm) {
    this.spinner.show();
    this.service.updateSelectionDetails(form.value).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.modalRef.hide();
          form.reset();
          this.onSuccess(data.message);
          this.viewAllSelectionDetails(1);
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  printPdf() {
    this.spinner.show();
    let html = '<head><style> table { font-family: arial, sans-serif; border-collapse: collapse; width: 100%; text-align:center } td, th { border: 1px solid #dddddd; text-align: left; padding: 8px; }</style></head>';
    html += '<table>'
    html += '<tbody>'
    html += '<tr><td colspan=4 style="text-align:center;v-align:center;padding-top:1px;padding-bottom:1px;"><h2>Swayam</h2></td></tr>'
    html += '<tr><td colspan=4 style="text-align:center;v-align:center;padding-top:1px;padding-bottom:1px;"><h5>Inventory Receipt</h5></td></tr>'
    html += '<tr>'
    html += '<td><b>Product Name&nbsp;:&nbsp;</b>' + this.selectionValue.productName + '</td>'
    html += '<td><b>Sub Product Name&nbsp;:&nbsp;</b>' + this.selectionValue.subProductName + '</td>'
    html += '<td><b>Buyer Name&nbsp;:&nbsp;</b>' + this.selectionValue.buyerCompanyName + '</td>'
    html += '<td><b>Seller Name&nbsp;:&nbsp;</b>' + this.selectionValue.sellerCompanyName + '</td>'
    html += '</tr>'
    html += '<tr>'
    html += '<td><b>TCN&nbsp;:&nbsp;</b>' + this.selectionValue.tcnNo + '</td>'
    html += '<td><b>Trade Timestamp&nbsp;:&nbsp;</b>' + this.datePipe.transform(this.selectionValue.tradeTimeStamp, 'MMM d, y h:mm:ss a') + '</td>'
    html += '<td><b>Location&nbsp;:&nbsp;</b>' + this.selectionValue.location + '</td>'
    html += '<td><b>Allotment No&nbsp;:&nbsp;</b>' + this.selectionValue.allotmentNo + '</td>'
    html += '</tr>'
    html += '<tr>'
    html += '<td><b>Trade Rate&nbsp;:&nbsp;</b>' + this.selectionValue.tradeRate + '</td>'
    html += '<td><b>Trade Qty&nbsp;:&nbsp;</b>' + this.selectionValue.tradeQuantity + '</td>'
    html += '<td><b>Pending Qty&nbsp;:&nbsp;</b>' + this.selectionValue.pendingQuantity + '</td>'
    html += '<td></td>'
    html += '</tr>'
    html += '</tbody>'
    html += '</table>'
    html += '<table>'
    html += '<thead>'
    html += '<tr>'
    html += '<th>State Name</th>'
    html += '<th>Region Name</th>'
    html += '<th>District Name</th>'
    html += '<th>City Name</th>'
    html += '<th>Quantity</th>'
    html += '<th>Unit Name</th>'
    html += '<th>Lot No</th>'
    html += '<th>Party Lot No</th>'
    // html += '<th>Pr No</th>'
    html += '<th>Storage Location</th>'
    html += '<th>Status</th>'
    html += '</tr>'
    html += '</thead>'
    html += '<tbody>'
    for (let i = 0; i < this.selectionList.length; i++) {
      html += '<tr>'
      html += '<td>' + this.selectionList[i].stateName + '</td>'
      html += '<td>' + this.selectionList[i].regionName + '</td>'
      html += '<td>' + this.selectionList[i].districtName + '</td>'
      html += '<td>' + this.selectionList[i].cityName + '</td>'
      html += '<td>' + this.selectionList[i].quantity + '</td>'
      html += '<td>' + this.selectionList[i].unitName + '</td>'
      html += '<td>' + this.selectionList[i].lotNo + '</td>'
      html += '<td>' + this.selectionList[i].partyLotNo + '</td>'
      // html += '<td>' + this.selectionList[i].prNo + '</td>'
      html += '<td>' + this.selectionList[i].storageLocation + '</td>'
      html += '<td>' + this.selectionList[i].allotedStatus + '</td>'
      html += '</tr>'
    }
    html += '</tbody>'
    html += '</table>'
    this.service.generatePdf(html, this.selectionValue.tcnNo + '_Inventory.pdf').then((data) => {
      this.spinner.hide();
      console.log(data)
    }).catch((error) => {
      this.spinner.hide();
      console.log(error)
    })
  }

  onError(message: string): void {
    this.notifications.create(this.translate.instant('alert.error'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 3000, showProgressBar: true });
  }

  onSuccess(message: string): void {
    this.notifications.create(this.translate.instant('alert.success'),
      this.translate.instant(message),
      NotificationType.Success, { timeOut: 3000, showProgressBar: true });
  }

  search: string;
  suggestions$: Observable<DataSourceType[]>;
  errorMessage: string;
  onSelect(event: TypeaheadMatch): void {
    this.buyerCompanyId = event.item.companyId;
    this.sellerCompanyId = event.item.companyId;
  }
}
interface DataSourceType {
  companyId: any;
  companyName: any;
}
interface DataSourceEnvelop {
  allCompany: DataSourceType[];
}
