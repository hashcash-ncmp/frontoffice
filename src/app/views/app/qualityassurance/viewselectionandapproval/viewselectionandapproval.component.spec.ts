import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewselectionandapprovalComponent } from './viewselectionandapproval.component';

describe('ViewselectionandapprovalComponent', () => {
  let component: ViewselectionandapprovalComponent;
  let fixture: ComponentFixture<ViewselectionandapprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewselectionandapprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewselectionandapprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
