import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/data/api.service';
import { AbstractControl, FormArray, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import { NotificationsService, NotificationType } from "angular2-notifications";
import { TranslateService } from '@ngx-translate/core';
import { noop, Observable, Observer, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { RANGES } from 'src/main';

@Component({
  selector: 'app-selectionandapproval',
  templateUrl: './selectionandapproval.component.html',
  styleUrls: ['./selectionandapproval.component.scss']
})
export class SelectionandapprovalComponent implements OnInit {
  ranges:any=[];
  bsValue = new Date();
  bsRangeValue: Date[];
  stateId: number = 0;
  tncId: any = '';
  stateList: Array<any> = [];
  zoneList: Array<any> = [];
  allotmentNoObject: any = {};
  allotmentNo: string = '';
  productId: number = 0;
  subProductId: number = 0;
  seasonId: number = 0;
  companyId: any = null;
  productList: Array<any> = [];
  subProductList: Array<any> = [];
  seasonList: Array<any> = [];
  tcnList: Array<any> = [];
  isAddEnable: boolean = false;
  productUnit: any = null;
  regionList: Array<any> = [];
  districtList: Array<any> = [];
  cityList: Array<any> = [];
  isRegionFound: boolean = false;
  buyerName: string = '';
  quantity: number = 0;
  userType: string = '';
  companyList: Array<any> = [];
  buyerCompanyId: any = 0;
  sellerCompanyId: any = 0;
  minimumTradeLimit: any = 0;
  dueSelectionDate: any = null;
  selectionId: any = 0;
  selectionName: any = null;
  paymentModeId: any = 0;

  traderRegionId: number = 0;
  traderRegionName: string = '';
  traderZoneId: number = 0;
  traderZoneName: string = '';
  traderCityId: number = 0;
  traderCityName: string = '';

  sellerRegionId: number = 0;
  sellerRegionName: string = '';
  sellerZoneId: number = 0;
  sellerZoneName: string = '';
  sellerCityId: number = 0;
  sellerCityName: string = '';

  constructor(private http: HttpClient, private spinner: NgxSpinnerService, private service: ApiService,
    private notifications: NotificationsService, private translate: TranslateService) {
    //this.bsRangeValue = [this.bsValue, this.bsValue];
    this.ranges=RANGES;
  }

  ngOnInit(): void {
    this.userType = localStorage.getItem('BusinessTypeId');
    this.companyId = localStorage.getItem('CompanyID');
    this.GetAllProducts();
    this.getStateList();
    //this.getSeason();
    this.getAllCompanyByComId();
  }

  GetAllProducts() {
    this.productUnit = null;
    this.spinner.show();
    this.service.GetAllProducts({ companyId: this.companyId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.productList = data.productList != null ? data.productList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  GetAllSubProducts(productId: number) {
    this.seasonId = 0;
    this.seasonList = [];
    this.productUnit = null;
    this.subProductId = 0;
    this.subProductList = [];
    this.spinner.show();
    this.service.GetAllSubProducts({ companyId: this.companyId, productId: productId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.subProductList = data.subProductList != null ? data.subProductList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getStateList() {
    let param = {};
    if (this.userType != null && this.userType == "3")
      param['companyId'] = this.companyId;
    this.spinner.show();
    this.service.getStateList(param).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.stateList = data.stateList != null ? data.stateList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getSeason() {
    this.seasonId = 0;
    this.seasonList = [];
    this.spinner.show();
    this.service.getSeason({ productId: this.productId, subProductId: this.subProductId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.seasonList = data.seasonList != null ? data.seasonList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getAllCompanyByComId() {
    this.buyerCompanyId = 0;
    this.sellerCompanyId = 0;
    const url = this.service.WEBSERVICE + '/selection/getAllCompanyByComId';
    this.suggestions$ = new Observable((observer: Observer<string>) => {
      observer.next(this.search);
    }).pipe(
      switchMap((query: string) => {
        if (query) {
          this.spinner.show();
          return this.http.post<DataSourceEnvelop>(
            url, { searchKeyword: query, companyId: this.companyId, flag: "seller" }).pipe(
              map((data: DataSourceEnvelop) => { this.spinner.hide(); return data && data.allCompany || []; }),
              tap(() => noop, err => {
                this.spinner.hide();
                this.errorMessage = err && err.message || 'Something goes wrong';
              })
            );
        }
        return of([]);
      })
    );
  }

  getSelectionTcn() {
    this.tncId = '';
    this.tcnList = [];
    (this.sellerForm.get('sellerFormArray') as FormArray).clear();
    this.sellerForm.reset();
    this.isAddEnable = false;
    if (this.bsRangeValue[0] != null && this.bsRangeValue[1] != null && this.productId != 0 && this.subProductId != 0 && this.seasonId != 0 && this.stateId != 0 && this.buyerCompanyId > 0 && this.sellerCompanyId > 0) {
      let param = {
        toDate: this.formatDate(this.bsRangeValue[1]),
        productId: this.productId,
        subProductId: this.subProductId,
        fromDate: this.formatDate(this.bsRangeValue[0]),
        companyId: this.companyId,
        seasonId: this.seasonId,
        stateId: this.stateId,
        sellerCompanyId: this.companyId,
        buyerCompanyId: this.buyerCompanyId
      };
      this.spinner.show();
      this.service.getSelectionTcn(param).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.tcnList = data.tcnDetails != null ? data.tcnDetails : [];
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }
    else {
      //this.onError('Please Select Valid Options!');
    }
  }

  permissionForAdding(tcnNo: string) {
    this.buyerName = '';
    this.quantity = 0;
    this.selectionId = 0;
    this.selectionName = null;
    this.dueSelectionDate = null;
    this.paymentModeId = 0;
    (this.sellerForm.get('sellerFormArray') as FormArray).clear();
    this.sellerForm.reset();
    this.isAddEnable = false;
    let businessType: any = 0;
    if (this.userType == '3')
      businessType = 3;
    else if (this.userType == "1")
      businessType = 1;
    this.spinner.show();
    this.service.permissionForAdding({ tcnNo: tcnNo, productId: this.productId, subProductId: this.subProductId, companyId: this.companyId, businessType: businessType }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          if (data.tradeDetails != null) {
            this.buyerName = data.tradeDetails.buyerCompanyName;
            this.quantity = data.tradeDetails.tradeQuantity;
            this.minimumTradeLimit = data.tradeInfo != null ? data.tradeInfo.minimumTradeLimit != null ? data.tradeInfo.minimumTradeLimit : 0 : 0;
            this.dueSelectionDate = data.tradeInfo != null ? data.tradeInfo.dueSelectionDate != null ? data.tradeInfo.dueSelectionDate : '' : '';
            this.selectionId = data.tradeDetails != null ? data.tradeDetails.selectionTypeId != null ? data.tradeDetails.selectionTypeId : 0 : 0;
            this.selectionName = data.tradeDetails != null ? data.tradeDetails.selectionTypeName != null ? data.tradeDetails.selectionTypeName : '' : '';
            this.paymentModeId = data.tradeDetails != null ? data.tradeDetails.paymentModeId != null ? data.tradeDetails.paymentModeId : 0 : 0;
            let noOfRows = Math.ceil(Number(this.quantity) / Number(this.minimumTradeLimit));
            if (this.userType == "3") {
              this.sellerRegionId = data.traderTradingAddrInfo != null ? data.traderTradingAddrInfo.regionId != null ? data.traderTradingAddrInfo.regionId : 0 : 0;
              this.sellerRegionName = data.traderTradingAddrInfo != null ? data.traderTradingAddrInfo.regionName != null ? data.traderTradingAddrInfo.regionName : '' : '';
              this.sellerZoneId = data.traderTradingAddrInfo != null ? data.traderTradingAddrInfo.zoneId != null ? data.traderTradingAddrInfo.zoneId : 0 : 0;
              this.sellerZoneName = data.traderTradingAddrInfo != null ? data.traderTradingAddrInfo.zoneName != null ? data.traderTradingAddrInfo.zoneName : '' : '';
              this.sellerCityId = data.traderTradingAddrInfo != null ? data.traderTradingAddrInfo.stationId != null ? data.traderTradingAddrInfo.stationId : 0 : 0;
              this.sellerCityName = data.traderTradingAddrInfo != null ? data.traderTradingAddrInfo.stationName != null ? data.traderTradingAddrInfo.stationName : '' : '';
              if ((this.selectionId == 1 && this.paymentModeId != 3) || this.selectionId == 2 || this.selectionId == 3)
                for (let i = 0; i < noOfRows; i++)
                  this.addSellerRow();
              else
                this.addSellerRow();
            } else if (this.userType == "1") {
              this.traderRegionId = data.traderTradingAddrInfo != null ? data.traderTradingAddrInfo.regionId != null ? data.traderTradingAddrInfo.regionId : 0 : 0;
              this.traderRegionName = data.traderTradingAddrInfo != null ? data.traderTradingAddrInfo.regionName != null ? data.traderTradingAddrInfo.regionName : '' : '';
              this.traderZoneId = data.traderTradingAddrInfo != null ? data.traderTradingAddrInfo.zoneId != null ? data.traderTradingAddrInfo.zoneId : 0 : 0;
              this.traderZoneName = data.traderTradingAddrInfo != null ? data.traderTradingAddrInfo.zoneName != null ? data.traderTradingAddrInfo.zoneName : '' : '';
              this.traderCityId = data.traderTradingAddrInfo != null ? data.traderTradingAddrInfo.stationId != null ? data.traderTradingAddrInfo.stationId : 0 : 0;
              this.traderCityName = data.traderTradingAddrInfo != null ? data.traderTradingAddrInfo.stationName != null ? data.traderTradingAddrInfo.stationName : '' : '';
              if ((this.selectionId == 1 && this.paymentModeId != 3) || this.selectionId == 2 || this.selectionId == 3)
                for (let i = 0; i < noOfRows; i++)
                  this.addSellerRow();
              else
                this.addSellerRow();
              if (this.traderRegionId > 0 && this.traderZoneId == 0 && this.traderCityId == 0)
                this.getRDCList(this.traderRegionId, 0, 'district');
            }
            else {
              if ((this.selectionId == 1 && this.paymentModeId != 3) || this.selectionId == 2 || this.selectionId == 3)
                for (let i = 0; i < noOfRows; i++)
                  this.addSellerRow();
              else
                this.addSellerRow();
            }
            this.isAddEnable = true;
          } else {
            this.onInfo('Trade Details Not Found!');
          }
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  Getunit() {
    this.spinner.show();
    this.service.Getunit({ productId: this.productId, subProductId: this.subProductId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.productUnit = data.productUnits;
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getRDCList(id: number, index: number, type: string) {
    let param = {};
    if (this.userType != null && this.userType == "3")
      param['companyId'] = this.companyId;
    if ("region" == type) {
      param['stateId'] = id;
      this.regionList = [];
      this.districtList = [];
      this.cityList = [];
      this.isRegionFound = false;
    }
    else if ("district" == type) {
      this.districtList[index] = [];
      this.cityList[index] = [];
      param['regionId'] = id;
      this.sellerFormArray.controls[index].get('districZoneId').setValue('');
      this.sellerFormArray.controls[index].get('cityId').setValue('');
    }
    else if ("city" == type) {
      this.cityList[index] = [];
      param['districtId'] = id;
      this.sellerFormArray.controls[index].get('cityId').setValue('');
    }
    this.spinner.show();
    this.service.getRDCList(param).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          if ("region" == type) {
            if (data.regionList != null && data.regionList.length > 0) {
              this.isRegionFound = true;
              this.regionList = data.regionList != null ? data.regionList : [];
            }
            else {
              this.isRegionFound = false;
              this.regionList = [{ regionId: 0, regionName: 'N/A' }];
              this.districtList[0] = data.districtList != null ? data.districtList : [];
            }
          }
          else if ("district" == type && this.isRegionFound)
            this.districtList[index] = data.districtList != null ? data.districtList : [];
          else if ("city" == type)
            this.cityList[index] = data.cityList != null ? data.cityList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  formatDate(date: Date) {
    return date.getFullYear() + "-" + (date.getMonth() >= 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-" + (date.getDate() > 9 ? date.getDate() : "0" + date.getDate());
  }

  sellerForm = new FormGroup({
    sellerFormArray: new FormArray([], Validators.required)
  });

  get sellerFormArray() {
    return this.sellerForm.get('sellerFormArray') as FormArray;
  }

  addSellerRow() {
    if (this.productId == 1 && this.subProductId == 185) {
      this.sellerFormArray.push(new FormGroup({
        districZoneId: new FormControl(this.userType == "3" ? this.sellerZoneId : this.userType == "1" ? this.traderZoneId > 0 ? this.traderZoneId : '' : '', Validators.required),
        cityId: new FormControl(this.userType == "3" ? this.sellerCityId : this.userType == "1" ? this.traderCityId > 0 ? this.traderCityId : '' : '', Validators.required),
        quantity: new FormControl(this.minimumTradeLimit, Validators.required),
        lotNo: new FormControl('', Validators.required),
        partyLotNo: new FormControl('',Validators.required),
        productUnit: new FormControl(this.productUnit.productUnitId != null ? this.productUnit.productUnitId : '', Validators.required),
        prNo: new FormControl('', Validators.required),
        storageLocation: new FormControl('', Validators.required),
        regionId: new FormControl(this.userType == "1" ? this.traderRegionId : (this.userType == "3" ? this.sellerRegionId : ''), Validators.required)
      }));
    } else {
      this.sellerFormArray.push(new FormGroup({
        districZoneId: new FormControl(this.userType == "3" ? this.sellerZoneId : this.userType == "1" ? this.traderZoneId > 0 ? this.traderZoneId : '' : '', Validators.required),
        cityId: new FormControl(this.userType == "3" ? this.sellerCityId : this.userType == "1" ? this.traderCityId > 0 ? this.traderCityId : '' : '', Validators.required),
        quantity: new FormControl({ value: this.minimumTradeLimit, disabled: true }, Validators.required),
        lotNo: new FormControl({ value: '', disabled: true }, Validators.required),
        partyLotNo: new FormControl({ value: '', disabled: true },Validators.required),
        productUnit: new FormControl(this.productUnit.productUnitId != null ? this.productUnit.productUnitId : '', Validators.required),
        prNo: new FormControl({ value: '', disabled: true }, Validators.required),
        storageLocation: new FormControl('', Validators.required),
        regionId: new FormControl(this.userType == "1" ? this.traderRegionId : (this.userType == "3" ? this.sellerRegionId : ''), Validators.required)
      }));
    }
    //console.log(this.sellerFormArray.value);
  }

  removeSellerRow(index: number) {
    if (index > 0) {
      this.sellerFormArray.removeAt(index);
      this.regionList.splice(index, 1);
      this.districtList.splice(index, 1);
      this.cityList.splice(index, 1);
    }
  }

  checkDuplicateLotNo(lotNo: string, index: number) {
    for (let i = 0; i < this.sellerFormArray.controls.length; i++) {
      if (i == index)
        continue
      if (this.sellerFormArray.controls[i].get("lotNo").value == lotNo) {
        this.sellerFormArray.controls[index].get("lotNo").setValue('');
        this.onError(lotNo + " Lot No Already Exists!");
      }
    }
  }

  onSellerFormSubmit(form: FormGroup) {
    if (form.valid && this.companyId > 0 && this.productId > 0 && this.subProductId > 0 && this.tncId != null && this.tncId.length > 0 && this.stateId > 0 && this.seasonId > 0) {
      let totalQty: number = 0;
      this.sellerFormArray.controls.forEach(element => {
        totalQty = totalQty + Number(element.get('quantity').value);
      });
      let param1 = {
        "companyId": this.companyId,
        "totalQty": totalQty,
        "tcnNo": this.tncId
      }
      this.spinner.show();
      this.service.checkQty(param1).subscribe(
        data => {
          if (data['error'].errorData != '0') {
            this.spinner.hide();
            this.onError(data['error'].errorMsg);
          } else {
            let param = {
              companyId: this.companyId,
              productId: this.productId,
              subProductId: this.subProductId,
              tcnNo: this.tncId,
              stateId: this.stateId,
              seasonId: this.seasonId,
              selectionRequest: (form.get('sellerFormArray') as FormArray).value
            }
            //this.spinner.show();
            this.service.addedSelectionDetails(param).subscribe(
              data => {
                this.spinner.hide();
                if (data['error'].errorData != '0') {
                  this.onError(data['error'].errorMsg);
                } else {
                  (form.get('sellerFormArray') as FormArray).clear();
                  form.reset();
                  this.onSuccess(data.error.errorMsg);
                  this.bsValue = new Date();
                  this.bsRangeValue = [this.bsValue, this.bsValue];
                  this.stateId = 0;
                  this.tncId = '';
                  this.productId = 0;
                  this.subProductId = 0;
                  this.seasonId = 0;
                  this.subProductList = [];
                  this.isAddEnable = false;
                  this.productUnit = null;
                  this.tcnList = [];
                  this.cityList = [];
                  if (this.isRegionFound)
                    this.districtList = [];
                  this.buyerName = '';
                  this.quantity = 0;
                  this.selectionId = 0;
                  this.selectionName = null;
                  this.paymentModeId = 0;
                  this.dueSelectionDate = null;
                  this.buyerCompanyId = 0;
                  this.sellerCompanyId = 0;
                  this.search = '';
                  this.seasonList = [];
                  (document.getElementById("searchBar") as HTMLInputElement).value = '';
                  this.minimumTradeLimit = 0;
                  this.dueSelectionDate = '';
                  this.traderRegionId = 0;
                  this.traderRegionName = '';
                  this.traderZoneId = 0;
                  this.traderZoneName = '';
                  this.traderCityId = 0;
                  this.traderCityName = '';
                  this.sellerRegionId = 0;
                  this.sellerRegionName = '';
                  this.sellerZoneId = 0;
                  this.sellerZoneName = '';
                  this.sellerCityId = 0;
                  this.sellerCityName = '';
                }
              }, error => {
                this.spinner.hide();
                this.onError(error.message);
                console.log(error)
              }
            );
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    } else {
      this.onError('Please Enter Valid Info!');
    }
  }

  onError(message: string): void {
    this.notifications.create(this.translate.instant('alert.error'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 3000, showProgressBar: true });
  }

  onSuccess(message: string): void {
    this.notifications.create(this.translate.instant('alert.success'),
      this.translate.instant(message),
      NotificationType.Success, { timeOut: 3000, showProgressBar: true });
  }

  onInfo(message: string): void {
    this.notifications.create(this.translate.instant('alert.warning'),
      this.translate.instant(message),
      NotificationType.Success, { timeOut: 3000, showProgressBar: true });
  }

  search: string;
  suggestions$: Observable<DataSourceType[]>;
  errorMessage: string;
  onSelect(event: TypeaheadMatch): void {
    this.buyerCompanyId = event.item.companyId;
    this.sellerCompanyId = event.item.companyId;
  }
}
interface DataSourceType {
  companyId: any;
  companyName: any;
}
interface DataSourceEnvelop {
  allCompany: DataSourceType[];
}