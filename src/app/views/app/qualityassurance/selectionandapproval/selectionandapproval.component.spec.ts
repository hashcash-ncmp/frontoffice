import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectionandapprovalComponent } from './selectionandapproval.component';

describe('SelectionandapprovalComponent', () => {
  let component: SelectionandapprovalComponent;
  let fixture: ComponentFixture<SelectionandapprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectionandapprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectionandapprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
