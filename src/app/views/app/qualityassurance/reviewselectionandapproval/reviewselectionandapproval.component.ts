import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, Validators, FormControl, NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/data/api.service';
import { noop, Observable, Observer, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { RANGES } from 'src/main';

@Component({
  selector: 'app-reviewselectionandapproval',
  templateUrl: './reviewselectionandapproval.component.html',
  styleUrls: ['./reviewselectionandapproval.component.scss']
})
export class ReviewselectionandapprovalComponent implements OnInit {
  ranges: any = [];
  modalRef: BsModalRef;
  tcnNo: string = '';
  tcnList: Array<any> = [];
  selectionList: Array<any> = [];
  checkBoxArray: Array<boolean> = [];
  companyId: any = '';
  allotmentArray: Array<any> = [];
  declineAllotmentArray: Array<any> = [];
  declineForm = new FormGroup({
    selectionDetailsRequest: new FormControl('', Validators.required),
    subject: new FormControl('', Validators.required),
    message: new FormControl('', Validators.required)
  });
  totalCount = 0;
  pageNo: number = 1;
  pageSize: number = 20;

  bsValue = new Date();
  bsRangeValue: Date[];
  stateId: number = 0;
  tncId: string = '';
  stateList: Array<any> = [];
  productId: number = 0;
  subProductId: number = 0;
  seasonId: number = 0;
  productList: Array<any> = [];
  subProductList: Array<any> = [];
  seasonList: Array<any> = [];
  userType: string = '';
  selectionValue: any = {};
  buyerCompanyId: any = 0;
  sellerCompanyId: any = 0;

  constructor(private http: HttpClient, private spinner: NgxSpinnerService, private service: ApiService,
    private notifications: NotificationsService, private translate: TranslateService,
    private modalService: BsModalService) {
    //this.bsRangeValue = [this.bsValue, this.bsValue];
    this.ranges = RANGES;
  }

  ngOnInit(): void {
    this.companyId = localStorage.getItem('CompanyID');
    this.userType = localStorage.getItem('BusinessTypeId');
    this.GetAllProducts();
    this.getStateList();
    this.getAllCompanyByComId();
  }

  openModal(template: TemplateRef<any>) {
    this.declineForm.get('selectionDetailsRequest').setValue(this.declineAllotmentArray);
    this.modalRef = this.modalService.show(template, { backdrop: 'static' });
  }

  getAllCompanyByComId() {
    this.buyerCompanyId = 0;
    this.sellerCompanyId = 0;
    const url = this.service.WEBSERVICE + '/selection/getAllCompanyByComId';
    this.suggestions$ = new Observable((observer: Observer<string>) => {
      observer.next(this.search);
    }).pipe(
      switchMap((query: string) => {
        if (query) {
          this.spinner.show();
          return this.http.post<DataSourceEnvelop>(
            url, { searchKeyword: query, companyId: this.companyId, flag: (this.userType == "3" ? "seller" : "buyer") }).pipe(
              map((data: DataSourceEnvelop) => { this.spinner.hide(); return data && data.allCompany || []; }),
              tap(() => noop, err => {
                this.spinner.hide();
                this.errorMessage = err && err.message || 'Something goes wrong';
              })
            );
        }
        return of([]);
      })
    );
  }

  GetAllProducts() {
    this.selectionValue = {};
    this.selectionList = [];
    this.spinner.show();
    this.service.GetAllProducts({ companyId: this.companyId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.productList = data.productList != null ? data.productList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  GetAllSubProducts(productId: number) {
    this.selectionValue = {};
    this.seasonId = 0;
    this.selectionList = [];
    this.subProductId = 0;
    this.subProductList = [];
    this.spinner.show();
    this.service.GetAllSubProducts({ companyId: this.companyId, productId: productId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.subProductList = data.subProductList != null ? data.subProductList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getStateList() {
    this.selectionValue = {};
    let param = {};
    if (this.userType != null && this.userType == "3")
      param['companyId'] = this.companyId;
    this.selectionList = [];
    this.spinner.show();
    this.service.getStateList(param).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.stateList = data.stateList != null ? data.stateList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getSeason() {
    this.selectionValue = {};
    this.seasonId = 0;
    this.selectionList = [];
    this.spinner.show();
    this.service.getSeason({ productId: this.productId, subProductId: this.subProductId }).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.seasonList = data.seasonList != null ? data.seasonList : [];
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  getTcnBuyer() {
    this.selectionValue = {};
    this.selectionList = [];
    this.tncId = '';
    this.tcnList = [];
    if (this.bsRangeValue[0] != null && this.bsRangeValue[1] != null && this.productId != 0 && this.subProductId != 0 && this.seasonId != 0 && this.stateId != 0 && this.buyerCompanyId > 0 && this.sellerCompanyId > 0) {
      let param = {
        toDate: this.formatDate(this.bsRangeValue[1]),
        productId: this.productId,
        subProductId: this.subProductId,
        fromDate: this.formatDate(this.bsRangeValue[0]),
        companyId: this.companyId,
        seasonId: this.seasonId,
        stateId: this.stateId,
        sellerCompanyId: this.sellerCompanyId,
        buyerCompanyId: this.companyId
      }
      this.spinner.show();
      this.service.getTcnBuyer(param).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.onError(data['error'].errorMsg);
          } else {
            this.tcnList = data.tcnList != null ? data.tcnList : [];
          }
        }, error => {
          this.spinner.hide();
          this.onError(error.message);
          console.log(error)
        }
      );
    }
    else {
      //this.onError('Please Select Valid Options!');
    }
  }

  formatDate(date: Date) {
    return date.getFullYear() + "-" + (date.getMonth() >= 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-" + (date.getDate() > 9 ? date.getDate() : "0" + date.getDate());
  }

  viewSelectionDetailsAgainstTCN(pageNo: number) {
    this.pageNo = pageNo;
    this.checkBoxArray = [];
    this.allotmentArray = [];
    this.declineAllotmentArray = [];
    this.declineForm.reset();
    if (this.tncId != null && this.tncId.length > 0 && this.bsRangeValue[0] != null && this.bsRangeValue[1] != null && this.productId != 0 && this.subProductId != 0 && this.seasonId != 0 && this.stateId != 0) {
      this.spinner.show();
      this.service.viewSelectionDetailsAgainstTCN({ companyId: this.companyId, tcnNo: this.tncId, toDate: this.formatDate(this.bsRangeValue[1]), fromDate: this.formatDate(this.bsRangeValue[0]), productId: this.productId, subProductId: this.subProductId, seasonId: this.seasonId, stateId: this.stateId, pageNo: this.pageNo, noOfItemsPerPage: this.pageSize }).subscribe(
        data => {
          this.spinner.hide();
          if (data['error'].errorData != '0') {
            this.selectionValue = {};
            this.selectionList = [];
            this.onError(data['error'].errorMsg);
          } else {
            this.selectionValue = data.selectionValue != null ? data.selectionValue : {};
            this.selectionList = data.selectionDetailsList != null ? data.selectionDetailsList : [];
            this.totalCount = data.totalCount != null ? data.totalCount : 0;
          }
        }, error => {
          this.spinner.hide();
          this.selectionValue = {};
          this.selectionList = [];
          this.onError(error.message);
          console.log(error)
        }
      );
    }
  }

  addRemoveObject(isChecked: any, object: any) {
    if (isChecked) {
      this.allotmentArray.push({
        allotmentNo: this.selectionValue.allotmentNo,
        tcnNo: this.selectionValue.tcnNo,
        companyId: this.companyId,
        quantity: object.quantity,
        allotmentDtlId: object.allotmentDtlId
      });
      this.declineAllotmentArray.push({
        allotmentNo: this.selectionValue.allotmentNo,
        allotmentDtlId: object.allotmentDtlId
      });
    } else {
      this.allotmentArray.splice(this.allotmentArray.findIndex(function (i) {
        return i.allotmentDtlId == object.allotmentDtlId;
      }), 1);
      this.declineAllotmentArray.splice(this.declineAllotmentArray.findIndex(function (i) {
        return i.allotmentDtlId == object.allotmentDtlId;
      }), 1);
    }
  }

  approveSelection() {
    this.spinner.show();
    this.service.approveSelection(this.allotmentArray).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.onSuccess('Done');
          this.checkBoxArray = [];
          this.viewSelectionDetailsAgainstTCN(1);
          this.allotmentArray = [];
          this.declineAllotmentArray = [];
          this.declineForm.reset();
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  rejectSelection(form: NgForm) {
    this.spinner.show();
    this.service.rejectSelection(form.value).subscribe(
      data => {
        this.spinner.hide();
        if (data['error'].errorData != '0') {
          this.onError(data['error'].errorMsg);
        } else {
          this.modalRef.hide();
          this.onSuccess('Done');
          this.checkBoxArray = [];
          this.allotmentArray = [];
          this.declineAllotmentArray = [];
          form.reset();
          this.viewSelectionDetailsAgainstTCN(1);
        }
      }, error => {
        this.spinner.hide();
        this.onError(error.message);
        console.log(error)
      }
    );
  }

  onError(message: string): void {
    this.notifications.create(this.translate.instant('alert.error'),
      this.translate.instant(message),
      NotificationType.Error, { timeOut: 3000, showProgressBar: true });
  }

  onSuccess(message: string): void {
    this.notifications.create(this.translate.instant('alert.success'),
      this.translate.instant(message),
      NotificationType.Success, { timeOut: 3000, showProgressBar: true });
  }

  search: string;
  suggestions$: Observable<DataSourceType[]>;
  errorMessage: string;
  onSelect(event: TypeaheadMatch): void {
    this.buyerCompanyId = event.item.companyId;
    this.sellerCompanyId = event.item.companyId;
  }
}
interface DataSourceType {
  companyId: any;
  companyName: any;
}
interface DataSourceEnvelop {
  allCompany: DataSourceType[];
}
