import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewselectionandapprovalComponent } from './reviewselectionandapproval.component';

describe('ReviewselectionandapprovalComponent', () => {
  let component: ReviewselectionandapprovalComponent;
  let fixture: ComponentFixture<ReviewselectionandapprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewselectionandapprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewselectionandapprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
