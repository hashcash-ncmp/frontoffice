import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectionandapprovallistComponent } from './selectionandapprovallist.component';

describe('SelectionandapprovallistComponent', () => {
  let component: SelectionandapprovallistComponent;
  let fixture: ComponentFixture<SelectionandapprovallistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectionandapprovallistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectionandapprovallistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
