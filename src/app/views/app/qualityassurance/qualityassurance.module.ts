import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { QualityassuranceRoutingModule } from './qualityassurance-routing.module';
import { QualityassuranceComponent } from './qualityassurance.component';
import { SelectionandapprovalComponent } from './selectionandapproval/selectionandapproval.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxSpinnerModule } from "ngx-spinner";
import { ViewselectionandapprovalComponent } from './viewselectionandapproval/viewselectionandapproval.component';
import { ReviewselectionandapprovalComponent } from './reviewselectionandapproval/reviewselectionandapproval.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { SelectionandapprovallistComponent } from './selectionandapprovallist/selectionandapprovallist.component';
@NgModule({
  declarations: [QualityassuranceComponent, SelectionandapprovalComponent, ViewselectionandapprovalComponent, ReviewselectionandapprovalComponent, SelectionandapprovallistComponent],
  imports: [
    CommonModule,
    QualityassuranceRoutingModule,
    SharedModule,
    LayoutContainersModule,
    FormsModule,
    ReactiveFormsModule,
    SimpleNotificationsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgxSpinnerModule,
    PaginationModule,
    PaginationModule.forRoot(),
    TypeaheadModule.forRoot(),
  ],
  providers: [BsModalService, DatePipe],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QualityassuranceModule { }
