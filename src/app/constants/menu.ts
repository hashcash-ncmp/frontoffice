import { environment } from 'src/environments/environment';
import { UserRole } from '../shared/auth.roles';
const adminRoot = environment.adminRoot;

export interface IMenuItem {
  id?: string;
  icon?: string;
  label: string;
  to: string;
  newWindow?: boolean;
  subs?: IMenuItem[];

  roles?: UserRole[];
}

const data: IMenuItem[] = [
  {
    icon: 'simple-icon-notebook',
    label: 'Profile',
    to: `${adminRoot}/account`,
    roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller],
    subs: [
      {
        icon: 'simple-icon-user',
        label: 'Personal Information',
        to: `${adminRoot}/account/personalinfo`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
      },
      {
        icon: 'iconsminds-bank',
        label: 'Banking Information',
        to: `${adminRoot}/account/bankinginfo`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
      },
      {
        icon: 'iconsminds-office',
        label: 'Company Information',
        to: `${adminRoot}/account/companyinfo`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller],
        subs: [

          {
            icon: 'simple-icon-plus',
            label: 'CompanyLocation',
            to: `${adminRoot}/account/addcompany`,
            roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
          },
          {
            icon: 'simple-icon-notebook',
            label: 'Company Details',
            to: `${adminRoot}/account/companyinfo`,
            roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
          },

          {
            icon: 'simple-icon-location-pin',
            label: 'shipment Location',
            to: `${adminRoot}/account/addshipmetbranch`,
            roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
          }]
      },
      {
        icon: 'iconsminds-box-close',
        label: 'Product Preferences',
        to: `${adminRoot}/account/productpref`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
      },
      {
        icon: 'iconsminds-box-close',
        label: 'My Wallet',
        to: `${adminRoot}/account/mywallet`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
      },
      {
        icon: 'simple-icon-settings',
        label: 'Session Mng Pref',
        to: `${adminRoot}/account/sessionmngpref`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
      },
      // {
      //   icon: 'simple-icon-plus',
      //   label: 'Add Bank',
      //   to: `${adminRoot}/account/addbank`,
      //   roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
      // }
    ],
  },

  {
    icon: 'simple-icon-layers',
    label: 'Order',
    to: `${adminRoot}/tradefloor`,
    roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller],
    subs: [
      {
        icon: 'simple-icon-location-pin',
        label: 'Create Order',
        to: `${adminRoot}/tradefloor/offer`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
      },
      {
        icon: 'simple-icon-location-pin',
        label: 'Dashboard',
        to: `${adminRoot}/tradefloor/Dashboard`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
      },
      // {
      //   icon: 'simple-icon-briefcase',
      //   label: 'My Negotiation',
      //   to: `${adminRoot}/tradefloor/nigotiation`,
      //   // roles: [UserRole.Admin],
      // },
      {
        icon: 'simple-icon-docs',
        label: 'Trade History',
        to: `${adminRoot}/tradefloor/history`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
      },
      {
        icon: 'simple-icon-docs',
        label: 'Market Insights',
        to: `${adminRoot}/tradefloor/market-insights`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
      }
    ]
  },
  {
    icon: 'iconsminds-financial',
    label: 'Payments',
    to: `${adminRoot}/payments`,
    roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller, UserRole.None],
    subs: [
      {
        icon: 'iconsminds-wallet',
        label: 'Deposit/Withdraw',
        to: `${adminRoot}/payments/deposit`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller, UserRole.None]
      },
      {
        icon: 'iconsminds-sand-watch-2',
        label: 'Pending Payments',
        to: `${adminRoot}/payments/pendingpayment`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller, UserRole.None]
      },

      {
        icon: 'simple-icon-location-pin',
        label: 'Confirm Payments',
        to: `${adminRoot}/payments/confirmpayment`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller, UserRole.None]
      },
      {
        icon: 'simple-icon-location-pin',
        label: ' Payments History',
        to: `${adminRoot}/payments/PaymentHistory`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller, UserRole.None]
      },
      {
        icon: 'simple-icon-location-pin',
        label: 'Trade Payments',
        to: `${adminRoot}/payments/tardepayment`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller, UserRole.None]
      }

    ]
  },
  // {
  //   icon: 'iconsminds-wallet',
  //   label: 'Account',
  //   to: `${adminRoot}/marketinsig`,
  //   roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller],
  //   subs: [
  //     {
  //       icon: 'iconsminds-coins',
  //       label: 'Payble',
  //       to: `${adminRoot}/accou/`,
  //       roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
  //     },
  //     {
  //       icon: 'iconsminds-financial',
  //       label: 'Receivable',
  //       to: `${adminRoot}/accou/`,
  //       roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
  //     }

  //   ]
  // },
  {
    icon: 'simple-icon-badge',
    label: 'Select & Approve',
    to: `${adminRoot}/qualityassuarance`,
    roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller],
    subs: [
      {
        icon: 'simple-icon-plus',
        label: 'Add Inventory',
        to: `${adminRoot}/qualityassuarance/selectionandapproval`,
        roles: [UserRole.Trader, UserRole.Seller]
      },
      {
        icon: 'simple-icon-eye',
        label: 'View Inventory',
        to: `${adminRoot}/qualityassuarance/viewselectionandapproval`,
        roles: [UserRole.Trader, UserRole.Seller]
      },
      {
        icon: 'simple-icon-check',
        label: 'Approve Inventory',
        to: `${adminRoot}/qualityassuarance/reviewselectionandapproval`,
        roles: [UserRole.Trader, UserRole.Buyer]
      },
      {
        icon: 'iconsminds-notepad',
        label: 'Confirm Inventory',
        to: `${adminRoot}/qualityassuarance/selectionandapprovallist`,
        roles: [UserRole.Trader, UserRole.Buyer]
      }
    ]
  },
  {
    icon: 'iconsminds-bus-2',
    label: 'Delivery',
    to: `${adminRoot}/delivery`,
    roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller],
    subs: [
      {
        icon: 'simple-icon-plus',
        label: 'Add Delivery',
        to: `${adminRoot}/delivery/adddelivery`,
        roles: [UserRole.Trader, UserRole.Buyer]
      },
      {
        icon: 'simple-icon-eye',
        label: 'View Delivery',
        to: `${adminRoot}/delivery/viewdelivery`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
      },

    ]
  },
  {
    icon: 'iconsminds-ship',
    label: 'Shipment',
    to: `${adminRoot}/shipments`,
    roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller],
    subs: [
      {
        icon: 'simple-icon-plus',
        label: 'Add Shipment',
        to: `${adminRoot}/shipments/addshipment`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
      },
      {
        icon: 'simple-icon-eye',
        label: 'View Shipment',
        to: `${adminRoot}/shipments/viewshipment`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
      },

    ]
  },
  {
    icon: 'iconsminds-monitor-analytics',
    label: 'Report',
    to: `${adminRoot}/report`,
    roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
  },
  {
    icon: 'iconsminds-management',
    label: 'Claim Mng',
    to: `${adminRoot}/claim`,
    roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller],
    subs: [
      {
        icon: 'iconsminds-layer-forward',
        label: 'Request claim',
        to: `${adminRoot}/claim/claimrequest`,
        roles: [UserRole.Trader, UserRole.Buyer]
      },
      {
        icon: 'simple-icon-eye',
        label: 'View Claim Status',
        to: `${adminRoot}/claim/claimstatus`,
        roles: [UserRole.Trader, UserRole.Buyer, UserRole.Seller]
      },

    ]
  }
  // {
  //   icon: 'iconsminds-air-balloon-1',
  //   label: 'menu.vien',
  //   to: `${adminRoot}/vien`,
  //   roles: [UserRole.Admin, UserRole.Editor],
  //   subs: [
  //     {
  //       icon: 'simple-icon-paper-plane',
  //       label: 'menu.start',
  //       to: `${adminRoot}/vien/start`,
  //       // roles: [UserRole.Admin],
  //     },
  //   ],
  // },
  // {
  //   icon: 'iconsminds-three-arrow-fork',
  //   label: 'menu.second-menu',
  //   to: `${adminRoot}/second-menu`,
  //   // roles: [UserRole.Editor],
  //   subs: [
  //     {
  //       icon: 'simple-icon-paper-plane',
  //       label: 'menu.second',
  //       to: `${adminRoot}/second-menu/second`,
  //     },
  //   ],
  // },
  // {
  //   icon: 'iconsminds-bucket',
  //   label: 'menu.blank-page',
  //   to: `${adminRoot}/blank-page`,
  // },
  // {
  //   icon: 'iconsminds-library',
  //   label: 'menu.docs',
  //   to: 'https://vien-docs.coloredstrategies.com/',
  //   newWindow: true,
  // },
];
export default data;
